import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import { Link } from 'react-router-dom'
import Spinner from 'react-bootstrap/Spinner'
import Select from 'react-select'
import AsyncSelect from 'react-select/async'
import axios from 'axios'
import { useToasts } from 'react-toast-notifications'
import { useForm } from 'react-hook-form'
import * as ApiClickargo from '../ApiList.js'

function Form(props) {

    const { register, handleSubmit, errors, watch, setError, clearError, setValue, getValues } = useForm()

    const { addToast } = useToasts()

    const [isLoading, setIsLoading] = useState(true);

    const [isLocalStorageExist, setIsLocalStorageExist] = useState(false)

    const [isPPJK, setIsPPJK] = useState(false)

    const [inputForm, setInputForm] = useState([]);

    const [inputContainerForm, setInputContainerForm] = useState([]);

    const [arrCountryList, setArrCountryList] = useState([])

    const [arrStateList, setArrStateList] = useState([])

    const [arrBLNumber, setArrBLNumber] = useState([])

    const [docSuratKuasa, setDocSuratKuasa] = useState([])

    useEffect(() => {
        if ('ckDOInputForm' in localStorage && 'ckDOCountryList' in localStorage && 'ckHeaderKey' in localStorage && 'ckHeaderSecret' in localStorage && 'ckHeaderToken' in localStorage) {
            setIsLocalStorageExist(true)

            //check if users ppjk
            axios
                .get(ApiClickargo.NLE_DATAPELIMPAHAN + '/?ppjk=' + props.dataUserPortal.npwp)
                .then(response => {
                    let dataPPJK = response.data.content
                    //filter DO only
                    var newArrayPPJK = dataPPJK.filter(function (el) {
                        return el.delivery_order === 1
                    })
                    if (newArrayPPJK.length > 0) {
                        setIsPPJK(true)
                        //console.log(newArrayPPJK)
                        setArrBLNumber(newArrayPPJK)
                    } else {
                        setIsPPJK(false)
                        console.log(newArrayPPJK)
                    }
                })
                .catch(error => {
                    console.log(error)
                    addToast('Failed get data pelimpahan', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                })

            //check if data in user profile oss in nle api exist then set state
            axios
                .get(ApiClickargo.NLE_USER_PROFILE_OSS + '/' + props.dataUserPortal.npwp,
                    {
                        headers: {
                            'beacukai-api-key': ApiClickargo.NLE_USER_PROFILE_OSS_API_KEY
                        }
                    })
                .then(response => {
                    if (response.data['data Customer Oss'].length > 0) {

                        let dataOss = response.data['data Customer Oss'][0]

                        setTimeout(() => {

                            JSON.parse(localStorage.getItem('ckDOInputForm')).map((input, index) => {
                                setInputForm([...inputForm, {
                                    //
                                    actionType: input.actionType,
                                    actionTypeLabel: input.actionTypeLabel,
                                    freightMode: input.freightMode,
                                    freightModeLabel: input.freightModeLabel,
                                    moveType: input.moveType,
                                    moveTypeLabel: input.moveTypeLabel,
                                    blType: input.blType,
                                    blTypeLabel: input.blTypeLabel,
                                    blNumber: input.blNumber,
                                    blNumberLabel: input.blNumberLabel,
                                    //place of receipt
                                    placeOfReceiptLocation: input.placeOfReceiptLocation,
                                    placeOfReceiptLocationLabel: input.placeOfReceiptLocationLabel,
                                    placeOfReceiptCountry: input.placeOfReceiptCountry,
                                    placeOfReceiptCountryLabel: input.placeOfReceiptCountryLabel,
                                    //port of loading
                                    portOfLoadingLocation: input.portOfLoadingLocation,
                                    portOfLoadingLocationLabel: input.portOfLoadingLocationLabel,
                                    portOfLoadingCountry: input.portOfLoadingCountry,
                                    portOfLoadingCountryLabel: input.portOfLoadingCountryLabel,
                                    //port of destination
                                    portOfDestinationLocation: input.portOfDestinationLocation,
                                    portOfDestinationLocationLabel: input.portOfDestinationLocationLabel,
                                    portOfDestinationCountry: input.portOfDestinationCountry,
                                    portOfDestinationCountryLabel: input.portOfDestinationCountryLabel,
                                    //place of discharge
                                    placeOfDischargeLocation: input.placeOfDischargeLocation,
                                    placeOfDischargeLocationLabel: input.placeOfDischargeLocationLabel,
                                    placeOfDischargeCountry: input.placeOfDischargeCountry,
                                    placeOfDischargeCountryLabel: input.placeOfDischargeCountryLabel,
                                    placeOfDischargeState: input.placeOfDischargeState,
                                    placeOfDischargeStateLabel: input.placeOfDischargeStateLabel,
                                    placeOfDischargeAddress: input.placeOfDischargeAddress,
                                    //requestor
                                    requestorNPWP: props.dataUserPortal.npwp,
                                    requestorNIB: dataOss.NIB,
                                    requestorName: dataOss.NAME_PERSON,
                                    requestorAddress: dataOss.ADDRESS,
                                    //consignee
                                    consigneeName: input.consigneeName,
                                    consigneeNPWP: input.consigneeNPWP,
                                    //notify
                                    notifyPartyName: input.notifyPartyName,
                                    notifyPartyNPWP: input.notifyPartyNPWP,
                                    //payment information
                                    paymentInformationBankCode: input.paymentInformationBankCode,
                                    paymentInformationBankName: input.paymentInformationBankName,
                                    paymentInformationAccountNumber: input.paymentInformationAccountNumber,
                                    paymentInformationAmount: input.paymentInformationAmount,
                                }])

                                if (input.placeOfDischargeCountryLabel.length > 0) {
                                    //search state
                                    axios
                                        .post(ApiClickargo.CLICKARGO_STATE_LIST, {
                                            country_name: input.placeOfDischargeCountryLabel
                                        })
                                        .then(response => {
                                            console.log('success get state list')
                                            console.log(response.data.data)

                                            setArrStateList(response.data.data)
                                        })
                                        .catch(error => {
                                            console.log('failed to get state list')
                                            console.log(error)
                                        })
                                }

                                setDocSuratKuasa(JSON.parse(localStorage.getItem('ckDOSuratKuasa')))

                                setTimeout(() => {
                                    setValue([
                                        //
                                        { actionType: input.actionType },
                                        { freightMode: input.freightMode },
                                        { moveType: input.moveType },
                                        { blType: input.blType },
                                        { blNumber: input.blNumber },
                                        //place of receipt
                                        { placeOfReceiptLocation: input.placeOfReceiptLocation },
                                        { placeOfReceiptCountry: input.placeOfReceiptCountry },
                                        //port of loading
                                        { portOfLoadingLocation: input.portOfLoadingLocation },
                                        { portOfLoadingCountry: input.portOfLoadingCountry },
                                        //port of destination
                                        { portOfDestinationLocation: input.portOfDestinationLocation },
                                        { portOfDestinationCountry: input.portOfDestinationCountry },
                                        //place of discharge
                                        { placeOfDischargeLocation: input.placeOfDischargeLocation },
                                        { placeOfDischargeCountry: input.placeOfDischargeCountry },
                                        { placeOfDischargeState: input.placeOfDischargeState },
                                        { placeOfDischargeAddress: input.placeOfDischargeAddress },
                                        //requestor
                                        { requestorNPWP: props.dataUserPortal.npwp },
                                        { requestorNIB: dataOss.NIB },
                                        { requestorName: dataOss.NAME_PERSON },
                                        { requestorAddress: dataOss.ADDRESS },
                                        //consignee
                                        { consigneeName: input.consigneeName },
                                        //payment information
                                        { paymentInformationBankCode: input.paymentInformationBankCode },
                                        { paymentInformationBankName: input.paymentInformationBankName },
                                        { paymentInformationAccountNumber: input.paymentInformationAccountNumber },
                                        { paymentInformationAmount: input.paymentInformationAmount },
                                    ])
                                }, 1000);
                            })


                            setIsLoading(false)

                        }, 1000);

                    } else {

                        axios
                            .get(ApiClickargo.NLE_USER_PROFILE + '/' + props.dataUserPortal.npwp,
                                {
                                    headers: {
                                        'beacukai-api-key': ApiClickargo.NLE_USER_PROFILE_API_KEY
                                    }
                                })
                            .then(response => {
                                if (response.data['data Customer'].length > 0) {

                                    let dataProfile = response.data['data Customer'][0]

                                    setTimeout(() => {

                                        JSON.parse(localStorage.getItem('ckDOInputForm')).map((input, index) => {
                                            setInputForm([...inputForm, {
                                                //
                                                actionType: input.actionType,
                                                actionTypeLabel: input.actionTypeLabel,
                                                freightMode: input.freightMode,
                                                freightModeLabel: input.freightModeLabel,
                                                moveType: input.moveType,
                                                moveTypeLabel: input.moveTypeLabel,
                                                blType: input.blType,
                                                blTypeLabel: input.blTypeLabel,
                                                blNumber: input.blNumber,
                                                blNumberLabel: input.blNumberLabel,
                                                //place of receipt
                                                placeOfReceiptLocation: input.placeOfReceiptLocation,
                                                placeOfReceiptLocationLabel: input.placeOfReceiptLocationLabel,
                                                placeOfReceiptCountry: input.placeOfReceiptCountry,
                                                placeOfReceiptCountryLabel: input.placeOfReceiptCountryLabel,
                                                //port of loading
                                                portOfLoadingLocation: input.portOfLoadingLocation,
                                                portOfLoadingLocationLabel: input.portOfLoadingLocationLabel,
                                                portOfLoadingCountry: input.portOfLoadingCountry,
                                                portOfLoadingCountryLabel: input.portOfLoadingCountryLabel,
                                                //port of destination
                                                portOfDestinationLocation: input.portOfDestinationLocation,
                                                portOfDestinationLocationLabel: input.portOfDestinationLocationLabel,
                                                portOfDestinationCountry: input.portOfDestinationCountry,
                                                portOfDestinationCountryLabel: input.portOfDestinationCountryLabel,
                                                //place of discharge
                                                placeOfDischargeLocation: input.placeOfDischargeLocation,
                                                placeOfDischargeLocationLabel: input.placeOfDischargeLocationLabel,
                                                placeOfDischargeCountry: input.placeOfDischargeCountry,
                                                placeOfDischargeCountryLabel: input.placeOfDischargeCountryLabel,
                                                placeOfDischargeState: input.placeOfDischargeState,
                                                placeOfDischargeStateLabel: input.placeOfDischargeStateLabel,
                                                placeOfDischargeAddress: input.placeOfDischargeAddress,
                                                //requestor
                                                requestorNPWP: props.dataUserPortal.npwp,
                                                requestorNIB: input.requestorNIB,
                                                requestorName: dataProfile.NM_KONTAK_PERSON,
                                                requestorAddress: dataProfile.ALAMAT,
                                                //consignee
                                                consigneeName: input.consigneeName,
                                                consigneeNPWP: input.consigneeNPWP,
                                                //notify
                                                notifyPartyName: input.notifyPartyName,
                                                notifyPartyNPWP: input.notifyPartyNPWP,
                                                //payment information
                                                paymentInformationBankCode: input.paymentInformationBankCode,
                                                paymentInformationBankName: input.paymentInformationBankName,
                                                paymentInformationAccountNumber: input.paymentInformationAccountNumber,
                                                paymentInformationAmount: input.paymentInformationAmount,
                                            }])

                                            if (input.placeOfDischargeCountryLabel.length > 0) {
                                                //search state
                                                axios
                                                    .post(ApiClickargo.CLICKARGO_STATE_LIST, {
                                                        country_name: input.placeOfDischargeCountryLabel
                                                    })
                                                    .then(response => {
                                                        console.log('success get state list')
                                                        console.log(response.data.data)

                                                        setArrStateList(response.data.data)
                                                    })
                                                    .catch(error => {
                                                        console.log('failed to get state list')
                                                        console.log(error)
                                                    })
                                            }

                                            setDocSuratKuasa(JSON.parse(localStorage.getItem('ckDOSuratKuasa')))

                                            setTimeout(() => {
                                                setValue([
                                                    //
                                                    { actionType: input.actionType },
                                                    { freightMode: input.freightMode },
                                                    { moveType: input.moveType },
                                                    { blType: input.blType },
                                                    { blNumber: input.blNumber },
                                                    //place of receipt
                                                    { placeOfReceiptLocation: input.placeOfReceiptLocation },
                                                    { placeOfReceiptCountry: input.placeOfReceiptCountry },
                                                    //port of loading
                                                    { portOfLoadingLocation: input.portOfLoadingLocation },
                                                    { portOfLoadingCountry: input.portOfLoadingCountry },
                                                    //port of destination
                                                    { portOfDestinationLocation: input.portOfDestinationLocation },
                                                    { portOfDestinationCountry: input.portOfDestinationCountry },
                                                    //place of discharge
                                                    { placeOfDischargeLocation: input.placeOfDischargeLocation },
                                                    { placeOfDischargeCountry: input.placeOfDischargeCountry },
                                                    { placeOfDischargeState: input.placeOfDischargeState },
                                                    { placeOfDischargeAddress: input.placeOfDischargeAddress },
                                                    //requestor
                                                    { requestorNPWP: props.dataUserPortal.npwp },
                                                    { requestorNIB: input.requestorNIB },
                                                    { requestorName: dataProfile.NM_KONTAK_PERSON },
                                                    { requestorAddress: dataProfile.ALAMAT },
                                                    //consignee
                                                    { consigneeName: input.consigneeName },
                                                    //payment information
                                                    { paymentInformationBankCode: input.paymentInformationBankCode },
                                                    { paymentInformationBankName: input.paymentInformationBankName },
                                                    { paymentInformationAccountNumber: input.paymentInformationAccountNumber },
                                                    { paymentInformationAmount: input.paymentInformationAmount },
                                                ])
                                            }, 1000);
                                        })

                                        const valuesContainer = [...inputContainerForm]

                                        setIsLoading(false)

                                    }, 1000);
                                } else {
                                    setTimeout(() => {

                                        JSON.parse(localStorage.getItem('ckDOInputForm')).map((input, index) => {
                                            setInputForm([...inputForm, {
                                                //
                                                actionType: input.actionType,
                                                actionTypeLabel: input.actionTypeLabel,
                                                freightMode: input.freightMode,
                                                freightModeLabel: input.freightModeLabel,
                                                moveType: input.moveType,
                                                moveTypeLabel: input.moveTypeLabel,
                                                blType: input.blType,
                                                blTypeLabel: input.blTypeLabel,
                                                blNumber: input.blNumber,
                                                blNumberLabel: input.blNumberLabel,
                                                //place of receipt
                                                placeOfReceiptLocation: input.placeOfReceiptLocation,
                                                placeOfReceiptLocationLabel: input.placeOfReceiptLocationLabel,
                                                placeOfReceiptCountry: input.placeOfReceiptCountry,
                                                placeOfReceiptCountryLabel: input.placeOfReceiptCountryLabel,
                                                //port of loading
                                                portOfLoadingLocation: input.portOfLoadingLocation,
                                                portOfLoadingLocationLabel: input.portOfLoadingLocationLabel,
                                                portOfLoadingCountry: input.portOfLoadingCountry,
                                                portOfLoadingCountryLabel: input.portOfLoadingCountryLabel,
                                                //port of destination
                                                portOfDestinationLocation: input.portOfDestinationLocation,
                                                portOfDestinationLocationLabel: input.portOfDestinationLocationLabel,
                                                portOfDestinationCountry: input.portOfDestinationCountry,
                                                portOfDestinationCountryLabel: input.portOfDestinationCountryLabel,
                                                //place of discharge
                                                placeOfDischargeLocation: input.placeOfDischargeLocation,
                                                placeOfDischargeLocationLabel: input.placeOfDischargeLocationLabel,
                                                placeOfDischargeCountry: input.placeOfDischargeCountry,
                                                placeOfDischargeCountryLabel: input.placeOfDischargeCountryLabel,
                                                placeOfDischargeState: input.placeOfDischargeState,
                                                placeOfDischargeStateLabel: input.placeOfDischargeStateLabel,
                                                placeOfDischargeAddress: input.placeOfDischargeAddress,
                                                //requestor
                                                requestorNPWP: input.requestorNPWP,
                                                requestorNIB: input.requestorNIB,
                                                requestorName: input.requestorName,
                                                requestorAddress: input.requestorAddress,
                                                //consignee
                                                consigneeName: input.consigneeName,
                                                consigneeNPWP: input.consigneeNPWP,
                                                //notify
                                                notifyPartyName: input.notifyPartyName,
                                                notifyPartyNPWP: input.notifyPartyNPWP,
                                                //payment information
                                                paymentInformationBankCode: input.paymentInformationBankCode,
                                                paymentInformationBankName: input.paymentInformationBankName,
                                                paymentInformationAccountNumber: input.paymentInformationAccountNumber,
                                                paymentInformationAmount: input.paymentInformationAmount,
                                            }])

                                            if (input.placeOfDischargeCountryLabel.length > 0) {
                                                //search state
                                                axios
                                                    .post(ApiClickargo.CLICKARGO_STATE_LIST, {
                                                        country_name: input.placeOfDischargeCountryLabel
                                                    })
                                                    .then(response => {
                                                        console.log('success get state list')
                                                        console.log(response.data.data)

                                                        setArrStateList(response.data.data)
                                                    })
                                                    .catch(error => {
                                                        console.log('failed to get state list')
                                                        console.log(error)
                                                    })
                                            }

                                            setDocSuratKuasa(JSON.parse(localStorage.getItem('ckDOSuratKuasa')))

                                            setTimeout(() => {
                                                setValue([
                                                    //
                                                    { actionType: input.actionType },
                                                    { freightMode: input.freightMode },
                                                    { moveType: input.moveType },
                                                    { blType: input.blType },
                                                    { blNumber: input.blNumber },
                                                    //place of receipt
                                                    { placeOfReceiptLocation: input.placeOfReceiptLocation },
                                                    { placeOfReceiptCountry: input.placeOfReceiptCountry },
                                                    //port of loading
                                                    { portOfLoadingLocation: input.portOfLoadingLocation },
                                                    { portOfLoadingCountry: input.portOfLoadingCountry },
                                                    //port of destination
                                                    { portOfDestinationLocation: input.portOfDestinationLocation },
                                                    { portOfDestinationCountry: input.portOfDestinationCountry },
                                                    //place of discharge
                                                    { placeOfDischargeLocation: input.placeOfDischargeLocation },
                                                    { placeOfDischargeCountry: input.placeOfDischargeCountry },
                                                    { placeOfDischargeState: input.placeOfDischargeState },
                                                    { placeOfDischargeAddress: input.placeOfDischargeAddress },
                                                    //requestor
                                                    { requestorNPWP: input.requestorNPWP },
                                                    { requestorNIB: input.requestorNIB },
                                                    { requestorName: input.requestorName },
                                                    { requestorAddress: input.requestorAddress },
                                                    //consignee
                                                    { consigneeName: input.consigneeName },
                                                    //payment information
                                                    { paymentInformationBankCode: input.paymentInformationBankCode },
                                                    { paymentInformationBankName: input.paymentInformationBankName },
                                                    { paymentInformationAccountNumber: input.paymentInformationAccountNumber },
                                                    { paymentInformationAmount: input.paymentInformationAmount },
                                                ])
                                            }, 1000);
                                        })

                                        setIsLoading(false)

                                    }, 1000);
                                }
                            })

                            .catch(error => {
                                console.log(error)
                                addToast('Something wrong. Please Check the server', {
                                    appearance: 'error',
                                    autoDismiss: true,
                                })
                            })

                    }
                })
                .catch(error => {
                    console.log(error)
                    addToast('Something wrong. Please Check the server', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                })

            //set country list value
            setArrCountryList(JSON.parse(localStorage.getItem('ckDOCountryList')))

        } else {
            setIsLocalStorageExist(false)
        }


    }, [])

    const handleInputChange = (field, event) => {
        const values = [...inputForm]

        if (field === "actionType") {
            values[0].actionType = event.value
            values[0].actionTypeLabel = event.label

            setValue([
                { actionType: inputForm[0].actionType },
            ])

            clearError('actionType')
        }

        if (field === "freightMode") {
            values[0].freightMode = event.value
            values[0].freightModeLabel = event.label

            setValue([
                { freightMode: inputForm[0].freightMode },
            ])

            clearError('freightMode')
        }

        if (field === "moveType") {
            values[0].moveType = event.value
            values[0].moveTypeLabel = event.label

            setValue([
                { moveType: inputForm[0].moveType },
            ])

            clearError('moveType')
        }

        if (field === "blType") {
            values[0].blType = event.value
            values[0].blTypeLabel = event.label

            setValue([
                { blType: inputForm[0].blType },
            ])

            clearError('blType')
        }

        //bl number type input
        if (field === "blNumber") {
            values[0].blNumber = event.target.value
            values[0].blNumberLabel = event.target.value

            setValue([
                { blNumber: inputForm[0].blNumber },
            ])

            clearError('blNumber')
        }

        //bl number type select
        if (field === "blNumberSelect") {
            values[0].blNumber = event.value
            values[0].blNumberLabel = event.label

            setValue([
                { blNumber: inputForm[0].blNumber },
            ])

            clearError('blNumber')

            axios
                .get(ApiClickargo.NLE_DATAPELIMPAHAN + '/?ppjk=' + props.dataUserPortal.npwp)
                .then(response => {
                    let dataPPJK = response.data.content
                    //filter DO only
                    var newArrayPPJK = dataPPJK.filter(function (el) {
                        return el.bl_no === event.value
                    })
                    if (newArrayPPJK.length > 0) {
                        setDocSuratKuasa(newArrayPPJK)
                    }
                })
                .catch(error => {
                    console.log(error)
                    addToast('Failed get data pelimpahan', {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                })
        }

        if (field === "placeOfReceiptLocation") {
            values[0].placeOfReceiptLocation = event.value
            values[0].placeOfReceiptLocationLabel = event.label

            axios
                .post(ApiClickargo.CLICKARGO_FIND_COUNTRY, {
                    country_code: event.value.substring(0, 2)
                })
                .then(response => {
                    const updatePlaceOfReceipt = [...inputForm]

                    updatePlaceOfReceipt[0].placeOfReceiptCountry = response.data.data.iso2
                    updatePlaceOfReceipt[0].placeOfReceiptCountryLabel = response.data.data.name

                    setInputForm(updatePlaceOfReceipt)

                    setValue([
                        { placeOfReceiptCountry: response.data.data.iso2 },
                    ])

                    clearError('placeOfReceiptCountry')
                })
                .catch(error => {
                    console.log('failed to get country list')
                    console.log(error)
                })

            setValue([
                { placeOfReceiptLocation: inputForm[0].placeOfReceiptLocation },
            ])

            clearError('placeOfReceiptLocation')
        }

        if (field === "placeOfReceiptCountry") {
            values[0].placeOfReceiptCountry = event.value
            values[0].placeOfReceiptCountryLabel = event.label

            setValue([
                { placeOfReceiptCountry: inputForm[0].placeOfReceiptCountry },
            ])

            clearError('placeOfReceiptCountry')
        }

        if (field === "portOfLoadingLocation") {
            values[0].portOfLoadingLocation = event.value
            values[0].portOfLoadingLocationLabel = event.label

            axios
                .post(ApiClickargo.CLICKARGO_FIND_COUNTRY, {
                    country_code: event.value.substring(0, 2)
                })
                .then(response => {
                    const updatePlaceOfLoading = [...inputForm]

                    updatePlaceOfLoading[0].portOfLoadingCountry = response.data.data.iso2
                    updatePlaceOfLoading[0].portOfLoadingCountryLabel = response.data.data.name

                    setInputForm(updatePlaceOfLoading)

                    setValue([
                        { portOfLoadingCountry: response.data.data.iso2 },
                    ])

                    clearError('portOfLoadingCountry')
                })
                .catch(error => {
                    console.log('failed to get country list')
                    console.log(error)
                })

            setValue([
                { portOfLoadingLocation: inputForm[0].portOfLoadingLocation },
            ])

            clearError('portOfLoadingLocation')
        }

        if (field === "portOfLoadingCountry") {
            values[0].portOfLoadingCountry = event.value
            values[0].portOfLoadingCountryLabel = event.label

            setValue([
                { portOfLoadingCountry: inputForm[0].portOfLoadingCountry },
            ])

            clearError('portOfLoadingCountry')
        }

        if (field === "portOfDestinationLocation") {
            values[0].portOfDestinationLocation = event.value
            values[0].portOfDestinationLocationLabel = event.label

            axios
                .post(ApiClickargo.CLICKARGO_FIND_COUNTRY, {
                    country_code: event.value.substring(0, 2)
                })
                .then(response => {
                    const updatePlaceOfDestination = [...inputForm]

                    updatePlaceOfDestination[0].portOfDestinationCountry = response.data.data.iso2
                    updatePlaceOfDestination[0].portOfDestinationCountryLabel = response.data.data.name

                    setInputForm(updatePlaceOfDestination)

                    setValue([
                        { portOfDestinationCountry: response.data.data.iso2 },
                    ])

                    clearError('portOfDestinationCountry')
                })
                .catch(error => {
                    console.log('failed to get country list')
                    console.log(error)
                })

            setValue([
                { portOfDestinationLocation: inputForm[0].portOfDestinationLocation },
            ])

            clearError('portOfDestinationLocation')
        }

        if (field === "portOfDestinationCountry") {
            values[0].portOfDestinationCountry = event.value
            values[0].portOfDestinationCountryLabel = event.label

            setValue([
                { portOfDestinationCountry: inputForm[0].portOfDestinationCountry },
            ])

            clearError('portOfDestinationCountry')
        }

        if (field === "placeOfDischargeLocation") {
            values[0].placeOfDischargeLocation = event.value
            values[0].placeOfDischargeLocationLabel = event.label

            axios
                .post(ApiClickargo.CLICKARGO_FIND_COUNTRY, {
                    country_code: event.value.substring(0, 2)
                })
                .then(response => {
                    const updatePlaceOfDischarge = [...inputForm]

                    updatePlaceOfDischarge[0].placeOfDischargeCountry = response.data.data.iso2
                    updatePlaceOfDischarge[0].placeOfDischargeCountryLabel = response.data.data.name
                    updatePlaceOfDischarge[0].placeOfDischargeState = ''
                    updatePlaceOfDischarge[0].placeOfDischargeStateLabel = 'Choose State'

                    setInputForm(updatePlaceOfDischarge)

                    setValue([
                        { placeOfDischargeCountry: response.data.data.iso2 },
                        { placeOfDischargeState: '' },
                    ])

                    clearError('placeOfDischargeCountry')
                    clearError('placeOfDischargeState')

                    //search state
                    axios
                        .post(ApiClickargo.CLICKARGO_STATE_LIST, {
                            country_name: inputForm[0].placeOfDischargeCountryLabel
                        })
                        .then(response => {
                            console.log('success get state list')
                            console.log(response.data.data)

                            setArrStateList(response.data.data)
                        })
                        .catch(error => {
                            console.log('failed to get state list')
                            console.log(error)
                        })
                })
                .catch(error => {
                    console.log('failed to get country list')
                    console.log(error)
                })

            setValue([
                { placeOfDischargeLocation: inputForm[0].placeOfDischargeLocation },
            ])

            clearError('placeOfDischargeLocation')
        }

        if (field === "placeOfDischargeCountry") {
            values[0].placeOfDischargeCountry = event.value
            values[0].placeOfDischargeCountryLabel = event.label

            //search state
            axios
                .post(ApiClickargo.CLICKARGO_STATE_LIST, {
                    country_name: inputForm[0].placeOfDischargeCountryLabel
                })
                .then(response => {
                    console.log('success get state list')
                    console.log(response.data.data)

                    setArrStateList(response.data.data)
                })
                .catch(error => {
                    console.log('failed to get state list')
                    console.log(error)
                })

            setValue([
                { placeOfDischargeCountry: inputForm[0].placeOfDischargeCountry },
            ])

            clearError('placeOfDischargeCountry')
        }

        if (field === "placeOfDischargeState") {
            values[0].placeOfDischargeState = event.value
            values[0].placeOfDischargeStateLabel = event.label

            setValue([
                { placeOfDischargeState: inputForm[0].placeOfDischargeState },
            ])

            clearError('placeOfDischargeState')
        }

        if (field === "placeOfDischargeAddress") {
            values[0].placeOfDischargeAddress = event.target.value

            setValue([
                { placeOfDischargeAddress: inputForm[0].placeOfDischargeAddress },
            ])

            clearError('placeOfDischargeAddress')
        }

        if (field === "requestorNPWP") {
            values[0].requestorNPWP = event.target.value

            setValue([
                { requestorNPWP: inputForm[0].requestorNPWP },
            ])

            clearError('requestorNPWP')
        }

        if (field === "requestorNIB") {
            values[0].requestorNIB = event.target.value

            setValue([
                { requestorNIB: inputForm[0].requestorNIB },
            ])

            clearError('requestorNIB')
        }

        if (field === "requestorName") {
            values[0].requestorName = event.target.value

            setValue([
                { requestorName: inputForm[0].requestorName },
            ])

            clearError('requestorName')
        }

        if (field === "requestorAddress") {
            values[0].requestorAddress = event.target.value

            setValue([
                { requestorAddress: inputForm[0].requestorAddress },
            ])

            clearError('requestorAddress')
        }

        if (field === "consigneeName") {
            values[0].consigneeName = event.target.value

            setValue([
                { consigneeName: inputForm[0].consigneeName },
            ])

            clearError('consigneeName')
        }

        if (field === "consigneeNPWP") {
            values[0].consigneeNPWP = event.target.value
        }

        if (field === "notifyPartyName") {
            values[0].notifyPartyName = event.target.value
        }

        if (field === "notifyPartyNPWP") {
            values[0].notifyPartyNPWP = event.target.value
        }

        // if (field === "paymentInformationBankCode") {
        //     values[0].paymentInformationBankCode = event.target.value

        //     setValue([
        //         { paymentInformationBankCode: inputForm[0].paymentInformationBankCode },
        //     ])

        //     clearError('paymentInformationBankCode')
        // }

        // if (field === "paymentInformationBankName") {
        //     values[0].paymentInformationBankName = event.target.value

        //     setValue([
        //         { paymentInformationBankName: inputForm[0].paymentInformationBankName },
        //     ])

        //     clearError('paymentInformationBankName')
        // }

        // if (field === "paymentInformationAccountNumber") {
        //     values[0].paymentInformationAccountNumber = event.target.value

        //     setValue([
        //         { paymentInformationAccountNumber: inputForm[0].paymentInformationAccountNumber },
        //     ])

        //     clearError('paymentInformationAccountNumber')
        // }

        // if (field === "paymentInformationAmount") {
        //     values[0].paymentInformationAmount = event.target.value

        //     setValue([
        //         { paymentInformationAmount: inputForm[0].paymentInformationAmount },
        //     ])

        //     clearError('paymentInformationAmount')
        // }

        setInputForm(values)
    }

    useEffect(() => {
        if (errors.actionType) {
            addToast('Action Type is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.freightMode) {
            addToast('Freight Mode is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.moveType) {
            addToast('Move Type is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.blType) {
            addToast('B/L Type is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.blNumber) {
            addToast('B/L Number is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.placeOfReceiptLocation) {
            addToast('Place of Receipt is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.placeOfReceiptCountry) {
            addToast('Place of Receipt Country is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.portOfLoadingLocation) {
            addToast('Port of Loading is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.portOfLoadingCountry) {
            addToast('Port of Loading Country is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.portOfDestinationLocation) {
            addToast('Port of Destination is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.portOfDestinationCountry) {
            addToast('Port of Destination Country is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.placeOfDischargeLocation) {
            addToast('Place of Discharge is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.placeOfDischargeCountry) {
            addToast('Place of Discharge Country is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.placeOfDischargeState) {
            addToast('Place of Discharge State is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.placeOfDischargeAddress) {
            addToast('Place of Discharge Address is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.requestorNPWP) {
            addToast('Requestor NPWP is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.requestorNIB) {
            addToast('Requestor NIB is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.requestorName) {
            addToast('Requestor Name is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.requestorAddress) {
            addToast('Requestor Address is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        if (errors.consigneeName) {
            addToast('Consignee Name is Required!', {
                appearance: 'error',
                autoDismiss: true,
            })
        }

        // if (errors.paymentInformationBankCode) {
        //     addToast('Bank Code is Required!', {
        //         appearance: 'error',
        //         autoDismiss: true,
        //     })
        // }

        // if (errors.paymentInformationBankName) {
        //     addToast('Bank Name is Required!', {
        //         appearance: 'error',
        //         autoDismiss: true,
        //     })
        // }

        // if (errors.paymentInformationAccountNumber) {
        //     addToast('Account Number is Required!', {
        //         appearance: 'error',
        //         autoDismiss: true,
        //     })
        // }

        // if (errors.paymentInformationAmount) {
        //     addToast('Total Amount is Required!', {
        //         appearance: 'error',
        //         autoDismiss: true,
        //     })
        // }
    }, [errors])

    //location
    const fetchDataLocation = (inputValue, callback) => {
        if (!inputValue) {
            callback([]);
        } else {
            setTimeout(() => {
                axios
                    .post(ApiClickargo.CLICKARGO_UNLOCS, {
                        data: inputValue
                    })
                    .then((data) => {
                        const tempArray = [];
                        data.data.data.forEach((element) => {
                            tempArray.push({ label: `${element.name}` + ' (' + `${element.code}` + ')', value: element.code });
                        });
                        callback(tempArray);
                    })
                    .catch((error) => {
                        console.log(error, "catch the hoop")
                    });
            });
        }
    }

    const reviewPage = () => {
        localStorage.setItem('ckDOInputForm', JSON.stringify(inputForm, null, 2))
        if (isPPJK === true) {
            localStorage.setItem('ckDOSuratKuasa', JSON.stringify(docSuratKuasa, null, 2))
        }

        props.history.push({
            pathname: '/clickargo_do/review'
        })
    }

    let optionActionType = [
        { value: '', label: 'Please Select Action Type' },
        { value: 'import', label: 'Import' },
        // { value: 'export', label: 'Export' }
    ]

    let optionFreightMode = [
        { value: '', label: 'Please Select Freight Mode' },
        { value: 'ocean', label: 'Ocean Freight' }
    ]

    let optionMoveType = [
        { value: '', label: 'Please Select Move Type' },
        { value: 'POL To Door', label: 'Port of Loading to Door' },
        { value: 'Port To Port', label: 'Port of Loading to Port of Destination' },
        { value: 'POD To Door', label: 'Port of Destination to Door' }
    ]

    let optionBlType = [
        { value: '', label: 'Please Select BL Type' },
        { value: 'mabl', label: 'MABL (Master)' },
        { value: 'habl', label: 'HABL (Host)' }
    ]

    let optionCountryList = arrCountryList.map(obj => {
        return { value: obj.iso2, label: obj.name }
    })

    let optionStateList = arrStateList.map(obj => {
        return { value: obj.name, label: obj.name }
    })

    let optionBLNumber = arrBLNumber.map(obj => {
        return { value: obj.bl_no, label: obj.bl_no }
    })

    let optionSealNumberType = [
        { value: '', label: 'Select One' },
        { value: 'carrier', label: 'Carrier' },
        { value: 'shipper', label: 'Shipper' }
    ]

    if (isLocalStorageExist === true) {
        return (
            isLoading ? (
                <Fragment>
                    <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
                </Fragment>
            ) : (
                    <Fragment>
                        <form onSubmit={handleSubmit(reviewPage)}>
                            <div style={{ marginBottom: "10px" }}>
                                <div style={{ float: "left" }}>
                                    <h3 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Clickargo E-DO</h3>
                                </div>
                                <div style={{ float: "right" }}>
                                    <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                                </div>
                            </div>

                            <br />
                            <br />

                            <div style={{ marginBottom: "10px" }}>
                                <Link to="/chooseprinciple" style={{ display: "inline", color: "#2d9ff7" }} >
                                    Back
                                </Link>
                            </div>

                            <div className="kt-portlet">
                                <div className="kt-portlet__body">

                                    <div className="row">
                                        <div className="col-md-12">

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>

                                                <div className="row">
                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Action Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`actionType`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].actionTypeLabel, value: inputForm[0].actionType }}
                                                                options={optionActionType}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('actionType', event)} />
                                                            {errors[`actionType`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`actionType`].message}</span>}
                                                        </div>
                                                    </div>
                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Freight Mode<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`freightMode`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].freightModeLabel, value: inputForm[0].freightMode }}
                                                                options={optionFreightMode}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('freightMode', event)} />
                                                            {errors[`freightMode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`freightMode`].message}</span>}
                                                        </div>
                                                    </div>
                                                    <div className="col-4">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Move Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`moveType`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].moveTypeLabel, value: inputForm[0].moveType }}
                                                                options={optionMoveType}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('moveType', event)} />
                                                            {errors[`moveType`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`moveType`].message}</span>}
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>

                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>B/L Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`blType`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].blTypeLabel, value: inputForm[0].blType }}
                                                                options={optionBlType}
                                                                placeholder="Please Select BL Type"
                                                                onChange={event => handleInputChange('blType', event)}
                                                            />
                                                            {errors[`blType`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`blType`].message}</span>}
                                                        </div>
                                                    </div>
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>B/L Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`blNumber`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            {isPPJK ? <Fragment>
                                                                <Select
                                                                    value={{ label: inputForm[0].blNumberLabel, value: inputForm[0].blNumber }}
                                                                    options={optionBLNumber}
                                                                    placeholder="Please Select BL Number"
                                                                    onChange={event => handleInputChange('blNumberSelect', event)}
                                                                />
                                                            </Fragment> : <Fragment>
                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        placeholder="Enter B/L Number"
                                                                        value={inputForm[0].blNumber}
                                                                        onChange={event => handleInputChange('blNumber', event)} />
                                                                </Fragment>}
                                                            {errors[`blNumber`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`blNumber`].message}</span>}
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Place of Receipt<span className={dashforgeCss.txDanger}>*</span></legend>

                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`placeOfReceiptLocation`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <AsyncSelect
                                                                value={{ label: inputForm[0].placeOfReceiptLocationLabel, value: inputForm[0].placeOfReceiptLocation }}
                                                                loadOptions={fetchDataLocation}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('placeOfReceiptLocation', event)} />
                                                            {errors[`placeOfReceiptLocation`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`placeOfReceiptLocation`].message}</span>}
                                                        </div>
                                                    </div>
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`placeOfReceiptCountry`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].placeOfReceiptCountryLabel, value: inputForm[0].placeOfReceiptCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => handleInputChange('placeOfReceiptCountry', event)} />
                                                            {errors[`placeOfReceiptCountry`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`placeOfReceiptCountry`].message}</span>}
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Port Of Loading<span className={dashforgeCss.txDanger}>*</span></legend>

                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`portOfLoadingLocation`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <AsyncSelect
                                                                value={{ label: inputForm[0].portOfLoadingLocationLabel, value: inputForm[0].portOfLoadingLocation }}
                                                                loadOptions={fetchDataLocation}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('portOfLoadingLocation', event)} />
                                                            {errors[`portOfLoadingLocation`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`portOfLoadingLocation`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`portOfLoadingCountry`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].portOfLoadingCountryLabel, value: inputForm[0].portOfLoadingCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => handleInputChange('portOfLoadingCountry', event)} />
                                                            {errors[`portOfLoadingCountry`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`portOfLoadingCountry`].message}</span>}
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Port Of Destination<span className={dashforgeCss.txDanger}>*</span></legend>

                                                <div className="row">

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`portOfDestinationLocation`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <AsyncSelect
                                                                value={{ label: inputForm[0].portOfDestinationLocationLabel, value: inputForm[0].portOfDestinationLocation }}
                                                                loadOptions={fetchDataLocation}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('portOfDestinationLocation', event)} />
                                                            {errors[`portOfDestinationLocation`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`portOfDestinationLocation`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`portOfDestinationCountry`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].portOfDestinationCountryLabel, value: inputForm[0].portOfDestinationCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => handleInputChange('portOfDestinationCountry', event)} />
                                                            {errors[`portOfDestinationCountry`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`portOfDestinationCountry`].message}</span>}
                                                        </div>
                                                    </div>

                                                </div>

                                            </fieldset>

                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Place Of Delivery<span className={dashforgeCss.txDanger}>*</span></legend>

                                                <div className="row">

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`placeOfDischargeLocation`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <AsyncSelect
                                                                value={{ label: inputForm[0].placeOfDischargeLocationLabel, value: inputForm[0].placeOfDischargeLocation }}
                                                                loadOptions={fetchDataLocation}
                                                                placeholder="Enter Location"
                                                                onChange={event => handleInputChange('placeOfDischargeLocation', event)} />
                                                            {errors[`placeOfDischargeLocation`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`placeOfDischargeLocation`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`placeOfDischargeCountry`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <Select
                                                                value={{ label: inputForm[0].placeOfDischargeCountryLabel, value: inputForm[0].placeOfDischargeCountry }}
                                                                options={optionCountryList}
                                                                onChange={event => handleInputChange('placeOfDischargeCountry', event)} />
                                                            {errors[`placeOfDischargeCountry`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`placeOfDischargeCountry`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`placeOfDischargeState`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            {inputForm[0].placeOfDischargeCountry.length > 0 ? (
                                                                <Select
                                                                    value={{ label: inputForm[0].placeOfDischargeStateLabel, value: inputForm[0].placeOfDischargeState }}
                                                                    options={optionStateList}
                                                                    onChange={event => handleInputChange('placeOfDischargeState', event)} />
                                                            ) : (
                                                                    <Select
                                                                        value={{ label: "Please Choose Country First", value: "" }}
                                                                        options={[]}
                                                                        isDisabled />
                                                                )}
                                                            {errors[`placeOfDischargeState`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`placeOfDischargeState`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`placeOfDischargeAddress`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <textarea
                                                                className="form-control"
                                                                cols="50" rows="5"
                                                                placeholder="Address"
                                                                onChange={event => handleInputChange('placeOfDischargeAddress', event)}
                                                                value={inputForm[0].placeOfDischargeAddress}></textarea>
                                                            {errors[`placeOfDischargeAddress`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`placeOfDischargeAddress`].message}</span>}
                                                        </div>
                                                    </div>

                                                </div>

                                            </fieldset>

                                        </div>

                                        <div className="col-md-6">
                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Requestor</legend>

                                                <div className="row">

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>NPWP<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`requestorNPWP`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].requestorNPWP}
                                                                onChange={event => handleInputChange('requestorNPWP', event)} />
                                                            {errors[`requestorNPWP`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`requestorNPWP`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>NIB<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`requestorNIB`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].requestorNIB}
                                                                onChange={event => handleInputChange('requestorNIB', event)} />
                                                            {errors[`requestorNIB`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`requestorNIB`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`requestorName`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].requestorName}
                                                                onChange={event => handleInputChange('requestorName', event)} />
                                                            {errors[`requestorName`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`requestorName`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Address<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`requestorAddress`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <textarea
                                                                className="form-control"
                                                                value={inputForm[0].requestorAddress}
                                                                onChange={event => handleInputChange('requestorAddress', event)}></textarea>
                                                            {errors[`requestorAddress`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`requestorAddress`].message}</span>}
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>
                                        </div>

                                        <div className="col-md-6" style={{ paddingTop: '10px' }}>
                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>

                                                <div className="row">

                                                    <div className="col-12">
                                                        <h6 className={dashforgeCss.txBlack}><b>Consignee</b></h6>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`consigneeName`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].consigneeName}
                                                                onChange={event => handleInputChange('consigneeName', event)} />
                                                            {errors[`consigneeName`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`consigneeName`].message}</span>}
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>NPWP</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].consigneeNPWP}
                                                                onChange={event => handleInputChange('consigneeNPWP', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <h6 className={dashforgeCss.txBlack}><b>Notify Party</b></h6>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Name</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].notifyPartyName}
                                                                onChange={event => handleInputChange('notifyPartyName', event)} />
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>NPWP</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].notifyPartyNPWP}
                                                                onChange={event => handleInputChange('notifyPartyNPWP', event)} />
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>

                                        {/* <div className="col-md-4">
                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                <legend>Payment Information</legend>
    
                                                <div className="row">
    
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <input
                                                                type="hidden"
                                                                name={`paymentInformationBankCode`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <label className={dashforgeCss.txBlack}>Bank Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].paymentInformationBankCode}
                                                                onChange={event => handleInputChange('paymentInformationBankCode', event)} />
                                                            {errors[`paymentInformationBankCode`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentInformationBankCode`].message}</span>}
                                                        </div>
                                                    </div>
    
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Bank Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`paymentInformationBankName`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].paymentInformationBankName}
                                                                onChange={event => handleInputChange('paymentInformationBankName', event)} />
                                                            {errors[`paymentInformationBankName`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentInformationBankName`].message}</span>}
                                                        </div>
                                                    </div>
    
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Account Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`paymentInformationAccountNumber`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].paymentInformationAccountNumber}
                                                                onChange={event => handleInputChange('paymentInformationAccountNumber', event)} />
                                                            {errors[`paymentInformationAccountNumber`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentInformationAccountNumber`].message}</span>}
                                                        </div>
                                                    </div>
    
                                                    <div className="col-12" style={{ paddingBottom: '10px' }}><hr /></div>
    
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className={dashforgeCss.txBlack}>Total (IDR)<span className={dashforgeCss.txDanger}>*</span></label>
                                                            <input
                                                                type="hidden"
                                                                name={`paymentInformationAmount`}
                                                                ref={register({
                                                                    required: {
                                                                        value: true,
                                                                        message: 'This input field is required!'
                                                                    }
                                                                })} />
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                value={inputForm[0].paymentInformationAmount}
                                                                onChange={event => handleInputChange('paymentInformationAmount', event)} />
                                                            {errors[`paymentInformationAmount`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`paymentInformationAmount`].message}</span>}
                                                        </div>
                                                    </div>
    
                                                </div>
                                            </fieldset>
                                        </div> */}

                                    </div>

                                    <div style={{ textAlign: 'center' }}>
                                        <hr />
                                        <button
                                            type="submit"
                                            className="btn btn-primary btn-md">
                                            Next
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </form>
                        {/* <br />
                        <pre>
                            {JSON.stringify(docSuratKuasa, null, 2)}
                        </pre> */}
                        {/* <br />
                        <pre>
                            {JSON.stringify(inputForm, null, 2)}
                        </pre> */}
                        {/* <br />
                        <pre>
                            {JSON.stringify(inputContainerForm, null, 2)}
                        </pre> */}
                        {/* <br />
                        <pre>
                            {JSON.stringify(getValues(), null, 2)}
                        </pre> */}
                    </Fragment>
                )
        )
    } else {
        return (
            <Fragment>
                <div className="kt-portlet">
                    <div className="kt-portlet__body">

                        <div className="row">
                            <div className="col-md-12">

                                <label>Oops! You can't directly access this page. Please start from E-DO Menu</label>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }


}

export default Form
