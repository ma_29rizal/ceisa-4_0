import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../../Vessel/BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import axios from 'axios'
import { useToasts } from 'react-toast-notifications'
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap'
import { useForm } from 'react-hook-form'
import moment from 'moment'
import * as ApiClickargo from '../../ApiList.js'

function Payment(props) {

    const { register, handleSubmit, errors, watch, setError, clearError, setValue, getValues } = useForm()

    const [modal, setModal] = useState(false)

    const toggle = () => setModal(!modal)

    const [isLoading, setIsLoading] = useState(true)

    const [onConfirmMethodPayment, setOnConfirmMethodPayment] = useState(false)

    const { addToast } = useToasts()

    const [orderDetailBilling, setOrderDetailBilling] = useState([])

    const [paymentMethod, setPaymentMethod] = useState([
        {
            payment: '',
        }
    ])

    useEffect(() => {
        const headerOrderGetDetailBilling = {
            'Key': localStorage.getItem('ckHeaderKey'),
            'Secret': localStorage.getItem('ckHeaderSecret'),
        }

        axios
            .post(ApiClickargo.CLICKARGO_ORDER_GET_DETAIL_BILLING, {
                order_no: localStorage.getItem('ckDOJobNumber')
            }, {
                headers: headerOrderGetDetailBilling
            })
            .then(response => {
                setOrderDetailBilling(response.data.data)
                setIsLoading(false)
            })
            .catch(error => {
                console.log(error)
                addToast('Failed get Billing Information, please try again!', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            })
    }, [])

    const onCompletePayment = () => {
        setModal(!modal)
    }

    const handleInputChange = (field, event) => {
        const values = [...paymentMethod]

        if (field === "payment") {
            values[0].payment = event.target.value
        }

        setPaymentMethod(values)
    }

    const onConfirmed = () => {

        const headerOrderUpdateBillingVA = {
            'Secret': localStorage.getItem('ckHeaderSecret'),
            'Key': localStorage.getItem('ckHeaderKey'),
            'Accept': 'application/json'
        }

        setOnConfirmMethodPayment(true)

        axios
            .put(ApiClickargo.CLICKARGO_ORDER_UPDATE_BILLING_VA, {
                uuid: orderDetailBilling.uuid,
                bank: paymentMethod[0].payment.substring(3).toUpperCase(),
                payment_method: paymentMethod[0].payment.substring(0, 2),
                payment_expired: moment().add('1', 'd').format('YYYY-MM-DD hh:mm')
            }, {
                headers: headerOrderUpdateBillingVA
            })
            .then(response => {
                console.log(response)
                window.location.reload(false)
            })
            .catch(error => {
                console.log(error)
                addToast(error.response.data.message, {
                    appearance: 'error',
                    autoDismiss: true,
                })
                setOnConfirmMethodPayment(false)

            })
    }

    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 2
    })

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    {orderDetailBilling.status === 'PAID' ? '' : (
                        <div className={`row ` + dashforgeCss.marginBottom20}>
                            <div className={`col-12 ` + dashforgeCss.textRight}>
                                <button className="btn btn-primary" onClick={onCompletePayment}>Complete Payment</button>
                            </div>
                        </div>
                    )}
                    <div className="row">
                        <div className="col-12">
                            <h4 className={dashforgeCss.txBlack}>Payment Detail</h4>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Customer Name</label>
                                        <h6 className={dashforgeCss.txBlack}>{orderDetailBilling.customer}</h6>
                                    </div>
                                    <div className="form-group">
                                        <label>Customer Email</label>
                                        <h6 className={dashforgeCss.txBlack}>{orderDetailBilling.customer_email}</h6>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Order No</label>
                                        <h6 className={dashforgeCss.txBlack}>{orderDetailBilling.order_no}</h6>
                                    </div>
                                    <div className="form-group">
                                        <label>Status</label>
                                        <h6 className={dashforgeCss.txBlack}>{orderDetailBilling.status}</h6>
                                    </div>
                                </div>
                            </div>
                            {orderDetailBilling.component.map((inputComponent, indexComponent) => (
                                <Fragment>
                                    <hr />
                                    <div className={dashforgeCss.marginTop15 + ` row`}>
                                        <div className={`col-md-8 ` + dashforgeCss.txBlack}>{inputComponent.name}</div>
                                        <div className={`col-md-4 ` + dashforgeCss.txBlack}><b>{formatter.format(inputComponent.amount)}</b></div>
                                    </div>
                                </Fragment>
                            ))}
                            <hr />
                            <div className={dashforgeCss.marginTop15 + ` row`}>
                                <div className={`col-md-8 ` + dashforgeCss.txBlack}>Total</div>
                                <div className={`col-md-4 ` + dashforgeCss.txBlack}><b>{formatter.format(orderDetailBilling.amount_total)}</b></div>
                            </div>
                        </div>
                    </div>
                    {/* <br />
                    <pre>
                        {JSON.stringify(orderDetailBilling, null, 2)}
                    </pre> */}

                    <Modal isOpen={modal} toggle={toggle} backdrop="static">
                        {orderDetailBilling.bank_account_no === null ? (
                            <form onSubmit={handleSubmit(onConfirmed)}>
                                <ModalBody>
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>CHOOSE PAYMENT METHOD</legend>

                                        <div className="row">
                                            <div className="form-group">
                                                <input
                                                    type="radio"
                                                    className="ml-3"
                                                    onChange={event => handleInputChange('payment', event)}
                                                    value="VA Bank BNI"
                                                    name={`payment`}
                                                    ref={register({
                                                        required: {
                                                            value: true,
                                                            message: 'This input field is required!'
                                                        }
                                                    })}
                                                />&nbsp;
                                        <img style={{ width: '50px', paddingBottom: '5px' }} src={window.location.origin + '/assets/images/clickargo-bni.png'} />
                                        &nbsp;BNI Virtual Account
                                        {errors[`payment`] && <br />}
                                                {errors[`payment`] && <span style={{ fontSize: 13, color: "red" }}>{errors[`payment`].message}</span>}
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div className="row">
                                        <div className="col-12">
                                            {orderDetailBilling.component.map((inputComponent, indexComponent) => (
                                                <Fragment>
                                                    <hr />
                                                    <div className={dashforgeCss.marginTop15 + ` row`}>
                                                        <div className={`col-md-8 ` + dashforgeCss.txBlack}>{inputComponent.name}</div>
                                                        <div className={`col-md-4 ` + dashforgeCss.txBlack}><b>{formatter.format(inputComponent.amount)}</b></div>
                                                    </div>
                                                </Fragment>
                                            ))}
                                            <hr />
                                            <div className={dashforgeCss.marginTop15 + ` row`}>
                                                <div className={`col-md-8 ` + dashforgeCss.txBlack}>Total</div>
                                                <div className={`col-md-4 ` + dashforgeCss.txBlack}><b>{formatter.format(orderDetailBilling.amount_total)}</b></div>
                                            </div>
                                        </div>
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="secondary" onClick={toggle}>Close</Button>{' '}
                                    <Button color="primary" type="submit">
                                        {onConfirmMethodPayment && <i className="fa fa-circle-notch fa-spin"></i>}
                                        {onConfirmMethodPayment && <span>Please wait...</span>}
                                        {!onConfirmMethodPayment && <span>Confirm</span>}
                                    </Button>
                                </ModalFooter>
                            </form>
                        ) : (
                                <Fragment>
                                    <ModalBody>
                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                            <legend>PAYMENT DETAIL</legend>

                                            <div className={`form-group ` + dashforgeCss.textCenter}>
                                                <label className={dashforgeCss.txBlack}>Payment Method</label>
                                                <h6 className={dashforgeCss.txBlack}>{orderDetailBilling.payment_method === 'VA' ? 'Virtual Account' : 'VA ' + orderDetailBilling.bank_code}</h6>
                                            </div>

                                            <div className={`form-group ` + dashforgeCss.textCenter}>
                                                <label className={dashforgeCss.txBlack}>Amount</label>
                                                <h6 className={dashforgeCss.txBlack}>{formatter.format(orderDetailBilling.amount_total)}</h6>
                                            </div>

                                            <div className={`form-group ` + dashforgeCss.textCenter}>
                                                <label className={dashforgeCss.txBlack}>Account Number</label>
                                                <h6 style={{ color: 'red' }}>{orderDetailBilling.bank_account_no}</h6>
                                            </div>

                                            <div className={`form-group ` + dashforgeCss.textCenter}>
                                                <label className={dashforgeCss.txBlack}>Please pay before <b>{moment(orderDetailBilling.payment_expired).format('DD-MM-YYYY hh:mm:ss')}</b></label>
                                            </div>

                                        </fieldset>
                                        <div className="row">
                                            <div className="col-12">
                                                {orderDetailBilling.component.map((inputComponent, indexComponent) => (
                                                    <Fragment>
                                                        <hr />
                                                        <div className={dashforgeCss.marginTop15 + ` row`}>
                                                            <div className={`col-md-8 ` + dashforgeCss.txBlack}>{inputComponent.name}</div>
                                                            <div className={`col-md-4 ` + dashforgeCss.txBlack}><b>{formatter.format(inputComponent.amount)}</b></div>
                                                        </div>
                                                    </Fragment>
                                                ))}
                                            </div>
                                        </div>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary" onClick={toggle}>Close</Button>
                                    </ModalFooter>
                                </Fragment>
                            )}

                    </Modal>
                </Fragment>
            )
    )

}

export default Payment
