import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../../Vessel/BookingRequest/dashforge.module.css'
import moment from 'moment'
import Spinner from 'react-bootstrap/Spinner'

function Documents(props) {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false)
        }, 1000);
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <table class={`table ` + dashforgeCss.tableDocument}>
                        <thead>
                            <tr>
                                <th style={{ textAlign: 'left' }}>Category</th>
                                <th style={{ textAlign: 'left' }}>Time</th>
                                <th style={{ textAlign: 'left' }}>Date</th>
                                <th style={{ textAlign: 'left' }}>Number</th>
                                <th style={{ textAlign: 'left' }}>Uploader</th>
                                <th style={{ textAlign: 'left' }}>Description</th>
                                <th style={{ textAlign: 'left' }}>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                            {props.detailsOrder.documents.map((inputDocuments, indexDocuments) => (
                                <Fragment>
                                    <tr>
                                        <td>{inputDocuments.admin_uploader === null ? 'User' : 'System Administrator'}</td>
                                        <td>{moment(inputDocuments.created_at).format('h:mm')}</td>
                                        <td>{moment(inputDocuments.created_at).format('YYYY-MM-DD')}</td>
                                        <td>{inputDocuments.number}</td>
                                        <td>{inputDocuments.admin_uploader === null ? 'User' : 'Admin'}</td>
                                        <td>{inputDocuments.type}</td>
                                        {inputDocuments.type === 'surat_kuasa' ? (
                                            <Fragment>
                                                <td><a target="_blank" href={inputDocuments.path}><i className="fas fa-download" style={{ color: '#1053b3' }}></i></a></td>
                                            </Fragment>
                                        ) : (
                                                <Fragment>
                                                    <td><a target="_blank" href={`http://storage.api.quarkspark.com/` + inputDocuments.path}><i className="fas fa-download" style={{ color: '#1053b3' }}></i></a></td>
                                                </Fragment>
                                            )}
                                    </tr>
                                </Fragment>
                            ))}
                        </tbody>
                    </table>
                </Fragment>
            )
    )
}

export default Documents
