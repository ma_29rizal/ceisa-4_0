import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../Vessel/BookingRequest/dashforge.module.css'
import { UncontrolledCollapse } from 'reactstrap'
import Spinner from 'react-bootstrap/Spinner'
import axios from 'axios'
import Swal from 'sweetalert2'
import { useToasts } from 'react-toast-notifications'
import * as ApiClickargo from '../ApiList.js'

function MyTransaction(props) {

    const { addToast } = useToasts()

    const [isLoading, setIsLoading] = useState(true)

    const [arrVesselList, setArrVesselList] = useState([]);

    const detailShipment = (job_number, date_convert) => {
        localStorage.setItem('ckDOJobNumber', job_number)
        localStorage.setItem('ckDODateConvert', date_convert)
        props.history.push('/clickargo_do/my_transaction_detail')
    }

    useEffect(() => {
        var month_name = function (dt) {
            var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            return mlist[dt.getMonth()];
        }

        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        axios
            .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
                tax_number: props.dataUserPortal.npwp,
                email: props.dataUserPortal.email,
                // tax_number: '541267799887999',
                // email: 'kusnle5@yopmail.com'
            }, {
                headers: headers
            })
            .then(responseNPWP => {
                if (responseNPWP.data.success === true) {
                    //save header token
                    localStorage.setItem('ckHeaderKey', responseNPWP.data.data.key)
                    localStorage.setItem('ckHeaderSecret', responseNPWP.data.data.secret)
                    localStorage.setItem('ckHeaderToken', responseNPWP.data.data.token)

                    axios
                        .post(ApiClickargo.CLICKARGO_ORDER_LIST_BY_NPWP, {
                            // tax_number: '541267799887999'
                            tax_number: props.dataUserPortal.npwp
                        })
                        .then(response => {
                            console.log('success get job vessel list')
                            const values = [...arrVesselList]
                            response.data.data.filter(opt => opt.orders[0].transaction === 'do' && opt.orders[0].type === 'import').map((inputOrder, indexOrder) => {
                                const dateOrder = new Date(inputOrder.created_at)
                                values.push({
                                    reference_number: inputOrder.value,
                                    created_at: dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear(),
                                    created_by: JSON.parse(inputOrder.orders[0].attribute).user_name,
                                    orders:
                                        inputOrder.orders.map((orderDetails, indexOrderDetails) => (
                                            {
                                                type: orderDetails.type,
                                                move_type: orderDetails.move_type,
                                                created_at: dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear(),
                                                job_number: orderDetails.job_number,
                                                product_type: JSON.parse(orderDetails.attribute).product_type,
                                                freight_mode: orderDetails.freight_mode,
                                                transaction: orderDetails.transaction,
                                                user_status: orderDetails.user_status,
                                                bl_number: JSON.parse(orderDetails.attribute).bl_number,
                                                bl_date: JSON.parse(orderDetails.attribute).bl_date,
                                            }
                                        ))
                                })
                                setArrVesselList(values)
                            })
                            setIsLoading(false)
                        })
                        .catch(error => {
                            if (!error.response) {
                                addToast('Please check your internet connection!', {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                            } else {
                                addToast('Failed get DO Transaction, please try again!', {
                                    appearance: 'error',
                                    autoDismiss: false,
                                })
                                console.log(error.response.data.message)
                            }
                            setIsLoading(false)
                        })
                }
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.data.success === false) {
                        Swal.fire({
                            title: 'Sorry',
                            text: "You need to register first before using the platform. Just a simple step! Lets go :)",
                            icon: 'error',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes!'
                        }).then((result) => {
                            if (result.value) {
                                //set service name
                                localStorage.setItem('ckServiceSelected', 'DO MyTransaction')

                                props.history.push("/clickargo_register")
                            }
                        })
                    }
                }
            })
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    {arrVesselList.map((inputOrder, indexOrder) => (
                        <Fragment>
                            <div className="kt-portlet" id={`toggler` + indexOrder}>
                                <div className="kt-portlet__body" style={{ cursor: 'pointer', paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                                    <div className="kt-widget15">
                                        <div className="row">
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Reference No</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.reference_number}</h5>
                                            </div>
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Created At</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.created_at}</h5>
                                            </div>
                                            <div className={dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd15And20}>
                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Created By</h4>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{inputOrder.created_by}</h5>
                                            </div>
                                            <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-3 ` + dashforgeCss.pd60And20}>
                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>Click to See Transaction</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {inputOrder.orders.map((orderDetails, indexOrderDetails) => (
                                <Fragment>
                                    <UncontrolledCollapse toggler={`#toggler` + indexOrder} style={{ paddingLeft: '10px', paddingRight: '5px' }}>
                                        <div className="kt-portlet">
                                            <div className="kt-portlet__body" onClick={() => detailShipment(orderDetails.job_number, orderDetails.created_at)} style={{ cursor: 'pointer', paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                                                <div className="kt-widget15">
                                                    <div className={`col-12`}>
                                                        <div className="row">
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20}>
                                                                <h3 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.type}</h3>
                                                                <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.move_type}</span>
                                                                <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.created_at}</span>
                                                                <h5 className={dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails.job_number}</h5>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>BL NO</h4>
                                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.bl_number}</h5>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>BL Date</h4>
                                                                <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.bl_date}</h4>
                                                            </div>
                                                            <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Transaction</h4>
                                                                <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails.transaction}</h4>
                                                            </div>
                                                            <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-4 ` + dashforgeCss.pd60And20}>
                                                                <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Status</h4>
                                                                <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>{orderDetails.user_status}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </UncontrolledCollapse>
                                </Fragment>
                            ))}
                        </Fragment>
                    ))}

                </Fragment>
            )
    )
}

export default MyTransaction
