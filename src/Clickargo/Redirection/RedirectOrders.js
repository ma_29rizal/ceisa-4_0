import React, { useState, Fragment, useEffect } from 'react'
import axios from 'axios'
import { useToasts } from 'react-toast-notifications'
import Swal from 'sweetalert2'
import * as ApiClickargo from '../ApiList.js'

function RedirectOrders(props) {

    const { addToast } = useToasts()

    useEffect(() => {
        var month_name = function (dt) {
            var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            return mlist[dt.getMonth()];
        }
        //auth check
        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        axios
            .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
                tax_number: props.dataUserPortal.npwp,
                email: props.dataUserPortal.email,
                // tax_number: '541267799887999',
                // email: 'kusnle5@yopmail.com'
            }, {
                headers: headers
            })
            .then(responseNPWP => {
                if (responseNPWP.data.success === true) {
                    //get order detail api
                    axios
                        .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_UUID, {
                            uuid: props.match.params.id
                        }, {
                            headers: {
                                'Content-Type': 'application/json',
                                'Key': responseNPWP.data.data.key,
                                'Secret': responseNPWP.data.data.secret
                            }
                        })
                        .then(responseOrderDetail => {
                            const dateOrder = new Date(responseOrderDetail.data.data.created_at)
                            //do
                            if (responseOrderDetail.data.data.transaction === 'do') {
                                localStorage.setItem('ckDOJobNumber', responseOrderDetail.data.data.job_number)
                                localStorage.setItem('ckDODateConvert', dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear())
                                props.history.push('/clickargo_do/my_transaction_detail')
                            } else if (responseOrderDetail.data.data.transaction === 'sp2') {
                                localStorage.setItem('ckSP2JobNumber', responseOrderDetail.data.data.job_number)
                                localStorage.setItem('ckSP2DateConvert', dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear())
                                props.history.push('/clickargo_sp2/my_transaction_detail')
                            } else if (responseOrderDetail.data.data.transaction === 'freight') {
                                //country list
                                axios
                                    .get(ApiClickargo.CLICKARGO_COUNTRY_LIST)
                                    .then(response => {
                                        localStorage.setItem('ckSmCountryList', JSON.stringify(response.data.data, null, 2))
                                    })
                                    .catch(error => {
                                        if (error.response) {
                                            console.log(error.response.data.message)
                                        }
                                    })

                                localStorage.setItem('ckSmJobNumber', responseOrderDetail.data.data.job_number)
                                localStorage.setItem('ckSmDateConvert', dateOrder.getDate() + ' ' + month_name(dateOrder) + ' ' + dateOrder.getFullYear())
                                props.history.push('/shipment_management_detail')
                            }
                        })
                        .catch(error => {
                            console.log(error)
                            addToast('Failed get data order details', {
                                appearance: 'error',
                                autoDismiss: true,
                            })
                        })
                }
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.data.success === false) {
                        Swal.fire({
                            title: 'Sorry',
                            text: "You're not registered in clickargo services. Please get back to the dashboard page.",
                            icon: 'error',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes!'
                        }).then((result) => {
                            if (result.value) {
                                this.props.history.push("/dashboard")
                            }
                        })
                    }
                }
            })
    }, [])

    return (
        <div>
            Please wait, we will redirect you in a few second!
        </div>
    )
}

export default RedirectOrders
