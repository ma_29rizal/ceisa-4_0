import React, {Component, Card, CardContent} from 'react';
// import Iframe from 'react-iframe'
import {Route, HashRouter, NavLink} from 'react-router-dom';
import logo_digico from '../assets/media/digico/DigicoAsset.png';
import * as Helper from '../Helper.js';
import * as Constants from '../Constants.js';

class ChooseVessel extends Component{
    constructor(props) {
        super(props);
        this.state  = {
            items:[],
            npwp: this.props.dataUserPortal.npwp,
        }
    }
    async componentDidMount() {

    }
    render(){
        // const mystyle = {
        //     color: "white",
        //     backgroundColor: "DodgerBlue",
        //     padding: "10px",
        //     fontFamily: "Arial"
        // };
        const {npwp} = this.state;
        return(
            <HashRouter>
                <div>
                    <div style={{float : 'left', width: '30%', height: '200px', margin: '10px'}}>
                        <div onClick={() => {
                            this.props.history.push('/vessel');
                        }} className="card" style={{justifyContent: 'center', display:'flex'}}>
                            <div className="row" style={{justifyContent: 'center', display:'flex'}}>
                                <div>
                                    <b>International </b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{float : 'left', width: '30%', height: '200px', margin: '10px'}}>
                        <div onClick={() => {
                            this.props.history.push('/prahuhub/register');
                        }} className="card" style={{justifyContent: 'center', display:'flex'}}>
                            <div className="row" style={{justifyContent: 'center', display:'flex'}}>
                                <div>
                                    <b>Domestic </b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </HashRouter>
        );
    }
}
export default ChooseVessel;
