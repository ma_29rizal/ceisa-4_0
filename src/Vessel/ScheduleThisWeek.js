import React, { useState, Fragment, useEffect } from 'react'

import axios from 'axios'
import BootstrapTable from 'react-bootstrap-table-next'
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css"
import paginationFactory from 'react-bootstrap-table2-paginator'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css'
import { Button } from 'reactstrap';
import Swal from 'sweetalert2'
import { Redirect } from 'react-router'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import * as Constants from '../Constants.js'
import * as ApiClickargo from '../Clickargo/ApiList.js'

function ScheduleThisWeek(props) {

    console.log(props)

    const [scheduleData, setScheduleData] = useState([])

    const [isRedirect, setIsRedirect] = useState(false)

    useEffect(() => {
        axios
            .get(ApiClickargo.CLICKARGO_SCHEDULE_THIS_WEEK)
            .then(response => {
                // console.log(response.data.data)
                setScheduleData(response.data.data)
            })
            .catch(error => {
                if(!error.response) {
                    Swal.fire(
                        'Oops!',
                        'Please check your internet connection!',
                        'error'
                    )
                } else {
                    console.log(error.response.data.message)
                }
            })
    }, [])

    const [inputFields, setInputFields] = useState([
        {
            shipmentType: '',
            shipmentTypeLabel: 'Please Select Shipment Type',
            productType: '',
            productTypeLabel: 'Please Select Product Type',
            //transport
            transportMoveType: '',
            transportMoveTypeLabel: '',
            transportPOCReceipt: '',
            transportPOCDelivery: '',
            transportEarliestDepartureDate: '',
            transportLatestDeliveryDate: '',
            //pre carriage
            preCarriageStart: '',
            preCarriageStartLabel: 'Enter Location',
            preCarriageMode: '',
            preCarriageModeLabel: 'Please choose transport mode',
            preCarriageETD: '',
            preCarriageETA: '',
            //main carriage
            mainCarriagePortOfLoading: '',
            mainCarriagePortOfLoadingName: '',
            mainCarriagePortOfLoadingCountry: '',
            mainCarriagePortOfDischarge: '',
            mainCarriagePortOfDischargeName: '',
            mainCarriagePortOfDischargeCountry: '',
            mainCarriageVessel: '',
            mainCarriageVoyage: '',
            mainCarriageETD: '',
            mainCarriageETA: '',
            //on carriage
            onCarriageStart: '',
            onCarriageStartLabel: 'Enter Location',
            onCarriageMode: '',
            onCarriageModeLabel: 'Please choose transport mode',
            onCarriageETD: '',
            onCarriageETA: '',
            //general details
            generalDetailsCarrier: '',
            generalDetailsCarrierLabel: '',
            generalDetailsCarrierReceipt: '',
            generalDetailsCarrierDelivery: '',
            generalDetailsEarliestDeparture: '',
            generalDetailsLatestDelivery: '',
            generalDetailsBookingOffice: '',
            generalDetailsBookingOfficeLabel: '',
            generalDetailsContractNumber: '',
            //parties
            partiesShipperName: '',
            partiesShipperEmail: '',
            partiesShipperTaxNumber: '',
            partiesShipperAddress1 : '', //update parties //22-05-2010
            partiesShipperAddress2 : '', //update parties //22-05-2010
            partiesShipperCountryCode: '',
            partiesShipperCountryName: 'Please fill country name (Ex: Indonesia)',
            partiesShipperCity: '',
            partiesShipperPostalCode: '',
            partiesForwarder: 'PT GATOTKACA TRANS SYSTEMINDO',
            partiesConsignee: '',
            partiesConsigneeAddress1: '', //update parties //22-05-2010
            partiesConsigneeAddress2: '', //update parties //22-05-2010
            partiesConsigneeCountryCode: '',
            partiesConsigneeCountryName: 'Please fill country name (Ex: Indonesia)',
            partiesConsigneeCity: '',
            partiesConsigneePostalCode: '',
            partiesShipperInttraId: '',
            partiesForwarderInttraId: '',
            partiesForwarderAddress: 'JL ENGGANO NO. 40C TANJUNG PRIOK, JAKARTA UTARA, DKI JAKARTA, 14310, INDONESIA', //update parties
            //additional party
            //contact party
            contractPartyName: '', //update additional party //22-05-2010
            contractPartyAddress1: '', //update additional party //22-05-2010
            contractPartyAddress2: '', //update additional party //22-05-2010
            contractPartyCountryCode: '', //update additional party //22-05-2010
            contractPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            contractPartyCity : '', //update additional party //22-05-2010
            contractPartyPostalCode : '', //update additional party //22-05-2010
            //notify party
            notifyPartyName: '', //update additional party //22-05-2010
            notifyPartyAddress1: '', //update additional party //22-05-2010
            notifyPartyAddress2: '', //update additional party //22-05-2010
            notifyPartyCountryCode: '', //update additional party //22-05-2010
            notifyPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            notifyPartyCity : '', //update additional party //22-05-2010
            notifyPartyPostalCode : '', //update additional party //22-05-2010
            //first additional notify party
            firstNotifyPartyName: '', //update additional party //22-05-2010
            firstNotifyPartyAddress1: '', //update additional party //22-05-2010
            firstNotifyPartyAddress2: '', //update additional party //22-05-2010
            firstNotifyPartyCountryCode: '', //update additional party //22-05-2010
            firstNotifyPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            firstNotifyPartyCity : '', //update additional party //22-05-2010
            firstNotifyPartyPostalCode : '', //update additional party //22-05-2010
            //second additional notify party
            secondNotifyPartyName: '', //update additional party //22-05-2010
            secondNotifyPartyAddress1: '', //update additional party //22-05-2010
            secondNotifyPartyAddress2: '', //update additional party //22-05-2010
            secondNotifyPartyCountryCode: '', //update additional party //22-05-2010
            secondNotifyPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            secondNotifyPartyCity : '', //update additional party //22-05-2010
            secondNotifyPartyPostalCode : '', //update additional party //22-05-2010
            //references
            referencesShipperRefNumber: '',
            referencesPurchaseOrderNumber: '',
            referencesForwarderRefNumber: '',
            referencesConsigneeNumber:'', //update references
            referencesBLNumber:'', //update references
            referencesContractParty:'', //update references
            //comments and notifications
            customerComments: '',
            partnerEmailNotifications: '',
            //update new field schedule
            scheduleOriginUnloc: '',
            scheduleOriginPortName: '',
            scheduleOriginCountry: '',
            scheduleDestinationUnloc: '',
            scheduleDestinationPortName: '',
            scheduleDestinationCountry: '',

        }
    ]);

    const [inputFieldsContainer, setInputFieldsContainer] = useState([
        {
            //container
            containerType: '',
            containerTypeLabel: 'Please Select Container Type',
            containerComments: '',
            //container cargo
            cargo: [
                {
                    cargoDescription: '',
                    cargoHsCode: '',
                    cargoHsCodeLabel: 'Enter Number',
                    cargoWeight: '',
                    cargoWeightType: 'KGM',
                    cargoPackageCount: '',
                    cargoPackageType: '',
                    cargoPackageTypeLabel: 'Please select package type',
                    cargoGrossVolume: '',
                    cargoGrossVolumeType: 'MTQ',
                    cargoPrimaryImoClass: '',
                    cargoPrimaryImoClassLabel: 'Please select IMO Class',
                    cargoUndgNumber: '',
                    cargoPackingGroup: '',
                    cargoPackingGroupLabel: 'Please select Packing Group',
                    cargoProperShippingName: '',
                    cargoEmergencyContactName: '',
                    cargoEmergencyContactNumber: '',
                }
            ]
        }
    ]);

    const [inputFieldsPaymentDetails, setInputFieldsPaymentDetails] = useState([
        {
            paymentDetailsChangeType: '',
            paymentDetailsChangeTypeLabel: 'Select one',
            paymentDetailsFreightTerm: '',
            paymentDetailsFreightTermLabel: 'Select one',
            paymentDetailsPayer: '',
            paymentDetailsPayerLabel: 'Select one',
            paymentDetailsPaymentLocation: '',
            paymentDetailsPaymentLocationLabel: 'Enter Location',
        }
    ]);

    const submitHandler = (uuid) => {
        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        setTimeout(() => {

            axios
                .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
                    tax_number: props.dataUserPortal.npwp,
                    email: props.dataUserPortal.email,
                    // tax_number: '541267799887999',
                    // email: 'kusnle5@yopmail.com'
                }, {
                    headers: headers
                })
                .then(responseNPWP => {
                    if (responseNPWP.data.success === true) {

                        axios
                            .post(ApiClickargo.CLICKARGO_FIND_SCHEDULE, {
                                uuid: uuid
                            })
                            .then(responseSchedule => {

                                const values = [...inputFields]
                                //transport
                                values[0].transportMoveType = ''
                                values[0].transportMoveTypeLabel = 'Please Select Move Type'
                                values[0].transportPOCReceipt = responseSchedule.data.data.origin_port_name + ' (' + responseSchedule.data.data.origin_unloc + ')'
                                values[0].transportPOCDelivery = responseSchedule.data.data.destination_port_name + ' (' + responseSchedule.data.data.destination_unloc + ')'
                                values[0].transportEarliestDepartureDate = moment(responseSchedule.data.data.origin_departure_date).format('DD/MM/YYYY')
                                values[0].transportLatestDeliveryDate = moment(responseSchedule.data.data.destination_arrival_date).format('DD/MM/YYYY')
                                
                                //main carriage
                                values[0].mainCarriagePortOfLoading = responseSchedule.data.data.origin_unloc
                                values[0].mainCarriagePortOfLoadingName = responseSchedule.data.data.origin_port_name
                                values[0].mainCarriagePortOfLoadingCountry = responseSchedule.data.data.origin_country
                                values[0].mainCarriagePortOfDischarge = responseSchedule.data.data.destination_unloc
                                values[0].mainCarriagePortOfDischargeName = responseSchedule.data.data.destination_port_name
                                values[0].mainCarriagePortOfDischargeCountry = responseSchedule.data.data.destination_country
                                values[0].mainCarriageVessel = responseSchedule.data.data.vessel_name
                                values[0].mainCarriageVoyage = responseSchedule.data.data.voyage_name
                                values[0].mainCarriageETD = responseSchedule.data.data.origin_departure_date
                                values[0].mainCarriageETA = responseSchedule.data.data.destination_arrival_date

                                //general details
                                values[0].generalDetailsCarrier = responseSchedule.data.data.scac
                                values[0].generalDetailsCarrierLabel = responseSchedule.data.data.carrier_name
                                values[0].generalDetailsCarrierReceipt = responseSchedule.data.data.origin_unloc
                                values[0].generalDetailsCarrierDelivery = responseSchedule.data.data.destination_unloc
                                values[0].generalDetailsEarliestDeparture = responseSchedule.data.data.origin_departure_date
                                values[0].generalDetailsLatestDelivery = responseSchedule.data.data.destination_arrival_date
                                values[0].generalDetailsBookingOffice = ''
                                values[0].generalDetailsBookingOfficeLabel = 'Enter Location'
                                values[0].generalDetailsContractNumber = ''
                                
                                //parties
                                values[0].partiesShipperName = responseNPWP.data.data.name
                                values[0].partiesShipperEmail = responseNPWP.data.data.email
                                values[0].partiesShipperInttraId = responseNPWP.data.data.inttra_id
                                values[0].partiesForwarderInttraId = responseNPWP.data.data.inttra_id

                                //schedule
                                values[0].scheduleOriginUnloc = responseSchedule.data.data.origin_unloc
                                values[0].scheduleOriginPortName = responseSchedule.data.data.origin_port_name
                                values[0].scheduleOriginCountry = responseSchedule.data.data.origin_country
                                values[0].scheduleDestinationUnloc = responseSchedule.data.data.destination_unloc
                                values[0].scheduleDestinationPortName = responseSchedule.data.data.destination_port_name
                                values[0].scheduleDestinationCountry = responseSchedule.data.data.destination_country

                                setInputFields(values)
                                //save header token
                                localStorage.setItem('ckHeaderKey', responseNPWP.data.data.key)
                                localStorage.setItem('ckHeaderSecret', responseNPWP.data.data.secret)
                                localStorage.setItem('ckHeaderToken', responseNPWP.data.data.token)

                                props.history.push({
                                    pathname: '/booking_request_back',
                                    state: {
                                        generalArr: inputFields,
                                        containerArr: inputFieldsContainer,
                                        paymentDetailsArr: inputFieldsPaymentDetails
                                    }
                                })

                            })
                            .catch(error => {
                                if (error.response) {
                                    console.log(error.response.data.message)
                                }
                            })
                        
                    } 
                })
                .catch(error => {
                    if (error.response) {
                        if(error.response.data.success === false) {
                            Swal.fire({
                                title: 'Sorry',
                                text: "You need to register first before using the platform. Just a simple step! Lets go :)",
                                icon: 'error',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes!'
                            }).then((result) => {
                                if (result.value) {
                                    //save schedule uuid
                                    localStorage.setItem('scheduleUuid', uuid)
                                    //save move type
                                    localStorage.setItem('moveType', '')
                                    localStorage.setItem('moveTypeLabel', 'Please Select Move Type')
                                    //set service name
                                    localStorage.setItem('ckServiceSelected', 'Vessel')

                                    props.history.push("/clickargo_register")
                                }
                            })
                        }
                    }
                })

        }, 1200);
    }

    const durationFormatter = (cell) => {
        return (
            cell + ' Days'
        )
    }

    const actionFormatter = (cell) => {
        return (
            <div>
                <Button
                    type="button"
                    color="default" 
                    onClick={() => submitHandler(cell)}>
                    <img src={window.location.origin + '/assets/images/Clickargo2.png'} /><span>Book Now</span></Button >
            </div>
        )
    }

    const columns = [
        {
            dataField: 'vessel_name',
            text: 'Vessel',
            sort: true
        },
        {
            dataField: 'carrier_name',
            text: 'Agent',
            sort: true
        },
        {
            dataField: 'service_name',
            text: 'Service',
            sort: true
        },
        {
            dataField: 'voyage_name',
            text: 'Voyage No.',
            sort: true
        },
        {
            dataField: 'imo_number',
            text: 'Imo',
            sort: true
        },
        {
            dataField: 'origin_departure_date',
            text: 'Departure',
            sort: true
        },
        {
            dataField: 'destination_arrival_date',
            text: 'Arrival',
            sort: true
        },
        {
            dataField: 'origin',
            text: 'Origin'
        },
        {
            dataField: 'destination',
            text: 'Destination'
        },
        {
            dataField: 'total_duration',
            text: 'Duration',
            sort: true,
            formatter: durationFormatter
        },
        {
            dataField: 'uuid',
            text: '',
            formatter: actionFormatter
        }
    ];

    const options = {
        // pageStartIndex: 0,
        sizePerPage: 10,
        hideSizePerPage: true,
        hidePageListOnlyOnePage: true
    };

    
    return (
        <div>
            <div>
                <hr style={{ border: "10px solid #f3f3f7" }} />
            </div>

            <div className="kt-portlet__head">
                <div className="kt-portlet__head-label">
                    <h3 className="kt-portlet__head-title">
                        <span>
                            <label className="kt-font-boldest"> Schedule List</label>
                        </span>
                    </h3>
                </div>
            </div>

            <div className="kt-portlet__body">
                <div className="kt-widget15">
                    <BootstrapTable
                        bootstrap4
                        keyField='uuid'
                        data={scheduleData}
                        columns={columns}
                        pagination={paginationFactory(options)} />
                </div>
            </div>
        </div>
    )
    
}

export default withRouter(ScheduleThisWeek)
