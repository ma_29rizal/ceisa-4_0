import React, { useState, Fragment, useEffect } from 'react'
import { Link } from 'react-router-dom'
import dashforgeCss from '../BookingRequest/dashforge.module.css'
import { withRouter } from 'react-router-dom'
import { Tabs, Tab } from 'react-bootstrap'
import Status from '../ShipmentManagement/Tabs/Status'
import BookingRequest from '../ShipmentManagement/Tabs/BookingRequest'
import ShippingInstruction from '../ShipmentManagement/Tabs/ShippingInstruction'
import Documents from '../ShipmentManagement/Tabs/Documents'
import Spinner from 'react-bootstrap/Spinner'
import { ToastProvider, useToasts } from 'react-toast-notifications'
import axios from 'axios'
import * as ApiClickargo from '../../Clickargo/ApiList.js'

function Detail(props) {

    const { addToast } = useToasts()

    const [activeTabs, setActiveTabs] = useState('shipmentStatus');

    const [countryList, setCountryList] = useState([])

    const [orderDetails, setOrderDetails] = useState([])

    const [orderCreatedAt, setOrderCreatedAt] = useState('')

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        //update state
        setTimeout(() => {
            setCountryList(JSON.parse(localStorage.getItem('ckSmCountryList')))
            setOrderCreatedAt(localStorage.getItem('ckSmDateConvert'))

            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckSmJobNumber')
                })
                .then(response => {
                    console.log('success get detail vessel')
                    setOrderDetails([...orderDetails, response.data.data])

                    if (response.data.data.shipping_instructions.length > 0) {
                        setActiveTabs('shipmentStatus')
                    } else {
                        setActiveTabs('shipmentSI')
                    }

                    setIsLoading(false)
                })
                .catch(error => {
                    addToast(error.response.data.message, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                })
        }, 1000);

    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div style={{ marginBottom: "10px" }}>
                        <Link to="/shipment_management" style={{ display: "inline", color: "#2d9ff7" }} >
                            Back
                        </Link>
                    </div>

                    <div className="kt-portlet">
                        <div className="kt-portlet__body" style={{ paddingLeft: '25px', paddingTop: '0px', paddingRight: '0px', paddingBottom: '0px' }}>
                            <div className="kt-widget15">
                                <div className={`col-12`}>
                                    <div className="row">
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20}>
                                            <h3 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].type}</h3>
                                            <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails[0].move_type}</span>
                                            <span className={dashforgeCss.upperTitle + ` ` + dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderCreatedAt}</span>
                                            <h5 className={dashforgeCss.displayBlock + ` ` + dashforgeCss.marginBottom5 + ` ` + dashforgeCss.txBlack}>{orderDetails[0].job_number}</h5>
                                        </div>
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Product Type</h4>
                                            <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].details.product_type}</h5>
                                        </div>
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Freight Mode</h4>
                                            <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].freight_mode}</h4>
                                        </div>
                                        <div className={`col-md-2` + ` ` + dashforgeCss.pd15And20 + ` ` + dashforgeCss.alignSelfCenter}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Transaction</h4>
                                            <h4 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txBlack}>{orderDetails[0].transaction}</h4>
                                        </div>
                                        <div className={dashforgeCss.statusBox + ` ` + dashforgeCss.alignSelfCenter + ` col-md-4 ` + dashforgeCss.pd60And20}>
                                            <h4 className={dashforgeCss.txGrey + ` ` + dashforgeCss.upperTitle + ` ` + dashforgeCss.tx14}>Status</h4>
                                            <h5 className={dashforgeCss.upperTitle + ` ` + dashforgeCss.txWhite}>{orderDetails[0].user_status}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="kt-portlet">
                        <div className="kt-portlet__body">
                            <div className="kt-widget15">
                                <ToastProvider>
                                    <Tabs activeKey={activeTabs} onSelect={k => setActiveTabs(k)} unmountOnExit>
                                        <Tab eventKey="shipmentStatus" title="Status">
                                            <Status />
                                        </Tab>
                                        <Tab eventKey="shipmentBR" title="Booking Request">
                                            <BookingRequest />
                                        </Tab>
                                        <Tab eventKey="shipmentSI" title="Shipping Instruction">
                                            <ShippingInstruction dataCountryList={countryList} dataUserPortal={props.dataUserPortal} />
                                        </Tab>
                                        <Tab eventKey="shipmentDocuments" title="Documents">
                                            <Documents dataUserPortal={props.dataUserPortal} />
                                        </Tab>
                                    </Tabs>
                                </ToastProvider>
                            </div>
                        </div>
                    </div>
                </Fragment>
            )
    )
}

export default withRouter(Detail)
