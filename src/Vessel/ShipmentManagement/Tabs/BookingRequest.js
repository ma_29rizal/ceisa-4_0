import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../BookingRequest/dashforge.module.css'
import Spinner from 'react-bootstrap/Spinner'
import axios from 'axios'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import * as ApiClickargo from '../../../Clickargo/ApiList.js'

function BookingRequest() {

    const [orderDetails, setOrderDetails] = useState([])

    const [isLoading, setIsLoading] = useState(true);

    const [orderMoveType, setOrderMoveType] = useState('');

    const [showPreCarriage, setShowPreCarriage] = useState(false);
    const [showOnCarriage, setShowOnCarriage] = useState(false);

    const [isHazmat, setIsHazmat] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckSmJobNumber')
                })
                .then(response => {
                    console.log('success get detail vessel')
                    const dataVessel = response.data.data

                    setOrderDetails([...orderDetails, dataVessel])

                    //set move type
                    if (dataVessel.move_type === 'DoorToDoor') {
                        setOrderMoveType([...orderMoveType, 'Door To Door'])
                        setShowPreCarriage(true)
                        setShowOnCarriage(true)
                    } else if (dataVessel.move_type === 'DoorToPort') {
                        setOrderMoveType([...orderMoveType, 'Door To Port'])
                        setShowPreCarriage(true)
                        setShowOnCarriage(false)
                    } else if (dataVessel.move_type === 'PortToDoor') {
                        setOrderMoveType([...orderMoveType, 'Port To Door'])
                        setShowPreCarriage(false)
                        setShowOnCarriage(true)
                    } else if (dataVessel.move_type === 'PortToPort') {
                        setOrderMoveType([...orderMoveType, 'Port To Port'])
                        setShowPreCarriage(false)
                        setShowOnCarriage(false)
                    }

                    //set product type
                    if (dataVessel.details.product_type === 'special') {
                        setIsHazmat(true)
                    } else if (dataVessel.details.product_type === 'dangerous') {
                        setIsHazmat(true)
                    } else if (dataVessel.details.product_type === 'general') {
                        setIsHazmat(false)
                    } else if (dataVessel.details.product_type === 'others') {
                        setIsHazmat(false)
                    }

                    setIsLoading(false)
                })
                .catch(error => {
                    console.log(error)
                    // console.log(error.response.data.message)
                })
        }, 1000);
    }, [])

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <Accordion defaultActiveKey="0">
                        {orderDetails[0].booking_requests.sort((a, b) => b.version - a.version).map((inputBr, indexBr) => (
                            <Fragment>
                                <Card>
                                    <Accordion.Toggle as={Card.Header} eventKey={indexBr}>
                                        <div className={dashforgeCss.txBlack} style={{ paddingLeft: '15px', paddingTop: '15px', paddingRight: '15px', paddingBottom: '15px' }}>
                                            {orderDetails[0].job_number + ` (` + inputBr.reference_no + `) - version ` + inputBr.version}
                                        </div>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey={indexBr}>
                                        <Card.Body>
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>General Details</legend>

                                                        <div className="row">

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Carrier /NVOCC/ Booking Agent<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.carrier_name} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Booking Office<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.booking_office} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Contract Number<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.contract_number} />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </fieldset>

                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>Parties</legend>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <legend>SHIPPER</legend>

                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper Name</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_name} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper's INTTRA ID</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_inttra_id} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper's Tax Number</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_npwp} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6"></div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_address_1} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper City<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_city} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper Address Line 2<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_address_2} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_postal_code} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Shipper Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.shipper_country_name} />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <legend>Forwarder</legend>

                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Forwarder</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.forwarder} />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </fieldset>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <legend>Consignee</legend>

                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Consignee Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.consignee} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6"></div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Consignee Address Line 1<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.consignee_address_1} />

                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Consignee City<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.consignee_city} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Consignee Address Line 2<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.consignee_address_2} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Consignee Postal Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.consignee_postal_code} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Consignee Country<span className={dashforgeCss.txDanger}>*</span></label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.consignee_country_name} />
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </fieldset>

                                                    </fieldset>

                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>Additional Party (optional)</legend>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <legend>Contract Party</legend>

                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Contract Party</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.contract_party_name} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6"></div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.contract_party_address_1} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.contract_party_city} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.contract_party_address_2} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.contract_party_postal_code} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.contract_party_country_name} />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <legend>Notify Party</legend>

                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Notify Party</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.notify_party_name} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6"></div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.notify_party_address_1} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.notify_party_city} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.notify_party_address_2} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.notify_party_postal_code} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.notify_party_country_name} />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <legend>First Additional Notify Party</legend>

                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>First Additional Notify Party</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.first_additional_party} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6"></div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.first_additional_party_address_1} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.first_additional_party_city} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.first_additional_party_address_2} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.first_additional_party_postal_code} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.first_additional_party_country_name} />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>

                                                        <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                            <legend>Second Additional Notify Party</legend>

                                                            <div className="row">

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Second Additional Notify Party</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.second_additional_party} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6"></div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 1</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.second_additional_party_address_1} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>City</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.second_additional_party_city} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Address Line 2</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.second_additional_party_address_2} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Postal Code</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.second_additional_party_postal_code} />
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label className={dashforgeCss.txBlack}>Country</label>
                                                                        <input
                                                                            type="text"
                                                                            disabled
                                                                            className="form-control"
                                                                            value={inputBr.details.second_additional_party_country_name} />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>

                                                    </fieldset>

                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>References</legend>

                                                        <div className="row">

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Shipper's References Number(s)</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.shipper_ref_number} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Purchase Order Number(s)</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.po_number} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Forwarder's References Number(s)</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.forwarder_ref_number} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Consignee's Reference Number</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.consignee_ref_number} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Contract Party Reference Number</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.contract_party_ref_number} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>B/L Reference Number</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.bl_ref_number} />
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </fieldset>

                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>Transport</legend>

                                                        <div className="row">

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Move type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={orderMoveType} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Earliest Departure Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.earliest_departure} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Place of Carrier Receipt<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.port_of_load_name + ` (` + inputBr.details.main_carriage.port_of_load + `)`} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Latest Delivery Date<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.latest_delivery} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Place of Carrier Delivery<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.port_of_discharge_name + ` (` + inputBr.details.main_carriage.port_of_discharge + `)`} />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </fieldset>

                                                    {showPreCarriage && <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>Pre-Carriage</legend>

                                                        <div className="row">

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Start<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.pre_carriage.start} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>ETD<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.pre_carriage.etd} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Mode</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.pre_carriage.eta} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>ETA<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.pre_carriage.mode} />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </fieldset>
                                                    }

                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>Main Carriage</legend>

                                                        <div className="row">

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Port of Load<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.port_of_load_name + ` (` + inputBr.details.main_carriage.port_of_load + `)`} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>ETD Vessel</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.etd} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Port of Discharge<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.port_of_discharge_name + ` (` + inputBr.details.main_carriage.port_of_discharge + `)`} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>ETA Vessel</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.eta} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Vessel<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.vessel} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Voyage<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.main_carriage.voyage} />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </fieldset>

                                                    {showOnCarriage && <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>On Carriage</legend>

                                                        <div className="row">

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>End<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.on_carriage.end} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>ETD<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.on_carriage.etd} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Mode</label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.on_carriage.mode} />
                                                                </div>
                                                            </div>

                                                            <div className="col-6">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>ETA<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.on_carriage.eta} />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </fieldset>
                                                    }

                                                    {orderDetails[0].containers.filter(order => order.order_integration_uuid === inputBr.uuid).map((inputContainer, indexContainer) => (
                                                        <Fragment key={`${inputContainer}~${indexContainer}`}>
                                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                                <legend>Container</legend>

                                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                                    <legend>Container {indexContainer + 1}</legend>

                                                                    <div className="row">

                                                                        <div className="col-3">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Container Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputContainer.name + ` (` + inputContainer.code + `)`} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-9">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Container Comments<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputContainer.attribute.container_comments} />
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    {inputContainer.cargos.map((inputCargo, indexCargo) => (
                                                                        <div>
                                                                            <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '10px', marginTop: '10px' }}>

                                                                                <div className="row">
                                                                                    <div className="col-md-8">
                                                                                        <h6 className={dashforgeCss.txBlack}>Cargo</h6>
                                                                                        <span className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}><div className={dashforgeCss.txDanger}>NOTE:</div> The sum of all Cargo Weights reflects the Gross Weight of the Cargo (excluding Tare) for the entire booking.</span>
                                                                                        <p className={dashforgeCss.tx12 + ' ' + dashforgeCss.txBlack}>Inaccurate declaration of cargo weight may endanger crew, port worker, and vessel safety. Please verify the reasonable accuracy of this information prior to submission</p>
                                                                                    </div>
                                                                                </div>

                                                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                                                    <div className="row">

                                                                                        <div className="col-6">
                                                                                            <div className="form-group">
                                                                                                <label className={dashforgeCss.txBlack}>Cargo Description<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                                <input
                                                                                                    type="text"
                                                                                                    disabled
                                                                                                    className="form-control"
                                                                                                    value={inputCargo.description} />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="col-6">
                                                                                            <div className="form-group">
                                                                                                <label className={dashforgeCss.txBlack}>Package Count<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                                <input
                                                                                                    type="text"
                                                                                                    disabled
                                                                                                    className="form-control"
                                                                                                    value={inputCargo.quantity} />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="col-6">
                                                                                            <div className="form-group">
                                                                                                <label className={dashforgeCss.txBlack}>HS Code<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                                <input
                                                                                                    type="text"
                                                                                                    disabled
                                                                                                    className="form-control"
                                                                                                    value={inputCargo.hs_code} />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="col-6">
                                                                                            <div className="form-group">
                                                                                                <label className={dashforgeCss.txBlack}>Package Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                                <input
                                                                                                    type="text"
                                                                                                    disabled
                                                                                                    className="form-control"
                                                                                                    value={inputCargo.package_code} />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="col-6">
                                                                                            <div className="form-group">
                                                                                                <label className={dashforgeCss.txBlack}>Cargo Weight (Excludes Tares)<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                                <div className="input-group" style={{ flexDirection: 'row' }}>
                                                                                                    <div className="gross-volume-wrapper" style={{ display: 'flex' }}>
                                                                                                        <input
                                                                                                            type="text"
                                                                                                            disabled
                                                                                                            className="form-control"
                                                                                                            value={inputCargo.weight} />
                                                                                                        <div className="input-group-append">
                                                                                                            <select
                                                                                                                disabled
                                                                                                                className="dropdown-toggle"
                                                                                                                type="button"
                                                                                                                data-toggle="dropdown"
                                                                                                                aria-haspopup="true"
                                                                                                                aria-expanded="false"
                                                                                                                value={inputCargo.weight_uom}>
                                                                                                                <option value="KGM" className="dropdown-item">Kgs</option>
                                                                                                                <option value="LBS" className="dropdown-item">Pounds</option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="col-6">
                                                                                            <div className="form-group">
                                                                                                <label className={dashforgeCss.txBlack}>Gross Volume<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                                <div className="input-group" style={{ flexDirection: 'row' }}>
                                                                                                    <div className="gross-volume-wrapper" style={{ display: 'flex' }}>
                                                                                                        <input
                                                                                                            type="text"
                                                                                                            disabled
                                                                                                            className="form-control"
                                                                                                            value={inputCargo.gross_volume} />
                                                                                                        <div className="input-group-append">
                                                                                                            <select
                                                                                                                disabled
                                                                                                                className="dropdown-toggle"
                                                                                                                type="button"
                                                                                                                data-toggle="dropdown"
                                                                                                                aria-haspopup="true"
                                                                                                                aria-expanded="false"
                                                                                                                value={inputCargo.gross_volume_uom}>
                                                                                                                <option value="MTQ">Cubic meters</option>
                                                                                                                <option value="FTQ">Cubic feet</option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>

                                                                                    {isHazmat && <Fragment>
                                                                                        <div className="row">
                                                                                            <div className="col-12">
                                                                                                <h6 className={dashforgeCss.txBlack}>Hazmat Details</h6>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div className="row">

                                                                                            <div className="col-12">
                                                                                                <div className="form-group">
                                                                                                    <label className={dashforgeCss.txBlack}>Primary IMO Class</label>
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        disabled
                                                                                                        className="form-control"
                                                                                                        value={JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_imo} />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div className="col-6">
                                                                                                <div className="form-group">
                                                                                                    <label className={dashforgeCss.txBlack}>UNDG Number</label>
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        disabled
                                                                                                        className="form-control"
                                                                                                        value={JSON.parse(inputCargo.attribute.replace(/\\/g, "")).undg_number} />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div className="col-6">
                                                                                                <div className="form-group">
                                                                                                    <label className={dashforgeCss.txBlack}>Emergency Contact Name</label>
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        disabled
                                                                                                        className="form-control"
                                                                                                        value={JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_name} />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div className="col-6">
                                                                                                <div className="form-group">
                                                                                                    <label className={dashforgeCss.txBlack}>Packing Group</label>
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        disabled
                                                                                                        className="form-control"
                                                                                                        value={JSON.parse(inputCargo.attribute.replace(/\\/g, "")).packing_group} />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div className="col-6">
                                                                                                <div className="form-group">
                                                                                                    <label className={dashforgeCss.txBlack}>Emergency Contact Number</label>
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        disabled
                                                                                                        className="form-control"
                                                                                                        value={JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_emergency_contact_number} />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div className="col-6">
                                                                                                <div className="form-group">
                                                                                                    <label className={dashforgeCss.txBlack}>Proper Shipping Name</label>
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        disabled
                                                                                                        className="form-control"
                                                                                                        value={JSON.parse(inputCargo.attribute.replace(/\\/g, "")).cargo_proper_shipping} />
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </Fragment>}

                                                                                </fieldset>

                                                                            </fieldset>
                                                                        </div>

                                                                    ))}

                                                                </fieldset>

                                                            </fieldset>

                                                        </Fragment>
                                                    ))}

                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>Payment Details</legend>

                                                        {inputBr.details.payment_detail.map((inputPaymentDetails, indexPaymentDetails) => (
                                                            <Fragment key={`${inputPaymentDetails}~${indexPaymentDetails}`}>
                                                                <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                                    <div className="row">

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Charge Type<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputPaymentDetails.charge_type} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Freight Term<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputPaymentDetails.freight_term} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Payer<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputPaymentDetails.payer} />
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-6">
                                                                            <div className="form-group">
                                                                                <label className={dashforgeCss.txBlack}>Payment Location<span className={dashforgeCss.txDanger}>*</span></label>
                                                                                <input
                                                                                    type="text"
                                                                                    disabled
                                                                                    className="form-control"
                                                                                    value={inputPaymentDetails.payment_location} />
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </fieldset>

                                                            </Fragment>
                                                        ))}
                                                    </fieldset>

                                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                                        <legend>Comments & Notifications</legend>

                                                        <div className="row">

                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Customer Comments</label>
                                                                    <textarea
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.customer_comments}></textarea>
                                                                </div>
                                                            </div>

                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <label className={dashforgeCss.txBlack}>Partner Email Notifications<span className={dashforgeCss.txDanger}>*</span></label>
                                                                    <input
                                                                        type="text"
                                                                        disabled
                                                                        className="form-control"
                                                                        value={inputBr.details.partner_email} />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </fieldset>

                                                </div>
                                            </div>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Fragment>
                        ))}
                    </Accordion>
                </Fragment>
            )
    )
}

export default BookingRequest
