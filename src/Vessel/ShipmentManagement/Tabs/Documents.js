import React, { useState, Fragment, useEffect } from 'react'
import dashforgeCss from '../../BookingRequest/dashforge.module.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import * as ApiClickargo from '../../../Clickargo/ApiList.js'
import { useToasts } from 'react-toast-notifications'
import axios from 'axios'
import Select from 'react-select'
import AsyncSelect from 'react-select/async'
import { useForm } from 'react-hook-form'
import uuid from 'react-uuid'
import Spinner from 'react-bootstrap/Spinner'
import moment from 'moment'
import Swal from 'sweetalert2'

function Documents(props) {

    const [isLoading, setIsLoading] = useState(true)

    const { addToast } = useToasts()

    const { register, handleSubmit, errors, watch, clearError, setValue, getValues } = useForm()

    const [modal, setModal] = useState(false)

    const toggle = () => setModal(!modal)

    const [isBL, setIsBL] = useState(false)
    const [isOther, setIsOther] = useState(false)
    const [onSubmitDocument, setOnSubmitDocument] = useState(false)

    const [documentList, setDocumentList] = useState([])
    const [orderUuid, setOrderUuid] = useState([])

    useEffect(() => {
        setTimeout(() => {
            axios
                .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                    job_number: localStorage.getItem('ckSmJobNumber')
                })
                .then(response => {
                    const orderDetails = response.data.data

                    setDocumentList([orderDetails.documents])
                    setOrderUuid([orderDetails.export_uuid])

                    //spinner false after all data is updated
                    setIsLoading(false)
                })
                .catch(error => {
                    addToast(error.response.data.message, {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                })
        }, 1000);
    }, [])

    const [inputFieldsDocuments, setInputFieldsDocuments] = useState([
        {
            orderType: 'export',
            documentDate: '',
            documentType: '',
            documentTypeLabel: 'Please select document type',
            documentNumber: '',
            documentFile: '',
            portOfDischarge: '',
            portOfDischargeLabel: 'Enter Location',
            npwpNumber: '',
            importUuid: '',
            exportUuid: '',
            documentTypeOther: ''
        }
    ])

    //on modal closed
    useEffect(() => {
        if (modal === false) {
            const values = [...inputFieldsDocuments]

            values[0].orderType = 'export'
            values[0].documentDate = ''
            values[0].documentType = ''
            values[0].documentTypeLabel = 'Please select document type'
            values[0].documentNumber = ''
            values[0].documentFile = ''
            values[0].portOfDischarge = ''
            values[0].portOfDischargeLabel = 'Enter Location'
            values[0].npwpNumber = ''
            values[0].importUuid = ''
            values[0].exportUuid = ''
            values[0].documentTypeOther = ''

            setInputFieldsDocuments(values)

            if (inputFieldsDocuments[0].documentType === 'mbl') {
                setIsBL(true)
                setIsOther(false)
            } else if (inputFieldsDocuments[0].documentType === 'other') {
                setIsOther(true)
                setIsBL(false)
            } else {
                setIsBL(false)
                setIsOther(false)
            }
        }
    }, [modal])

    const handleInputChangeDocuments = (field, event) => {
        const values = [...inputFieldsDocuments]

        if (field === "documentType") {
            values[0].documentType = event.value
            values[0].documentTypeLabel = event.label

            if (event.value === 'mbl') {
                setIsBL(true)
                setIsOther(false)
            } else if (event.value === 'other') {
                setIsOther(true)
                setIsBL(false)
            } else {
                setIsBL(false)
                setIsOther(false)
            }

            setValue([
                { documentType: inputFieldsDocuments[0].documentType },
            ])

            clearError('documentType')
        }

        if (field === "documentNumber") {
            values[0].documentNumber = event.target.value
        }

        if (field === "documentDate") {
            values[0].documentDate = event.target.value
        }

        if (field === "documentFile") {
            values[0].documentFile = event.target.files[0]
        }

        if (field === "portOfDischarge") {
            values[0].portOfDischarge = event.value
            values[0].portOfDischargeLabel = event.label
        }

        if (field === "npwpNumber") {
            values[0].npwpNumber = event.target.value
        }

        if (field === "documentTypeOther") {
            values[0].documentTypeOther = event.target.value
        }

        setInputFieldsDocuments(values)
    }

    const onSubmit = e => {

        setOnSubmitDocument(true)

        //formData
        let formData = new FormData()

        formData.append('order_type', inputFieldsDocuments[0].orderType)
        formData.append('number', inputFieldsDocuments[0].documentNumber)
        formData.append('date', inputFieldsDocuments[0].documentDate)
        formData.append('file', inputFieldsDocuments[0].documentFile)

        if (inputFieldsDocuments[0].documentType === 'mbl') {
            formData.append('port_of_discharge', inputFieldsDocuments[0].portOfDischarge)
            formData.append('npwp_number', inputFieldsDocuments[0].npwpNumber)
        }

        if (inputFieldsDocuments[0].documentType === 'other') {
            formData.append('type', inputFieldsDocuments[0].documentTypeOther)
        } else {
            formData.append('type', inputFieldsDocuments[0].documentType)
        }

        formData.append('export_uuid', orderUuid[0])

        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        //CHECK NPWP
        setTimeout(() => {
            axios
                .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
                    tax_number: props.dataUserPortal.npwp,
                    email: props.dataUserPortal.email,
                    // tax_number: '541267799887999',
                    // email: 'kusnle5@yopmail.com'
                }, {
                    headers: headers
                })
                .then(responseNPWP => {
                    if (responseNPWP.data.success === true) {
                        axios
                            .post(ApiClickargo.CLICKARGO_UPLOAD_DOCUMENT, formData, {
                                headers: {
                                    'Key': responseNPWP.data.data.key,
                                    'Secret': responseNPWP.data.data.secret,
                                }
                            })
                            .then(responseDocument => {
                                setOnSubmitDocument(false)
                                setModal(!modal)
                                addToast(responseDocument.data.message, {
                                    appearance: 'success',
                                    autoDismiss: true,
                                })

                                axios
                                    .post(ApiClickargo.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
                                        job_number: localStorage.getItem('ckSmJobNumber')
                                    })
                                    .then(responseOrder => {
                                        const orderDetails = responseOrder.data.data

                                        setDocumentList([orderDetails.documents])
                                        setOrderUuid([orderDetails.export_uuid])
                                    })
                                    .catch(error => {
                                        addToast(error.response.data.message, {
                                            appearance: 'error',
                                            autoDismiss: true,
                                        })
                                    })
                            })
                            .catch(error => {
                                setOnSubmitDocument(false)
                                addToast(error.response.data.message, {
                                    appearance: 'error',
                                    autoDismiss: true,
                                })
                            })
                    }
                })
                .catch(error => {
                    Swal.fire({
                        title: 'Sorry',
                        text: "You need to register first before using the platform. Just a simple step! Lets go :)",
                        icon: 'error',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes!'
                    }).then((result) => {
                        if (result.value) {
                            //set service name
                            localStorage.setItem('ckServiceSelected', 'Vessel My Transaction')

                            props.history.push("/clickargo_register")
                        }
                    })
                })
        }, 1000);
    }

    let optionDocumentType = [
        { value: '', label: 'Please select document type' },
        { value: 'invoices', label: 'Invoices' },
        { value: 'packing lists', label: 'Packing Lists' },
        { value: 'msds', label: 'MSDS' },
        { value: 'surveyor', label: 'Laporan Surveyor' },
        { value: 'mbl', label: 'BL / MBL' },
        { value: 'coo', label: 'COO' },
        { value: 'do', label: 'DO' },
        { value: 'other', label: 'Other' },
    ]

    //booking office
    const fetchLocation = (inputValue, callback) => {
        if (!inputValue) {
            callback([]);
        } else {
            setTimeout(() => {
                axios
                    .post(ApiClickargo.CLICKARGO_UNLOCS, {
                        data: inputValue
                    })
                    .then((data) => {
                        const tempArray = [];
                        data.data.data.forEach((element) => {
                            tempArray.push({ label: `${element.name}` + ' (' + `${element.code}` + ')', value: element.code });
                        });
                        callback(tempArray);
                    })
                    .catch((error) => {
                        console.log(error, "catch the hoop")
                    });
            });
        }
    }

    return (
        isLoading ? (
            <Fragment>
                <Spinner animation="grow" variant="primary" size="md" className={dashforgeCss.alignSelfCenter} />
            </Fragment>
        ) : (
                <Fragment>
                    <div className={dashforgeCss.marginBottom30 + ` ` + dashforgeCss.textRight}>
                        <button className="btn btn-primary" onClick={toggle}>Add Document</button>
                    </div>

                    <table className={`table ` + dashforgeCss.tableDocument}>
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Time</th>
                                <th>Date</th>
                                <th>Number</th>
                                <th>Uploader</th>
                                <th>Description</th>
                                <th className={dashforgeCss.textCenter}>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                            {documentList[0].map((inputDocument, indexDocument) => (
                                <Fragment key={indexDocument}>
                                    <tr>
                                        <td className={dashforgeCss.textCenter}>{inputDocument.admin_uploader === null ? 'User' : 'System Administrator'}</td>
                                        <td className={dashforgeCss.textCenter}>{moment(inputDocument.created_at).format('H:mm a')}</td>
                                        <td className={dashforgeCss.textCenter}>{moment(inputDocument.created_at).format('DD MMMM YYYY')}</td>
                                        <td className={dashforgeCss.textCenter}>{inputDocument.number}</td>
                                        <td className={dashforgeCss.textCenter}>{inputDocument.uploader}</td>
                                        <td className={dashforgeCss.textCenter}>{inputDocument.type}</td>
                                        <td className={dashforgeCss.textCenter}><a target="_blank" href={`http://storage.api.quarkspark.com/` + inputDocument.path}><i className="fas fa-download" style={{ color: '#1053b3' }}></i></a></td>
                                    </tr>
                                </Fragment>
                            ))}
                        </tbody>
                    </table>

                    {/* <br />
                    <pre>
                        {JSON.stringify(documentList, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(inputFieldsDocuments, null, 2)}
                    </pre> */}
                    {/* <br />
                    <pre>
                        {JSON.stringify(getValues(), null, 2)}
                    </pre> */}


                    <Modal isOpen={modal} toggle={toggle} backdrop="static">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <ModalHeader toggle={toggle}>Add Document</ModalHeader>
                            <ModalBody>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="form-group">
                                            <label className={dashforgeCss.txBlack}>Select document type<span className={dashforgeCss.txDanger}>*</span></label>
                                            <input
                                                type="hidden"
                                                name="documentType"
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} />
                                            <Select
                                                options={optionDocumentType}
                                                value={{ label: inputFieldsDocuments[0].documentTypeLabel, value: inputFieldsDocuments[0].documentType }}
                                                onChange={event => handleInputChangeDocuments('documentType', event)}
                                            />
                                            {errors.documentType && <span style={{ fontSize: 13, color: "red" }}>{errors.documentType.message}</span>}
                                        </div>
                                        {isOther === true ? (<Fragment>
                                            <div className="form-group">
                                                <label className={dashforgeCss.txBlack}>Document Description<span className={dashforgeCss.txDanger}>*</span></label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="documentTypeOther"
                                                    placeholder="Enter Document Description"
                                                    onChange={event => handleInputChangeDocuments('documentTypeOther', event)}
                                                    ref={register({
                                                        required: {
                                                            value: true,
                                                            message: 'This input field is required!'
                                                        }
                                                    })} />
                                                {errors.documentTypeOther && <span style={{ fontSize: 13, color: "red" }}>{errors.documentTypeOther.message}</span>}
                                            </div>
                                        </Fragment>) : <Fragment></Fragment>}
                                        <div className="form-group">
                                            <label className={dashforgeCss.txBlack}>Document Number<span className={dashforgeCss.txDanger}>*</span></label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="documentNumber"
                                                placeholder="Enter Document Number"
                                                onChange={event => handleInputChangeDocuments('documentNumber', event)}
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} />
                                            {errors.documentNumber && <span style={{ fontSize: 13, color: "red" }}>{errors.documentNumber.message}</span>}
                                        </div>
                                        <div className="form-group">
                                            <label className={dashforgeCss.txBlack}>Document Date<span className={dashforgeCss.txDanger}>*</span></label>
                                            <input
                                                type="date"
                                                className="form-control"
                                                name="documentDate"
                                                onChange={event => handleInputChangeDocuments('documentDate', event)}
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} />
                                            {errors.documentDate && <span style={{ fontSize: 13, color: "red" }}>{errors.documentDate.message}</span>}
                                        </div>
                                        <div className="form-group">
                                            <label className={dashforgeCss.txBlack}>File<span className={dashforgeCss.txDanger}>*</span></label>
                                            <input
                                                type="file"
                                                className="form-control"
                                                name="documentFile"
                                                onChange={event => handleInputChangeDocuments('documentFile', event)}
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} />
                                            {errors.documentFile && <span style={{ fontSize: 13, color: "red" }}>{errors.documentFile.message}</span>}
                                        </div>
                                        {isBL === true ? (
                                            <Fragment>
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Port of Discharge</label>
                                                    <AsyncSelect
                                                        value={{ label: inputFieldsDocuments[0].portOfDischargeLabel, value: inputFieldsDocuments[0].portOfDischarge }}
                                                        loadOptions={fetchLocation}
                                                        placeholder="Enter Location"
                                                        onChange={event => handleInputChangeDocuments('portOfDischarge', event)} />
                                                    {errors.portOfDischarge && <span style={{ fontSize: 13, color: "red" }}>{errors.portOfDischarge.message}</span>}
                                                </div>
                                            </Fragment>
                                        ) : <Fragment></Fragment>}
                                        {isBL === true ? (
                                            <Fragment>
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>NPWP Number</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="npwpNumber"
                                                        placeholder="Enter NPWP Number"
                                                        onChange={event => handleInputChangeDocuments('npwpNumber', event)} />
                                                    {errors.npwpNumber && <span style={{ fontSize: 13, color: "red" }}>{errors.npwpNumber.message}</span>}
                                                </div>
                                            </Fragment>
                                        ) : <Fragment></Fragment>}
                                    </div>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={toggle}>Close</Button>{' '}
                                <Button color="primary" type="submit">
                                    {onSubmitDocument && <i className="fa fa-circle-notch fa-spin"></i>}
                                    {onSubmitDocument && <span>Please wait...</span>}
                                    {!onSubmitDocument && <span>Add File</span>}</Button>
                            </ModalFooter>
                        </form>
                    </Modal>

                </Fragment>
            )
    )
}

export default Documents
