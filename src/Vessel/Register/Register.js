import React, { useState, Fragment, useEffect } from 'react'

import Select from 'react-select'
import { useForm } from 'react-hook-form'
import axios from 'axios'
import Swal from 'sweetalert2'
import { useToasts } from 'react-toast-notifications'
import dashforgeCss from '../BookingRequest/dashforge.module.css'
import moment from 'moment'
import * as ApiClickargo from '../../Clickargo/ApiList.js'

function Register(props) {

    const { register, handleSubmit, errors, watch, clearError, setValue, getValues } = useForm({
        // by setting validateCriteriaMode to 'all',
        // all validation errors for single field will display at once
        validateCriteriaMode: "all",
        mode: "onChange"
    });

    const [isLoading, setIsLoading] = useState(false);

    const { addToast } = useToasts()

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-primary',
            cancelButton: 'btn btn-info'
        },
        buttonsStyling: false
    })

    const [inputFields, setInputFields] = useState([
        {
            //personal information
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: '',
            verifyPassword: '',
            //company information
            companyName: '',
            companyNIB: '',
            companyTaxNumber: props.dataUserPortal.npwp,
            companyType: '',
            companyTypeLabel: 'Select company type',
            companyCountry: 'Indonesia',
            companyCountryLabel: 'Indonesia',
            companyState: '',
            companyStateLabel: 'Choose One',
            companyCity: '',
            companyCityLabel: 'Choose One',
            companyDistrict: '',
            companyDistrictLabel: 'Choose One',
            companySubDistrict: '',
            companySubDistrictLabel: 'Choose One',
            companyPostalCode: '',
            companyAddress: '',
            companyNPWP: '',
            companyNPWPFiles: null,
            //inttra information
            inttraID: '',
            //personal contact details
            pcdName: '',
            pcdEmail: '',
            pcdPhone: '',
            //terms of service
            termsOfService: false,
            nleTermOfService: false
        }
    ]);

    //if data in user profile oss in nle api exist then set state
    useEffect(() => {
        //check if data in user profile oss in nle api exist then set state

        axios
            .get(ApiClickargo.NLE_USER_PROFILE_OSS + '/' + inputFields[0].companyTaxNumber,
                {
                    headers: {
                        'beacukai-api-key': ApiClickargo.NLE_USER_PROFILE_OSS_API_KEY
                    }
                })
            .then(response => {
                if (response.data['data Customer Oss'].length > 0) {

                    let dataOss = response.data['data Customer Oss'][0]

                    setTimeout(() => {
                        //set value
                        const values = [...inputFields]
                        //personal information
                        values[0].firstName = dataOss.NAME_PERSON.replace(/ .*/, '')
                        values[0].lastName = dataOss.NAME_PERSON.substr(dataOss.NAME_PERSON.indexOf(" ") + 1)
                        values[0].username = dataOss.NAME_PERSON.replace(/\s/g, '').toLowerCase()
                        values[0].email = props.dataUserPortal.email

                        //company information
                        values[0].companyName = dataOss.COMPANY_NAME
                        values[0].companyNIB = dataOss.NIB

                        values[0].companyState = dataOss.PROVINCE
                        values[0].companyStateLabel = dataOss.PROVINCE

                        values[0].companyCity = dataOss.CITY
                        values[0].companyCityLabel = dataOss.CITY

                        values[0].companyDistrict = dataOss.DISTRICT
                        values[0].companyDistrictLabel = dataOss.DISTRICT

                        values[0].companySubDistrict = dataOss.SUB_DISTRICT
                        values[0].companySubDistrictLabel = dataOss.SUB_DISTRICT

                        values[0].companyPostalCode = dataOss.POSTAL_CODE

                        values[0].companyAddress = dataOss.ADDRESS

                        //personal contact detail
                        values[0].pcdName = dataOss.NAME_PERSON
                        values[0].pcdEmail = props.dataUserPortal.email
                        values[0].pcdPhone = dataOss.PHONE
                        setInputFields(values)
                    }, 1000);

                } else {
                    setTimeout(() => {
                        axios
                            .post(ApiClickargo.CLICKARGO_STATE_LIST, {
                                country_name: inputFields[0].companyCountry
                            })
                            .then(response => {
                                setValue([
                                    { companyCountryHidden: inputFields[0].companyCountry },
                                    { companyStateHidden: inputFields[0].companyState },
                                    { companyCityHidden: inputFields[0].companyCity },
                                    { companyDistrictHidden: inputFields[0].companyDistrict },
                                    { companySubDistrictHidden: inputFields[0].companySubDistrict },
                                ])
                                if (response.data.data.length > 0) {
                                    setArrStateList(response.data.data)
                                    setIsStateEmpty(false)
                                    setIsCityEmpty(false)
                                    setIsDistrictEmpty(false)
                                    setIsSubDistrictEmpty(false)
                                } else {
                                    setArrStateList([])
                                    setIsStateEmpty(true)
                                    setIsCityEmpty(true)
                                    setIsDistrictEmpty(true)
                                    setIsSubDistrictEmpty(true)
                                }
                            })
                            .catch(error => {
                                if (error.response) {
                                    Swal.fire(
                                        'Oops...!',
                                        'Failed to get state data',
                                        'error'
                                    )
                                    console.log(error.response.data.message)
                                }
                            })
                    }, 1000);
                    //get nle user profile
                    axios
                        .get(ApiClickargo.NLE_USER_PROFILE + '/' + inputFields[0].companyTaxNumber,
                            {
                                headers: {
                                    'beacukai-api-key': ApiClickargo.NLE_USER_PROFILE_API_KEY
                                }
                            })
                        .then(response => {
                            if (response.data['data Customer'].length > 0) {

                                let dataProfile = response.data['data Customer'][0]

                                setTimeout(() => {
                                    //set value
                                    const values = [...inputFields]
                                    //personal information
                                    values[0].firstName = dataProfile.NM_KONTAK_PERSON.replace(/ .*/, '')
                                    values[0].lastName = dataProfile.NM_KONTAK_PERSON.substr(dataProfile.NM_KONTAK_PERSON.indexOf(" ") + 1)
                                    values[0].username = dataProfile.NM_KONTAK_PERSON.replace(/\s/g, '').toLowerCase()
                                    values[0].email = props.dataUserPortal.email

                                    //company information
                                    values[0].companyAddress = dataProfile.ALAMAT

                                    //personal contact detail
                                    values[0].pcdName = dataProfile.NM_KONTAK_PERSON
                                    values[0].pcdEmail = props.dataUserPortal.email
                                    values[0].pcdPhone = dataProfile.TELP
                                    setInputFields(values)
                                }, 1000);
                            } else {
                                addToast('Failed to get User Profile, please complete the blank form.', {
                                    appearance: 'error',
                                    autoDismiss: true,
                                })
                            }
                        })

                        .catch(error => {
                            console.log(error)
                            addToast('Something wrong. Please Check the server', {
                                appearance: 'error',
                                autoDismiss: true,
                            })
                        })
                }
            })
            .catch(error => {
                console.log(error)
                addToast('Something wrong. Please Check the server', {
                    appearance: 'error',
                    autoDismiss: true,
                })
            })
    }, [])

    const [arrCountryList, setArrCountryList] = useState([])

    const [isStateEmpty, setIsStateEmpty] = useState(false)

    const [isCityEmpty, setIsCityEmpty] = useState(false)

    const [isDistrictEmpty, setIsDistrictEmpty] = useState(false)

    const [isSubDistrictEmpty, setIsSubDistrictEmpty] = useState(false)

    const [arrStateList, setArrStateList] = useState([]);

    const [arrCityList, setArrCityList] = useState([]);

    const [arrDistrictList, setArrDistrictList] = useState([]);

    const [arrSubDistrictList, setArrSubDistrictList] = useState([]);

    const handleInputChange = (field, event) => {
        const values = [...inputFields];

        if (field === "firstName") {
            values[0].firstName = event.target.value
        }

        if (field === "lastName") {
            values[0].lastName = event.target.value
        }

        if (field === "email") {
            values[0].email = event.target.value
        }

        if (field === "password") {
            values[0].password = event.target.value
        }

        if (field === "username") {
            values[0].username = event.target.value
        }

        if (field === "verifyPassword") {
            values[0].verifyPassword = event.target.value
        }

        if (field === "companyName") {
            values[0].companyName = event.target.value
        }

        if (field === "companyNIB") {
            values[0].companyNIB = event.target.value
        }

        if (field === "companyTaxNumber") {
            values[0].companyTaxNumber = event.target.value
        }

        if (field === "companyType") {
            values[0].companyType = event.value
            values[0].companyTypeLabel = event.label

            setValue([
                { companyType: inputFields[0].companyType }
            ])

            clearError('companyType')
        }

        if (field === "companyCountry") {
            values[0].companyCountry = event.value
            values[0].companyCountryLabel = event.label

            //clear Error from country until sub district  if any
            clearError('companyCountryHidden')
            clearError('companyStateHidden')
            clearError('companyCityHidden')
            clearError('companyDistrictHidden')
            clearError('companySubDistrictHidden')

            //search state by country name
            axios
                .post(ApiClickargo.CLICKARGO_STATE_LIST, {
                    country_name: values[0].companyCountry
                })
                .then(response => {
                    //clear input from state until sub district
                    values[0].companyState = ''
                    values[0].companyStateLabel = 'Choose One'
                    values[0].companyCity = ''
                    values[0].companyCityLabel = 'Choose One'
                    values[0].companyDistrict = ''
                    values[0].companyDistrictLabel = 'Choose One'
                    values[0].companySubDistrict = ''
                    values[0].companySubDistrictLabel = 'Choose One'

                    setValue([
                        { companyCountryHidden: inputFields[0].companyCountry },
                        { companyStateHidden: inputFields[0].companyState },
                        { companyCityHidden: inputFields[0].companyCity },
                        { companyDistrictHidden: inputFields[0].companyDistrict },
                        { companySubDistrictHidden: inputFields[0].companySubDistrict },
                    ])

                    if (response.data.data.length > 0) {
                        setArrStateList(response.data.data)
                        setIsStateEmpty(false)
                        setIsCityEmpty(false)
                        setIsDistrictEmpty(false)
                        setIsSubDistrictEmpty(false)
                    } else {
                        setArrStateList([])
                        setIsStateEmpty(true)
                        setIsCityEmpty(true)
                        setIsDistrictEmpty(true)
                        setIsSubDistrictEmpty(true)
                    }
                })
                .catch(error => {
                    if (error.response) {
                        Swal.fire(
                            'Oops...!',
                            'Failed to get state data',
                            'error'
                        )
                        console.log(error.response.data.message)
                    }
                })
        }

        if (field === "companyStateText") {
            values[0].companyState = event.target.value
            values[0].companyStateLabel = event.target.value
        }

        if (field === "companyStateSelect") {
            values[0].companyState = event.value
            values[0].companyStateLabel = event.label

            setValue([
                { companyStateHidden: inputFields[0].companyState },
                { companyCityHidden: inputFields[0].companyCity },
                { companyDistrictHidden: inputFields[0].companyDistrict },
                { companySubDistrictHidden: inputFields[0].companySubDistrict },
            ])

            //clear Error from state until sub district  if any
            clearError('companyStateHidden')
            clearError('companyCityHidden')
            clearError('companyDistrictHidden')
            clearError('companySubDistrictHidden')

            //search city by state name
            axios
                .post(ApiClickargo.CLICKARGO_INDONESIA_CITY, {
                    state_name: values[0].companyState
                })
                .then(response => {
                    //clear input from state until sub district
                    values[0].companyCity = ''
                    values[0].companyCityLabel = 'Choose One'
                    values[0].companyDistrict = ''
                    values[0].companyDistrictLabel = 'Choose One'
                    values[0].companySubDistrict = ''
                    values[0].companySubDistrictLabel = 'Choose One'

                    if (response.data.data.length > 0) {
                        setArrCityList(response.data.data)
                        setIsStateEmpty(false)
                        setIsCityEmpty(false)
                        setIsDistrictEmpty(false)
                        setIsSubDistrictEmpty(false)
                    }
                })
                .catch(error => {
                    if (error.response) {
                        setArrCityList([])
                        setIsCityEmpty(true)
                        setIsDistrictEmpty(true)
                        setIsSubDistrictEmpty(true)
                        console.log(error.response.data.message)
                    }
                })
        }

        if (field === "companyCityText") {
            values[0].companyCity = event.target.value
            values[0].companyCityLabel = event.target.value
        }

        if (field === "companyCitySelect") {
            values[0].companyCity = event.value
            values[0].companyCityLabel = event.label

            setValue([
                { companyCityHidden: inputFields[0].companyCity },
                { companyDistrictHidden: inputFields[0].companyDistrict },
                { companySubDistrictHidden: inputFields[0].companySubDistrict },
            ])

            //clear Error from city until sub district if any
            clearError('companyCityHidden')
            clearError('companyDistrictHidden')
            clearError('companySubDistrictHidden')

            //search district by city name
            axios
                .post(ApiClickargo.CLICKARGO_INDONESIA_DISTRICT, {
                    city_name: values[0].companyCity
                })
                .then(response => {
                    //clear input from city until sub district
                    values[0].companyDistrict = ''
                    values[0].companyDistrictLabel = 'Choose One'
                    values[0].companySubDistrict = ''
                    values[0].companySubDistrictLabel = 'Choose One'

                    if (response.data.data.length > 0) {
                        setArrDistrictList(response.data.data)
                        setIsCityEmpty(false)
                        setIsDistrictEmpty(false)
                        setIsSubDistrictEmpty(false)
                    }
                })
                .catch(error => {
                    if (error.response) {
                        setArrDistrictList([])
                        setIsDistrictEmpty(true)
                        setIsSubDistrictEmpty(true)
                        console.log(error.response.data.message)
                    }
                })
        }

        if (field === "companyDistrictText") {
            values[0].companyDistrict = event.target.value
            values[0].companyDistrictLabel = event.target.value
        }

        if (field === "companyDistrictSelect") {
            values[0].companyDistrict = event.value
            values[0].companyDistrictLabel = event.label

            setValue([
                { companyDistrictHidden: inputFields[0].companyDistrict }
            ])

            //clear Error from district until sub district if any
            clearError('companyDistrictHidden')
            clearError('companySubDistrictHidden')

            //search sub district by district name
            axios
                .post(ApiClickargo.CLICKARGO_INDONESIA_SUB_DISTRICT, {
                    district_name: values[0].companyDistrict
                })
                .then(response => {
                    //clear input sub district
                    values[0].companySubDistrict = ''
                    values[0].companySubDistrictLabel = 'Choose One'

                    if (response.data.data.length > 0) {
                        setArrSubDistrictList(response.data.data)
                        setIsDistrictEmpty(false)
                        setIsSubDistrictEmpty(false)
                    }
                })
                .catch(error => {
                    if (error.response) {
                        setArrSubDistrictList([])
                        setIsSubDistrictEmpty(true)
                        console.log(error.response.data.message)
                    }
                })
        }

        if (field === "companySubDistrictText") {
            values[0].companySubDistrict = event.target.value
            values[0].companySubDistrictLabel = event.target.value
        }

        if (field === "companySubDistrictSelect") {
            values[0].companySubDistrict = event.value
            values[0].companySubDistrictLabel = event.label

            setValue([
                { companySubDistrictHidden: inputFields[0].companySubDistrict }
            ])

            //clear Error sub district if any
            clearError('companySubDistrictHidden')
        }

        if (field === "companyPostalCode") {
            values[0].companyPostalCode = event.target.value
        }

        if (field === "companyAddress") {
            values[0].companyAddress = event.target.value
        }

        if (field === "companyNPWP") {
            values[0].companyNPWP = event.target.value
            values[0].companyNPWPFiles = event.target.files[0]
        }

        if (field === "inttraID") {
            values[0].inttraID = event.target.value
        }

        if (field === "pcdName") {
            values[0].pcdName = event.target.value
        }

        if (field === "pcdEmail") {
            values[0].pcdEmail = event.target.value
        }

        if (field === "pcdPhone") {
            values[0].pcdPhone = event.target.value
        }

        setInputFields(values);
    }

    useEffect(() => {
        //country list
        axios
            .get(ApiClickargo.CLICKARGO_COUNTRY_LIST)
            .then(response => {
                setArrCountryList(response.data.data)
                setArrStateList([])
                setIsStateEmpty(true)
                setIsCityEmpty(true)
                setIsDistrictEmpty(true)
                setIsSubDistrictEmpty(true)
            })
            .catch(error => {
                if (error.response) {
                    Swal.fire(
                        'Oops...!',
                        'Failed to get country data',
                        'error'
                    )
                    console.log(error.response.data.message)
                }
            })
        setValue([
            { companyCountryHidden: inputFields[0].companyCountry }
        ])
        // setTimeout(() => {
        //     axios
        //         .post('http://elogistic.ms.api.demo-qs.xyz/api/v1/helper/state-list', {
        //             country_name: inputFields[0].companyCountry
        //         })
        //         .then(response => {
        //             setValue([
        //                 { companyCountryHidden: inputFields[0].companyCountry },
        //                 { companyStateHidden: inputFields[0].companyState },
        //                 { companyCityHidden: inputFields[0].companyCity },
        //                 { companyDistrictHidden: inputFields[0].companyDistrict },
        //                 { companySubDistrictHidden: inputFields[0].companySubDistrict },
        //             ])
        //             if (response.data.data.length > 0) {
        //                 setArrStateList(response.data.data)
        //                 setIsStateEmpty(false)
        //                 setIsCityEmpty(false)
        //                 setIsDistrictEmpty(false)
        //                 setIsSubDistrictEmpty(false)
        //             } else {
        //                 setArrStateList([])
        //                 setIsStateEmpty(true)
        //                 setIsCityEmpty(true)
        //                 setIsDistrictEmpty(true)
        //                 setIsSubDistrictEmpty(true)
        //             }
        //         })
        //         .catch(error => {
        //             if (error.response) {
        //                 Swal.fire(
        //                     'Oops...!',
        //                     'Failed to get state data',
        //                     'error'
        //                 )
        //                 console.log(error.response.data.message)
        //             }
        //         })
        // }, 1000);
    }, [])

    const [inputFieldsGeneral, setInputFieldsGeneral] = useState([
        {
            shipmentType: '',
            shipmentTypeLabel: 'Please Select Shipment Type',
            productType: '',
            productTypeLabel: 'Please Select Product Type',
            //transport
            transportMoveType: '',
            transportMoveTypeLabel: '',
            transportPOCReceipt: '',
            transportPOCDelivery: '',
            transportEarliestDepartureDate: '',
            transportLatestDeliveryDate: '',
            //pre carriage
            preCarriageStart: '',
            preCarriageStartLabel: 'Enter Location',
            preCarriageMode: '',
            preCarriageModeLabel: 'Please choose transport mode',
            preCarriageETD: '',
            preCarriageETA: '',
            //main carriage
            mainCarriagePortOfLoading: '',
            mainCarriagePortOfLoadingName: '',
            mainCarriagePortOfLoadingCountry: '',
            mainCarriagePortOfDischarge: '',
            mainCarriagePortOfDischargeName: '',
            mainCarriagePortOfDischargeCountry: '',
            mainCarriagePortOfDischarge: '',
            mainCarriageETD: '',
            mainCarriageETA: '',
            mainCarriageVessel: '',
            mainCarriageVoyage: '',
            //on carriage
            onCarriageStart: '',
            onCarriageStartLabel: 'Enter Location',
            onCarriageMode: '',
            onCarriageModeLabel: 'Please choose transport mode',
            onCarriageETD: '',
            onCarriageETA: '',
            //general details
            generalDetailsCarrier: '',
            generalDetailsCarrierLabel: '',
            generalDetailsCarrierReceipt: '',
            generalDetailsCarrierDelivery: '',
            generalDetailsEarliestDeparture: '',
            generalDetailsLatestDelivery: '',
            generalDetailsBookingOffice: '',
            generalDetailsBookingOfficeLabel: '',
            generalDetailsContractNumber: '',
            //parties
            partiesShipperName: '',
            partiesShipperEmail: '',
            partiesShipperTaxNumber: '',
            partiesShipperAddress1: '', //update parties //22-05-2010
            partiesShipperAddress2: '', //update parties //22-05-2010
            partiesShipperCountryCode: '',
            partiesShipperCountryName: 'Please fill country name (Ex: Indonesia)',
            partiesShipperCity: '',
            partiesShipperPostalCode: '',
            partiesForwarder: 'PT GATOTKACA TRANS SYSTEMINDO',
            partiesConsignee: '',
            partiesConsigneeAddress1: '', //update parties //22-05-2010
            partiesConsigneeAddress2: '', //update parties //22-05-2010
            partiesConsigneeCountryCode: '',
            partiesConsigneeCountryName: 'Please fill country name (Ex: Indonesia)',
            partiesConsigneeCity: '',
            partiesConsigneePostalCode: '',
            partiesShipperInttraId: '',
            partiesForwarderInttraId: '',
            partiesForwarderAddress: 'JL ENGGANO NO. 40C TANJUNG PRIOK, JAKARTA UTARA, DKI JAKARTA, 14310, INDONESIA', //update parties
            //additional party
            //contact party
            contractPartyName: '', //update additional party //22-05-2010
            contractPartyAddress1: '', //update additional party //22-05-2010
            contractPartyAddress2: '', //update additional party //22-05-2010
            contractPartyCountryCode: '', //update additional party //22-05-2010
            contractPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            contractPartyCity: '', //update additional party //22-05-2010
            contractPartyPostalCode: '', //update additional party //22-05-2010
            //notify party
            notifyPartyName: '', //update additional party //22-05-2010
            notifyPartyAddress1: '', //update additional party //22-05-2010
            notifyPartyAddress2: '', //update additional party //22-05-2010
            notifyPartyCountryCode: '', //update additional party //22-05-2010
            notifyPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            notifyPartyCity: '', //update additional party //22-05-2010
            notifyPartyPostalCode: '', //update additional party //22-05-2010
            //first additional notify party
            firstNotifyPartyName: '', //update additional party //22-05-2010
            firstNotifyPartyAddress1: '', //update additional party //22-05-2010
            firstNotifyPartyAddress2: '', //update additional party //22-05-2010
            firstNotifyPartyCountryCode: '', //update additional party //22-05-2010
            firstNotifyPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            firstNotifyPartyCity: '', //update additional party //22-05-2010
            firstNotifyPartyPostalCode: '', //update additional party //22-05-2010
            //second additional notify party
            secondNotifyPartyName: '', //update additional party //22-05-2010
            secondNotifyPartyAddress1: '', //update additional party //22-05-2010
            secondNotifyPartyAddress2: '', //update additional party //22-05-2010
            secondNotifyPartyCountryCode: '', //update additional party //22-05-2010
            secondNotifyPartyCountryName: 'Please fill country name (Ex: Indonesia)', //update additional party //22-05-2010
            secondNotifyPartyCity: '', //update additional party //22-05-2010
            secondNotifyPartyPostalCode: '', //update additional party //22-05-2010
            //references
            referencesShipperRefNumber: '',
            referencesForwarderRefNumber: '',
            referencesPurchaseOrderNumber: '',
            referencesConsigneeNumber: '', //update references
            referencesBLNumber: '', //update references
            referencesContractParty: '', //update references
            //comments and notifications
            customerComments: '',
            partnerEmailNotifications: '',
            //update new field schedule
            scheduleOriginUnloc: '',
            scheduleOriginPortName: '',
            scheduleOriginCountry: '',
            scheduleDestinationUnloc: '',
            scheduleDestinationPortName: '',
            scheduleDestinationCountry: '',
        }
    ]);

    const [inputFieldsContainer, setInputFieldsContainer] = useState([
        {
            //container
            containerType: '',
            containerTypeLabel: 'Please Select Container Type',
            containerComments: '',
            //container cargo
            cargo: [
                {
                    cargoDescription: '',
                    cargoHsCode: '',
                    cargoHsCodeLabel: 'Enter Number',
                    cargoWeight: '',
                    cargoWeightType: 'KGM',
                    cargoPackageCount: '',
                    cargoPackageType: '',
                    cargoPackageTypeLabel: 'Please select package type',
                    cargoGrossVolume: '',
                    cargoGrossVolumeType: 'MTQ',
                    cargoPrimaryImoClass: '',
                    cargoPrimaryImoClassLabel: 'Please select IMO Class',
                    cargoUndgNumber: '',
                    cargoPackingGroup: '',
                    cargoPackingGroupLabel: 'Please select Packing Group',
                    cargoProperShippingName: '',
                    cargoEmergencyContactName: '',
                    cargoEmergencyContactNumber: '',
                }
            ]
        }
    ]);

    const [inputFieldsPaymentDetails, setInputFieldsPaymentDetails] = useState([
        {
            paymentDetailsChangeType: '',
            paymentDetailsChangeTypeLabel: 'Select one',
            paymentDetailsFreightTerm: '',
            paymentDetailsFreightTermLabel: 'Select one',
            paymentDetailsPayer: '',
            paymentDetailsPayerLabel: 'Select one',
            paymentDetailsPaymentLocation: '',
            paymentDetailsPaymentLocationLabel: 'Enter Location',
        }
    ]);

    const onSubmit = e => {

        setIsLoading(true)

        setTimeout(() => {
            let formData = new FormData()

            //personal information
            formData.append('user[firstname]', inputFields[0].firstName)
            formData.append('user[lastname]', inputFields[0].lastName)
            formData.append('user[email]', inputFields[0].email)
            formData.append('company[username]', inputFields[0].username) //updated 08-06-2020
            // formData.append('user[password]', inputFields[0].password)
            formData.append('company[type]', inputFields[0].companyType) //updated 11-06-2020
            //company information
            formData.append('company[name]', inputFields[0].companyName)
            formData.append('company[reg_number]', inputFields[0].companyNIB)
            formData.append('company[tax_number]', inputFields[0].companyTaxNumber)
            formData.append('company[country]', inputFields[0].companyCountry)
            formData.append('company[state]', inputFields[0].companyState)
            formData.append('company[city]', inputFields[0].companyCity)
            formData.append('company[district]', inputFields[0].companyDistrict)
            formData.append('company[sub_district]', inputFields[0].companySubDistrict)
            formData.append('company[postal_code]', inputFields[0].companyPostalCode)
            formData.append('company[address]', inputFields[0].companyAddress)
            formData.append('company[company_inttra_id]', inputFields[0].inttraID)
            formData.append('company[contact_person]', inputFields[0].pcdName)
            formData.append('company[email]', inputFields[0].pcdEmail)
            formData.append('company[phone]', inputFields[0].pcdPhone)
            formData.append('company[is_special]', 1)
            // formData.append('tax_card', inputFields[0].companyNPWPFiles)

            const headers = {
                'Content-Type': 'multipart/form-data',
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }

            axios
                .post(ApiClickargo.CLICKARGO_REGISTER, formData, {
                    headers: headers
                })
                .then(responseRegister => {

                    axios
                        .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
                            tax_number: inputFields[0].companyTaxNumber,
                            email: inputFields[0].email,
                        }, {
                            headers: headers
                        })
                        .then(responseNPWP => {

                            //save header token
                            localStorage.setItem('ckHeaderKey', responseNPWP.data.data.key)
                            localStorage.setItem('ckHeaderSecret', responseNPWP.data.data.secret)
                            localStorage.setItem('ckHeaderToken', responseNPWP.data.data.token)

                            if (localStorage.getItem('ckServiceSelected') === 'Vessel') {

                                axios
                                    .post(ApiClickargo.CLICKARGO_FIND_SCHEDULE, {
                                        uuid: localStorage.getItem('scheduleUuid')
                                    })
                                    .then(responseSchedule => {

                                        const values = [...inputFieldsGeneral]
                                        //transport
                                        values[0].transportMoveType = localStorage.getItem('moveType')
                                        values[0].transportMoveTypeLabel = localStorage.getItem('moveTypeLabel')
                                        values[0].transportPOCReceipt = responseSchedule.data.data.origin_port_name + ' (' + responseSchedule.data.data.origin_unloc + ')'
                                        values[0].transportPOCDelivery = responseSchedule.data.data.destination_port_name + ' (' + responseSchedule.data.data.destination_unloc + ')'
                                        values[0].transportEarliestDepartureDate = moment(responseSchedule.data.data.origin_departure_date).format('DD/MM/YYYY')
                                        values[0].transportLatestDeliveryDate = moment(responseSchedule.data.data.destination_arrival_date).format('DD/MM/YYYY')

                                        //main carriage
                                        values[0].mainCarriagePortOfLoading = responseSchedule.data.data.origin_unloc
                                        values[0].mainCarriagePortOfLoadingName = responseSchedule.data.data.origin_port_name
                                        values[0].mainCarriagePortOfLoadingCountry = responseSchedule.data.data.origin_country
                                        values[0].mainCarriagePortOfDischarge = responseSchedule.data.data.destination_unloc
                                        values[0].mainCarriagePortOfDischargeName = responseSchedule.data.data.destination_port_name
                                        values[0].mainCarriagePortOfDischargeCountry = responseSchedule.data.data.destination_country
                                        values[0].mainCarriageVessel = responseSchedule.data.data.vessel_name
                                        values[0].mainCarriageVoyage = responseSchedule.data.data.voyage_name
                                        values[0].mainCarriageETD = responseSchedule.data.data.origin_departure_date
                                        values[0].mainCarriageETA = responseSchedule.data.data.destination_arrival_date

                                        //general details
                                        values[0].generalDetailsCarrier = responseSchedule.data.data.scac
                                        values[0].generalDetailsCarrierLabel = responseSchedule.data.data.carrier_name
                                        values[0].generalDetailsCarrierReceipt = responseSchedule.data.data.origin_unloc
                                        values[0].generalDetailsCarrierDelivery = responseSchedule.data.data.destination_unloc
                                        values[0].generalDetailsEarliestDeparture = responseSchedule.data.data.origin_departure_date
                                        values[0].generalDetailsLatestDelivery = responseSchedule.data.data.destination_arrival_date
                                        values[0].generalDetailsBookingOffice = ''
                                        values[0].generalDetailsBookingOfficeLabel = 'Enter Location'
                                        values[0].generalDetailsContractNumber = ''

                                        //parties
                                        values[0].partiesShipperName = responseRegister.data.data.name
                                        values[0].partiesShipperEmail = responseRegister.data.data.email
                                        values[0].partiesShipperInttraId = responseNPWP.data.data.inttra_id
                                        values[0].partiesForwarderInttraId = responseNPWP.data.data.inttra_id

                                        //schedule
                                        values[0].scheduleOriginUnloc = responseSchedule.data.data.origin_unloc
                                        values[0].scheduleOriginPortName = responseSchedule.data.data.origin_port_name
                                        values[0].scheduleOriginCountry = responseSchedule.data.data.origin_country
                                        values[0].scheduleDestinationUnloc = responseSchedule.data.data.destination_unloc
                                        values[0].scheduleDestinationPortName = responseSchedule.data.data.destination_port_name
                                        values[0].scheduleDestinationCountry = responseSchedule.data.data.destination_country

                                        setInputFieldsGeneral(values)

                                        props.history.push({
                                            pathname: '/booking_request_back',
                                            state: {
                                                generalArr: inputFieldsGeneral,
                                                containerArr: inputFieldsContainer,
                                                paymentDetailsArr: inputFieldsPaymentDetails
                                            }
                                        })

                                    })
                                    .catch(error => {
                                        if (error.response) {
                                            console.log(error.response.data.message)
                                        }
                                    })

                            } else if (localStorage.getItem('ckServiceSelected') === 'DO') {
                                props.history.push({
                                    pathname: '/clickargo_do/form',
                                })
                            } else if (localStorage.getItem('ckServiceSelected') === 'SP2') {
                                //document transaction type
                                const headerSP2 = {
                                    'Secret': responseNPWP.data.data.secret,
                                    'Key': responseNPWP.data.data.key,
                                    'Content-Type': 'application/json',
                                    'Accept-Language': 'application/json'
                                }
                                axios
                                    .post(ApiClickargo.CLICKARGO_SP2_DOCS_TRANSACTION_TYPE, {
                                        category_id: 'I',
                                        terminal_id: 'KOJA'
                                    }, {
                                        headers: headerSP2
                                    })
                                    .then(reponseSP2Docs => {
                                        //save cksp2 documents transaction type
                                        localStorage.setItem('ckSP2Docs', JSON.stringify(reponseSP2Docs.data.data, null, 2))
                                        this.props.history.push('/clickargo_sp2/form')
                                    })
                                    .catch(error => {
                                        if (error.response) {
                                            if (error.response.data.success === false) {
                                                addToast(error.response.data.message, {
                                                    appearance: 'error',
                                                    autoDismiss: false,
                                                })
                                            }
                                        }
                                    })
                            } else if (localStorage.getItem('ckServiceSelected') === 'DO MyTransaction') {
                                props.history.push({
                                    pathname: '/clickargo_do/my_transaction',
                                })
                            } else if (localStorage.getItem('ckServiceSelected') === 'Vessel My Transaction') {
                                props.history.push({
                                    pathname: '/shipment_management',
                                })
                            }

                        })
                        .catch(error => {
                            setIsLoading(false)
                            addToast(error.response.data.message, {
                                appearance: 'error',
                                autoDismiss: false,
                            })
                        })

                })
                .catch(error => {
                    setIsLoading(false)
                    addToast(error.response.data.message, {
                        appearance: 'error',
                        autoDismiss: false,
                    })
                })
        }, 2000);


    };

    let optionCountryList = arrCountryList.map(obj => {
        return { value: obj.name, label: obj.name }
    })

    let optionStateList = arrStateList.map(obj => {
        return { value: obj.name, label: obj.name }
    })

    let optionCityList = arrCityList.map(obj => {
        return { value: obj.name, label: obj.name }
    })

    let optionDistrictList = arrDistrictList.map(obj => {
        return { value: obj.name, label: obj.name }
    })

    let optionSubDistrictList = arrSubDistrictList.map(obj => {
        return { value: obj.name, label: obj.name }
    })

    let optionCompanyType = [
        { value: '', label: 'Select company Type' },
        { value: 'shipper', label: 'Shipper' },
        { value: 'cargo owner', label: 'Cargo Owner' },
        { value: 'truck company', label: 'Truck Company' },
        { value: 'freight forwarder', label: 'Freight Forwarder' }
    ]

    var href = 'http://clickargo.com/';

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div style={{ marginBottom: "10px" }}>
                    <div style={{ float: "left" }}>
                        <h3 style={{ display: "inline" }} className={dashforgeCss.txBlack}>Registration Form Clickargo</h3>
                    </div>
                    <div style={{ float: "right" }}>
                        <img style={{ width: '200px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                    </div>
                </div>

                <br />
                <br />
                <br />
                <br />

                <div className="kt-portlet kt-portlet--height-fluid">
                    <div className="kt-portlet__body">
                        <div className="kt-widget15">
                            <div className="row">

                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>PERSONAL INFORMATION</legend>

                                        <div className="row">

                                            <div className="col-md-4">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>First Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Ex: John"
                                                        type="text"
                                                        onChange={event => handleInputChange('firstName', event)}
                                                        name="firstName"
                                                        value={inputFields[0].firstName}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.firstName && <span style={{ fontSize: 13, color: "red" }}>{errors.firstName.message}</span>}
                                                </div>
                                            </div>

                                            <div className="col-md-4">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Last Name<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Ex: Doe"
                                                        type="text"
                                                        onChange={event => handleInputChange('lastName', event)}
                                                        name="lastName"
                                                        value={inputFields[0].lastName}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.lastName && <span style={{ fontSize: 13, color: "red" }}>{errors.lastName.message}</span>}
                                                </div>
                                            </div>

                                            <div className="col-md-4">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Email<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Ex: johndoe@gmail.com"
                                                        type="text"
                                                        onChange={event => handleInputChange('email', event)}
                                                        name="email"
                                                        value={inputFields[0].email}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            },
                                                            pattern: {
                                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                                message: "Invalid email address"
                                                            }
                                                        })}
                                                        disabled />
                                                    {errors.email && <span style={{ fontSize: 13, color: "red" }}>{errors.email.message}</span>}
                                                </div>

                                            </div>

                                        </div>

                                    </fieldset>
                                </div>

                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>COMPANY INFORMATION</legend>

                                        <div className="row">
                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Name: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter your company name"
                                                        type="text"
                                                        onChange={event => handleInputChange('companyName', event)}
                                                        name="companyName"
                                                        value={inputFields[0].companyName}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.companyName && <span style={{ fontSize: 13, color: "red" }}>{errors.companyName.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Company Username<span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Ex: johndoe99"
                                                        type="text"
                                                        onChange={event => handleInputChange('username', event)}
                                                        name="username"
                                                        value={inputFields[0].username}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.username && <span style={{ fontSize: 13, color: "red" }}>{errors.username.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Registration Number (NIB): <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter your company registration number"
                                                        type="text"
                                                        onChange={event => handleInputChange('companyNIB', event)}
                                                        name="companyNIB"
                                                        value={inputFields[0].companyNIB}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.companyNIB && <span style={{ fontSize: 13, color: "red" }}>{errors.companyNIB.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Tax Number: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter your company tax number"
                                                        type="text"
                                                        onChange={event => handleInputChange('companyTaxNumber', event)}
                                                        name="companyTaxNumber"
                                                        value={inputFields[0].companyTaxNumber}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })}
                                                        disabled />
                                                    {errors.companyTaxNumber && <span style={{ fontSize: 13, color: "red" }}>{errors.companyTaxNumber.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Company Type: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="hidden"
                                                        name="companyType"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    <Select
                                                        options={optionCompanyType}
                                                        value={{ label: inputFields[0].companyTypeLabel, value: inputFields[0].companyType }}
                                                        onChange={event => handleInputChange('companyType', event)} />
                                                    {errors.companyType && <span style={{ fontSize: 13, color: "red" }}>{errors.companyType.message}</span>}
                                                </div>
                                            </div>
                                        </div>

                                        <br />

                                        <div className="row">
                                            <div className="col-2">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Country: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        type="hidden"
                                                        name="companyCountryHidden"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    <Select
                                                        options={optionCountryList}
                                                        value={{ label: inputFields[0].companyCountryLabel, value: inputFields[0].companyCountry }}
                                                        onChange={event => handleInputChange('companyCountry', event)} />
                                                    {errors.companyCountryHidden && <span style={{ fontSize: 13, color: "red" }}>{errors.companyCountryHidden.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <div className="form-group">
                                                    {!isStateEmpty ? (<label className={dashforgeCss.txBlack}>State: <span className={dashforgeCss.txDanger}>*</span></label>) : (<label className={dashforgeCss.txBlack}>State:</label>)}
                                                    {!isStateEmpty ? <input
                                                        type="hidden"
                                                        name="companyStateHidden"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} /> : null}
                                                    {!isStateEmpty ?
                                                        (
                                                            <Select
                                                                options={optionStateList}
                                                                value={{ label: inputFields[0].companyStateLabel, value: inputFields[0].companyState }}
                                                                onChange={event => handleInputChange('companyStateSelect', event)} />
                                                        )
                                                        :
                                                        (<input
                                                            type="text"
                                                            className="form-control"
                                                            placeholder="Please enter state"
                                                            onChange={event => handleInputChange('companyStateText', event)}
                                                            value={inputFields[0].companyState} />
                                                        )}
                                                    {errors.companyStateHidden && <span style={{ fontSize: 13, color: "red" }}>{errors.companyStateHidden.message}</span>}

                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <div className="form-group">
                                                    {!isCityEmpty ? (<label className={dashforgeCss.txBlack}>City: <span className={dashforgeCss.txDanger}>*</span></label>) : (<label className={dashforgeCss.txBlack}>City:</label>)}
                                                    {!isCityEmpty ? <input
                                                        type="hidden"
                                                        name="companyCityHidden"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} /> : null}
                                                    {!isCityEmpty ?
                                                        (<Select
                                                            options={optionCityList}
                                                            value={{ label: inputFields[0].companyCityLabel, value: inputFields[0].companyCity }}
                                                            onChange={event => handleInputChange('companyCitySelect', event)} />)
                                                        :
                                                        (<input
                                                            type="text"
                                                            className="form-control"
                                                            placeholder="Please enter city"
                                                            onChange={event => handleInputChange('companyCityText', event)}
                                                            value={inputFields[0].companyCity} />)}
                                                    {errors.companyCityHidden && <span style={{ fontSize: 13, color: "red" }}>{errors.companyCityHidden.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <div className="form-group">
                                                    {/* {!isDistrictEmpty ? (<label>District: <span className="tx-danger">*</span></label>) : (<label>District:</label>)} */}
                                                    <label className={dashforgeCss.txBlack}>District:</label>
                                                    {/* {!isDistrictEmpty ? <input
                                                        type="hidden"
                                                        name="companyDistrictHidden"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} /> : null} */}
                                                    {!isDistrictEmpty ?
                                                        (<Select
                                                            options={optionDistrictList}
                                                            value={{ label: inputFields[0].companyDistrictLabel, value: inputFields[0].companyDistrict }}
                                                            onChange={event => handleInputChange('companyDistrictSelect', event)} />)
                                                        :
                                                        (<input
                                                            type="text"
                                                            className="form-control"
                                                            placeholder="Please enter district"
                                                            onChange={event => handleInputChange('companyDistrictText', event)}
                                                            value={inputFields[0].companyDistrict} />)}
                                                    {/* {errors.companyDistrictHidden && <span style={{ fontSize: 13, color: "red" }}>{errors.companyDistrictHidden.message}</span>} */}
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <div className="form-group">
                                                    {/* {!isSubDistrictEmpty ? (<label>Sub-district: <span className="tx-danger">*</span></label>) : (<label>Sub-district:</label>)} */}
                                                    <label className={dashforgeCss.txBlack}>Sub-district:</label>
                                                    {/* {!isDistrictEmpty ? <input
                                                        type="hidden"
                                                        name="companySubDistrictHidden"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} /> : null} */}
                                                    {!isSubDistrictEmpty ?
                                                        (<Select
                                                            options={optionSubDistrictList}
                                                            value={{ label: inputFields[0].companySubDistrictLabel, value: inputFields[0].companySubDistrict }}
                                                            onChange={event => handleInputChange('companySubDistrictSelect', event)} />)
                                                        :
                                                        (<input
                                                            type="text"
                                                            className="form-control"
                                                            placeholder="Please enter sub-district"
                                                            onChange={event => handleInputChange('companySubDistrictText', event)}
                                                            value={inputFields[0].companySubDistrict} />)}
                                                    {/* {errors.companySubDistrictHidden && <span style={{ fontSize: 13, color: "red" }}>{errors.companySubDistrictHidden.message}</span>} */}
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Postal Code: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter postal code"
                                                        type="text"
                                                        value={inputFields[0].companyPostalCode}
                                                        onChange={event => handleInputChange('companyPostalCode', event)}
                                                        name="companyPostalCode"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.companyPostalCode && <span style={{ fontSize: 13, color: "red" }}>{errors.companyPostalCode.message}</span>}
                                                </div>
                                            </div>
                                        </div>

                                        <br />

                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Address: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <textarea
                                                        className="form-control"
                                                        placeholder="Please enter full address"
                                                        onChange={event => handleInputChange('companyAddress', event)}
                                                        name="companyAddress"
                                                        value={inputFields[0].companyAddress}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })}></textarea>
                                                    {errors.companyAddress && <span style={{ fontSize: 13, color: "red" }}>{errors.companyAddress.message}</span>}
                                                </div>
                                            </div>
                                        </div>

                                        {/* <br />

                                        <div className="row">
                                            <div className="col-4">
                                                <div className="form-group">
                                                    <label>Tax Card (NPWP): <span className="tx-danger">*</span></label>
                                                    <input
                                                        type="file"
                                                        className="form-control"
                                                        onChange={event => handleInputChange('companyNPWP', event)}
                                                        name="companyNPWP"
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.companyNPWP && <span style={{ fontSize: 13, color: "red" }}>{errors.companyNPWP.message}</span>}
                                                </div>
                                            </div>
                                        </div> */}

                                    </fieldset>
                                </div>

                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>INTTRA INFORMATION</legend>

                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>ID:</label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter Inttra ID"
                                                        type="text"
                                                        onChange={event => handleInputChange('inttraID', event)}
                                                        value={inputFields[0].inttraID} />
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>

                                <div className="col-md-12">
                                    <fieldset className={dashforgeCss.formFieldset} style={{ marginBottom: '15px' }}>
                                        <legend>PERSONAL CONTACT DETAILS</legend>

                                        <div className="row">
                                            <div className="col-4">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Name: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter name"
                                                        type="text"
                                                        name="pcdName"
                                                        onChange={event => handleInputChange('pcdName', event)}
                                                        value={inputFields[0].pcdName}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.pcdName && <span style={{ fontSize: 13, color: "red" }}>{errors.pcdName.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Email: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter email"
                                                        type="email"
                                                        name="pcdEmail"
                                                        onChange={event => handleInputChange('pcdEmail', event)}
                                                        value={inputFields[0].pcdEmail}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            },
                                                            pattern: {
                                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                                message: "Invalid email address"
                                                            }
                                                        })}
                                                        disabled />
                                                    {errors.pcdEmail && <span style={{ fontSize: 13, color: "red" }}>{errors.pcdEmail.message}</span>}
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div className="form-group">
                                                    <label className={dashforgeCss.txBlack}>Phone: <span className={dashforgeCss.txDanger}>*</span></label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Please enter phone"
                                                        type="text"
                                                        name="pcdPhone"
                                                        onChange={event => handleInputChange('pcdPhone', event)}
                                                        value={inputFields[0].pcdPhone}
                                                        ref={register({
                                                            required: {
                                                                value: true,
                                                                message: 'This input field is required!'
                                                            }
                                                        })} />
                                                    {errors.pcdPhone && <span style={{ fontSize: 13, color: "red" }}>{errors.pcdPhone.message}</span>}
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>

                                    <br />

                                    <div className="row">
                                        <div className="col-12">
                                            <label style={styles.labelCheckbox} className={dashforgeCss.txBlack}><input
                                                style={styles.inputCheckbox}
                                                type="checkbox"
                                                name="termsOfService"
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} /> I agree to the clickargo <a href="https://clickargo.com/term.html" target="_blank" style={{ textDecoration: 'underline', fontWeight: 'bold' }}>Terms of Service</a> and <a href="https://clickargo.com/privacy-policy.html" target="_blank" style={{ textDecoration: 'underline', fontWeight: 'bold' }}>Privacy Policy</a></label>
                                            {errors.termsOfService && <span style={{ fontSize: 13, color: "red" }}>{errors.termsOfService.message}</span>}
                                        </div>
                                        <div className="col-12">
                                            <label style={styles.labelCheckbox} className={dashforgeCss.txBlack}><input
                                                style={styles.inputCheckbox}
                                                type="checkbox"
                                                name="nleTermOfService"
                                                ref={register({
                                                    required: {
                                                        value: true,
                                                        message: 'This input field is required!'
                                                    }
                                                })} /> Agree to send the data to Clickargo for processing transaction based on services that user create</label>
                                            {errors.nleTermOfService && <span style={{ fontSize: 13, color: "red" }}>{errors.nleTermOfService.message}</span>}
                                        </div>
                                    </div>

                                    {/* <br />
                                    <pre>
                                        {JSON.stringify(inputFields, null, 2)}
                                    </pre>
                                    <br />
                                    <pre>
                                        {JSON.stringify(getValues(), null, 2)}
                                    </pre> */}


                                    <div className="text-center">
                                        <button
                                            type="submit"
                                            className="btn btn-primary submit-btn"
                                            disabled={isLoading}>
                                            {isLoading && <i className="fa fa-circle-notch fa-spin"></i>}
                                            {isLoading && <span>Please wait...</span>}
                                            {!isLoading && <span>Submit</span>}
                                        </button>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>
    )
}

const styles = {
    labelCheckbox: {
        display: 'block',
        paddingLeft: '15px',
        textIndent: '-15px'
    },
    inputCheckbox: {
        width: '20px',
        height: '20px',
        padding: '0',
        margin: '0',
        verticalAlign: 'bottom',
        position: 'relative',
        top: '-1px',
        overflow: 'hidden'
    }
};

export default Register
