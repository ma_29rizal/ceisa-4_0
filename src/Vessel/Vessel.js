import React,{Component, Fragment} from 'react'
import * as Constants from '../Constants.js'
import * as Helper from '../Helper.js'
import Form from './Form'
import Filter from './Filter'
import Schedule from './Schedule'
import { Card, Button } from 'reactstrap'
import axios from 'axios'
import AsyncSelect from 'react-select/async'
import Swal from 'sweetalert2'
import Select from 'react-select'
import ScheduleThisWeek from './ScheduleThisWeek.js'
import * as ApiClickargo from '../Clickargo/ApiList.js'

const initialState = {
  origin_unloc: '',
  origin_unloc_error: '',
  destination_unloc: '',
  destination_unloc_error: '',
  move_type: '',
  move_type_label: '',
  move_type_error: '',
  list_schedule: [],
  isSubmitted: false,
  isSubmittedFilter: false,
  type: '',
  filter_carrier_name: '',
  filter_service_name: '',
  date_of_shipment: '',
  duration: '',
  isLoading: false,
  showThisWeekSchedule: true
}

class Vessel extends Component{
  constructor(props){
    super(props)
    this.state = initialState
  }

  fetchData = (inputValue, callback) => {
    if (!inputValue) {
        callback([]);
      } else {
    setTimeout(() => {
        axios
              .post(ApiClickargo.CLICKARGO_UNLOCS, {
                data: inputValue
              })
              .then((data) => {
                  const tempArray = [];
                  data.data.data.forEach((element) => {
                      tempArray.push({ label: `${element.name}` + ' (' + `${element.code}` + ')', value: element.code });
                  });
                  callback(tempArray);          
              })
              .catch((error) => {
                console.log(error, "catch the hoop")
              });
      });
    }
  }

  onSearchPol = (selectedOption) => {
    if (selectedOption) {
      

    this.setState({
        origin_unloc: selectedOption
      });
    }
  };

  onSearchPod = (selectedOption) => {
    if (selectedOption) {

    this.setState({
        destination_unloc: selectedOption
      });
    }
  };

  changeHandlerMoveType = e => {
      this.setState({
          move_type: e.value,
          move_type_label: e.label
      })
  }

  validate = () => {
    let origin_unloc_error = "";
    let destination_unloc_error = "";
    let move_type_error = "";

    if(!this.state.origin_unloc) {
      origin_unloc_error = "Port of Origin is Empty"
    }

    if(!this.state.destination_unloc) {
      destination_unloc_error = "Port of Destination is Empty"
    }

    if(!this.state.move_type) {
      move_type_error = "Move Type is Empty"
    }

    if(origin_unloc_error || destination_unloc_error || move_type_error) {
      this.setState({
        origin_unloc_error: origin_unloc_error,
        destination_unloc_error: destination_unloc_error,
        move_type_error: move_type_error
      })
      return false
    }

    return true
   }

  submitHandler = e => {
      e.preventDefault();

      const isValid = this.validate()
      if(isValid) {
        this.setState({
          origin_unloc_error: '',
          destination_unloc_error: '',
          move_type_error: '',
          isSubmitted: false
        });

        const headers = {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
      }

      setTimeout(() => {

        axios
          .post(ApiClickargo.CLICKARGO_CHECK_SCHEDULE, {
            origin_unloc: this.state.origin_unloc.value,
            destination_unloc: this.state.destination_unloc.value
          }, {
              headers: headers
          })
          .then(response => {
              this.setState({isSubmitted: true, isSubmittedFilter: true, showThisWeekSchedule: false})
              this.setState({list_schedule:response.data.data})
          })
          .catch(error => {
            if (!error.response) {
              Swal.fire(
                'Oops!',
                'Please check your internet connection!',
                'error'
              )
            } else {
              if (error.response.data.success === false) {
                Swal.fire(
                  'Oops!',
                  'Schedule not available :(',
                  'error'
                )
                this.setState({isSubmitted: true, isSubmittedFilter: true, showThisWeekSchedule: false})
                this.setState({list_schedule:null})
              } 
            }
          })

      }, 1200);
    }
  }

  contentVesselHeader = (breadcrump) =>{
    return(
      <div className="kt-portlet__head">

        <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">
            {
              breadcrump.map((bcp, i) => {
                return (
                  <span key={i}>
                  {
                    bcp.link === null 
                    ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                    : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  } 
                  {
                    i !== (breadcrump.length-1)
                    ? " > "
                    : null
                  }
                  </span>
                )
              })
            }
            </h3>
        </div>    
      </div>
    )
  }

  callbackFunctionType = (childData) => {
    this.setState({type: childData})
  }

  callbackFunctionCarrierName = (childData) => {
      this.setState({filter_carrier_name: childData})
  }

  callbackFunctionServiceName = (childData) => {
      this.setState({filter_service_name: childData})
  }

  callBackFunctionDateOfShipment = (childData) => {
      this.setState({date_of_shipment: childData})
  }

  callBackFunctionDuration = (childData) => {
      this.setState({duration: childData})
  }

  componentDidMount(){

  }

  componentDidUpdate(prevProps, prevState) {
    if(
          this.state.type !== prevState.type || 
          this.state.filter_carrier_name !== prevState.filter_carrier_name || 
          this.state.filter_service_name !== prevState.filter_service_name ||
          this.state.date_of_shipment !== prevState.date_of_shipment) 
          {
            this.setState({isSubmitted: false})
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }

            axios
              .post(ApiClickargo.CLICKARGO_CHECK_SCHEDULE, {
                      origin_unloc: this.state.origin_unloc.value,
                      destination_unloc: this.state.destination_unloc.value,
                      type: this.state.type,
                      filter_carrier_name: this.state.filter_carrier_name,
                      filter_service_name: this.state.filter_service_name,
                      date_of_shipment: this.state.date_of_shipment,
                      duration: this.state.duration
                }, {
                  headers: headers
            })
              .then(response => {
                  // console.log(response)
                  this.setState({isSubmitted: true, isSubmittedFilter: true, showThisWeekSchedule: false})
                  this.setState({list_schedule:response.data.data})
            })
              .catch(error => {
                  if (error.response.data.success === false) {
                    Swal.fire(
                      'Oops!',
                      'Schedule not available :(',
                      'error'
                    )
                    this.setState({isSubmitted: true, isSubmittedFilter: true, showThisWeekSchedule: false})
                    this.setState({list_schedule:null})
                  }
            })
      }
  }

  render(){

    let optionMoveType = [
      { value: '', label: 'Please Select Move Type' },
      { value: 'DoorToDoor', label: 'Door To Door' },
      { value: 'DoorToPort', label: 'Door To Port' },
      { value: 'PortToDoor', label: 'Port To Door' },
      { value: 'PortToPort', label: 'Port To Port' }
    ]

    const { 
          origin_unloc, origin_unloc_error, 
          destination_unloc, destination_unloc_error,
          move_type, move_type_label, move_type_error
          } = this.state

    const breadcrump = [
      {
        label: 'Vessel',
        link: null
      }
    ];

    return(
      
      <Fragment>
          
        <div className="kt-portlet kt-portlet--height-fluid">

          <div className="kt-portlet__head">
            <div className="kt-portlet__head-label">
              <h3 className="kt-portlet__head-title">
                <span>
                  <label className="kt-font-boldest"> Find Schedule</label>
                </span>
              </h3>
            </div>
          </div>
          
        <div className="kt-portlet__body">
            <div className="kt-widget15">
              <form onSubmit={this.submitHandler}>
                <div className="row">
                  <div className="col-md-3">
                    <h6>Port of loading</h6>
                    <AsyncSelect
                      value={origin_unloc}
                      loadOptions={this.fetchData}
                      placeholder="Select Port of Loading"
                      onChange={(e) => {
                        this.onSearchPol(e);
                      }}
                      defaultOptions={false}
                    />
                    {origin_unloc_error ? (
                      <span style={{ fontSize: 13, color: "red" }}>{origin_unloc_error}</span>
                    ) : null}
                  </div>
                  <div className="col-md-3">
                    <h6>Port of destination</h6>
                    <AsyncSelect
                      value={destination_unloc}
                      loadOptions={this.fetchData}
                      placeholder="Select Port of Destination"
                      onChange={(e) => {
                        this.onSearchPod(e);
                      }}
                      defaultOptions={false}
                    />
                    {destination_unloc_error ? (
                      <span style={{ fontSize: 13, color: "red" }}>{destination_unloc_error}</span>
                    ) : null}
                  </div>
                  <div className="col-md-3">
                    <h6>Move Type</h6>
                    <Select
                      defaultValue={{label: 'Please Select Move Type', value: ''}}
                      options={optionMoveType}
                      onChange={this.changeHandlerMoveType} />
                    {move_type_error ? (
                      <span style={{ fontSize: 13, color: "red" }}>{move_type_error}</span>
                    ) : null}
                  </div>
                  <div className="col-md-3">
                    <Button style={{ marginTop: 25 }} type="submit" color="primary" size="md">Search</Button>
                  </div>
                </div>
              </form>
            </div>
          </div>

          {this.state.showThisWeekSchedule && <ScheduleThisWeek
                                                dataUserPortal={this.props.dataUserPortal} />}

          {this.state.isSubmittedFilter && <Filter
                                              parentCallbackType={this.callbackFunctionType} 
                                              parentCallbackCarrierName={this.callbackFunctionCarrierName} 
                                              parentCallbackServiceName={this.callbackFunctionServiceName}
                                              parentCallbackDateOfShipment={this.callBackFunctionDateOfShipment}
                                              parentCallbackDuration={this.callBackFunctionDuration} />}

          {this.state.isSubmitted && <Schedule 
                                              dataSchedule={this.state.list_schedule} 
                                              dataLoading={this.state.isLoading}
                                              dataUserPortal={this.props.dataUserPortal}
                                              dataMoveType={move_type}
                                              dataMoveTypeLabel={move_type_label}/>}                                
        </div>

      </Fragment>
    )
  }

}

export default Vessel;