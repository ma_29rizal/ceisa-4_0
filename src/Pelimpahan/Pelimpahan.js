import React,{Component, Fragment} from 'react'
import {
  API_DATA_PELIMPAHAN, 
  API_SURAT_KUASA, 
  API_FILE_SURAT_KUASA,
  API_DATA_PELIMPAHAN_CREATOR,
  API_DATA_PELIMPAHAN_PPJK,
  GET_LIST_BL2_0,
  GET_LIST_BL1_6,
  GET_LIST_BL2_3,
  GET_PROFILE
} from '../Constants.js'
import * as Helper from '../Helper.js'
import {Link } from 'react-router-dom';
import memoize from 'memoize-one';
import DataTable from 'react-data-table-component'
import uuid from "uuid";
import Autosuggest from 'react-autosuggest'
import {Tabs, Tab} from 'react-bootstrap';
import 
  { Modal, ModalHeader, ModalBody, Button, ModalFooter, Form, FormGroup, Label, Input, FormText } 
from 'reactstrap'
import axios from "axios";
import './Pelimpahan.css'

const perPage = 10
const paginationOptions = { noRowsPerPage: true }

const DOCUMENT_TYPE_OPTIONS = [
  {
    value: 'BC 2.0', 
    label: 'BC 2.0',
  },
  {
    value: 'BC 1.6', 
    label: 'BC 1.6',
  },
  {
    value: 'BC 2.3', 
    label: 'BC 2.3',
  },
]

class Pelimpahan extends Component{
  constructor(props){
    super(props)
    this.state = {
      npwp: this.props.dataUserPortal.npwp,
      activeTabs: 'pelimpahanKepada',
      pelimpahan_data: [],
      activePerPage: perPage,
      currentPage: 0,
      totalData: 0,
      loadingData: true,
      loadingSaveData: false,
      modal: false,
      modalEdit: false,
      fileInput: null,
      rowData: [],
      errorMessage: '',
      formStatus: 'create',
      idDetil: '',
      id_surat_kuasa: '',
      created_npwp: '',
      blOptions: [{value: "", label: "Loading ..."}],
      blDateByBLNo: [],
      bl_no: '',
      bl_date: '',
      ppjk: '',
      nama_ppjk: '',
      document_type: '',
      modalEditSuratKuasa: false,
      fileNameSuratKuasa: '',
      showPelimpahan: true,
      pelimpahan_data_diterima: [],
      no_ppjk: null,
      selectedDocType: 'BC 2.0',
      indexRow: null,
      input_props: null,
      //autosuggest
      value: '',
      suggestions: [],
      detailDataOnClick: null,
      dateValidation: null,
      //autosuggest
      // checkbox
      delivery_order: 0,
      sp2: 0,
      trucking: 0,
      warehouse: 0,
      depo: 0,
      domestic: 0,
      checkall: false,
      

    }
  }
  async componentDidMount() {
    this.fetchingData(1, perPage)
    this.fetchingPelimpahanDiterima(1, perPage)
  }

  // autosuggest
  escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }
  getSuggestions(value) {
    const escapedValue = this.escapeRegexCharacters(value.trim());
    if (escapedValue === '') {
      return [];
    }
    const regex = new RegExp('^' + escapedValue, 'i');    
    if(this.state.modalEdit === true) {
      const suggestionFormEdit = this.state.blOptions.filter(suggestion => regex.test(suggestion.label))
      return suggestionFormEdit
    }
    else {
      const suggestions = this.state.rowData[this.state.indexRow].bl_options;
      const suggest = suggestions.filter(suggestion => regex.test(suggestion.label));
      return suggest;
    }
  }

  getSuggestionValue = (suggestion) => {
      return suggestion.label;  
  }

  renderSuggestion = (suggestion) => {
    return <span>{suggestion.label}</span>
  }
  onSuggestionSelected = (evt) => {
    const {input_props, indexRow, rowData} = this.state;
    input_props.value = rowData[indexRow].bl_no
    var pelimpahan = this.state.rowData.slice();
    var newPelimpahan = pelimpahan.map((pelimpahanData, index) => {
      for (var key in pelimpahanData) {
        if ( key === "bl_no" && (pelimpahanData["document_type"] === "BC 2.0" || pelimpahanData["document_type"] === "BC 1.6" || pelimpahanData["document_type"] === "BC 2.3") ) {
          let selected_bl_date = pelimpahanData["bl_date_by_no"][this.state.input_props.value] || "";
          pelimpahanData["bl_date"] = selected_bl_date.substr(0,10);
        }
      }
      return pelimpahanData;
    });
    this.setState({rowData:newPelimpahan, bl_no: evt.target.textContent, dateValidation: true})
  }

  onChangeAutoComplete = (event, { newValue, method }) => {
    var item = {
      id: event.target.id,
      name: event.target.name,
      value: event.target.value
    };
    const currentRowData = this.state.rowData;
    currentRowData[this.state.indexRow].bl_no = newValue;
    this.setState({
      rowData: currentRowData,
      input_props: item,
      dateValidation: false
    });
  };
  
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  onChangeAutoCompleteEdit = (event, {newValue, method}) => {
    var item = {
      id: event.target.id,
      name: event.target.name,
      value: newValue
    };
    this.setState({bl_no: newValue, input_props: item, dateValidation: false})
  }
  // autosuggest

  addRow = () => {
    const {rowData} = this.state
    let currentData = rowData
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var pelimpahan = {
      id: id,
      bl_no: "",
      bl_date: "",
      ppjk: this.state.ppjk,
      nama_ppjk: this.state.nama_ppjk,
      document_type: DOCUMENT_TYPE_OPTIONS[0].value,
      bl_options: [{value: "", label: "Loading ..."}],
      bl_date_by_no: []
    }
    currentData.push(pelimpahan)
    let newIndex = currentData.length-1
    this.setState({rowData: currentData, indexRow: newIndex}, 
      () => {
      this.getBlByDocType(DOCUMENT_TYPE_OPTIONS[0].value, newIndex)
    }
    );
  }

  handleRowDel(pelimpahan) {
    const {rowData} = this.state
    let currentData = rowData
    var index = currentData.indexOf(pelimpahan);
    currentData.splice(index, 1);
    this.setState({rowData: currentData});
  };

  handleChange = (event, firstLoad) => {
    const {document_type, blDateByBLNo, bl_no, bl_date, input_props} = this.state
    if ( event.target.name === "document_type" ) {
      const docType = event.target.value;
      const newValBlNo = (docType === "BC 2.0" || docType === "BC 1.6" || docType === 'BC 2.3') || !firstLoad ? "": bl_no
      const newValBlDate = (docType === "BC 2.0" || docType === "BC 1.6" || docType === 'BC 2.3') || !firstLoad ? "": bl_date
      this.setState({
        [event.target.name] : event.target.value,
        blOptions: [{value: "", label: "Loading ..."}],
        blDateByBLNo: [],
        bl_no: newValBlNo,
        bl_date: newValBlDate,
      }, () => {
        let apiBl = null;
        let resultName = null;
        if ( docType === "BC 2.0" ) {
          apiBl = GET_LIST_BL2_0
          resultName = "Data BL BC 2.0";
        } else if ( docType === "BC 1.6" ) {
          apiBl = GET_LIST_BL1_6
          resultName = "Data BL BC 1.6"
        } else if ( docType === "BC 2.3" ) {
          apiBl = GET_LIST_BL2_3
          resultName = "Data BL BC23"
        } else {
          apiBl = null
          resultName = null
        }
        if ( apiBl !== null ) {
          fetch(apiBl + this.state.npwp)
            .then(Response =>Response.json())
            .then((result) => {
              let newBLOptions = [];
              let newBLDate = [];
              result[resultName].map((item, index) => {
                let bldata = {value: item.bl_no, label: item.bl_no}
                newBLOptions.push(bldata)
                newBLDate[item.bl_no] = item.bl_date
              });
              this.setState({
                blOptions: newBLOptions,
                blDateByBLNo: newBLDate,
                bl_no: this.state.detailDataOnClick === null ? '' : this.state.detailDataOnClick.bl_no,
                bl_date: this.state.detailDataOnClick === null ? "" : this.state.detailDataOnClick.bl_date,
              })
            }, 
            (error) => {
              this.setState({
                blOptions: [],
                blDateByBLNo: [],
                bl_no: this.state.detailDataOnClick === null ? '' : this.state.detailDataOnClick.bl_no,
                bl_date: this.state.detailDataOnClick === null ? "" : this.state.detailDataOnClick.bl_date,
              })
              console.log("Err", error);
            })
        } else {
          this.setState({blOptions: [], blDateByBLNo: []})
        }
      })
    } else if ( typeof(firstLoad) === 'object' && input_props.name === "bl_no") {
        let bl_date_by_no = blDateByBLNo[firstLoad.suggestionValue].substr(0,10) || "";
        // cek apakah bl number ada di dalam option
        let options = this.state.blOptions.map(opt => opt.value)
        let checkBLNumber = options.indexOf(firstLoad.suggestionValue) !== -1
        this.setState({[event.target.name] : event.target.value, bl_date: bl_date_by_no, dateValidation: checkBLNumber})
    } else if (event.target.name === 'ppjk') {
        this.setState({ppjk: event.target.value})
        if (event.target.value !== '') {
          axios.get(`${GET_PROFILE}${event.target.value}`)
          .then(res => {
            if (res.status === 200) {
              this.setState({
                nama_ppjk: res.data['data Customer'][0].NM_PERUSAHAAN,
              })
            }
          })
          .catch(err => {
            this.setState({
              nama_ppjk: ''
            })
          })
        }
    }
    else {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  handleFileChange = (event) => {
    this.setState({fileInput: event.target.files[0]})
  }
  handleCheckBox = (event) => {
    const {name, checked, value} = event.target;
    if (checked) {
      if (name === 'checkall') {
        this.setState({
          checkall: true,
          delivery_order: 1,
          sp2: 1,
          trucking: 1,
          warehouse: 1,
          depo: 1,
          domestic: 1

        })
      } else {
        this.setState({
          [name]: 1
        })
      }
    } else {
      if (name === 'checkall') {
        this.setState({
          checkall: false,
          delivery_order: 0,
          sp2: 0,
          trucking: 0,
          warehouse: 0,
          depo: 0,
          domestic: 0
        })
      } else {
        this.setState({
          [name]: 0,
        })
      }
    }
  }
  getBlByDocType(docType, index) {
    let currentRowData = this.state.rowData
    if ( typeof currentRowData[index] !== "undefined" ) {
      currentRowData[index]['bl_options'] = [{value: "", label: "Loading ..."}]
      currentRowData[index]['loadingBLOptions'] = true
    }
    
    this.setState({rowData: currentRowData})

    let apiBl = null;
    let resultName = null;
    if ( docType === "BC 2.0" ) {
      apiBl = GET_LIST_BL2_0
      resultName = "Data BL BC 2.0";
    } else if ( docType === "BC 1.6" ) {
      apiBl = GET_LIST_BL1_6
      resultName = "Data BL BC 1.6"
    } else if ( docType === 'BC 2.3'){
      apiBl = GET_LIST_BL2_3
      resultName = "Data BL BC23"
    } else {
      apiBl = null
      resultName = null
    }
    if ( apiBl !== null ) {
      fetch(apiBl + this.state.npwp)
        .then(Response =>Response.json())
        .then((result) => {
          let newBLOptions = [];
          let newBLDate = [];
          result[resultName].map((item, index) => {
            let bldata = {value: item.bl_no, label: item.bl_no}
            newBLOptions.push(bldata)
            newBLDate[item.bl_no] = item.bl_date
          });
          let currentRowData = this.state.rowData
          if ( typeof currentRowData[index] !== "undefined" ) {
            currentRowData[index]["bl_no"] = currentRowData[index]["bl_no"] || ""//newBLOptions.length ? newBLOptions[0].value: ""
            currentRowData[index]["bl_date"] = currentRowData[index]["bl_date"] || "" //newBLOptions.length ? newBLDate[newBLOptions[0].value]: null
            currentRowData[index]['bl_options'] = newBLOptions
            currentRowData[index]['bl_date_by_no'] = newBLDate
            currentRowData[index]['loadingBLOptions'] = false
          }
          this.setState({rowData: currentRowData})
        }, 
        (error) => {
          let currentRowData = this.state.rowData
          if ( typeof currentRowData[index] !== "undefined" ) {
            currentRowData[index]["bl_no"] = ""
            currentRowData[index]["bl_date"] = ""
            currentRowData[index]['bl_options'] = []
            currentRowData[index]['bl_date_by_no'] = []
            currentRowData[index]['loadingBLOptions'] = false
          }
          this.setState({rowData: currentRowData})
          console.log("Err", error);
        })
    } else {
      let currentRowData = this.state.rowData
      if ( typeof currentRowData[index] !== "undefined" ) {
        currentRowData[index]["bl_no"] = ""
        currentRowData[index]["bl_date"] = ""
        currentRowData[index]['bl_options'] = []
        currentRowData[index]['bl_date_by_no'] = []
        currentRowData[index]['loadingBLOptions'] = false
      }
      this.setState({rowData: currentRowData})
    }
  }
  handleFormPelimpahanTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var pelimpahan = this.state.rowData.slice();    
    var newPelimpahan = pelimpahan.map((pelimpahanData, index) => {
      for (var key in pelimpahanData) {
        let nama_perusahaan = ''
        if (key === 'ppjk' && item.name ==='ppjk') {
            pelimpahan[index].ppjk = item.value
            axios.get(`${GET_PROFILE}${item.value}`)
            .then(res => {
              if(res.status === 200) {
                nama_perusahaan = res.data['data Customer'][0].NM_PERUSAHAAN
                pelimpahan[index].nama_ppjk = res.data['data Customer'][0].NM_PERUSAHAAN
                this.setState({
                  nama_ppjk: nama_perusahaan
                })
              }
            })
            .catch(err => {
              this.setState({
                nama_ppjk: ''
              })
            })
          }
         if (key == item.name && pelimpahanData.id == item.id) {
          pelimpahanData[key] = item.value
          if ( key === "document_type" ) {
            pelimpahan[this.state.indexRow].bl_no = ''
            this.getBlByDocType(item.value, index)
          }
          if ( key === "bl_no" && (pelimpahanData["document_type"] === "BC 2.0" || pelimpahanData["document_type"] === "BC 1.6" || pelimpahanData["document_type"] === "BC 2.3") ) {
            let selected_bl_date = pelimpahanData["bl_date_by_no"][item.value] || "";
            pelimpahanData["bl_date"] = selected_bl_date.substr(0,10);
          }
        }
      }
      return pelimpahanData;
    });
    this.setState({rowData:newPelimpahan, [evt.target.name]: evt.target.value})
  }
  renderCheckBox() {
    const {checkall, delivery_order, sp2, trucking, warehouse, depo, domestic} = this.state;
    return(
      <React.Fragment>
        <FormGroup check>
          <Label check>
            <Input type="checkbox" name='checkall' checked={this.state.checkall} onChange={this.handleCheckBox}/>All</Label>
        </FormGroup>
        <div className='row mt-2'>
          <div className='col-lg-3 mt-2'>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name='delivery_order' disabled={checkall ? true : false} checked={checkall ? true : (delivery_order === 1 ? true : false) }  value={delivery_order} onChange={this.handleCheckBox}/>DO</Label>
            </FormGroup>
          </div>
          <div className='col-lg-3 mt-2'>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name='sp2' value={sp2} disabled={checkall ? true : false} checked={checkall ? true : (sp2 === 1 ? true : false)} onChange={this.handleCheckBox}/>SP2</Label>
            </FormGroup>
          </div>
          <div className='col-lg-3 mt-2'>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name='trucking' disabled={checkall ? true : false} checked={checkall ? true : (trucking === 1 ? true : false)} value={trucking} onChange={this.handleCheckBox}/>Trucking</Label>
            </FormGroup>
          </div>
          <div className='col-lg-3 mt-2'>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name='warehouse' value={warehouse} disabled={checkall ? true : false} checked={checkall ? true : (warehouse === 1 ? true : false)} onChange={this.handleCheckBox}/>Warehouse</Label>
            </FormGroup>
          </div>
            <div className='col-lg-3 mt-2'>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name='depo' value={depo} disabled={checkall ? true : false} checked={checkall ? true : (depo === 1 ? true : false)} onChange={this.handleCheckBox}/>Depo</Label>
            </FormGroup>
          </div>
          <div className='col-lg-3 mt-2'>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" name='domestic' value={domestic} disabled={checkall ? true : false} checked={checkall ? true : (domestic === 1 ? true : false)} onChange={this.handleCheckBox}/>Domestic</Label>
            </FormGroup>
          </div>
          
        </div>
      </React.Fragment>
    )
  }
  dataPelimpahanKepada(){
    const {
      pelimpahan_data, totalData, loadingData, rowData, errorMessage, loadingSaveData,
      blOptions,
      bl_no, bl_date, ppjk, nama_ppjk, document_type, idDetil
    } = this.state;
    const columns = [
      {
        name: 'BL Number',
        selector: 'bl_no',
        sortable: true,
        width: '180px'
      },
      {
        name: 'BL Date',
        selector: 'bl_date',
        sortable: true,
        width: '150px'
      },
      {
        name: 'PPJK',
        selector: 'ppjk',
        sortable: true,
        width: '150px'
      },
      {
        name: 'PPJK Name',
        selector: 'nama_ppjk',
        sortable: true,
        width: '150px'
      },
      {
        name: 'DO',
        sortable: true,
        width: '80px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.delivery_order === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'SP2',
        sortable: true,
        width: '80px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.sp2 === 1 ? <i className="fa fa-check"></i> : '-' }</span>
      },
      {
        name: 'Trucking',
        selector: 'trucking',
        sortable: true,
        width: '100px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.trucking === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'Warehouse',
        sortable: true,
        width: '100px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.warehouse === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'Depo',
        sortable: true,
        width: '80px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.depo === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'Domestic',
        sortable: true,
        width: '100px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.domestic === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'File Surat',
        width: '180px',
        cell: row => <span><a href={row.surat_kuasa.file} target="_blank"> <i className="fa fa-download"></i> Surat Kuasa </a><span style={{color: '#7b8afa'}} title="Edit File Surat Kuasa" onClick={() => this.editSuratKuasa(row.surat_kuasa.filename)}><i className='fas fa-edit'></i></span></span>
      },
    ];
    const datatableColumns = memoize(rowHandler => columns.concat(
      [
        {
          cell: row => <span className="link" style={{color: "#7b8afa"}} onClick={(row) => rowHandler.editData(row)} id={row.id}><i className="fas fa-edit mt-2"></i></span>,
          ignoreRowClick: true,
          allowOverflow: true,
          button: true,
          width: '10px'
        },
        {
          cell: row => <span className="link" style={{color: "#7b8afa"}} onClick={(row) => rowHandler.deleteData(row)} id={row.id}><i className="fas fa-trash mt-2"></i></span>,
          ignoreRowClick: true,
          allowOverflow: true,
          button: true,
          width: '10px'
        }
      ]
    ));
    const dataHandler = {
      editData: this.editDatatable,
      deleteData: this.deleteDatatable,
    };
    return(
      <React.Fragment>
        <Modal isOpen={this.state.modalEdit} toggle={this.handleModalEdit}>
          <ModalHeader toggle={this.handleModalEdit}>Edit Data Pelimpahan</ModalHeader>
          <Form onSubmit={this.editPelimpahan.bind(this)}>
            <ModalBody>
              {
                errorMessage !== '' && errorMessage !== null ? (
                  <div class="alert alert-danger"><strong>Error</strong>: {errorMessage}</div>
                ): (null)
              }
              <FormGroup>
                <Label>Document Type</Label>
                <Input type="select" name="document_type" id="document_type" defaultValue={document_type} onChange={(e) => this.handleChange(e)}>
                  {
                    DOCUMENT_TYPE_OPTIONS.map((item, i) => {
                      return (
                        <option value={item.value} key={i}>{item.label}</option>
                      )
                    })
                  }
                </Input>
              </FormGroup>

              <FormGroup>
                <Label>BL Number</Label>
                <Autosuggest 
                  suggestions={this.state.suggestions}
                  onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                  onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                  getSuggestionValue={this.getSuggestionValue}
                  renderSuggestion={this.renderSuggestion}
                  onSuggestionSelected={this.handleChange}
                  inputProps={
                    {
                      placeholder: "BL Number",
                      value: this.state.bl_no,
                      onChange: this.onChangeAutoCompleteEdit,
                      className: 'form-control form-edit',
                      name:'bl_no',

                    }
                  }
                />
              </FormGroup>
              <FormGroup>
                <Label>BL Date</Label>
                <Input type="date" value={bl_date || ''} innerRef={bl_date || ''} name="bl_date" onChange={(e) => this.handleChange(e)} readOnly={this.state.dateValidation}/>
              </FormGroup>
              <FormGroup>
                <Label>FF/PPJK</Label>
                <Input type="text" value={ppjk || ''} innerRef={ppjk || ''} name="ppjk" onChange={(e) => this.handleChange(e)}/>
              </FormGroup>
              <FormGroup>
                <Label>PPJK Name</Label>
                <Input type="text" value={nama_ppjk || ''} innerRef={nama_ppjk || ''} name="nama_ppjk" onChange={(e) => this.handleChange(e)}/>
              </FormGroup>
              {this.renderCheckBox()}
            </ModalBody>
            <ModalFooter>
              <Button type="submit" color="primary" disabled={loadingSaveData} >{loadingSaveData ? (<span className="fa fa-circle-notch fa-spin"></span>): (null)} Save</Button>
            </ModalFooter>
          </Form>
        </Modal>
        <Modal isOpen={this.state.modal} toggle={this.handleModal} size='lg' style={{width: "1024px", maxWidth: "100%"}}>
          <ModalHeader toggle={this.handleModal}>Tambah Pelimpahan</ModalHeader>
            <ModalBody>
              <Form onSubmit={this.submitDataPelimpahan.bind(this)}>
                {
                  errorMessage !== '' && errorMessage !== null ? (
                    <div className="alert alert-danger"><strong>Error</strong>: {errorMessage}</div>
                  ): (null)
                }
                <FormGroup>
                  <Label>Upload Dokumen</Label>
                  <Input type="file" name="file" onChange={this.handleFileChange.bind(this)}  />
                </FormGroup>
                  <div className="table-editable">
                    <span
                      className="table-add float-left text-success  mr-2"
                      style={{cursor: 'pointer'}} 
                      onClick={() => this.addRow()}
                      >
                        <i className="fas fa-plus" aria-hidden="true"></i> Add BL Number & BL Date
                      </span>
                      <PelimpahanTable 
                        onPelimpahanDataTableUpdate={this.handleFormPelimpahanTable.bind(this)} 
                        onRowAdd={this.addRow.bind(this)} 
                        onRowDel={this.handleRowDel.bind(this)} 
                        pelimpahan={this.state.rowData}
                        optionsData={{document_type: DOCUMENT_TYPE_OPTIONS}}
                        selectedDocType={this.state.selectedDocType}
                        suggestions={this.state.suggestions}
                        ppjk={this.state.ppjk}
                        nama_ppjk={this.state.nama_ppjk}
                        value={this.state.value}
                        onChangeAutoComplete={this.onChangeAutoComplete}
                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                        getSuggestionValue={this.getSuggestionValue}
                        renderSuggestion={this.renderSuggestion}
                        onSuggestionSelected={this.onSuggestionSelected}/>
                  </div>
                  {this.renderCheckBox()}
                <FormGroup>
                  <Button className='mt-4' type="submit" color="primary" disabled={loadingSaveData} >{loadingSaveData ? (<span className="fa fa-circle-notch fa-spin"></span>): (null)} Save</Button>
                </FormGroup>
              </Form>
            </ModalBody>
          </Modal>
          <Modal isOpen={this.state.modalEditSuratKuasa} toggle={this.editSuratKuasa} size='lg' style={{width: '720px', maxWidth: '100%'}}>
          <ModalHeader toggle={this.editSuratKuasa}>Edit File Surat Kuasa</ModalHeader>
            <ModalBody>
              <Form onSubmit={this.submitSuratKuasa.bind(this)}>
                {
                  errorMessage !== '' && errorMessage !== null ? (
                    <div className="alert alert-danger"><strong>Error</strong>: {errorMessage}</div>
                  ): (null)
                }
                <FormGroup>
                  <Label>Upload Dokumen</Label>
                  <Input type="file" name="file" onChange={this.handleFileChange.bind(this)}  />
                </FormGroup>
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col" className='font-weight-bold'>BL Number</th>
                          <th scope="col" className='font-weight-bold'>BL Date</th>
                          <th scope="col" className='font-weight-bold'>PPJK</th>
                          <th scope="col" className='font-weight-bold'>PPJK Name</th>
                        </tr>
                      </thead>
                      {pelimpahan_data.map((item, index) => {
                        return(
                          <tbody key={index}>
                            <tr>
                              <td className='text-center'>{item.bl_no}</td>
                              <td className='text-center'>{item.bl_date}</td>
                              <td className='text-center'>{item.ppjk}</td>
                              <td className='text-center'>{item.nama_ppjk}</td>
                            </tr>
                          </tbody>
                        )
                      })}  
                      </table> 
                <FormGroup>
                  <ModalFooter>
                    <Button color="primary" type='submit' disabled={loadingData}>{loadingData ? <span className='fa fa-circle-notch fa-spin'></span> : null}Save</Button>
                  </ModalFooter>
                  
                </FormGroup>
              </Form>
            </ModalBody>
          </Modal>
          {this.state.loadingData === true ? (
            <p>Loading ...</p>
            ) : 
            <DataTable
              title="Data Pelimpahan"
              columns={datatableColumns(dataHandler)}
              data={pelimpahan_data}
              progressPending={loadingData}
              pagination
              paginationServer
              paginationTotalRows={totalData}
              paginationComponentOptions={paginationOptions}
              onChangeRowsPerPage={this.handlePerRowsChange}
              onChangePage={this.handlePageChange}
              className="customDataTable"
              highlightOnHover
              />}
      </React.Fragment>
    )
  }

  editSuratKuasa = (filename) => {
    if (filename === 'undefined'){
      this.setState({
        modalEditSuratKuasa: !this.state.modalEditSuratKuasa
      })
    }
    else {
      this.setState({
      modalEditSuratKuasa: ! this.state.modalEditSuratKuasa,
      fileNameSuratKuasa: filename
      })
    }
  }

  submitSuratKuasa = (e) => {
    e.preventDefault()
    const { fileInput, fileNameSuratKuasa } = this.state
    const newID = uuid()
    this.setState({loadingSaveData: true}, () => {
      const formData = new FormData()
      formData.append('file', fileInput)
      axios(
        {
          method: 'put',
          url: `https://esbbcext01.beacukai.go.id:8085/SuratKuasa/file?filename=${fileNameSuratKuasa}`,
          data: formData,
          headers: {
            'content-type': `application/x-www-form-urlencoded`,
          }
        }
      ).then(response => {
        if (response.data !== 'undefined' && response.data !== ''){
          const dataPost = {
            idSuratKuasa: response.data.idSuratKuasa,
            filename: response.data.filename,
            file: response.data.file
          }
          axios(
            {
              method: 'post',
              url: API_SURAT_KUASA,
              data: dataPost,
              headers: {
                'content-type': `application/json`,
              }
            }
          )
          .then(resPostData => {
            if ( resPostData.status === 200 ) {
              this.setState({errorMessage: '', fileInput: null, rowData: [], loadingSaveData: false})
              this.fetchingData(1, perPage)
              this.handleModal()
            } else {
              this.setState({errorMessage: "Failed to save data", loadingSaveData: false})
            }
          })
          .catch(error => {
            this.setState({errorMessage: "Failed to save data", loadingSaveData: false})
          })
        }
        else {
          this.setState({errorMessage: "Failed to upload file surat kuasa", loadingSaveData: false})
        }
      })
      .catch(error => {
        this.setState({errorMessage: "Failed to upload file surat kuasa", loadingSaveData: false})
      })
    })
    return false
  }


  deleteRow = (id) => {
    const {currentPage, activePerPage} = this.state
    let confirmDelete = window.confirm('Hapus Pelimpahan?')
    if (confirmDelete) {
      let url = `${API_DATA_PELIMPAHAN}?id=${id}`
      axios.delete(url)
      .then(response => {
        if ( response.status === 200 ) {
          this.fetchingData(currentPage, activePerPage)
        } else {
          console.log("Failed delete data with ID "+id)
        }
      })
      .catch(error => {
        console.log(error)
      })
    }
    else {
      return false
    } 
  }
  editDatatable = (row) => {
    const {pelimpahan_data, handleModalEdit} = this.state
    let id = row.target.id;
    if ( id === '' ) 
      id = row.target.parentNode.id;
    
    let index = -1;
    for(var i = 0; i < pelimpahan_data.length; i++) {
        if(parseInt(pelimpahan_data[i]['id']) === parseInt(id)) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      const detilData = typeof pelimpahan_data[index] === "undefined" ? false: pelimpahan_data[index]
      if ( detilData ) {
        const customEvent = {
          target: {
            name: "document_type",
            value: detilData.document_type
          }
        }
        let checkedAll = false
        if (detilData.delivery_order && detilData.sp2 &&  detilData.trucking && detilData.warehouse && detilData.depo && detilData.domestic) {
          checkedAll = true
        }
        this.setState({
          idDetil: id,
          id_surat_kuasa: detilData.id_surat_kuasa,
          created_npwp: detilData.created_npwp,
          bl_no: detilData.bl_no,
          bl_date: detilData.bl_date,
          ppjk: detilData.ppjk,
          nama_ppjk: detilData.nama_ppjk,
          document_type: detilData.document_type,
          detailDataOnClick: detilData,
          delivery_order: detilData.delivery_order,
          sp2: detilData.sp2,
          trucking: detilData.trucking,
          warehouse: detilData.warehouse,
          depo: detilData.depo,
          domestic: detilData.domestic,
          checkall: checkedAll
        }, ()=>{
          this.handleModalEdit()
          this.handleChange(customEvent, true)
        })
      }
    }
  }
  deleteDatatable = (row) => {
    const {currentPage, activePerPage} = this.state
    let id = row.target.id;
    if ( id === '' ) 
      id = row.target.parentNode.id;
    
    let confirmDelete = window.confirm('Hapus Pelimpahan?')
    if (confirmDelete) {
      let url = `${API_DATA_PELIMPAHAN}?id=${id}`
      axios.delete(url)
      .then(response => {
        if ( response.status === 200 ) {
          this.fetchingData(currentPage, activePerPage)
        } else {
          console.log("Failed delete data with ID "+id)
        }
      })
      .catch(error => {
        console.log(error)
      })
    }
    else {
      return false
    } 
  }

  handlePerRowsChange = async (currentRowsPerPage, currentPage) => {
    this.setState({activePerPage: currentRowsPerPage});
    this.fetchingData(currentPage, currentRowsPerPage);
  }
  handlePageChange  = async (page, totalRows) => {
    const {activePerPage} = this.state
    this.fetchingData(page, activePerPage)
  }
  handlePerRowsChangeDiterima = async (currentRowsPerPage, currentPage) => {
    this.fetchingPelimpahanDiterima(currentPage, currentRowsPerPage)
  }

  handlePageChangeDiterima = async (page, totalRows) => {
    const {activePerPage} = this.state
    this.fetchingPelimpahanDiterima(page, activePerPage)
  }

  fetchingData(page, limit) {
    const {npwp} = this.state
    page = page > 0 ? page-1: page
    // this.setState({ loadingData: true })
    fetch(`${API_DATA_PELIMPAHAN_CREATOR}${npwp}&page=${page}&size=${limit}`)
      .then(res => res.json())
      .then(parsedJSON => {
        this.setState({
          pelimpahan_data: parsedJSON.content,
          totalData: parsedJSON.totalElements,
          loadingData: false,
          currentPage: page,
        })
      })
      .catch(error => {
        this.setState({
          pelimpahan_data: [],
          totalData: 0,
          loadingData: false,
          currentPage: 0
        });
        console.log(`parsing data from ${API_DATA_PELIMPAHAN} failed`, error);
      });
  }



  editPelimpahan = (e) => {
    e.preventDefault()
    const {currentPage, activePerPage, idDetil, id_surat_kuasa, bl_no, bl_date, ppjk, nama_ppjk, document_type, created_npwp, delivery_order, sp2, trucking, warehouse, depo, domestic} = this.state
    this.setState({loadingSaveData: true}, () => {
      let url = `${API_DATA_PELIMPAHAN}?id=${idDetil}`
      let payload = {
        id_surat_kuasa: id_surat_kuasa,
        bl_no: bl_no,
        bl_date: bl_date,
        ppjk: ppjk,
        nama_ppjk: nama_ppjk,
        document_type: document_type,
        created_npwp: created_npwp,
        delivery_order: delivery_order,
        sp2: sp2,
        trucking: trucking,
        warehouse: warehouse,
        depo: depo,
        domestic: domestic
      }
      axios.put(url, payload)
      .then(response => {
        if ( response.status === 200 ) {
          this.setState({errorMessage: '', loadingSaveData: false, ppjk: '', nama_ppjk: ''})
          this.handleModalEdit()
          this.fetchingData(currentPage, activePerPage)
        } else {
          this.setState({errorMessage: "Failed to save data", loadingSaveData: false})
        }
      })
      .catch(err => {
        this.setState({errorMessage: "Failed to save data", loadingSaveData: false})
      })
    });
  }
  addPelimpahan = (e) => {
    const {rowData, npwp, fileInput} = this.state
    const newID = uuid()
    this.setState({loadingSaveData: true}, () => {
      const formData = new FormData();
      formData.append("file", fileInput);
      axios(
        {
          method: 'post',
          url: API_FILE_SURAT_KUASA,
          data: formData,
          headers: {
          'content-type': `application/json`,
          }
        }
      ).then(response => {
        if ( typeof response.data !== "undefined" && response.data !== "" ) {
          //POST Data To Server
          let detil_surat_kuasa = []
          rowData.map((item, i) => {
            let objectDetil = {
              id_surat_kuasa: newID,
              bl_no: item.bl_no,
              bl_date: item.bl_date,
              ppjk: item.ppjk,
              nama_ppjk: item.nama_ppjk,
              document_type: item.document_type,
              created_npwp: npwp,
              delivery_order: this.state.delivery_order,
              sp2: this.state.sp2,
              trucking: this.state.trucking,
              warehouse: this.state.warehouse,
              depo: this.state.depo,
              domestic: this.state.domestic,
              kd_document_type: item.kd_document_type
            }
            detil_surat_kuasa.push(objectDetil)
          })
          const dataPost = {
            idSuratKuasa: newID,
            filename: response.data.filename,
            file: response.data.file,
            detil_surat_kuasa: detil_surat_kuasa
          }
          axios(
            {
              method: 'post',
              url: API_SURAT_KUASA,
              data: dataPost,
              headers: {
                'content-type': `application/json`,
              }
            }
          ).then(resPostData => {
            if ( resPostData.status === 200 ) {
              this.setState({errorMessage: '', fileInput: null, rowData: [], loadingSaveData: false})
              this.fetchingData(1, perPage)
              this.handleModal()
            } else {
              this.setState({errorMessage: "Failed to save data", loadingSaveData: false})
            }
          })
          .catch(err => {
              this.setState({errorMessage: "Failed to save data", loadingSaveData: false})
          })
          //Akhir POST Data ke server
        } else {
          this.setState({errorMessage: "Failed to upload file surat kuasa", loadingSaveData: false})
        }
      })
      .catch(err => {
          this.setState({errorMessage: "Failed to upload file surat kuasa", loadingSaveData: false})
      })
    })
    return false
  }
  submitDataPelimpahan = (e) => {
    e.preventDefault()
    if (this.state.formStatus === 'create'){
      return this.addPelimpahan(e)
    }
    else {
      return this.editPelimpahan(e)
    }
    
    
  }
  handleModal = () => {
    this.setState({
      modal: !this.state.modal,
      rowData: []
    })
  }
  handleModalEdit = () => {
    if (this.state.modalEdit === true) {
      this.setState({
        ppjk: '',
        nama_ppjk: ''
      })
    }
    this.setState({
      modalEdit: !this.state.modalEdit,
      errorMessage: ''
    })
  }

  fetchingPelimpahanDiterima(page, limit) {
    const {npwp} = this.state
    page = page > 0 ? page-1: page
    fetch(`${API_DATA_PELIMPAHAN_PPJK}${npwp}&page=${page}&size=${limit}`)
      .then(res => res.json())
      .then(parsedJSON => {
        this.setState({
          pelimpahan_data_diterima: parsedJSON.content,
          totalElementDiterima: parsedJSON.totalElements,
          loadingData: false,
          currentPage: page,
        })
      })
      .catch(error => {
        this.setState({
          pelimpahan_data_diterima: [],
          totalElementDiterima: 0,
          loadingData: false,
          currentPage: 0
        });
        console.log(`parsing data from ${API_DATA_PELIMPAHAN_PPJK} failed`, error);
      });
  }


  dataPelimpahanDiterima(){
    const { pelimpahan_data_diterima, totalElementDiterima, loadingData, paginationOptions } = this.state
    const columns = [
      {
        name: 'BL Number',
        selector: 'bl_no',
        sortable: true,
        width: '180px'
      },
      {
        name: 'BL Date',
        selector: 'bl_date',
        sortable: true,
        width: '150px'
      },
      {
        name: 'PPJK',
        selector: 'ppjk',
        sortable: true,
        width: '150px'
      },
      {
        name: 'PPJK Name',
        selector: 'nama_ppjk',
        sortable: true,
        width: '150px'
      },
      {
        name: 'DO',
        sortable: true,
        width: '80px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.delivery_order === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'SP2',
        sortable: true,
        width: '80px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.sp2 === 1 ? <i className="fa fa-check"></i> : '-' }</span>
      },
      {
        name: 'Trucking',
        selector: 'trucking',
        sortable: true,
        width: '100px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.trucking === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'Warehouse',
        sortable: true,
        width: '100px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.warehouse === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'Depo',
        sortable: true,
        width: '80px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.depo === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'Domestic',
        sortable: true,
        width: '100px',
        cell: row => <span style={{color: '#7b8afa'}}>{row.domestic === 1 ? <i className="fa fa-check"></i> : '-'}</span>
      },
      {
        name: 'File Surat',
        width: '180px',
        cell: row => <span><a href={row.surat_kuasa.file} target="_blank"> <i className="fa fa-download"></i> Surat Kuasa </a></span>
      },
    ];
    return(
      <React.Fragment>
      {this.state.loadingData === true ? (<p>Loading ...</p>) : 
        (<DataTable
          title="Data Pelimpahan"
          columns={columns}
          data={pelimpahan_data_diterima}
          progressPending={loadingData}
          pagination
          paginationServer
          paginationTotalRows={totalElementDiterima}
          paginationComponentOptions={paginationOptions}
          onChangeRowsPerPage={this.handlePerRowsChangeDiterima}
          onChangePage={this.handlePageChangeDiterima}
          className="customDataTable"
          highlightOnHover
        />)
        }
      </React.Fragment>
    )
  }


  contentPelimpahanHeader = (breadcrump) =>{
    return(
      <div className="kt-portlet__head">

        <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">
            {
              breadcrump.map((bcp, i) => {
                return (
                  <span key={i}>
                  {
                    bcp.link === null 
                    ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                    : (<link to={'pelimpahan'} className="kt-link kt-font-transform-u" key={i}>{bcp.label}</link>)
                  } 
                  {
                    i !== (breadcrump.length-1)
                    ? " > "
                    : null
                  }
                  </span>
                )
              })
            }
            </h3>
        </div>
        <div className="kt-portlet__head-toolbar">
          <Button type="button" className="btn btn-outline-brand" onClick={() => this.handleModal()}>
            <i className="la la-file-text"></i>
            <span className="kt-menu__link-text">Tambah Pelimpahan</span>
          </Button>
        </div>    
      </div>
    )
  } 
  render() {
    const breadcrump = [
      {
        label: 'Pelimpahan',
        link: null
      }
    ];
    return(
      <div className="kt-portlet kt-portlet--height-fluid">
        {this.contentPelimpahanHeader(breadcrump)}
        <Fragment>
          <div className="kt-portlet__body">
            <Tabs defaultActiveKey="activeTrucks" activeKey={this.state.activeTabs} id="truck_tabs" onSelect={k => this.setState({activeTabs: k})}>
              <Tab eventKey="pelimpahanKepada" title="Pelimpahan Kepada">
                {this.dataPelimpahanKepada()}
              </Tab>
              <Tab eventKey="pelimpahanDiterima" title="Pelimpahan Diterima">
                {this.dataPelimpahanDiterima()}
              </Tab>
            </Tabs>
          </div>
        </Fragment>

      </div>
    )
  }
}

class PelimpahanTable extends React.Component {
  render() {
    var selectedDocType = this.props.selectedDocType
    var onPelimpahanDataTableUpdate = this.props.onPelimpahanDataTableUpdate;
    var rowDel = this.props.onRowDel;
    var pelimpahanData = this.props.pelimpahan.map((item, index) => {
      return (<PelimpahanDataRow 
        onPelimpahanDataTableUpdate={onPelimpahanDataTableUpdate} 
        pelimpahanData={item} options={this.props.optionsData} 
        selectedDocType={selectedDocType} 
        onDelEvent={rowDel.bind(this)} 
        key={item.id}
        ppjk={this.props.ppjk}
        nama_ppjk={this.props.nama_ppjk}
        suggestions={this.props.suggestions}
        onSuggestionsFetchRequested={this.props.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
        getSuggestionValue={this.props.getSuggestionValue}
        renderSuggestion={this.props.renderSuggestion}
        onChangeAutoComplete={this.props.onChangeAutoComplete}
        onSuggestionSelected={this.props.onSuggestionSelected}
        value={this.props.value}
        />)
    });
    return (
      <div>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th style={{width: '130px'}}>Doc Type</th>
              <th style={{width: "190px"}}>BL Number</th>
              <th style={{width: "160px"}}>BL Date</th>
              <th>FF / PPJK</th>
              <th>PPJK Name</th>
            </tr>
          </thead>
          <tbody>
            {pelimpahanData}
          </tbody>
        </table>
      </div>
    );
  }
}

class PelimpahanDataRow extends React.Component {
  render() {
    var selectedDocType= this.props.selectedDocType
    return (
      <tr className="eachRow">
        <EditableCell onPelimpahanDataTableUpdate={this.props.onPelimpahanDataTableUpdate} selectedDocType={selectedDocType} cellData={{
          type: "select",
          name: "document_type",
          value: this.props.pelimpahanData.document_type,
          options: this.props.options.document_type,
          id: this.props.pelimpahanData.id
        }}/>
        <EditableCell onPelimpahanDataTableUpdate={this.props.onPelimpahanDataTableUpdate} 
          suggestions={this.props.suggestions}
          onSuggestionsFetchRequested={this.props.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
          getSuggestionValue={this.props.getSuggestionValue}
          renderSuggestion={this.props.renderSuggestion}
          onChangeAutoComplete={this.props.onChangeAutoComplete}
          onSuggestionSelected={this.props.onSuggestionSelected}
          value={this.props.value}
          cellData={{
            type: "text",
            name: "bl_no",
            value: this.props.pelimpahanData.bl_no,
            docType: this.props.pelimpahanData.document_type,
            options: this.props.pelimpahanData.bl_options,
            id: this.props.pelimpahanData.id
          }}
        />
          <EditableCell onPelimpahanDataTableUpdate={this.props.onPelimpahanDataTableUpdate} cellData={{
            type: 'date',
            name: "bl_date",
            value: this.props.pelimpahanData.bl_date,
            docType: this.props.pelimpahanData.document_type,
            id: this.props.pelimpahanData.id
          }}/>
        <EditableCell onPelimpahanDataTableUpdate={this.props.onPelimpahanDataTableUpdate} value={this.props.ppjk} cellData={{
          type: "text",
          name: "ppjk",
          value: this.props.pelimpahanData.ppjk,
          id: this.props.pelimpahanData.id
        }}/>
        <EditableCell onPelimpahanDataTableUpdate={this.props.onPelimpahanDataTableUpdate} value={this.props.nama_ppjk} cellData={{
          type: "text",
          name: "nama_ppjk",
          value: this.props.pelimpahanData.nama_ppjk,
          id: this.props.pelimpahanData.id
        }}/>
        <td className="del-cell">
          <input type="button" onClick={this.props.onDelEvent.bind(this)} value="X" className="del-btn"/>
        </td>
      </tr>
    );

  }

}
class EditableCell extends React.Component {
  render() {
    const inputProps = {
      placeholder: "BL Number",
      value: this.props.cellData.value,
      onChange: this.props.onChangeAutoComplete,
      className: 'form-control',
      name:this.props.cellData.name,
      id:this.props.cellData.id
    };
    if ( this.props.cellData.type === "select" ) {
      return (
        <td>
          <Input type="select" name={this.props.cellData.name} id={this.props.cellData.id} defaultValue={this.props.cellData.value} onChange={this.props.onPelimpahanDataTableUpdate}>
            {
              this.props.cellData.options.map((item, i) => {
                return (
                  <option value={item.value} key={i}>{item.label}</option>
                )
              })
            }
          </Input>
        </td>
      );
    } else {
      if ( this.props.cellData.name === "bl_no" && (this.props.cellData.docType === "BC 2.0" || this.props.cellData.docType === "BC 1.6" || this.props.cellData.docType === "BC 2.3") ) {
        return (
          <td>
            <Autosuggest 
              suggestions={this.props.suggestions}
              onSuggestionsFetchRequested={this.props.onSuggestionsFetchRequested}
              onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
              getSuggestionValue={this.props.getSuggestionValue}
              renderSuggestion={this.props.renderSuggestion}
              onSuggestionSelected={this.props.onSuggestionSelected}
              inputProps={inputProps} />

          </td>
        );
      } else if ( this.props.cellData.name === "bl_date" && (this.props.cellData.docType === "BC 2.0" || this.props.cellData.docType === "BC 1.6" || this.props.cellData.docType === "BC 2.3") ) {
        return (
          <td>
            <Input type={this.props.cellData.type} name={this.props.cellData.name} id={this.props.cellData.id} defaultValue={this.props.cellData.value} innerRef={this.props.cellData.value} onChange={this.props.onPelimpahanDataTableUpdate} />
          </td>
        );
      } else{
          return (
            <td>
              <Input type={this.props.cellData.type} name={this.props.cellData.name} id={this.props.cellData.id} value={this.props.value} innerRef={this.props.value} onChange={this.props.onPelimpahanDataTableUpdate}/>
            </td>
          );
        }
    }
  }
}
export default Pelimpahan;
