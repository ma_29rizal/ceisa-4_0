import React, { Component } from 'react';
import { Modal, Button, Card, Row, CardColumns, Container, Form, Col, ListGroup, Tabs, Tab } from 'react-bootstrap';
import "../../assets/css/Home.css"

export default class SP2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false,
            showModalRequest: false,
        }
    }

    handleShow = () => {
        this.setState({
            showModal: !this.state.showModal,
        })
    }

    handleClose = () => {
        this.setState({
            showModal: false,
        })
    }
    handleShowModal = () => {
        this.setState({
            showModalRequest: !this.state.showModalRequest
        })
    }

    handleCloseModal = () => {
        this.setState({
            showModalRequest: false
        })
    }

    render() {
        return (
            <>
                <Button variant="primary" onClick={this.handleShow} size="sm">
                    Request SP2
                 </Button>
                <table className="table table-sm" style={{ textAlign: 'center' }}>
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Performa Billing</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>REQ12334455</td>
                            <td>BL123456780</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>REQ12334455</td>
                            <td>BL123456780</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>REQ12334455</td>
                            <td>BL123456780</td>
                        </tr>
                    </tbody>
                </table>
                <Modal show={this.state.showModal} onHide={this.handleClose} dialogClassName="modal-lg">
                    <Modal.Header closeButton={this.state.showModal}>
                        <Modal.Title>Platform DO</Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="show-grid">
                        <Container>
                            <Row>
                                <CardColumns>
                                    <Card className="text-center" style={{ width: '100%', height: '9rem' }} >
                                        <Card.Body onClick={this.handleShowModal}>
                                            <Card.Title><img src={require("../../assets/image/digico.png")}></img></Card.Title>
                                        </Card.Body>
                                    </Card>
                                    <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                        <Card.Body>
                                            <Card.Title>Other</Card.Title>
                                        </Card.Body>
                                    </Card>
                                    <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                        <Card.Body onClick={this.handleShowModal}>
                                            <Card.Title>iTruck</Card.Title>
                                        </Card.Body>
                                    </Card>
                                    <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                        <Card.Body>
                                            <Card.Title>Other</Card.Title>
                                        </Card.Body>
                                    </Card>
                                    <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                        <Card.Body onClick={this.handleShowModal}>
                                            <Card.Title><img src={require('../../assets/image/clickargo.png')}></img></Card.Title>
                                        </Card.Body>
                                    </Card>
                                    <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                        <Card.Body>
                                            <Card.Title>Other</Card.Title>
                                        </Card.Body>
                                    </Card>
                                </CardColumns>
                            </Row>

                        </Container>

                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showModalRequest} onHide={this.handleCloseModal} dialogClassName="modal-lg">
                    <Modal.Header closeButton={this.state.showModalRequest}>
                        <Modal.Title>Request SP2</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Tabs defaultActiveKey="Items" transition={false} id="noanim-tab-example">
                            <Tab eventKey="Items" title="Items">
                                <div>
                                    <Form size="sm">
                                        <Form.Row className="align-items-center">
                                            <Form.Label column sm="2" size="sm">
                                                Type Document
                                     </Form.Label>
                                            <Col sm="5">
                                                <Form.Control size="sm" as="select">
                                                    <option></option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Row>
                                        <Form.Row className="align-items-center">
                                            <Form.Label column sm="2" size="sm">
                                                No. SPPB
                                        </Form.Label>
                                            <Col sm="5">
                                                <Form.Control size="sm" type="text" placeholder="SPPB/12/2929292" />
                                            </Col>
                                            <Col sm="2">
                                                <Form.Control size="sm" type="text" placeholder="SPPB" />
                                            </Col>
                                        </Form.Row>
                                        <Form.Row className="align-items-center">
                                            <Form.Label column sm="2" size="sm">
                                                No. Delivery Order
                                        </Form.Label>
                                            <Col sm="5">
                                                <Form.Control size="sm" type="text" placeholder="SPPB/12/2929292" />
                                            </Col>
                                            <Col sm="2">
                                                <Form.Control size="sm" type="text" placeholder="Lunas" />
                                            </Col>
                                        </Form.Row>
                                        <br></br>
                                        <br></br>
                                        <ListGroup>
                                            <ListGroup.Item style={{ backgroundColor: 'red' }}>Data Container</ListGroup.Item>
                                        </ListGroup>
                                        <table className="table table-xs" style={{ textAlign: 'center' }}>
                                            <thead>
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Container Number</th>
                                                    <th scope="col">Container Size</th>
                                                    <th scope="col">Container Seal</th>
                                                    <th scope="col">Container Type</th>
                                                    <th scope="col">Vessel Name</th>
                                                    <th scope="col">Consigne</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><Form.Group controlId="formBasicCheckbox">
                                                        <Form.Check type="checkbox" label="1" />
                                                    </Form.Group></td>
                                                    <td>1234567</td>
                                                    <td>98653312</td>
                                                    <td>98653312</td>
                                                    <td>98653312</td>
                                                    <td>98653312</td>
                                                    <td>DO</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <Form.Row className="align-items-center">
                                            <Form.Label column sm="2" size="sm">
                                                Paid Thru
                                     </Form.Label>
                                            <Col sm="5">
                                                <Form.Control type="password" placeholder="" size="sm" />
                                            </Col>
                                        </Form.Row>
                                        <Form.Row className="align-items-center">
                                            <Form.Label column sm="2" size="sm">
                                                Terminal
                                     </Form.Label>
                                            <Col sm="5">
                                                <Form.Control type="password" placeholder="" size="sm" />
                                            </Col>
                                        </Form.Row>
                                        <div style={{ float: 'right', marginTop: '10px' }}>
                                            <Button variant="primary" style={{ marginRight: '3px' }} size="sm">Process</Button>
                                        </div>
                                    </Form>
                                </div>

                            </Tab>
                            <Tab eventKey="All Items" title="All Items">
                                <ListGroup>
                                    <ListGroup.Item style={{ backgroundColor: 'red' }}>Data Container</ListGroup.Item>
                                </ListGroup>
                                <table className="table table-sm" style={{ textAlign: 'center' }}>
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">No. BL</th>
                                            <th scope="col">No. SPPB</th>
                                            <th scope="col">No. DO</th>
                                            <th scope="col">Status DO</th>
                                            <th scope="col">SP 2</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><Form.Group controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" label="1" />
                                            </Form.Group></td>
                                            <td>123456789</td>
                                            <td>SPPB/12/2929292</td>
                                            <td>-</td>
                                            <td>098765432</td>
                                            <td>-</td>

                                        </tr>
                                        <tr>
                                            <td><Form.Group controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" label="2" />
                                            </Form.Group></td>
                                            <td>123456789</td>
                                            <td>SPPB/12/2929292</td>
                                            <td>-</td>
                                            <td>098765432</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td><Form.Group controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" label="3" />
                                            </Form.Group></td>
                                            <td>123456789</td>
                                            <td>SPPB/12/2929292</td>
                                            <td>-</td>
                                            <td>098765432</td>
                                            <td>-</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <ListGroup>
                                    <ListGroup.Item style={{ backgroundColor: 'red' }}>Data Container</ListGroup.Item>
                                </ListGroup>
                                <table className="table table-sm" style={{ textAlign: 'center' }}>
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Container Number</th>
                                            <th scope="col">Container Size</th>
                                            <th scope="col">Container Seal</th>
                                            <th scope="col">Container Type</th>
                                            <th scope="col">Vessel Name</th>
                                            <th scope="col">Consigne</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><Form.Group controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" label="1" />
                                            </Form.Group></td>
                                            <td>1234567</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>DO</td>
                                        </tr>
                                        <tr>
                                            <td><Form.Group controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" label="2" />
                                            </Form.Group></td>
                                            <td>1234567</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>DO</td>
                                        </tr>
                                        <tr>
                                            <td><Form.Group controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" label="3" />
                                            </Form.Group></td>
                                            <td>1234567</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>98653312</td>
                                            <td>DO</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <Form.Row className="align-items-center">
                                    <Form.Label column sm="2" size="sm">
                                        Paid Thru
                                     </Form.Label>
                                    <Col sm="5">
                                        <Form.Control type="password" placeholder="" size="sm" />
                                    </Col>
                                </Form.Row>
                                <Form.Row className="align-items-center">
                                    <Form.Label column sm="2" size="sm">
                                        Terminal
                                     </Form.Label>
                                    <Col sm="5">
                                        <Form.Control type="password" placeholder="" size="sm" />
                                    </Col>
                                </Form.Row>
                                <div style={{ float: 'right', marginTop: '10px' }}>
                                    <Button variant="primary" style={{ marginRight: '3px' }} size="sm">Process</Button>
                                </div>
                            </Tab>
                        </Tabs>


                    </Modal.Body>
                </Modal>

            </>
        )
    }
}