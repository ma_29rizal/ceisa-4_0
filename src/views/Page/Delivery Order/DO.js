import React, { Component } from 'react';
import { Modal, Button, Card, Form, Row, Col, CardColumns, Container } from 'react-bootstrap';


export default class DO extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false,
            showModalRequest: false,
        }
    }

    handleShow = () => {
        this.setState({
            showModal: !this.state.showModal,
        })
    }

    handleClose = () => {
        this.setState({
            showModal: false,
        })
    }
    handleShowModal = () => {
        this.setState({
            showModalRequest: !this.state.showModalRequest
        })
    }

    handleCloseModal = () => {
        this.setState({
            showModalRequest: false
        })
    }

    render() {
        return (
            <>
                <Button variant="primary" onClick={this.handleShow} size="sm">
                    Add Request
                 </Button>
                <table className="table" style={{ textAlign: 'center' }}>
                    <thead>
                        <tr>
                            <th scope="col">Req Number</th>
                            <th scope="col">BL Number</th>
                            <th scope="col">Forwarder</th>
                            <th scope="col">Shipping Line</th>
                            <th scope="col">Status</th>
                            <th scope="col">Date</th>
                            <th scope="col">No. Delivery Order</th>
                            <th scope="col">Container</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>REQ12334455</td>
                            <td>BL1234567890</td>
                            <td>ABC</td>
                            <td>XYZ</td>
                            <td>PAID</td>
                            <td>23122020</td>
                            <td>23122020</td>
                            <td>Detail</td>
                        </tr>
                    </tbody>
                </table>

                <Modal show={this.state.showModal} onHide={this.handleClose} dialogClassName="modal-lg">
                    <Modal.Header closeButton={this.state.showModal}>
                        <Modal.Title>Platform DO</Modal.Title>
                    </Modal.Header>
                    <Modal.Body  className="show-grid">
                        <Container>
                        <Row>
                        <CardColumns>
                            <Card className="text-center" style={{ width: '100%', height: '9rem'}} >
                                <Card.Body onClick={this.handleShowModal}>
                                    <Card.Title><img src={require('../../assets/image/digico.png')} alt="digico"></img></Card.Title>
                                </Card.Body>
                            </Card>
                            <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                            <Card.Body>
                                    <Card.Title>Other</Card.Title>
                                </Card.Body>
                               
                            </Card>
                            <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                            <Card.Body  onClick={this.handleShowModal}>
                                    <Card.Title><img src={require('../../assets/image/clickargo.png')} alt="clickargo"></img></Card.Title>
                                </Card.Body>
                            </Card>
                            <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                <Card.Body>
                                    <Card.Title>Other</Card.Title>
                                </Card.Body>
                            </Card>
                            <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                <Card.Body>
                                    <Card.Title>Other</Card.Title>
                                </Card.Body>
                            </Card>
                            <Card className="text-center" style={{ width: '100%', height: '9rem' }}>
                                <Card.Body>
                                    <Card.Title>Other</Card.Title>
                                </Card.Body>
                            </Card>
                            </CardColumns>
                        </Row>

                        </Container>
                        
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showModalRequest} onHide={this.handleCloseModal} dialogClassName="modal-lg">
                    <Modal.Header closeButton={this.state.showModalRequest}>
                        <Modal.Title>Add Request</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            <Form size="sm">
                                <Form.Group as={Row} controlId="formPlaintextPassword">
                                    <Form.Label column sm="2" size="sm">
                                        Nomor BL
                                     </Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="password" placeholder="Password" size="sm"/>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Form.Label column sm="2" size="sm">
                                        Tanggal BL
                                    </Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="password" placeholder="Password" size="sm"/>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} >
                                    <Form.Label column sm="2" size="sm">
                                        Forwarder
                                    </Form.Label>
                                    <Col sm="10">
                                        <Form.Control as="select" size="sm">
                                            <option></option>
                                        </Form.Control>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} >
                                    <Form.Label column sm="2" size="sm">
                                        Shipping
                                    </Form.Label>
                                    <Col sm="10">
                                        <Form.Control as="select" size="sm">
                                            <option></option>
                                        </Form.Control>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} >
                                    <Form.Label column sm="2" size="sm">
                                        Container
                                    </Form.Label>
                                    <Col sm="10">
                                        <Form.Control as="select" size="sm">
                                            <option></option>
                                        </Form.Control>
                                    </Col>
                                </Form.Group>
                                <Form.Row className="align-items-center">
                                        <Form.Label column sm="2" size="sm">
                                            Surat Kuasa
                                        </Form.Label>
                                    <Col sm="8">
                                        <Form.Control as="select" size="sm">
                                            <option>File..</option>
                                        </Form.Control>
                                    </Col>
                                    <Col xs="auto">
                                        <Button type="submit" className="mb-2" size="sm" >
                                            Add
                                        </Button>
                                    </Col>
                                </Form.Row>
                                <div style={{ float: 'right', marginTop:'10px' }}>
                                    <Button variant="primary" style={{ marginRight: '3px' }} size="sm">Submit</Button>
                                    <Button variant="primary" onClick={this.handleCloseModal} size="sm">Cancel</Button>
                                </div>

                            </Form>
                        </div>
                    </Modal.Body>
                </Modal>
            </>
        )
    }
}