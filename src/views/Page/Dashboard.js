import React, { useState, useEffect, Fragment, useRef } from "react";
import { Link, withRouter } from "react-router-dom";
import { compose, withProps, lifecycle, withStateHandlers } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer, Marker, InfoWindow } from "react-google-maps";
import { QuickActions } from "../../_metronic/layout/sub-header/quick-actions/QuickActions";
import { Portlet, PortletBody, PortletHeader, PortletHeaderToolbar } from "../../partials/content/Portlet";
import * as Constants from '../Constant';
import * as Helper from '../Constant';
import { createSkeletonProvider } from '@trainline/react-skeletor';
import { createSkeletonElement } from '@trainline/react-skeletor';
import { Button } from 'reactstrap';
import axios from 'axios';

const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");
const MapWithADirectionsRenderer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key="+Constants.googleMapAPI+"&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ width: `100%`, height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withStateHandlers(() => ({
    isOpen: -1,
  }), {
    onToggleOpen: ({ isOpen }) => (index) => ({
      isOpen: index,
    })
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentWillReceiveProps(nextProps) {
      if ((nextProps.directions !== this.props.directions) && (nextProps.markers !== this.props.markers)) {
        this.props.onToggleOpen(-1)
      }
      
    }
  })
)(props =>
  {
    let activeBounds = new window.google.maps.LatLngBounds();
    let newArrayLatLng = [];
    if ( typeof props.markers !== "undefined" ) {
      if ( props.markers.length ) {
        props.markers.map((item, i) => {
          const obj_latlng = {
            lat: parseFloat(item.lat),
            lng: parseFloat(item.lng)
          }
          newArrayLatLng.push(obj_latlng);
        });
        for (var i = 0; i < newArrayLatLng.length; i++) {
          activeBounds.extend(new window.google.maps.LatLng(
            newArrayLatLng[i].lat,
            newArrayLatLng[i].lng
          ));
        }
      }
    }
    return (
      <GoogleMap
        defaultZoom={12}
        defaultCenter={new window.google.maps.LatLng(-6.1566392, 106.84866)}
        ref={map => map && map.fitBounds(activeBounds)}
      >
        {props.directions && <DirectionsRenderer directions={props.directions} />}
        {
          props.markers && props.markers.length > 0 ?
            props.markers.map((item, i) => {
              return (
                <Marker
                  defaultZIndex={999}
                  position={{ lat: parseFloat(item.lat), lng: parseFloat(item.lng) }}
                  icon={{ url: item.icon }}
                  onClick={() => props.onToggleOpen(i)}
                  key={i}
                >
                {props.isOpen === i && <InfoWindow onCloseClick={() => props.onToggleOpen(-1)}>
                  <div>
                    <span className="d-block mb-1"><b>{props.markers[i].title}</b></span>
                    <span className="d-block">{props.markers[i].address}</span>
                    {
                      typeof props.markers[i].driver !== "undefined" ? (
                        <div style={{marginTop: "5px"}}>
                          <span className="d-block mb-1"><b>Driver</b></span>
                          <span className="d-block">{props.markers[i].driver}</span>
                        </div>
                      ): null
                    }
                    {
                      typeof props.markers[i].driver_hp !== "undefined" ? (
                        <div style={{marginTop: "5px"}}>
                          <span className="d-block mb-1"><b>Driver HP</b></span>
                          <span className="d-block">{props.markers[i].driver_hp}</span>
                        </div>
                      ): null
                    }
                  </div>
                </InfoWindow>}
                </Marker>
              );
            })
          : null
        }
      </GoogleMap>
    )
  }
);

function Dashboard(props) {
  const [mapBounds, setMapBounds] = useState(false);
  const [loadingMap, setLoadingMap] = useState(false);
  const [error, setError] = useState(false);
  const [truckings, setTruckings] = useState(null);
  const [loadingTrucks, setLoadingTrucks] = useState(true);
  const [documents, setDocuments] = useState([]);
  const [loadingDocuments, setLoadingDocuments] = useState(true);
  const [pageTruckings, setPageTruckings] = useState(0);
  const [activeTruckID, setActiveTruckID] = useState(null);
  const [sizeTruckings, setSizeTruckings] = useState(10);
  const [bookedDateTruckings, setBookedDateTruckings] = useState('');
  const [npwpTruckings, setNpwpTruckings] = useState('018245894059000');
  const [detail, setDetail] = useState(null);
  const [activeDirections, setActiveDirections] = useState(null);
  const [activeMarkers, setActiveMarkers] = useState([]);
  const [tempMarkers, setTempMarkers] = useState([]);
  const [firstLoad, setFirstLoad] = useState(true);
  const isMounted = useRef(null);

  const [data] = useState([{
    orderId: 'PEB - 4214124',
    truckId: 'TRUCK - 437',
    status: 'Validation',
    tracking: 'On Track - 110 KM to IDMRK',
    progress: 25
  }, {
    orderId: 'PEB - 1239237',
    truckId: 'TRUCK - 110',
    status: 'Waiting for Payment',
    tracking: 'On Track - 51 KM to IDTPP',
    progress: 75
  }, {
    orderId: 'PEB - 2451785',
    truckId: 'TRUCK - 326',
    status: 'Document Confirmation',
    tracking: 'On Track - 72 KM to IDTPP',
    progress: 50
  }, {
    orderId: 'PEB - 1237524',
    truckId: 'TRUCK - 89',
    status: 'SPBB - Release',
    tracking: 'Arrived at IDJBK',
    progress: 100
  }, {
    orderId: 'NPE - 4214123',
    truckId: 'TRUCK - 102',
    status: 'Ready to Export',
    tracking: 'Arrived at IDJBK',
    progress: 100
  }]);
  useEffect(() => {
    // executed when component mounted
    isMounted.current = true;
    return () => {
      // executed when unmount
      isMounted.current = false;
    }
  }, []);
  
  const handleUpdateMap = (item) => {
    setLoadingMap(true);
    const destinationList = item.booking.other_destination
    const destinationLen = destinationList.length
    // const destinationLen = 0;
    let waypoints
    const DirectionsService = new window.google.maps.DirectionsService();
    if (destinationLen > 0) {
      if (destinationLen > 1 && destinationLen !== 0) {
        waypoints = destinationList
                    .filter((item, i) => i !== destinationLen-1)
                    .map(item => {
                      return {
                              location: new window.google.maps.LatLng(parseFloat(item.latitude), parseFloat(item.longitude)),
                              stopover: true
                            }
                    })
      } else {
        waypoints = []
      }
      const destination = new window.google.maps.LatLng(parseFloat(destinationList[destinationLen -1].latitude), parseFloat(destinationList[destinationLen - 1].longitude));
      DirectionsService.route({
        origin: new window.google.maps.LatLng(parseFloat(item.pod_lat), parseFloat(item.pod_lon)),
        destination,
        waypoints,
        travelMode: window.google.maps.TravelMode.DRIVING,
      }, (result, status) => {
        if ( result.routes.length ) {
          var _route = result.routes[0].legs[0];
        }
  
        if (status === window.google.maps.DirectionsStatus.OK) {
  
          const destinationListMarker = destinationList.map((item, index) => {
            return {
                lat: item.latitude,
                lng: item.longitude,
                title: `Destination ${index+1}`,
                address: item.destination,
                icon: "assets/images/destination-icon-marker.png"
              
            }
          }) 
          var newMarkers = [
            {
              title: "POD/Origin",
              lat: item.pod_lat,
              lng: item.pod_lon,
              icon: 'assets/images/from-icon-marker.png',
              address: item.pod
            },
            ...destinationListMarker,
          ];
          setTempMarkers(newMarkers);
          setActiveDirections(result);
          getLiveTracking(item.idRequestBooking, item.container_no, newMarkers);
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    } 
    if (isMounted.current) {
      setLoadingMap(false)
    }
  }

  const handleUpdateMarker = (new_markers) => {
    setActiveMarkers(new_markers);
    setLoadingMap(false);
  }

  useEffect(() => {
    if (pageTruckings || sizeTruckings || bookedDateTruckings || npwpTruckings) {
      if ( firstLoad ) {
        fetchTruckings(npwpTruckings);
      }
      
    }
  }, [pageTruckings, sizeTruckings, bookedDateTruckings, npwpTruckings]);

  useEffect(() => {
    if ( activeTruckID !== null ) {
      //idRequestBooking = "fa5cc3b2-43ba-43ae-9474-81656b2e8692";
      //container_no = "SKLU0748988";
      let splitID = activeTruckID.split(";;;");
      const idRequestBooking = (splitID[0] || null);
      const container_no = (splitID[1] || null);
      if ( idRequestBooking !== null && container_no !== null ) {
        const intervalLiveTracking = setInterval(() => {
            fetch(`${Constants.E_TRUCK_LIVE_TRACKING}?idRequestBooking=${idRequestBooking}&container_no=${container_no}`)
              .then(res => res.text())
              .then(response => {
                const jsonResult = JSON.parse(response);
                var markerlist = tempMarkers;
                if ( typeof jsonResult.result !== "undefined" ) {
                  if ( jsonResult.result === "true" ) {
                    let objectTruck = {
                      title: "Truck",
                      lat: jsonResult.datacontainertracking[0].truck_lat,
                      lng: jsonResult.datacontainertracking[0].truck_lon,
                      icon: 'assets/images/truck-icon.png',
                      address: jsonResult.datacontainertracking[0].truck,
                      driver: jsonResult.datacontainertracking[0].driver,
                      driver_hp: jsonResult.datacontainertracking[0].driver_hp
                    };
                    if ( markerlist.length < 4 )
                      markerlist.push(objectTruck);
                  }
                }
                handleUpdateMarker(markerlist);
              })
              .catch(error => {
                handleUpdateMarker(tempMarkers);
                console.log(`parsing data failed`, error);
              });
        }, 2000);
        return () => clearInterval(intervalLiveTracking);
      }
    }
  }, [activeTruckID]);

  const getLiveTracking = (idRequestBooking, container_no, inputMarkers) => {
    let currentID = idRequestBooking+";;;"+container_no;
    setActiveTruckID(currentID);
  };
  async function fetchTruckings(npwp) {
    const res = await fetch(Constants.API_TRUCKING_LOG_STATUS + '?page=' + pageTruckings + '&size=' + sizeTruckings + '&bookedDate' + bookedDateTruckings + '=&npwp=' + npwp);
        res
          .json()
          .then(res => {
            setTruckings(res)
            setLoadingTrucks(false);
            if ( res.content.length > 0 ) {
              setDetail({ type: 'truckings', show: 'more', item: res.content[0]  })

              const intervalGoogleMaps = setInterval(() => {
                if (window.google && window.google.maps) {
                  handleUpdateMap(res.content[0])
                  clearInterval(intervalGoogleMaps)
                }
              }, 100)
            }
          })
          .catch(err => setError(err));
  }  
  useEffect(() => {
    if (npwpTruckings) {
      if ( firstLoad ) {
        fetchDocuments(npwpTruckings);
      }
      
    }
  }, [npwpTruckings]);
  
  async function fetchDocuments(npwp) {
    const res = await fetch(Constants.API_DOCUMENTS + npwp);
        res
          .json()
          .then(res => {
            setLoadingDocuments(false);
            if ( typeof res["Data Active Documents"] !== "undefined" )
              setDocuments(res["Data Active Documents"])
          })
          .catch(err => setError(err));
  }
  

  const handleProgressBg = (value) => {
    if (isNaN(value)) {
      if (value === 'Arrived Destination') {
        return {
          backgroundColor: 'success',
          percentage: 100
        }
      } else {
        return {
          backgroundColor: 'danger',
          percentage: 25
        }
      }
    } else {
      if (value > 75) {
        return {
          backgroundColor: 'success',
          percentage: value
        }
      } else if (value > 50) {
        return {
          backgroundColor: 'info',
          percentage: value
        }
      } else if (value > 25) {
        return {
          backgroundColor: 'warning',
          percentage: value
        }
      } else {
        return {
          backgroundColor: 'danger',
          percentage: value
        }
      }
    }
  }
  const TrList = createSkeletonElement('tr', 'pending_line');
  const SpanList = createSkeletonElement('span', 'pending_text');
  const TruckingList = truckings ? truckings: {"content": [undefined, undefined, undefined, undefined, undefined]};
  const DocumentsList = documents && documents.length ? documents: [undefined, undefined, undefined, undefined, undefined];

  //clickargo dev
  const [arrVesselList, setArrVesselList] = useState([]);
  
  useEffect(() => {
    axios
        .get(Constants.NLE_VESSEL_LIST+'?npwp='+'018245894059000')
        .then(response => {
          console.log('success get job vessel list')
          const filterVesselIdPlatform = response.data.filter(vessel => vessel.id_platform == 'PL002')
          const filterJobNumber = filterVesselIdPlatform.filter(vessel => vessel.job_number != '245345345')
          setArrVesselList(filterJobNumber)
        })
        .catch(error => {
          console.log('failed get job vessel list')
          console.log(error)
        })
  }, [])
  
  const clickargoVesselDetail = (event, job_number) => {
    event.preventDefault();

    const headers = {
      'Content-Type': 'application/json',
  }

    axios
      .post(Constants.CLICKARGO_FIND_ORDER_BY_JOB_NUMBER, {
        job_number: job_number
      }, {
        headers: headers
      })
      .then(response => {
        console.log('success get detail vessel')
          props.history.push({
            pathname: "/vessel/detail",
            state: {
              arrVesselDetail: response.data.data
            }
          })
      })
      .catch(error => {
        console.log(error.response.data.message)
      })
  }
  //clickargo dev


  const containerTableComponent = (containerList) => {
    return(
      <table 
      className="table table-bordered table1"
      >
        <thead>
          <tr>
            <th className="keyfield">NUMBER</th>
            <th className="keyfield">TYPE</th>
            <th className="keyfield">SIZE</th>
          </tr>    
        </thead>
        <tbody>
        {
          containerList.map((item, i) => {
            return (
              <tr key={i}>
                <td className="size">{item.container_no ? item.container_no: " " }</td>
                <td>
                  <span className="keyfield">Type</span><br/>
                  {item.container_type !== "null"? <span>{item.container_type}</span>: <span>-</span>}<br/>
                  {item.over_height? (<React.Fragment><span className="keyfield">Over Height</span><br/><span>{item.over_height}</span></React.Fragment>): (null)}
                  {item.over_width? (<React.Fragment><span className="keyfield">Over Width</span><br/><span>{item.over_width}</span></React.Fragment>): (null)}
                  {item.over_length? (<React.Fragment><span className="keyfield">Over Length</span><br/><span>{item.over_length}</span></React.Fragment>): (null)}
                  {item.over_weight? (<React.Fragment><span className="keyfield">Over Weight</span><br/><span>{item.over_weight}</span></React.Fragment>): (null)}
                  {item.over_temperature? (<React.Fragment><span className="keyfield">Temperature</span><br/><span>{item.over_temperature}</span></React.Fragment>): (null)}
                  <span className="keyfield">Dangerous Type</span><br/>
                  {item.dangerous_type? <span>{item.dangerous_type}</span>:<span>-</span> }<br/>
                  {item.dangerous_material? (<React.Fragment><span className="keyfield">Dangerous Material</span><br/>{item.dangerous_material}</React.Fragment>): (null)}
                </td>
                <td className="size">{item.container_size ? <span>{item.container_size}</span>: " " }</td> 
              </tr>
            )
          })
        }
        </tbody>
      </table>
    )
  }
 
  const desList = detail ? detail.item.booking.other_destination : null
  const desLen = desList ? desList.length : 0

  return (
      <>
        <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
          <div className="kt-container kt-container--fluid">
            <div className="kt-subheader__main">
              <h3 className="kt-subheader__title">Dashboard</h3>
            </div>
            <div className="kt-subheader__toolbar">
              <button className="btn kt-subheader__btn-secondary">Today</button>
              <button className="btn kt-subheader__btn-secondary">Month</button>
              <button className="btn kt-subheader__btn-secondary">Year</button>
              <button className="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Select dashboard daterange">
                <span className="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title"></span>&nbsp;
                <span className="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Nov 1 - Nov 30</span>
                <i className="flaticon2-calendar-1"></i>
              </button>
              <QuickActions />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-7 col-sm-12 col-xs-12" style={detail ? {marginBottom: "20px"}: {display:"none"}}>
            <Portlet className="kt-portlet--border-bottom-brand" style={{height: "100%"}}>
              <PortletBody fluid={true} className="p-0 overflow-hidden" style={{height: detail? "430px": "100%", position: "relative"}}>
                {
                  loadingMap ? (
                    <div className="global_overlay text-center">
                      <p><img src="/assets/images/signal.gif" /></p>
                      <p>Tracking Truck ...</p>
                    </div>
                  ): null
                }
                <MapWithADirectionsRenderer directions={activeDirections} markers={activeMarkers} handleComplete={setLoadingMap.bind(this)} />
              </PortletBody>
            </Portlet>
          </div>
          {detail ? (
            <div className="col-md-5 col-sm-12 col-xs-12">
                 <Portlet fluidHeight={true}>
                <PortletHeader
                  title={(detail.type === 'truckings' ? 'Trucking' : 'Booking') + ' Details'}
                />
                {detail.type === 'truckings' ? (
                  <div className="kt-portlet__body pt-2">
                    <table className="detailTable">
                      <tbody>
                        <tr>
                          <td style={{width: "150px"}} className="keyfield">ORDER-ID</td>
                          <td>{detail.item.idRequestBooking}</td>
                        </tr>
                        <tr>
                          <td className="keyfield">TRUCK PROVIDER</td>
                          <td>{detail.item.nama_platform}</td>
                        </tr>
                        <tr>
                          <td className="keyfield">ORIGIN</td>
                          <td>{detail.item.pod}</td>
                        </tr>
                        {Array.isArray(desList) && desLen > 0 ?
                          desList.map((item, i) => {
                            if (i+1 !== desLen) return (
                              <tr key={i}>
                                <td className="keyfield">WAYPOINT {desLen == 2? "": i+1}</td>
                                <td>{item.destination}</td>
                              </tr>
                            )
                            return (
                              <tr key={i}>
                                <td className="keyfield">DESTINATION</td>
                                <td>{item.destination}</td>
                              </tr>
                            )
                          }) 
                        : 
                          <tr>
                            <td className="keyfield">DESTINATION</td>
                            <td></td>
                          </tr>
                        }
                        <tr>
                          <td className="keyfield">TRUCK PLATE NO</td>
                          <td>{detail.item.truckPlateNo}</td>
                        </tr>
                        <tr>
                          <td colSpan={2} style={{width: "150px"}} className="keyfield">CONTAINER</td>
                        </tr>
                        <tr>
                          <td  colSpan={2}>
                            {containerTableComponent(detail.item.booking.container)}
                          </td>
                        </tr>
                        <tr>
                          <td className="keyfield">STATUS</td>
                          <td>{detail.item.status}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                ) : null}
              </Portlet>
            </div>
          ) : null}
          <div className="col-sm-12 col-md-12 col-lg-4">
            <Portlet className="kt-portlet--border-bottom-brand">
              <PortletHeader
                title={loadingDocuments ? 'Active Documents': 'Active Documents (' + (documents.length ? documents.length: 0) + ')'}
                toolbar={
                  <PortletHeaderToolbar>
                    <Link to="/customs" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                  </PortletHeaderToolbar>
                }
              />
              <PortletBody style={{height:"278px"}}>
                <table className='detailTable'>
                  <tbody>
                    {
                      (DocumentsList && DocumentsList.length && DocumentsList[0] !== undefined) || loadingDocuments ? 
                        DocumentsList.map((item, index) => {
                            if ( index < 5 ) {
                              const DataDocuments = (props) => (
                                <TrList key={`skeleton-documents-${index}`} style={{height: '3.5rem'}} className='border-bottom'>
                                  <td><SpanList>{props.item.PIB || '-'} - {props.item.NO_PIB || '-'}</SpanList></td>
                                  <td className="text-right"><SpanList>{props.item.STATUS}</SpanList></td>
                                </TrList>
                              );
                              const dummyData = {
                                item: {
                                  PIB: '___________',
                                  NO_PIB: '___________',
                                  STATUS: '________',
                                }
                              };
                              const pendingDocuments = (props) => props.item === undefined;
                              const ViewDocuments = createSkeletonProvider(
                                dummyData, 
                                pendingDocuments,
                                () => ({
                                  color: '#ddd'
                                })
                              )(DataDocuments);
                              
                              return <ViewDocuments item={item} key={index} />;
                            }
                          })
                      : (
                        <tr style={{height: '3.5rem'}}>
                          <td>Data is not available</td>
                        </tr>
                      )
                    }
                    
                  </tbody>
                </table>
              </PortletBody>
            </Portlet>
          </div>
          <div className="col-sm-12 col-md-12 col-lg-4">
            <Portlet className="kt-portlet--border-bottom-brand">
              <PortletHeader
                title={'Active DO (10)'}
                toolbar={
                  <PortletHeaderToolbar>
                    <Link to="/chooseprinciple" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                  </PortletHeaderToolbar>
                }
              />
              <PortletBody style={{height:"278px"}}>
                <table className='detailTable'>
                  <tbody>
                    {data.map((item, index) => {
                      const progress = handleProgressBg(item.progress)

                      return (
                        <tr key={index} style={{height: '3.5rem'}} className='border-bottom'>
                          <td>{(item.truckId).replace('TRUCK', 'DO')}</td>
                          <td className="text-right">{item.tracking}</td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
                
              </PortletBody>
            </Portlet>
          </div>
          { 
            (TruckingList && TruckingList.content.length && TruckingList.content[0] !== undefined) || loadingTrucks ? (
            <div className="col-sm-12 col-md-12 col-lg-4">
              <Portlet className="kt-portlet--border-bottom-brand">
                <PortletHeader
                  title={loadingTrucks ? 'Active Trucks': 'Active Trucks (' + truckings.totalElements + ')'}
                  toolbar={
                    <PortletHeaderToolbar>
                      <Link to="/trucking" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                    </PortletHeaderToolbar>
                  }
                />
                <PortletBody style={{height:"278px"}}>
                  <table className='detailTable'>
                    <tbody>
                      {
                        (
                          TruckingList.content || []).map((item, index) => {
                            if (index < 5) {
                              const DetailItem = (props) => (
                                <TrList key={`skeleton-${index}`} style={{height: '3.5rem', cursor: 'pointer'}} className='border-bottom' onClick={() => {
                                  setDetail({ type: 'truckings', show: 'less', item: item })
                                  handleUpdateMap(item)
                                  window.scrollTo(0, 0)
                                }}>
                                  <td><SpanList>{props.item.nama_platform || '-'} - {props.item.truckPlateNo || '-'}</SpanList></td>
                                  <td className="text-right"><SpanList>{props.item.status}</SpanList></td>
                                </TrList>
                              );
                              const dummyData = {
                                item: {
                                  nama_platform: '___________',
                                  truckPlateNo: '___________',
                                  status: '________',
                                }
                              };
                              const pendingTrucking = (props) => props.item === undefined;
                              const ViewTrucking = createSkeletonProvider(
                                dummyData, 
                                pendingTrucking,
                                () => ({
                                  color: '#ddd'
                                })
                              )(DetailItem);
                              
                              return <ViewTrucking item={item} key={index} />;
                            }
                          }
                        )
                      }
                    </tbody>
                  </table>
                  
                </PortletBody>
              </Portlet>
            </div>
          ) : (
            <div className="col-sm-12 col-md-12 col-lg-4">
              <Portlet className="kt-portlet--border-bottom-brand">
                <PortletHeader
                  title={'Active Trucks (0)'}
                  toolbar={
                    <PortletHeaderToolbar>
                      <Link to="/trucking" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                    </PortletHeaderToolbar>
                  }
                />
                <PortletBody style={{height:"278px"}}>
                  <table className='detailTable'>
                    <tbody>
                      <tr style={{height: '3.5rem'}}>
                        <td>Data is not available</td>
                      </tr>
                    </tbody>
                  </table>
                  
                </PortletBody>
              </Portlet>
            </div>
          )}
        </div>

        {/* vessel list */}
        <div className="col-sm-12 col-md-12 col-lg-5">
            <Portlet className="kt-portlet--border-bottom-brand">
              <PortletHeader
                title='Active Vessels (80)'
                toolbar={
                  <PortletHeaderToolbar>
                    <Link to="/vessel/list" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                  </PortletHeaderToolbar>
                }
              />
              <PortletBody style={{height:"278px"}}>
                <table className='detailTable'>
                  <tbody>
                    {arrVesselList.sort((a, b) => b.id_job_vessel - a.id_job_vessel).slice(0, 5).map((inputVesselList, indexVesselList) => (
                      <Fragment key={indexVesselList}>
                        <tr>
                          <td>{inputVesselList.job_number}</td>
                          <td>
                            <span className="ck-waiting-status">{inputVesselList.status}</span>
                          </td>
                          <td className="text-right">
                            <div>
                              <Button
                                type="button"
                                color="default" 
                                className="clickargo-button"
                                onClick={event => clickargoVesselDetail(event, inputVesselList.job_number)}
                              >
                                  <img src={window.location.origin + '/assets/images/Clickargo2.png'} /><span>Details</span></Button >
                            </div>
                          </td>
                        </tr>
                      </Fragment>
                    ))}
                  </tbody>
                </table>
                
              </PortletBody>
            </Portlet>
          </div>
      </>
    )
}

export default withRouter(Dashboard)