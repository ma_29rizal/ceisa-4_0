import React, { Component } from 'react';
import { Tab, Tabs, Form, Col, Button } from 'react-bootstrap';
import Vessel from './Vessel';

export default class Documents extends Component {
    constructor() {
        super()
        this.state = {
            delivery: {},
            customs: {},
            sp2: {},
            key: 1
        }
        this.handleSelect = this.handleSelect.bind(this)
    }

    showData(data) {
        console.log(data)
        this.setState({
            key: data
        })
    }

    handleSelect(key) {
        this.setState({
            key
        })
    }
    render() {
        return (
            <>
                <div>
                    <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="Search">
                            <Vessel />
                        </Tab>
                        <Tab eventKey={2} title="Result">
                           
                        </Tab>
                    </Tabs>
                </div>
            </>
        )
    }
}