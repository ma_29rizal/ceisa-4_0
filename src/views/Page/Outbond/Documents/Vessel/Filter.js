import React, { Component } from 'react';
import { Card, Button } from 'reactstrap';
import Select from 'react-select'
import axios from 'axios'
import moment from 'moment'
import * as Constants from '../../../../Constant'

var today = new Date();
var todayFormat = moment(today).format('YYYY-MM-DD')

const initialState = {
    get_carrier_object: [],
    get_service_object: [],
    filter_carrier_name: '',
    filter_service_name: '',
    date_of_shipment: todayFormat,
    duration: '+6 week',
    durationLabel: '6 Weeks',
    type: 'filter',
    button: 1
}

class Filter extends Component {

    constructor(props){
        super(props)

        this.state = initialState
    }

    componentDidMount() {
        axios
            .get(Constants.CLICKARGO_CARRIER_SERVICE_LIST)
            .then(response => {
                //save carrier object to state
                this.setState({get_carrier_object:Object.values(response.data.data)[0]})
                this.setState({get_service_object:Object.values(response.data.data)[1]})
            })
    }

    changeHandlerCarrier = e => {
        this.setState({
            filter_carrier_name: e.value,
        })
    }

    changeHandlerService = e => {
        this.setState({
            filter_service_name: e.value,
        })
    }

    changeHandlerDateOfShipment = e => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }

    changeHandlerDuration = e => {
        this.setState({
            duration: e.value,
        })
    }

    submitHandler = e => {
        e.preventDefault();

        //reset
        if (this.state.button === 1) {

            this.props.parentCallbackType(this.state.type);
            this.props.parentCallbackCarrierName(this.state.filter_carrier_name);
            this.props.parentCallbackServiceName(this.state.filter_service_name);
            this.props.parentCallbackDateOfShipment(this.state.date_of_shipment);
            this.props.parentCallbackDuration(this.state.duration);
        }
        //apply
        if (this.state.button === 2) {
    
            this.props.parentCallbackType(this.state.type);
            this.props.parentCallbackCarrierName(this.state.filter_carrier_name);
            this.props.parentCallbackServiceName(this.state.filter_service_name);
            this.props.parentCallbackDateOfShipment(this.state.date_of_shipment);
            this.props.parentCallbackDuration(this.state.duration);
            
        }
    }

    render() {

        let { 
                filter_carrier_name, 
                filter_service_name, 
                date_of_shipment, 
                duration, durationLabel, 
            } = this.state
        
        let optionsCarrier = Object.keys(this.state.get_carrier_object).map(obj => {
            return {value: obj, label: obj}
        })

        let optionsService = Object.keys(this.state.get_service_object).map(obj => {
            return {value: obj, label: obj}
        })

        let optionsDuration = [
            {value: '+6 week', label: '6 Weeks'}
        ]

        return (
            <div>
                <div>
                    <hr style={{ border: "10px solid #f3f3f7" }} />
                </div>

                <div className="kt-portlet__head">
                    <div className="kt-portlet__head-label">
                        <h3 className="kt-portlet__head-title">
                            <span>
                                <label className="kt-font-boldest"> Filter</label>
                            </span>
                        </h3>
                    </div>
                </div>

                <div className="kt-portlet__body">
                    <div className="kt-widget15">
                        <form onSubmit={this.submitHandler}>
                            <div className="row">
                                <div className="col-md-2">
                                    <h6>Agent Name</h6>
                                    <Select
                                        value={{ label: filter_carrier_name }}
                                        options={optionsCarrier}
                                        onChange={this.changeHandlerCarrier} />
                                </div>
                                <div className="col-md-2">
                                    <h6>Service Name</h6>
                                    <Select
                                        value={{ label: filter_service_name }}
                                        options={optionsService}
                                        onChange={this.changeHandlerService} />
                                </div>
                                <div className="col-md-2">
                                    <h6>Shipment Schedule</h6>
                                    <input
                                        value={date_of_shipment}
                                        name="date_of_shipment"
                                        type="date"
                                        className="form-control"
                                        onChange={this.changeHandlerDateOfShipment} />
                                </div>
                                <div className="col-md-2">
                                    <h6>Duration</h6>
                                    <Select
                                        value={{ label: durationLabel, value: duration }}
                                        options={optionsDuration}
                                        onChange={this.changeHandlerDuration} />
                                </div>
                                <div className="col-md-4">
                                    <Button
                                        onClick={() => (this.setState({
                                            button: 1,
                                            filter_carrier_name: '',
                                            filter_service_name: '',
                                            date_of_shipment: todayFormat,
                                            duration: '+6 week',
                                            durationLabel: '6 Weeks',
                                            type: ''
                                        }))}
                                        type="submit"
                                        style={{ marginTop: 25 }}
                                        color="default"
                                        size="md"
                                        value="reset">Reset</Button> &nbsp;
                                <Button
                                        onClick={() => (this.setState({
                                            button: 2,
                                            type: 'filter'
                                        }))}
                                        type="submit"
                                        style={{ marginTop: 25 }}
                                        color="primary"
                                        size="md"
                                        value="apply">Apply</Button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
       )
    }
}

export default Filter