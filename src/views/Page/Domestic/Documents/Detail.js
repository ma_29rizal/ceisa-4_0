import React,{Component, Fragment} from 'react';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css"

class Detail extends Component{
    constructor(props){
        super(props)
        this.state = {
            profile:{},
            customers:[],
            customersSelect:[],
            receiver:{
                state:{},
                city:{}
            },
            stuffingDate:new Date()
        }
        this.fetchProfile();
        this.fetchCustomers();
        this.changeReceiver = this.changeReceiver.bind(this);
    }

    fetchProfile = async ()=> {
        await fetch("https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/profile")
            .then(response => response.json())
            .then(data =>
                this.setState({profile: data})
            );
    }

    changeReceiver = (reactSelectEvent)=>{
        let value = reactSelectEvent.value;
        let receiver = this.state.customers.filter(cus=>{
            return cus.id===parseInt(value);
        })[0];

        this.setState({receiver:receiver});
    }

    fetchCustomers = async ()=> {
        await fetch("https://my-json-server.typicode.com/mudiadamz/jsonplaceholder2/customers")
            .then(response => response.json())
            .then(data => {
                let customersSelect = data.map(row=>{return {value:row.id,label:row.name}});
                this.setState({customersSelect: customersSelect})
                this.setState({customers: data})
            });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        // this.props.history.push('/prahuhub/muatan');
    }

    render(){
        const breadcrump = [
            {
                label: 'Register',
                link: null
            }
        ];
        return(
            <>
                {/*{this.contentSp2Header(breadcrump)}*/}
                <form autoComplete={"off"} onSubmit={this.handleSubmit} method="post">
                    <div className="kt-portlet kt-portlet--height-fluid">
                        <div className="kt-portlet__head">
                            <div className="kt-portlet__head-label">
                                <h3 className="kt-portlet__head-title">
                                    Pengirim Barang (Shipper)
                                </h3>
                            </div>
                        </div>
                        <Fragment>
                            <div className="kt-portlet__body">
                                <div className="kt-widget15">
                                    <div className="form-group">
                                        <label>Nama Perusahaan</label>
                                        <input
                                            value={this.state.profile.companyName || ''}
                                            readOnly={true}
                                            className={"form-control"}/>
                                    </div>
                                </div>
                            </div>
                        </Fragment>
                    </div>

                    <div className="kt-portlet kt-portlet--height-fluid">
                        <div className="kt-portlet__head">

                            <div className="kt-portlet__head-label">
                                <h3 className="kt-portlet__head-title">
                                    Penerima Barang (Consignee)
                                </h3>
                            </div>
                        </div>
                        <Fragment>
                            <div className="kt-portlet__body">
                                <div className="kt-widget15">

                                    <div className="form-group">
                                        <label>Pilih Penerima Barang</label>
                                        <Select
                                            onChange={this.changeReceiver}
                                            name="receiverId"
                                            // value={this.state.customers.filter(option => option.id === this.state.receiver.id)}
                                            placeholder="Pilih penerima"
                                            options={this.state.customersSelect}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <label>Nama Perusahaan</label>
                                        <input value={this.state.receiver.name||''} className={"form-control"} disabled={true} />
                                    </div>

                                    <div className="form-group">
                                        <label>Nomor Telepon /Whatsapp</label>
                                        <input value={this.state.receiver.phone||''} className={"form-control"} disabled={true}/>
                                    </div>

                                    <div className="form-group">
                                        <label>Provinsi</label>
                                        <input value={this.state.receiver.state.name||''} className={"form-control"} disabled={true}/>
                                    </div>

                                    <div className="form-group">
                                        <label>Kota</label>
                                        <input value={this.state.receiver.city.name||''} className={"form-control"} disabled={true}/>
                                    </div>

                                    <div className="form-group">
                                        <label>Alamat</label>
                                        <textarea value={this.state.receiver.address||''} className={"form-control"} disabled={true}/>
                                    </div>

                                </div>
                            </div>
                        </Fragment>
                    </div>

                    <div className="kt-portlet kt-portlet--height-fluid">
                        <Fragment>
                            <div className="kt-portlet__body">
                                <div className="kt-widget15">
                                    <div className="form-group">
                                        <table className="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th style={{width:"20%"}}>Jumlah Kontainer Yang Dipesan</th>
                                                <th>Deskripsi Umum Barang</th>
                                                <th style={{width:"20%"}}>Permintaan Tanggal Stuffing</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><input
                                                    placeholder="Max 9"
                                                    type="number"
                                                    className="form-control"
                                                    min={1}
                                                    max={9} /></td>
                                                <td><input className="form-control"/></td>
                                                <td><DatePicker
                                                    className="form-control"
                                                    selected={this.state.stuffingDate}
                                                    onChange={date => this.setState({stuffingDate: date})} /></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-default ml-2" type="button" 
                                        // onClick={e=>
                                        //     this.props.history.push('/prahuhub/hasil')}
                                            >
                                            <i className="flaticon2-back"/> Back
                                        </button>
                                        <button type="submit" className={"btn btn-info ml-2"}>
                                            <i className="flaticon-paper-plane"/> Next</button>
                                    </div>
                                </div>
                            </div>
                        </Fragment>
                    </div>
                </form>
            </>
        )
    }

}

export default Detail;