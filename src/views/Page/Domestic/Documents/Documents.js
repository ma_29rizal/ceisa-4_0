
import React, { Component } from 'react';
import { Tab, Tabs, Form, Col, Button } from 'react-bootstrap';
import Cari from './Cari'
import Hasil from './Hasil'
import Detail from './Detail'
import Muatan from './Muatan'
import Payment from './Payment'



export default class Documents extends Component {
    constructor() {
        super()
        this.state = {
            delivery: {},
            customs: {},
            sp2: {},
            key: 1
        }
        this.handleSelect = this.handleSelect.bind(this)
    }

    showData(data) {
        console.log(data)
        this.setState({
            key: data
        })
    }

    handleSelect(key) {
        this.setState({
            key
        })
    }
    render() {
        return (
            <>
                <div>
                    <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="Search">
                            <Cari />
                        </Tab>
                        <Tab eventKey={2} title="Result">
                            <Hasil/>
                        </Tab>
                        <Tab eventKey={3} title="Shipper">
                            <Detail/>
                        </Tab>
                        <Tab eventKey={4} title="Consigne">
                           
                        </Tab>
                        <Tab eventKey={5} title="Daftar Muatan">
                            <Muatan/>
                        </Tab>
                        <Tab eventKey={6} title="Payment">
                            <Payment/>
                        </Tab>
                    </Tabs>
                    
                </div>
            </>
        )
    }
}