import React,{Component, Fragment} from 'react';
import { Accordion, Card, FormLabel } from "react-bootstrap";
import { Modal,ModalBody,ModalHeader} from "reactstrap";

class Payment extends Component{
    constructor(props){
        super(props)
        this.state = {
            vessel : {},
            paymentMethods : [],
        }
        this.fetchVessels();
        this.fetchPaymentMethods();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    fetchVessels = async ()=> {
        await fetch("https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/vessels/1")
            .then(response => response.json())
            .then(data =>
                this.setState({vessel: data})
            );
    }

    fetchPaymentMethods = async ()=> {
        await fetch("https://my-json-server.typicode.com/mudiadamz/jsonplaceholder2/paymentMethods")
            .then(response => response.json())
            .then(data =>
                this.setState({paymentMethods: data})
            );
    }

    handleSubmit(event) {
        event.preventDefault();

        let data = new FormData(event.target);
        data.id = "";
        this.setState({disabled: true});
        fetch('https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/users', {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                this.setState({disabled: false, alertShow: true});
                // this.props.history.push('/prahuhub/payment');
            });
    }

    render(){
        const breadcrump = [
            {
                label: 'Register',
                link: null
            }
        ];

        return(
            <>
                <form autoComplete={"off"} onSubmit={this.handleSubmit} method="post">
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="kt-portlet kt-portlet--height-fluid">
                                <div className="kt-portlet__head">
                                    <div className="kt-portlet__head-label">
                                        <h3 className="kt-portlet__head-title">
                                            Metode Pembayaran
                                        </h3>
                                    </div>
                                </div>
                                <div className="kt-portlet__body">
                                    <div className="form-group">
                                        <div className="text-center">
                                            <input
                                                name="rsebagai" type="radio" id="rsbagai1"/>
                                            <label htmlFor="rsbagai1" className="ml-1">TUNAI</label>
                                            <input
                                                name="rsebagai" type="radio" id="rsbagai2" className="ml-3" />
                                            <label htmlFor="rsbagai2" className="ml-1">KREDIT</label>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <h6>Vitual Account</h6>
                                        <p>Anda dapat membayar dengan transfer melalui ATM, Internet Banking & Mobile Banking</p>
                                    </div>
                                    <div className="form-group">

                                        <Accordion >
                                            <ul className="list-group">{this.state.paymentMethods.map(bank=>(
                                                <Fragment key={bank.id}>
                                                    <li className="list-group-item d-flex justify-content-between align-items-center">
                                                        <Accordion.Toggle as={FormLabel}
                                                                          variant="default"
                                                                          eventKey=
                                                                              {bank.id}>
                                                            {bank.label}
                                                        </Accordion.Toggle>

                                                        <span className="badge badge-primary badge-pill">
                                                            <i className="flaticon2-right-arrow icon-sm"/>
                                                        </span>

                                                    </li>
                                                    <Accordion.Collapse
                                                        eventKey={bank.id} >
                                                        <li className="pt-2 pb-2 list-group-item">
                                                            {bank.virtual}
                                                        </li>
                                                    </Accordion.Collapse>
                                                </Fragment>
                                            ))
                                            }
                                            </ul>

                                        </Accordion>


                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="kt-portlet kt-portlet--height-fluid">
                                <div className="kt-portlet__head">
                                    <div className="kt-portlet__head-label">
                                        <h3 className="kt-portlet__head-title">
                                            Informasi Pengiriman
                                        </h3>
                                    </div>
                                </div>
                                <div className="kt-portlet__body">
                                    <div className="form-group text-center">
                                        {
                                            "undefined"===typeof this.state.vessel.vessel ?"":
                                                <>
                                                    <p>
                                                        <i className="flaticon2-lorry mr-2"/>
                                                        { this.state.vessel.vessel.origin } -  { this.state.vessel.vessel.departure }</p>
                                                    <p>
                                                        <i className="flaticon2-lorry mr-2"/>
                                                        { this.state.vessel.vessel.destination } -  { this.state.vessel.vessel.arrival }</p>
                                                </>
                                        }
                                    </div>
                                    <div className="form-group">
                                        <table className="table table-bordered">
                                            <tbody>
                                            {
                                                "undefined" === typeof this.state.vessel.partner ? "" :
                                                    <tr>
                                                        <td>Nama Partner</td>
                                                        <td>{this.state.vessel.partner.name}</td>
                                                    </tr>
                                            }
                                            <tr>
                                                <td>Jumlah Kontainer Dipesan</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kontainer</td>
                                                <td>{this.state.vessel.containerType||''}</td>
                                            </tr>
                                            <tr>
                                                <td>Pelayanan</td>
                                                <td>{this.state.vessel.serviceType||''}</td>
                                            </tr>
                                            {
                                                "undefined" === typeof this.state.vessel.vessel ? "" :
                                                    <>
                                                        <tr>
                                                            <td>Nama Kapal</td>
                                                            <td>{this.state.vessel.vessel.name || ''}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Voyage</td>
                                                            <td>{this.state.vessel.vessel.voyage||''}</td>
                                                        </tr>
                                                    </>
                                            }
                                            <tr>
                                                <td>Closing Time</td>
                                                <td>{this.state.vessel.closing||''}</td>
                                            </tr>
                                            <tr>
                                                <td>Permintaan Tanggal Stuffing</td>
                                                <td>{this.state.vessel.stuffing||''}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12 text-center">
                            <button type="button" className="btn btn-default" 
                            // onClick={e=>
                            //     this.props.history.push('/prahuhub/muatan')}
                                >
                                <i className="flaticon-cancel"/> Back</button>
                            <button type="submit" className="btn btn-success ml-3">
                                <i className="flaticon-paper-plane"/> Finish</button>
                        </div>
                    </div>
                </form>

                <Modal show={true}>
                    <ModalHeader>Header</ModalHeader>
                    <ModalBody>Body</ModalBody>
                </Modal>
            </>
        )
    }

}

export default Payment;