/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import { ReactComponent as FilePlusIcon } from "../../assets/layout-svg-icons/File-plus.svg";

class QuickActionsDropdownToggle extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    this.props.onClick(e);
  }

  render() {
    return (
      <a
        href="#"
        onClick={this.handleClick}
        id="kt_dashboard_daterangepicker"
        className="btn btn-icon"
        ref={this.props.innerRef}
      >
        {" "}
        <FilePlusIcon className="kt-svg-icon kt-svg-icon--success kt-svg-icon--md" />
      </a>
    );
  }
}

const AsQuickActionsDropdownToggle = React.forwardRef((props, ref) => (
  <QuickActionsDropdownToggle innerRef={ref} {...props} />
))

export class QuickActions extends React.Component {
  render() {
    return (
      <>
        <OverlayTrigger
          placement="left"
          overlay={<Tooltip id="quick-actions-tooltip">Quick actions</Tooltip>}
        >
          <Dropdown className="dropdown-inline" drop="down" alignRight>
            <Dropdown.Toggle
              as={AsQuickActionsDropdownToggle}
              id="dropdown-toggle-quick-actions-subheader"
            />

            <Dropdown.Menu className="dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
              <ul className="kt-nav">
                <li className="kt-nav__head">
                  Add anything or jump to:
                  <i
                    className="flaticon2-information"
                    data-toggle="kt-tooltip"
                    data-placement="right"
                    title=""
                    data-original-title="Click to learn more..."
                  />
                </li>
                <li className="kt-nav__separator" />
                <li className="kt-nav__item">
                  <a href="#" className="kt-nav__link">
                    <i className="kt-nav__link-icon flaticon2-document" />
                    <span className="kt-nav__link-text">Document</span>
                  </a>
                </li>
                <li className="kt-nav__item">
                  <a href="#" className="kt-nav__link">
                    <i className="kt-nav__link-icon flaticon2-delivery-package" />
                    <span className="kt-nav__link-text">Shipment</span>
                  </a>
                </li>
                <li className="kt-nav__item">
                  <a href="#" className="kt-nav__link">
                    <i className="kt-nav__link-icon flaticon2-delivery-truck" />
                    <span className="kt-nav__link-text">Trucking</span>
                  </a>
                </li>
                <li className="kt-nav__item">
                  <a href="#" className="kt-nav__link">
                    <i className="kt-nav__link-icon flaticon2-percentage" />
                    <span className="kt-nav__link-text">Billing</span>
                  </a>
                </li>
              </ul>
            </Dropdown.Menu>
          </Dropdown>
        </OverlayTrigger>
      </>
    );
  }
}
