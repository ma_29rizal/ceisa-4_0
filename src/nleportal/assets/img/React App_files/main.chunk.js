(this["webpackJsonplanding-page-nle"] = this["webpackJsonplanding-page-nle"] || []).push([["main"],{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/App.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".App {\n  text-align: center;\n}\n\n.App-logo {\n  height: 40vmin;\n  pointer-events: none;\n}\n\n@media (prefers-reduced-motion: no-preference) {\n  .App-logo {\n    animation: App-logo-spin infinite 20s linear;\n  }\n}\n\n.App-header {\n  background-color: #282c34;\n  min-height: 100vh;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  font-size: calc(10px + 2vmin);\n  color: white;\n}\n\n.App-link {\n  color: #61dafb;\n}\n\n@keyframes App-logo-spin {\n  from {\n    transform: rotate(0deg);\n  }\n  to {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Components/NewsCard.css":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/Components/NewsCard.css ***!
  \****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n  .list-news .card.servicepage{\n    margin: 0 auto;\n  }\n  \n  .list-news .container-news {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n  }\n  \n  .list-news .card-body {\n    padding: 10px;\n  }\n  \n  .list-news .number {\n    margin: 10px auto;\n    color: #a1a1a1;\n    font-size: 90%;\n  }", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/Home.css":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/Page/Home.css ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "* {\n    box-sizing: border-box;\n  }\n  \n  .body.news {\n    font-family: Open Sans;\n    font-size: 16px;\n    line-height: 1.4;\n    background-color: #d8e0e5;\n  }\n\n  iframe{\n    width: 160% !important;\n    height: 57% !important;\n    margin-top: 18% !important;\n    margin-left: 56% !important;\n    border-radius: 2% !important\n  }\n  \n  .card.news {\n    position: absolute;\n    overflow: hidden;\n    top: 50%;\n    left: 50%;\n    width: 226px;\n    transform: translateX(-50%) translateY(-50%);\n    background-color: #fff;\n    box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1);\n    transition: box-shadow 0.3s;\n  }\n  \n  .card.news a {\n    color: inherit;\n    text-decoration: none;\n  }\n  \n  .card.news:hover {\n    box-shadow: 0px 0px 50px rgba(0, 0, 0, 0.3);\n  }\n  \n  .cardDate {\n    position: absolute;\n    top: 20px;\n    right: 20px;\n    width: 45px;\n    height: 45px;\n    padding-top: 10px;\n    color: #fff;\n    text-align: center;\n    line-height: 13px;\n    font-weight: bold;\n    background-color: darkorange;\n    border-radius: 50%;\n  }\n  \n  .cardDateDay {\n    display: block;\n    font-size: 14px;\n  }\n  \n  .cardDateMonth {\n    display: block;\n    font-size: 10px;\n    text-transform: uppercase;\n  }\n  \n  .cardThumb {\n    height: 235px;\n    overflow: hidden;\n    background-color: #000;\n    transition: height 0.3s;\n  }\n  \n  .cardThumb img {\n    display: block;\n    opacity: 1;\n    transition: opacity 0.3s, transform 0.3s;\n    transform: scale(1);\n  }\n  \n  .card:hover .cardThumb img {\n    opacity: 0.6;\n    transform: scale(1.2);\n  }\n  \n  .card.news:hover .cardThumb {\n    height: 90px;\n  }\n  \n  .cardBody {\n    position: relative;\n    padding: 20px;\n    height: 185px;\n    transition: height 0.3s;\n  }\n  \n  .card.news:hover .cardBody {\n    height: 330px;\n  }\n  \n  .cardCategory {\n    position: absolute;\n    left: 0;\n    top: -25px;\n    height: 25px;\n    padding: 0 15px;\n    color: #fff;\n    font-size: 11px;\n    line-height: 25px;\n    text-transform: uppercase;\n    background-color: darkorange;\n  }\n  \n  .cardTitle {\n    margin: 0;\n    padding: 0 0 10px 0;\n    font-size: 22px;\n    color: #000;\n    font-weight: bold;\n  }\n  \n  .card.news:hover .cardTitle {\n    animation: titleBlur 0.3s;\n  }\n  \n  .cardSubtitle {\n    margin: 0;\n    padding: 0 0 10px 0;\n    font-size: 19px;\n    color: darkorange;\n  }\n  \n  .card:hover .cardSubtitle {\n    animation: subtitleBlur 0.3s;\n  }\n  \n  .cardDescription {\n    position: absolute;\n    left: 20px;\n    right: 20px;\n    bottom: 65px;\n    margin: 0;\n    padding: 0;\n    color: #666c74;\n    font-size: 14px;\n    line-height: 27px;\n    opacity: 0;\n    transition: opacity 0.2s, transform 0.2s;\n    transition-delay: 0s;\n    transform: translateY(25px);\n  }\n  \n  .card.news:hover .cardDescription {\n    opacity: 1;\n    transition-delay: 0.1s;\n    transform: translateY(0);\n  }\n  \n  .cardFooter {\n    position: absolute;\n    bottom: 3px !important;\n    left: 4px !important;\n    right: 20px;\n    font-size: 11px;\n    color: #a3a9ab;\n  }\n  \n  .cardFooter .icon--comment {\n    margin-left: 12px;\n  }\n  \n  .icon {\n    display: inline-block;\n    vertical-align: middle;\n    margin-right: 2px;\n  }\n  \n  .icon--comment {\n    background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDM0MiwgMjAxMC8wMS8xMC0xODowNjo0MyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0MjczZjY2NS0wYTk0LWEzNDQtYmJiMC0zMGVkMDcxMjBmZDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDY5RTlDNUZGOTM5MTFFNDgwRThEMTc2RjY3QzAwRDAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDY5RTlDNUVGOTM5MTFFNDgwRThEMTc2RjY3QzAwRDAiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjQyNzNmNjY1LTBhOTQtYTM0NC1iYmIwLTMwZWQwNzEyMGZkOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MjczZjY2NS0wYTk0LWEzNDQtYmJiMC0zMGVkMDcxMjBmZDgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5BHYl9AAAA+ElEQVR42mL8//8/AwxcunpNEUhlArEHEOtChS8D8Q4gnq6nrXUfppYRphGoqRpINQIxMwN28BeI64GaW+EagZragewKBuJAB1BzJePFK1dtgJzDDKQBWyYgkY8k8BqJ/R2KscnlgzQ6QTkg94sB6cVQDfJQDGIvg8rVQ9XaswAJfihHDuhXCSANwmxArA4VB7FFoXJyUDF+kB9vAxkqJPrxGsipKxlIB+tBGvvQPE4IgNT2MQE9/Q7ICCVBkydIDxNUIIEITdOBWAOo6SyIwwIMrQQsGk8B8QUg/gJNqweR0ylYIxAHAjHIudegiXkxUNEjQtYDBBgAiQ1MgGcurFEAAAAASUVORK5CYII=\");\n    width: 14px;\n    height: 14px;\n    margin-top: -2px;\n  }\n  \n  .icon--time {\n    background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAARCAYAAADkIz3lAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDM0MiwgMjAxMC8wMS8xMC0xODowNjo0MyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMzU3MGZlOS03OWMxLWZhNDktODljMi1mMmZjNGIzYjg4YmQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QThFNDY2NkJGOTNCMTFFNEIxNzJENDYzRUZFOUIzODUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QThFNDY2NkFGOTNCMTFFNEIxNzJENDYzRUZFOUIzODUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjAzNTcwZmU5LTc5YzEtZmE0OS04OWMyLWYyZmM0YjNiODhiZCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzU3MGZlOS03OWMxLWZhNDktODljMi1mMmZjNGIzYjg4YmQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz74EGBTAAABBElEQVR42pSSQQsBURSFH81GqSmlrMgPsFBKs7K1tmElW6UoKQs1G1JElB8gUlbWtpYWkmItW2pqStk6t87UM0m59U1zzz3z5r77XuB0vgyUUjUQUt/jBWYBGO94iarf8QjiYTMZgoyPIWu2gYfLZAcOvpVioCUe3RgBcZBnvqUm4UqPktzAFRTAkcU02IAkSMiKDuiACViBMo1rkAIN8RgUpyAMumBBLcwFpKYMrfEeZzZi3gRjrxjUjG2anmRE7cNYB32Ox5vhgZrUlH/XWVDhx3Ow13dtsXE5harWY4iabM6SX5ssODR6UaUmYRqaMQdKviMsesa/bs+S81M/7uPyLcAAMndCTAC9x+cAAAAASUVORK5CYII=\");\n    width: 10px;\n    height: 17px;\n    margin-top: -3px;\n  }\n  \n  @keyframes titleBlur {\n    0% {\n      opacity: 0.6;\n      text-shadow: 0px 5px 5px rgba(0, 0, 0, 0.6);\n    }\n    100% {\n      opacity: 1;\n      text-shadow: 0px 5px 5px transparent;\n    }\n  }\n  \n  @keyframes subtitleBlur {\n    0% {\n      opacity: 0.6;\n      text-shadow: 0px 5px 5px rgba(255, 140, 0, 0.6);\n    }\n    100% {\n      opacity: 1;\n      text-shadow: 0px 5px 5px rgba(255, 140, 0, 0);\n    }\n  }\n  \n  .block-with-text {\n    overflow: hidden;\n    position: relative;\n    line-height: 1.2em;\n    max-height: 4.8em; \n    text-align: justify;\n  \n  \n    margin-right: -1em;\n    padding-right: 1em;\n  }\n  .block-with-text:before {\n    content: '...';\n    position: absolute;\n    right: 0;\n    bottom: 0;\n  }\n  .block-with-text:after {\n    content: '';\n    position: absolute;\n    right: 0;\n    width: 1em;\n    height: 1em;\n    margin-top: 0.2em;\n    background: white;\n  }\n\n  /* @import url('https://fonts.googleapis.com/css?family=Montserrat:600|Open+Sans'); */\n\n*, *:before, *:after {\n  box-sizing: inherit;\n}\n\n/* html {\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box;\n}  */\n\n/* body {\n  font-family: 'Open Sans', sans-serif;\n  margin: 0;\n  background: #fff;\n  color: #999;\n} */\n\n\np.content-service {\n  font-size: 0.9rem;\n  margin: 1rem 0;\n  line-height: 1.5;\n}\n\nsection {\n  max-width: 1000px;\n  margin: 0 auto;\n  text-align: center;\n  padding: 30px;\n} \n\n\n.section-lead {\n  max-width: 600px;\n  margin: 1rem auto 1.5rem;\n}\n\n\n .service h4.title-service {\n  font-family: 'Montserrat', sans-serif;\n  font-weight: 600;\n  color: #56ceba;\n  font-size: 1.3rem;\n  margin: 1rem 0 0.6rem;\n}\n\n.services-grid {\n  display: flex;\n  align-items: center;\n}\n\n.service {\n  background: #fff;\n  margin: 20px;\n  padding: 20px;\n  border-radius: 4px;\n  text-align: center;\n  flex: 1 1;\n  display: flex;\n  flex-wrap: wrap;\n  border: 2px solid #e7e7e7;\n  transition: all 0.3s ease;\n}\n\n.service:hover {\n  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.08);\n}\n\n.service i {\n  font-size: 3.45rem;\n  margin: 1rem 0;\n}\n\n.service1 i,\n.service1 h4.title-service,\n.service1 .cta {\n  color: #3d87c4;\n}\n\n.service1:hover {\n  border: 2px solid #3d87c4;\n}\n\n.service2 i,\n.service2 h4.title-service,\n.service2 .cta {\n  color: #425fca;\n}\n\n.service2:hover {\n  border: 2px solid #425fca;\n}\n\n.service3 i,\n.service3 h4.title-service,\n.service3 .cta {\n  color: #9c42ca;\n}\n\n.service3:hover {\n  border: 2px solid #9c42ca;\n}\n\n.service .cta span {\n  font-size: 0.6rem;\n}\n\n.service > * {\n  flex: 1 1 100%;\n}\n\n.service .cta {\n  align-self: flex-end;\n}\n\n@media all and (max-width:900px) {\n  .services-grid {\n    display: flex;\n    flex-direction: column;\n  }\n}\n\n   ", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/NewsList.css":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/Page/NewsList.css ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "/* * {\n    margin: 0;\n    padding: 0;\n    border: 0;\n    -webkit-user-select: text; \n  } */\n  /* li {\n    width: 675px;\n    padding: 10px 20px;\n    margin: 10px auto;\n    list-style: none;\n    border: 1px solid rgba(0, 0, 0, 0.08);\n  } */\n  .ui-list>a {\n    display: flex;\n    flex-flow: row nowrap;\n    text-decoration: none;\n    font-family: Hiragino Sans GB;\n  }\n  .ui-list .cover {\n    position: relative;\n    flex: none;\n  }\n  .ui-list img {\n    margin-right: 15px;\n    width: 187px;\n    height: 140px;\n  }\n  .ui-list .video-time {\n    position: absolute;\n    bottom: 6px;right: 15px;\n  /*   底部背景图 */\n    display: inline-block;\n    height: 0;\n    width: 102px;\n    border-bottom: 34px solid rgba(0, 0, 0, 0.6);\n    border-left: 20px solid transparent;\n    line-height: 34px;\n    text-align: center;\n    color: #fff;\n    font-size: 22px;\n  }\n  .ui-list .news-wrap {\n    flex: 1 1;\n    display: flex;\n      flex-flow: column;\n  }\n  .ui-list .news-title {\n    line-height: 25px;\n    font-size: 18px;\n    color: #1f1f1f;\n    margin-left: 31%;\n    margin-top: -21%;\n  }\n  .ui-list .news-subtitle {\n    display: flex;\n    flex-flow: row nowrap;\n    margin: auto 0 10px 0; \n    color: #828282;\n  }\n  .ui-list .video-source {\n    display: flex;\n    flex-flow: row nowrap;\n    line-height: 30px;\n    padding-top: 5px;\n  }\n  .ui-list .origin-icon {\n    width: 30px;\n    height: 30px;\n    border-radius: 30px;\n    background: url(http://vimg1.ws.126.net/image/snapshot/2016/3/6/5/VBGPT3G65.jpg);\n    background-size: 30px;\n  }\n  .ui-list .origin-text {\n    padding-left: 7px;\n    font-size: 26px;\n  }\n  .ui-list .visitor-wrap {\n    margin-left: auto;\n    display: flex;\n    flex-flow: row nowrap;\n  }\n  .ui-list .view {\n    padding-right: 23px;\n    height: 34px;\n    text-indent: 34px;\n    line-height: 34px;\n    font-size: 28px;\n   background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAJ9SURBVHjarNZBiFV1FMfxz3s9B5mhhRA4krUoHdBVhhKEQbRoZT2iqaDAjS4MtANCErQpFy0iZP4YzKaCiIjUgke0aROURtAwurJ0JlcTjbSKeJVKjJtz5TG9mXdv01nde/mf8/3f/zm/c/6tmZkZI2wrHsJubMNEfu9jGQuYj4hf1gvSWgc0hS7uxiVcxq8JkMBJ7MTD+BO9iFioCxrDc3gM72FuvZ1GBCil7MMhfIszEXFrcF17ld8EjuMGXh4FWQX8IX1u4ngpZWIt0BiO4Uecw4qGFhErEXEWP+FYKWXTMNDzWETPBi0ievgZL6wGTWE/PvP/2TnsL6VMDYK6mfi1jms2y7vRMWbMbgXajs0jEr8PH+It3NcANofNpZTtbezBxRp+LTyJMziBLTV5F7GnnYJbqOn0BTZl4fRwuJQyPsJnETvaqe7lmqA38SK+xjiO4PNSynQp5a41fJYx2U6R9hvk+SpexVF8h3vwGs6WUp4opbRWre9jor2B8v0er+Ak5nE/3s4N/Ms6FRG/NwQ9ipfwSL5fw7sR8c2QttbvVGfYALQrm+fj+f5b6uzLiPhnyPpJLHey4nbiSg3IGziQz3/gI3wcETfW8dmBxXZV5zX/5gBu4RM8gw9GQFQ67WAJf2PviO6wgq/ymJbq7Cpn1F8RsVRVXQ+HU/3DbA4H8XoDSCtz2RtsqldxAc+u4Xck51QTm8b5arQP6ujTTFx3o/OhlNLFgxnzjo4qu4nTKbix/zJl87im8QBOD94b2kPaxakEzWaBaJD42fQ9FRH9Jtetp7N5zmcer+e1qlL81rrXrc6I5vkO7s1AT2Xg8YG/v56Cf3/UBfL2ABDUwE8cgpYXAAAAAElFTkSuQmCC') no-repeat 0 center;\n    background-size: 26px;\n  }\n  .ui-list .comment {\n    display: flex;\n  }\n  .ui-list .comment>span {\n    height: 30px;\n    line-height: 32px;\n   background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAeCAYAAADtlXTHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAAoSURBVHjaYvj//z8DEwMDA5Q4dOjQPiYGBoYAJgYGhk9IEvQkzgAGAPYrCErZG/NdAAAAAElFTkSuQmCC) center repeat-x;\n    background-size: 1px 30px;\n  }\n  .comment::before,.comment::after {\n    content: '';\n    width: 12px;\n    height: 30px;\n  }\n  .comment::before {     \n    background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAeCAYAAAAYa/93AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAEsSURBVHjalJS9K4VhGMZ/7+mUcsrg5Iwmzn+AMlyDwYBFSSmdUoo6BhMTfwUZKYMSMst0DQqj7ZDFgsXCIOpYnvQq78dz17Pdv+f+vpJut0uMVYi0aKBa5GC7BxgHRoChJKsG231AC5gDbsN7qGY4C9gGzoAZSe//pmQ7AZaAVWBd0lVRDW2gFn59ze2S7WlgANjNcv4FbDeATeAknW9ehBXgQNJd4Rxs9wNTgMpOehI4lfRVFhgDrmN2qQl0YoCGpJcY4CN2vXtjgTfb9RjgHhiOAW6A0RjgAli0XepcK5KegUtgIUYE9oC27cFSgKQnYKdMlHTeR0DN9kYe8Ec1QuFbwCdwLOkxF0iB88AacAjsS/rOBQJUB5aBWeA86FInKRLjcJETQfmaPwMAU2lniofRFw8AAAAASUVORK5CYII=);\n    background-size: cover;\n  }\n  .comment::after {\n    background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAeCAYAAAD3qZRJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAFhSURBVHjanNQ/qM1xGMfx1zlO9+ZfyUAy3HB3i24Wn0ndkix3MchCSRkMslsUVpmUomzsotQzSLKwGDBcg4RBGcSVY/mp45x7nN/PMz7P8+7z9Hw/36c3HA5Ni6rqYUOSn6P5/j+AHbiKLeO1wVjjZuzHYSxjDhcnoKq6g3lsxSa8wCNcweMkw/WULuM7vib5NKasquaS/PgLSvLK9FjFAl63WkQTz7A0npwF3cNKVQ1aQ0ne4QlOdlGC2zhTVYutoSQfcQ1nuyjBfRyqqr2toSS/cB1HuyjBUxzsCr3BYlX1W0PNiF+wrYuS/4U24ltrqKr62InPXZQWsJpkrQu01Li+08qX8aA1VFVHMEzycuKwTAHmcQmnuhj2HO7+UZkJVdV57GnMuv7dG2nejtPYhQtJ1kbrvdGz3DzgiebD3cLNxnMTx3I39uEAjuMhVpK8nzb2ADfwFs9xLMmHWZv5PQDAi21cV+36tgAAAABJRU5ErkJggg==);\n    background-size: cover;\n  }", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/agency.min.css":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/assets/css/agency.min.css ***!
  \******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(/*! ../img/map-image.png */ "./src/assets/img/map-image.png");
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "/*!\n * Start Bootstrap - Agency v5.2.1 (https://startbootstrap.com/template-overviews/agency)\n * Copyright 2013-2019 Start Bootstrap\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-agency/blob/master/LICENSE) */\n body {\n    overflow-x:hidden;\n    font-family:'Roboto Slab',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    p {\n    line-height:1.75;\n    }\n    \n    .text-primary {\n    color:purple!important;\n    }\n    \n    h1,h2,h3,h4,h5,h6 {\n    font-weight:700;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    .page-section {\n    padding:100px 0;\n    }\n    \n    .page-section h2.section-heading {\n    font-size:40px;\n    margin-top:0;\n    margin-bottom:15px;\n    }\n    \n    .page-section h3.section-subheading {\n    font-size:16px;\n    font-weight:400;\n    font-style:italic;\n    margin-bottom:75px;\n    text-transform:none;\n    font-family:'Droid Serif',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    .btn {\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    font-weight:700;\n    }\n    \n    .btn-xl {\n    font-size:18px;\n    padding:20px 40px;\n    }\n    \n    .btn-primary {\n    background-color:purple;\n    border-color:purple;\n    }\n    \n    .btn-primary:active,.btn-primary:focus,.btn-primary:hover {\n    background-color:#fec810!important;\n    color:#000;\n    border-color:#fec810!important;\n    }\n    \n    .btn-primary:active,.btn-primary:focus {\n    box-shadow:0 0 0 .2rem rgba(254,209,55,.5)!important;\n    }\n    \n    #mainNav {\n    background-color: #071847;\n    }\n    \n    #mainNav .navbar-toggler {\n    font-size:12px;\n    right:0;\n    text-transform:uppercase;\n    color:#000;\n    border:0;\n    background-color:purple;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    padding:13px;\n    }\n    \n    #mainNav .navbar-brand {\n    color:purple;\n    font-family:'Kaushan Script',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    #mainNav .navbar-nav .nav-item .nav-link {\n    font-size:90%;\n    font-weight:400;\n    letter-spacing:1px;\n    color: white;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    padding:.75em 1em;\n    }\n    \n    #mainNav .navbar-nav .nav-item .nav-link.active,#mainNav .navbar-nav .nav-item .nav-link:hover {\n    color:grey;\n    }\n    \n    header.masthead {\n    text-align:center;\n    color:#000;\n    background-repeat:no-repeat;\n    background-attachment:scroll;\n    background-position:center center;\n    background-size:cover;\n    }\n    \n    header.masthead .intro-text {\n    padding-top:150px;\n    padding-bottom:100px;\n    }\n    \n    header.masthead .intro-text .intro-lead-in {\n    font-size:22px;\n    font-style:italic;\n    line-height:22px;\n    margin-bottom:25px;\n    font-family:'Droid Serif',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    header.masthead .intro-text .intro-heading {\n    font-size:50px;\n    font-weight:700;\n    line-height:50px;\n    margin-bottom:25px;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    .service-heading {\n    text-transform:none;\n    margin:15px 0;\n    }\n    \n    #portfolio .portfolio-item {\n    right:0;\n    margin:0 0 15px;\n    }\n    \n    #portfolio .portfolio-item .portfolio-link {\n    position:relative;\n    display:block;\n    max-width:400px;\n    cursor:pointer;\n    margin:0 auto;\n    }\n    \n    #portfolio .portfolio-item .portfolio-link .portfolio-hover {\n    position:absolute;\n    width:100%;\n    height:100%;\n    transition:all ease .5s;\n    opacity:0;\n    background:rgba(254,209,54,.9);\n    }\n    \n    #portfolio .portfolio-item .portfolio-link .portfolio-hover:hover {\n    opacity:1;\n    }\n    \n    #portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content {\n    font-size:20px;\n    position:absolute;\n    top:50%;\n    width:100%;\n    height:20px;\n    margin-top:-12px;\n    text-align:center;\n    color:#000;\n    }\n    \n    #portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content i {\n    margin-top:-12px;\n    }\n    \n    #portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content h3,#portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content h4 {\n    margin:0;\n    }\n    \n    #portfolio .portfolio-item .portfolio-caption {\n    max-width:400px;\n    text-align:center;\n    background-color:#000;\n    margin:0 auto;\n    padding:25px;\n    }\n    \n    #portfolio .portfolio-item .portfolio-caption h4 {\n    text-transform:none;\n    margin:0;\n    }\n    \n    #portfolio .portfolio-item .portfolio-caption p {\n    font-size:16px;\n    font-style:italic;\n    font-family:'Droid Serif',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    margin:0;\n    }\n    \n    #portfolio * {\n    z-index:2;\n    }\n    \n    .portfolio-modal .modal-dialog {\n    max-width:100vw;\n    margin:1rem;\n    }\n    \n    .portfolio-modal .modal-content {\n    text-align:center;\n    padding:100px 0;\n    }\n    \n    .portfolio-modal .modal-content h2 {\n    font-size:3em;\n    margin-bottom:15px;\n    }\n    \n    .portfolio-modal .modal-content p.item-intro {\n    font-size:16px;\n    font-style:italic;\n    font-family:'Droid Serif',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    margin:20px 0 30px;\n    }\n    \n    .portfolio-modal .modal-content ul.list-inline {\n    margin-top:0;\n    margin-bottom:30px;\n    }\n    \n    .portfolio-modal .modal-content button {\n    cursor:pointer;\n    }\n    \n    .portfolio-modal .close-modal {\n    position:absolute;\n    top:25px;\n    right:25px;\n    width:75px;\n    height:75px;\n    cursor:pointer;\n    background-color:transparent;\n    }\n    \n    .portfolio-modal .close-modal:hover {\n    opacity:.3;\n    }\n    \n    .portfolio-modal .close-modal .lr {\n    z-index:1051;\n    width:1px;\n    height:75px;\n    margin-left:35px;\n    transform:rotate(45deg);\n    background-color:#212529;\n    }\n    \n    .portfolio-modal .close-modal .lr .rl {\n    z-index:1052;\n    width:1px;\n    height:75px;\n    transform:rotate(90deg);\n    background-color:#212529;\n    }\n    \n    .timeline {\n    position:relative;\n    list-style:none;\n    padding:0;\n    }\n    \n    .timeline:before {\n    position:absolute;\n    top:0;\n    bottom:0;\n    left:40px;\n    width:2px;\n    margin-left:-1.5px;\n    content:'';\n    background-color:#5F9EA0;\n    }\n    \n    .timeline>li {\n    position:relative;\n    min-height:50px;\n    margin-bottom:50px;\n    }\n    \n    .timeline>li:after,.timeline>li:before {\n    display:table;\n    content:' ';\n    }\n    \n    .timeline>li:after {\n    clear:both;\n    }\n    \n    .timeline>li .timeline-panel {\n    position:relative;\n    float:right;\n    width:100%;\n    text-align:left;\n    padding:0 20px 0 100px;\n    }\n    \n    .timeline>li .timeline-image {\n    position:absolute;\n    z-index:100;\n    left:0;\n    width:80px;\n    height:80px;\n    margin-left:0;\n    text-align:center;\n    color:#000;\n    border:7px solid #5F9EA0;\n    border-radius:100%;\n    background-color:purple;\n    }\n    \n    .timeline>li .timeline-image h4 {\n    font-size:10px;\n    line-height:14px;\n    margin-top:12px;\n    }\n    \n    .timeline>li.timeline-inverted>.timeline-panel {\n    float:right;\n    text-align:left;\n    padding:0 20px 0 100px;\n    }\n    \n    .timeline .timeline-heading h4 {\n    margin-top:0;\n    color:inherit;\n    }\n    \n    .timeline .timeline-heading h4.subheading {\n    text-transform:none;\n    }\n    \n    .team-member {\n    margin-bottom:50px;\n    text-align:center;\n    }\n    \n    .team-member img {\n    width:225px;\n    height:225px;\n    border:7px solid rgba(0,0,0,.1);\n    }\n    \n    .team-member h4 {\n    margin-top:25px;\n    margin-bottom:0;\n    text-transform:none;\n    }\n    \n    .team-member p {\n    margin-top:0;\n    }\n    \n    section#contact {\n    background-color:#212529;\n    background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n    background-repeat:no-repeat;\n    background-position:center;\n    }\n    \n    section#contact .form-group {\n    margin-bottom:25px;\n    }\n    \n    section#contact .form-group input,section#contact .form-group textarea {\n    padding:20px;\n    }\n    \n    section#contact .form-group input.form-control {\n    height:auto;\n    }\n    \n    section#contact .form-group textarea.form-control {\n    height:248px;\n    }\n    \n    section#contact .form-control:focus {\n    box-shadow:none;\n    border-color:purple;\n    }\n    \n    .footer {\n    text-align:center;\n    padding:25px 0;\n    }\n    \n    .footer span.copyright {\n    font-size:90%;\n    line-height:40px;\n    text-transform:none;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    .footer ul.quicklinks {\n    font-size:90%;\n    line-height:40px;\n    margin-bottom:0;\n    text-transform:none;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    ul.social-buttons li a {\n    font-size:20px;\n    line-height:50px;\n    display:block;\n    width:50px;\n    height:50px;\n    transition:all .3s;\n    color:#000;\n    border-radius:100%;\n    outline:0;\n    background-color:#212529;\n    }\n    \n    ul.social-buttons li a:active,ul.social-buttons li a:focus,ul.social-buttons li a:hover {\n    background-color:purple;\n    }\n    \n    a,section#contact .section-heading {\n    color:#000;\n    }\n    \n    a:hover,#mainNav .navbar-brand.active,#mainNav .navbar-brand:active,#mainNav .navbar-brand:focus,#mainNav .navbar-brand:hover {\n    color:navy;\n    }\n    \n    img::-moz-selection,img::selection {\n    background:0 0;\n    }\n    \n    .portfolio-modal .modal-content p,.portfolio-modal .modal-content img {\n    margin-bottom:30px;\n    }\n    \n    .timeline>li .timeline-panel:before,.timeline>li.timeline-inverted>.timeline-panel:before {\n    right:auto;\n    left:-15px;\n    border-right-width:15px;\n    border-left-width:0;\n    }\n    \n    .timeline>li .timeline-panel:after,.timeline>li.timeline-inverted>.timeline-panel:after {\n    right:auto;\n    left:-14px;\n    border-right-width:14px;\n    border-left-width:0;\n    }\n    \n    .timeline>li:last-child,.timeline .timeline-body>p,.timeline .timeline-body>ul,ul.social-buttons {\n    margin-bottom:0;\n    }\n    \n    section#contact ::-webkit-input-placeholder,section#contact :-moz-placeholder,section#contact ::-moz-placeholder,section#contact :-ms-input-placeholder {\n    font-weight:700;\n    color:#ced4da;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    @media min-width768px{\n    section {\n    padding:150px 0;\n    }\n    \n    header.masthead .intro-text {\n    padding-top:300px;\n    padding-bottom:200px;\n    }\n    \n    header.masthead .intro-text .intro-lead-in {\n    font-size:40px;\n    font-style:italic;\n    line-height:40px;\n    margin-bottom:25px;\n    font-family:'Droid Serif',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    header.masthead .intro-text .intro-heading {\n    font-size:75px;\n    font-weight:700;\n    line-height:75px;\n    margin-bottom:50px;\n    font-family:Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';\n    }\n    \n    .timeline:before {\n    left:50%;\n    }\n    \n    .timeline>li {\n    min-height:100px;\n    margin-bottom:100px;\n    }\n    \n    .timeline>li .timeline-panel {\n    float:left;\n    width:41%;\n    text-align:right;\n    padding:0 20px 20px 30px;\n    }\n    \n    .timeline>li .timeline-image {\n    left:50%;\n    width:100px;\n    height:100px;\n    margin-left:-50px;\n    }\n    \n    .timeline>li .timeline-image h4 {\n    font-size:13px;\n    line-height:18px;\n    margin-top:16px;\n    }\n    \n    .timeline>li.timeline-inverted>.timeline-panel {\n    float:right;\n    text-align:left;\n    padding:0 30px 20px 20px;\n    }\n    }\n    \n    @media min-width992px{\n    #mainNav {\n    padding-top:25px;\n    padding-bottom:25px;\n    transition:padding-top .3s,padding-bottom .3s;\n    border:none;\n    background-color:#008B8B;\n    }\n    \n    #mainNav .navbar-brand {\n    font-size:1.75em;\n    transition:all .3s;\n    }\n    \n    #mainNav .navbar-nav .nav-item .nav-link {\n    padding:1.1em 1em!important;\n    }\n    \n    #mainNav.navbar-shrink {\n    padding-top:0;\n    padding-bottom:0;\n    background-color:#212529;\n    }\n    \n    #mainNav.navbar-shrink .navbar-brand {\n    font-size:1.25em;\n    padding:12px 0;\n    }\n    \n    .timeline>li {\n    min-height:150px;\n    }\n    \n    .timeline>li .timeline-image {\n    width:150px;\n    height:150px;\n    margin-left:-75px;\n    }\n    \n    .timeline>li .timeline-image h4 {\n    font-size:18px;\n    line-height:26px;\n    margin-top:30px;\n    }\n    \n    .timeline>li .timeline-panel,.timeline>li.timeline-inverted>.timeline-panel {\n    padding:0 20px 20px;\n    }\n    }\n    \n    @media min-width767px{\n    #portfolio .portfolio-item {\n    margin:0 0 30px;\n    }\n    }\n    \n    @media min-width1200px{\n    .timeline>li {\n    min-height:170px;\n    }\n    \n    .timeline>li .timeline-panel {\n    padding:0 20px 20px 100px;\n    }\n    \n    .timeline>li .timeline-image {\n    width:170px;\n    height:170px;\n    margin-left:-85px;\n    }\n    \n    .timeline>li .timeline-image h4 {\n    margin-top:40px;\n    }\n    \n    .timeline>li.timeline-inverted>.timeline-panel {\n    padding:0 100px 20px 20px;\n    }\n    }", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/bootstrap.min.css":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/assets/css/bootstrap.min.css ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "/*!\n * Bootstrap v4.3.1 (https://getbootstrap.com/)\n * Copyright 2011-2019 The Bootstrap Authors\n * Copyright 2011-2019 Twitter, Inc.\n * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)\n/*# sourceMappingURL=bootstrap.min.css.map */\n\n\n*/:root {\n    --blue:#007bff;\n    --indigo:#6610f2;\n    --purple:#6f42c1;\n    --pink:#e83e8c;\n    --red:#dc3545;\n    --orange:#fd7e14;\n    --yellow:#ffc107;\n    --green:#28a745;\n    --teal:#20c997;\n    --cyan:#17a2b8;\n    --white:#fff;\n    --gray:#6c757d;\n    --gray-dark:#343a40;\n    --primary:#007bff;\n    --secondary:#6c757d;\n    --success:#28a745;\n    --info:#17a2b8;\n    --warning:#ffc107;\n    --danger:#dc3545;\n    --light:#f8f9fa;\n    --dark:#343a40;\n    --breakpoint-xs:0;\n    --breakpoint-sm:576px;\n    --breakpoint-md:768px;\n    --breakpoint-lg:992px;\n    --breakpoint-xl:1200px;\n    --font-family-sans-serif:0;\n    --font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,\"Liberation Mono\",\"Courier New\",monospace;\n    }\n    \n    *,::after,::before {\n    box-sizing:border-box;\n    }\n    \n    html {\n    font-family:sans-serif;\n    line-height:1.15;\n    -webkit-text-size-adjust:100%;\n    -webkit-tap-highlight-color:transparent;\n    }\n    \n    body {\n    font-family:0;\n    font-size:1rem;\n    font-weight:400;\n    line-height:1.5;\n    color:#212529;\n    text-align:left;\n    background-color:#fff;\n    margin:0;\n    }\n    \n    [tabindex=\"-1\"]:focus {\n    outline:0!important;\n    }\n    \n    hr {\n    box-sizing:content-box;\n    height:0;\n    overflow:visible;\n    margin-top:1rem;\n    margin-bottom:1rem;\n    border:0;\n    border-top:1px solid rgba(0,0,0,.1);\n    }\n    \n    h1,h2,h3,h4,h5,h6 {\n    margin-top:0;\n    margin-bottom:.5rem;\n    }\n    \n    abbr[data-original-title],abbr[title] {\n    text-decoration:underline dotted;\n    cursor:help;\n    border-bottom:0;\n    text-decoration-skip-ink:none;\n    }\n    \n    address {\n    margin-bottom:1rem;\n    font-style:normal;\n    line-height:inherit;\n    }\n    \n    dt {\n    font-weight:700;\n    }\n    \n    dd {\n    margin-bottom:.5rem;\n    margin-left:0;\n    }\n    \n    b,strong {\n    font-weight:bolder;\n    }\n    \n    small {\n    font-size:80%;\n    }\n    \n    sub,sup {\n    position:relative;\n    font-size:75%;\n    line-height:0;\n    vertical-align:baseline;\n    }\n    \n    sub {\n    bottom:-.25em;\n    }\n    \n    sup {\n    top:-.5em;\n    }\n    \n    a {\n    color:#007bff;\n    text-decoration:none;\n    background-color:transparent;\n    }\n    \n    code,kbd,pre,samp {\n    font-family:SFMono-Regular,Menlo,Monaco,Consolas,\"Liberation Mono\",\"Courier New\",monospace;\n    font-size:1em;\n    }\n    \n    pre {\n    margin-top:0;\n    margin-bottom:1rem;\n    overflow:auto;\n    display:block;\n    font-size:87.5%;\n    color:#212529;\n    }\n    \n    img {\n    vertical-align:middle;\n    border-style:none;\n    }\n    \n    svg {\n    overflow:hidden;\n    vertical-align:middle;\n    }\n    \n    table {\n    border-collapse:collapse;\n    }\n    \n    caption {\n    padding-top:.75rem;\n    padding-bottom:.75rem;\n    color:#6c757d;\n    text-align:left;\n    caption-side:bottom;\n    }\n    \n    th {\n    text-align:inherit;\n    }\n    \n    label {\n    display:inline-block;\n    margin-bottom:.5rem;\n    }\n    \n    button {\n    border-radius:0;\n    }\n    \n    button:focus {\n    outline:5px auto 0;\n    }\n    \n    button,input,optgroup,select,textarea {\n    font-family:inherit;\n    font-size:inherit;\n    line-height:inherit;\n    margin:0;\n    }\n    \n    button,input {\n    overflow:visible;\n    }\n    \n    button,select {\n    text-transform:none;\n    }\n    \n    select {\n    word-wrap:normal;\n    }\n    \n    [type=button],[type=reset],[type=submit],button {\n    -webkit-appearance:button;\n    }\n    \n    [type=button]:not(:disabled),[type=reset]:not(:disabled),[type=submit]:not(:disabled),button:not(:disabled) {\n    cursor:pointer;\n    }\n    \n    [type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner {\n    border-style:none;\n    padding:0;\n    }\n    \n    input[type=checkbox],input[type=radio] {\n    box-sizing:border-box;\n    padding:0;\n    }\n    \n    input[type=date],input[type=datetime-local],input[type=month],input[type=time] {\n    -webkit-appearance:listbox;\n    }\n    \n    textarea {\n    overflow:auto;\n    resize:vertical;\n    }\n    \n    fieldset {\n    min-width:0;\n    border:0;\n    margin:0;\n    padding:0;\n    }\n    \n    legend {\n    display:block;\n    width:100%;\n    max-width:100%;\n    margin-bottom:.5rem;\n    font-size:1.5rem;\n    line-height:inherit;\n    color:inherit;\n    white-space:normal;\n    padding:0;\n    }\n    \n    progress {\n    vertical-align:baseline;\n    }\n    \n    [type=search] {\n    outline-offset:-2px;\n    -webkit-appearance:none;\n    }\n    \n    [type=search]::-webkit-search-decoration {\n    -webkit-appearance:none;\n    }\n    \n    ::-webkit-file-upload-button {\n    font:inherit;\n    -webkit-appearance:button;\n    }\n    \n    summary {\n    display:list-item;\n    cursor:pointer;\n    }\n    \n    [hidden] {\n    display:none!important;\n    }\n    \n    .h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6 {\n    margin-bottom:.5rem;\n    font-weight:500;\n    line-height:1.2;\n    }\n    \n    .h1,h1 {\n    font-size:2.5rem;\n    }\n    \n    .h2,h2 {\n    font-size:2rem;\n    }\n    \n    .h3,h3 {\n    font-size:1.75rem;\n    }\n    \n    .h4,h4 {\n    font-size:1.5rem;\n    }\n    \n    .h5,h5 {\n    font-size:1.25rem;\n    }\n    \n    .h6,h6 {\n    font-size:1rem;\n    }\n    \n    .lead {\n    font-size:1.25rem;\n    font-weight:300;\n    }\n    \n    .display-1 {\n    font-size:6rem;\n    font-weight:300;\n    line-height:1.2;\n    }\n    \n    .display-2 {\n    font-size:5.5rem;\n    font-weight:300;\n    line-height:1.2;\n    }\n    \n    .display-3 {\n    font-size:4.5rem;\n    font-weight:300;\n    line-height:1.2;\n    }\n    \n    .display-4 {\n    font-size:3.5rem;\n    font-weight:300;\n    line-height:1.2;\n    }\n    \n    .small,small {\n    font-size:80%;\n    font-weight:400;\n    }\n    \n    .mark,mark {\n    background-color:#fcf8e3;\n    padding:.2em;\n    }\n    \n    .list-inline-item:not(:last-child) {\n    margin-right:.5rem;\n    }\n    \n    .initialism {\n    font-size:90%;\n    text-transform:uppercase;\n    }\n    \n    .blockquote {\n    margin-bottom:1rem;\n    font-size:1.25rem;\n    }\n    \n    .blockquote-footer {\n    display:block;\n    font-size:80%;\n    color:#6c757d;\n    }\n    \n    .blockquote-footer::before {\n    content:\"\\2014\\00A0\";\n    }\n    \n    .img-fluid {\n    max-width:100%;\n    height:auto;\n    }\n    \n    .img-thumbnail {\n    background-color:#fff;\n    border:1px solid #dee2e6;\n    border-radius:.25rem;\n    max-width:100%;\n    height:auto;\n    padding:.25rem;\n    }\n    \n    .figure-img {\n    margin-bottom:.5rem;\n    line-height:1;\n    }\n    \n    .figure-caption {\n    font-size:90%;\n    color:#6c757d;\n    }\n    \n    code {\n    font-size:87.5%;\n    color:#e83e8c;\n    word-break:break-word;\n    }\n    \n    a>code {\n    color:inherit;\n    }\n    \n    kbd {\n    font-size:87.5%;\n    color:#fff;\n    background-color:#212529;\n    border-radius:.2rem;\n    padding:.2rem .4rem;\n    }\n    \n    kbd kbd {\n    font-size:100%;\n    font-weight:700;\n    padding:0;\n    }\n    \n    pre code {\n    font-size:inherit;\n    color:inherit;\n    word-break:normal;\n    }\n    \n    .pre-scrollable {\n    max-height:340px;\n    overflow-y:scroll;\n    }\n    \n    .row {\n    display:flex;\n    flex-wrap:wrap;\n    margin-right:-15px;\n    margin-left:-15px;\n    }\n    \n    .no-gutters {\n    margin-right:0;\n    margin-left:0;\n    }\n    \n    .col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto {\n    position:relative;\n    width:100%;\n    padding-right:15px;\n    padding-left:15px;\n    }\n    \n    .col {\n    flex-basis:0;\n    flex-grow:1;\n    max-width:100%;\n    }\n    \n    .col-auto {\n    flex:0 0 auto;\n    width:auto;\n    max-width:100%;\n    }\n    \n    .col-1 {\n    flex:0 0 8.333333%;\n    max-width:8.333333%;\n    }\n    \n    .col-2 {\n    flex:0 0 16.666667%;\n    max-width:16.666667%;\n    }\n    \n    .col-3 {\n    flex:0 0 25%;\n    max-width:25%;\n    }\n    \n    .col-4 {\n    flex:0 0 33.333333%;\n    max-width:33.333333%;\n    }\n    \n    .col-5 {\n    flex:0 0 41.666667%;\n    max-width:41.666667%;\n    }\n    \n    .col-6 {\n    flex:0 0 50%;\n    max-width:50%;\n    }\n    \n    .col-7 {\n    flex:0 0 58.333333%;\n    max-width:58.333333%;\n    }\n    \n    .col-8 {\n    flex:0 0 66.666667%;\n    max-width:66.666667%;\n    }\n    \n    .col-9 {\n    flex:0 0 75%;\n    max-width:75%;\n    }\n    \n    .col-10 {\n    flex:0 0 83.333333%;\n    max-width:83.333333%;\n    }\n    \n    .col-11 {\n    flex:0 0 91.666667%;\n    max-width:91.666667%;\n    }\n    \n    .col-12 {\n    flex:0 0 100%;\n    max-width:100%;\n    }\n    \n    .order-first {\n    order:-1;\n    }\n    \n    .order-last {\n    order:13;\n    }\n    \n    .order-0 {\n    order:0;\n    }\n    \n    .order-1 {\n    order:1;\n    }\n    \n    .order-2 {\n    order:2;\n    }\n    \n    .order-3 {\n    order:3;\n    }\n    \n    .order-4 {\n    order:4;\n    }\n    \n    .order-5 {\n    order:5;\n    }\n    \n    .order-6 {\n    order:6;\n    }\n    \n    .order-7 {\n    order:7;\n    }\n    \n    .order-8 {\n    order:8;\n    }\n    \n    .order-9 {\n    order:9;\n    }\n    \n    .order-10 {\n    order:10;\n    }\n    \n    .order-11 {\n    order:11;\n    }\n    \n    .order-12 {\n    order:12;\n    }\n    \n    .offset-1 {\n    margin-left:8.333333%;\n    }\n    \n    .offset-2 {\n    margin-left:16.666667%;\n    }\n    \n    .offset-3 {\n    margin-left:25%;\n    }\n    \n    .offset-4 {\n    margin-left:33.333333%;\n    }\n    \n    .offset-5 {\n    margin-left:41.666667%;\n    }\n    \n    .offset-6 {\n    margin-left:50%;\n    }\n    \n    .offset-7 {\n    margin-left:58.333333%;\n    }\n    \n    .offset-8 {\n    margin-left:66.666667%;\n    }\n    \n    .offset-9 {\n    margin-left:75%;\n    }\n    \n    .offset-10 {\n    margin-left:83.333333%;\n    }\n    \n    .offset-11 {\n    margin-left:91.666667%;\n    }\n    \n    .table {\n    width:100%;\n    margin-bottom:1rem;\n    color:#212529;\n    }\n    \n    .table td,.table th {\n    vertical-align:top;\n    border-top:1px solid #dee2e6;\n    padding:.75rem;\n    }\n    \n    .table thead th {\n    vertical-align:bottom;\n    border-bottom:2px solid #dee2e6;\n    }\n    \n    .table tbody+tbody {\n    border-top:2px solid #dee2e6;\n    }\n    \n    .table-sm td,.table-sm th {\n    padding:.3rem;\n    }\n    \n    .table-bordered thead td,.table-bordered thead th {\n    border-bottom-width:2px;\n    }\n    \n    .table-striped tbody tr:nth-of-type(odd) {\n    background-color:rgba(0,0,0,.05);\n    }\n    \n    .table-hover tbody tr:hover {\n    color:#212529;\n    background-color:rgba(0,0,0,.075);\n    }\n    \n    .table-primary,.table-primary>td,.table-primary>th {\n    background-color:#b8daff;\n    }\n    \n    .table-primary tbody+tbody,.table-primary td,.table-primary th,.table-primary thead th {\n    border-color:#7abaff;\n    }\n    \n    .table-secondary,.table-secondary>td,.table-secondary>th {\n    background-color:#d6d8db;\n    }\n    \n    .table-secondary tbody+tbody,.table-secondary td,.table-secondary th,.table-secondary thead th {\n    border-color:#b3b7bb;\n    }\n    \n    .table-success,.table-success>td,.table-success>th {\n    background-color:#c3e6cb;\n    }\n    \n    .table-success tbody+tbody,.table-success td,.table-success th,.table-success thead th {\n    border-color:#8fd19e;\n    }\n    \n    .table-info,.table-info>td,.table-info>th {\n    background-color:#bee5eb;\n    }\n    \n    .table-info tbody+tbody,.table-info td,.table-info th,.table-info thead th {\n    border-color:#86cfda;\n    }\n    \n    .table-warning,.table-warning>td,.table-warning>th {\n    background-color:#ffeeba;\n    }\n    \n    .table-warning tbody+tbody,.table-warning td,.table-warning th,.table-warning thead th {\n    border-color:#ffdf7e;\n    }\n    \n    .table-danger,.table-danger>td,.table-danger>th {\n    background-color:#f5c6cb;\n    }\n    \n    .table-danger tbody+tbody,.table-danger td,.table-danger th,.table-danger thead th {\n    border-color:#ed969e;\n    }\n    \n    .table-light,.table-light>td,.table-light>th {\n    background-color:#fdfdfe;\n    }\n    \n    .table-light tbody+tbody,.table-light td,.table-light th,.table-light thead th {\n    border-color:#fbfcfc;\n    }\n    \n    .table-dark,.table-dark>td,.table-dark>th {\n    background-color:#c6c8ca;\n    }\n    \n    .table-dark tbody+tbody,.table-dark td,.table-dark th,.table-dark thead th {\n    border-color:#95999c;\n    }\n    \n    .table .thead-dark th {\n    color:#fff;\n    background-color:#343a40;\n    border-color:#454d55;\n    }\n    \n    .table .thead-light th {\n    color:#495057;\n    background-color:#e9ecef;\n    border-color:#dee2e6;\n    }\n    \n    .table-dark {\n    color:#fff;\n    background-color:#343a40;\n    }\n    \n    .table-dark td,.table-dark th,.table-dark thead th {\n    border-color:#454d55;\n    }\n    \n    .table-dark.table-striped tbody tr:nth-of-type(odd) {\n    background-color:rgba(255,255,255,.05);\n    }\n    \n    .table-dark.table-hover tbody tr:hover {\n    color:#fff;\n    background-color:rgba(255,255,255,.075);\n    }\n    \n    .table-responsive {\n    display:block;\n    width:100%;\n    overflow-x:auto;\n    -webkit-overflow-scrolling:touch;\n    }\n    \n    .form-control {\n    display:block;\n    width:100%;\n    height:calc(1.5em+.75rem+2px);\n    font-size:1rem;\n    font-weight:400;\n    line-height:1.5;\n    color:#495057;\n    background-color:#fff;\n    background-clip:padding-box;\n    border:1px solid #ced4da;\n    border-radius:.25rem;\n    transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    padding:.375rem .75rem;\n    }\n    \n    .form-control::-ms-expand {\n    background-color:transparent;\n    border:0;\n    }\n    \n    .form-control:focus {\n    color:#495057;\n    background-color:#fff;\n    outline:0;\n    box-shadow:0 0 0 .2rem rgba(0,123,255,.25);\n    border-color:#80bdff;\n    }\n    \n    .form-control:disabled,.form-control[readonly] {\n    background-color:#e9ecef;\n    opacity:1;\n    }\n    \n    .col-form-label {\n    padding-top:calc(.375rem+1px);\n    padding-bottom:calc(.375rem+1px);\n    margin-bottom:0;\n    font-size:inherit;\n    line-height:1.5;\n    }\n    \n    .col-form-label-lg {\n    padding-top:calc(.5rem+1px);\n    padding-bottom:calc(.5rem+1px);\n    font-size:1.25rem;\n    line-height:1.5;\n    }\n    \n    .col-form-label-sm {\n    padding-top:calc(.25rem+1px);\n    padding-bottom:calc(.25rem+1px);\n    font-size:.875rem;\n    line-height:1.5;\n    }\n    \n    .form-control-plaintext {\n    display:block;\n    width:100%;\n    padding-top:.375rem;\n    padding-bottom:.375rem;\n    margin-bottom:0;\n    line-height:1.5;\n    color:#212529;\n    background-color:transparent;\n    border:solid transparent;\n    border-width:1px 0;\n    }\n    \n    .form-control-sm {\n    height:calc(1.5em+.5rem+2px);\n    font-size:.875rem;\n    line-height:1.5;\n    border-radius:.2rem;\n    padding:.25rem .5rem;\n    }\n    \n    .form-control-lg {\n    height:calc(1.5em+1rem+2px);\n    font-size:1.25rem;\n    line-height:1.5;\n    border-radius:.3rem;\n    padding:.5rem 1rem;\n    }\n    \n    .form-group {\n    margin-bottom:1rem;\n    }\n    \n    .form-text {\n    display:block;\n    margin-top:.25rem;\n    }\n    \n    .form-row {\n    display:flex;\n    flex-wrap:wrap;\n    margin-right:-5px;\n    margin-left:-5px;\n    }\n    \n    .form-row>.col,.form-row>[class*=col-] {\n    padding-right:5px;\n    padding-left:5px;\n    }\n    \n    .form-check {\n    position:relative;\n    display:block;\n    padding-left:1.25rem;\n    }\n    \n    .form-check-input {\n    position:absolute;\n    margin-top:.3rem;\n    margin-left:-1.25rem;\n    }\n    \n    .form-check-inline {\n    display:inline-flex;\n    align-items:center;\n    padding-left:0;\n    margin-right:.75rem;\n    }\n    \n    .form-check-inline .form-check-input {\n    position:static;\n    margin-top:0;\n    margin-right:.3125rem;\n    margin-left:0;\n    }\n    \n    .valid-feedback {\n    display:none;\n    width:100%;\n    margin-top:.25rem;\n    font-size:80%;\n    color:#28a745;\n    }\n    \n    .valid-tooltip {\n    position:absolute;\n    top:100%;\n    z-index:5;\n    display:none;\n    max-width:100%;\n    margin-top:.1rem;\n    font-size:.875rem;\n    line-height:1.5;\n    color:#fff;\n    background-color:rgba(40,167,69,.9);\n    border-radius:.25rem;\n    padding:.25rem .5rem;\n    }\n    \n    .form-control.is-valid,.was-validated .form-control:valid {\n    padding-right:calc(1.5em+.75rem);\n    background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0088'%3e%3cpath fill='%2328a745' d='M2.36.73L.64.53c-.4-1.04.46-1.41.1-.8l1.11.43.4-3.8c.6-.631.6-.271.2.7l-44.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e\");\n    background-repeat:no-repeat;\n    background-position:center right calc(.375em+.1875rem);\n    background-size:calc(.75em+.375rem) calc(.75em+.375rem);\n    border-color:#28a745;\n    }\n    \n    .custom-select.is-valid,.was-validated .custom-select:valid {\n    padding-right:calc((1em+.75rem) * 3px / 4px 0 1.75rem);\n    background:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0045'%3e%3cpath fill='%23343a40' d='M20L02h4zm05L03h4z'/%3e%3c/svg%3e\") no-repeat right .75rem center/8px 10px #fff no-repeat center right 1.75rem calc(.75em+.375rem);\n    border-color:#28a745;\n    }\n    \n    .custom-control-input.is-valid:checked~.custom-control-label::before,.was-validated .custom-control-input:valid:checked~.custom-control-label::before {\n    background-color:#34ce57;\n    border-color:#34ce57;\n    }\n    \n    .custom-control-input.is-valid:focus~.custom-control-label::before,.was-validated .custom-control-input:valid:focus~.custom-control-label::before {\n    box-shadow:0 0 0 .2rem rgba(40,167,69,.25);\n    }\n    \n    .invalid-feedback {\n    display:none;\n    width:100%;\n    margin-top:.25rem;\n    font-size:80%;\n    color:#dc3545;\n    }\n    \n    .invalid-tooltip {\n    position:absolute;\n    top:100%;\n    z-index:5;\n    display:none;\n    max-width:100%;\n    margin-top:.1rem;\n    font-size:.875rem;\n    line-height:1.5;\n    color:#fff;\n    background-color:rgba(220,53,69,.9);\n    border-radius:.25rem;\n    padding:.25rem .5rem;\n    }\n    \n    .form-control.is-invalid,.was-validated .form-control:invalid {\n    padding-right:calc(1.5em+.75rem);\n    background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23dc3545' viewBox='-2-277'%3e%3cpath stroke='%23dc3545' d='M00l33m0-3L03'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E\");\n    background-repeat:no-repeat;\n    background-position:center right calc(.375em+.1875rem);\n    background-size:calc(.75em+.375rem) calc(.75em+.375rem);\n    border-color:#dc3545;\n    }\n    \n    .custom-select.is-invalid,.was-validated .custom-select:invalid {\n    padding-right:calc((1em+.75rem) * 3px / 4px 0 1.75rem);\n    background:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0045'%3e%3cpath fill='%23343a40' d='M20L02h4zm05L03h4z'/%3e%3c/svg%3e\") no-repeat right .75rem center/8px 10px #fff no-repeat center right 1.75rem calc(.75em+.375rem);\n    border-color:#dc3545;\n    }\n    \n    .custom-control-input.is-invalid:checked~.custom-control-label::before,.was-validated .custom-control-input:invalid:checked~.custom-control-label::before {\n    background-color:#e4606d;\n    border-color:#e4606d;\n    }\n    \n    .custom-control-input.is-invalid:focus~.custom-control-label::before,.was-validated .custom-control-input:invalid:focus~.custom-control-label::before {\n    box-shadow:0 0 0 .2rem rgba(220,53,69,.25);\n    }\n    \n    .form-inline {\n    display:flex;\n    flex-flow:row wrap;\n    align-items:center;\n    }\n    \n    .btn {\n    display:inline-block;\n    font-weight:400;\n    color:#212529;\n    text-align:center;\n    vertical-align:middle;\n    -webkit-user-select:none;\n    user-select:none;\n    background-color:transparent;\n    border:1px solid transparent;\n    font-size:1rem;\n    line-height:1.5;\n    border-radius:.25rem;\n    transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    padding:.375rem .75rem;\n    }\n    \n    .btn:hover {\n    color:#212529;\n    text-decoration:none;\n    }\n    \n    .btn.focus,.btn:focus {\n    outline:0;\n    box-shadow:0 0 0 .2rem rgba(0,123,255,.25);\n    }\n    \n    .btn.disabled,.btn:disabled {\n    opacity:.65;\n    }\n    \n    a.btn.disabled,fieldset:disabled a.btn {\n    pointer-events:none;\n    }\n    \n    .btn-primary:hover {\n    color:#fff;\n    background-color:#0069d9;\n    border-color:#0062cc;\n    }\n    \n    .btn-primary:not(:disabled):not(.disabled).active,.btn-primary:not(:disabled):not(.disabled):active,.show>.btn-primary.dropdown-toggle {\n    color:#fff;\n    background-color:#0062cc;\n    border-color:#005cbf;\n    }\n    \n    .btn-secondary:hover {\n    color:#fff;\n    background-color:#5a6268;\n    border-color:#545b62;\n    }\n    \n    .btn-secondary:not(:disabled):not(.disabled).active,.btn-secondary:not(:disabled):not(.disabled):active,.show>.btn-secondary.dropdown-toggle {\n    color:#fff;\n    background-color:#545b62;\n    border-color:#4e555b;\n    }\n    \n    .btn-success:hover {\n    color:#fff;\n    background-color:#218838;\n    border-color:#1e7e34;\n    }\n    \n    .btn-success:not(:disabled):not(.disabled).active,.btn-success:not(:disabled):not(.disabled):active,.show>.btn-success.dropdown-toggle {\n    color:#fff;\n    background-color:#1e7e34;\n    border-color:#1c7430;\n    }\n    \n    .btn-info:hover {\n    color:#fff;\n    background-color:#138496;\n    border-color:#117a8b;\n    }\n    \n    .btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle {\n    color:#fff;\n    background-color:#117a8b;\n    border-color:#10707f;\n    }\n    \n    .btn-warning:hover {\n    color:#212529;\n    background-color:#e0a800;\n    border-color:#d39e00;\n    }\n    \n    .btn-warning:not(:disabled):not(.disabled).active,.btn-warning:not(:disabled):not(.disabled):active,.show>.btn-warning.dropdown-toggle {\n    color:#212529;\n    background-color:#d39e00;\n    border-color:#c69500;\n    }\n    \n    .btn-danger:hover {\n    color:#fff;\n    background-color:#c82333;\n    border-color:#bd2130;\n    }\n    \n    .btn-danger:not(:disabled):not(.disabled).active,.btn-danger:not(:disabled):not(.disabled):active,.show>.btn-danger.dropdown-toggle {\n    color:#fff;\n    background-color:#bd2130;\n    border-color:#b21f2d;\n    }\n    \n    .btn-light:hover {\n    color:#212529;\n    background-color:#e2e6ea;\n    border-color:#dae0e5;\n    }\n    \n    .btn-light:not(:disabled):not(.disabled).active,.btn-light:not(:disabled):not(.disabled):active,.show>.btn-light.dropdown-toggle {\n    color:#212529;\n    background-color:#dae0e5;\n    border-color:#d3d9df;\n    }\n    \n    .btn-dark:hover {\n    color:#fff;\n    background-color:#23272b;\n    border-color:#1d2124;\n    }\n    \n    .btn-dark:not(:disabled):not(.disabled).active,.btn-dark:not(:disabled):not(.disabled):active,.show>.btn-dark.dropdown-toggle {\n    color:#fff;\n    background-color:#1d2124;\n    border-color:#171a1d;\n    }\n    \n    .btn-outline-primary {\n    color:#007bff;\n    border-color:#007bff;\n    }\n    \n    .btn-outline-primary.disabled,.btn-outline-primary:disabled {\n    color:#007bff;\n    background-color:transparent;\n    }\n    \n    .btn-outline-secondary {\n    color:#6c757d;\n    border-color:#6c757d;\n    }\n    \n    .btn-outline-secondary.disabled,.btn-outline-secondary:disabled {\n    color:#6c757d;\n    background-color:transparent;\n    }\n    \n    .btn-outline-success {\n    color:#28a745;\n    border-color:#28a745;\n    }\n    \n    .btn-outline-success.disabled,.btn-outline-success:disabled {\n    color:#28a745;\n    background-color:transparent;\n    }\n    \n    .btn-outline-info {\n    color:#17a2b8;\n    border-color:#17a2b8;\n    }\n    \n    .btn-outline-info.disabled,.btn-outline-info:disabled {\n    color:#17a2b8;\n    background-color:transparent;\n    }\n    \n    .btn-outline-warning {\n    color:#ffc107;\n    border-color:#ffc107;\n    }\n    \n    .btn-outline-warning.disabled,.btn-outline-warning:disabled {\n    color:#ffc107;\n    background-color:transparent;\n    }\n    \n    .btn-outline-danger {\n    color:#dc3545;\n    border-color:#dc3545;\n    }\n    \n    .btn-outline-danger.disabled,.btn-outline-danger:disabled {\n    color:#dc3545;\n    background-color:transparent;\n    }\n    \n    .btn-outline-light {\n    color:#f8f9fa;\n    border-color:#f8f9fa;\n    }\n    \n    .btn-outline-light.disabled,.btn-outline-light:disabled {\n    color:#f8f9fa;\n    background-color:transparent;\n    }\n    \n    .btn-outline-dark {\n    color:#343a40;\n    border-color:#343a40;\n    }\n    \n    .btn-outline-dark.disabled,.btn-outline-dark:disabled {\n    color:#343a40;\n    background-color:transparent;\n    }\n    \n    .btn-link {\n    font-weight:400;\n    color:#007bff;\n    text-decoration:none;\n    }\n    \n    .btn-link.focus,.btn-link:focus {\n    text-decoration:underline;\n    box-shadow:none;\n    }\n    \n    .btn-link.disabled,.btn-link:disabled {\n    color:#6c757d;\n    pointer-events:none;\n    }\n    \n    .btn-block+.btn-block {\n    margin-top:.5rem;\n    }\n    \n    .fade {\n    transition:opacity .15s linear;\n    }\n    \n    .fade:not(.show) {\n    opacity:0;\n    }\n    \n    .collapsing {\n    position:relative;\n    height:0;\n    overflow:hidden;\n    transition:height .35s ease;\n    }\n    \n    .dropdown,.dropleft,.dropright,.dropup {\n    position:relative;\n    }\n    \n    .dropdown-toggle {\n    white-space:nowrap;\n    }\n    \n    .dropdown-toggle::after {\n    display:inline-block;\n    margin-left:.255em;\n    vertical-align:.255em;\n    content:\"\";\n    border-top:.3em solid;\n    border-right:.3em solid transparent;\n    border-bottom:0;\n    border-left:.3em solid transparent;\n    }\n    \n    .dropdown-menu {\n    position:absolute;\n    top:100%;\n    left:0;\n    z-index:1000;\n    display:none;\n    float:left;\n    min-width:10rem;\n    font-size:1rem;\n    color:#212529;\n    text-align:left;\n    list-style:none;\n    background-color:#fff;\n    background-clip:padding-box;\n    border:1px solid rgba(0,0,0,.15);\n    border-radius:.25rem;\n    margin:.125rem 0 0;\n    padding:.5rem 0;\n    }\n    \n    .dropdown-menu-left {\n    right:auto;\n    left:0;\n    }\n    \n    .dropdown-menu-right {\n    right:0;\n    left:auto;\n    }\n    \n    .dropup .dropdown-menu {\n    top:auto;\n    bottom:100%;\n    margin-top:0;\n    margin-bottom:.125rem;\n    }\n    \n    .dropup .dropdown-toggle::after {\n    display:inline-block;\n    margin-left:.255em;\n    vertical-align:.255em;\n    content:\"\";\n    border-top:0;\n    border-right:.3em solid transparent;\n    border-bottom:.3em solid;\n    border-left:.3em solid transparent;\n    }\n    \n    .dropright .dropdown-menu {\n    top:0;\n    right:auto;\n    left:100%;\n    margin-top:0;\n    margin-left:.125rem;\n    }\n    \n    .dropright .dropdown-toggle::after {\n    display:inline-block;\n    margin-left:.255em;\n    content:\"\";\n    border-top:.3em solid transparent;\n    border-right:0;\n    border-bottom:.3em solid transparent;\n    border-left:.3em solid;\n    vertical-align:0;\n    }\n    \n    .dropleft .dropdown-menu {\n    top:0;\n    right:100%;\n    left:auto;\n    margin-top:0;\n    margin-right:.125rem;\n    }\n    \n    .dropleft .dropdown-toggle::after {\n    margin-left:.255em;\n    vertical-align:.255em;\n    content:\"\";\n    display:none;\n    }\n    \n    .dropleft .dropdown-toggle::before {\n    display:inline-block;\n    margin-right:.255em;\n    content:\"\";\n    border-top:.3em solid transparent;\n    border-right:.3em solid;\n    border-bottom:.3em solid transparent;\n    vertical-align:0;\n    }\n    \n    .dropdown-menu[x-placement^=bottom],.dropdown-menu[x-placement^=left],.dropdown-menu[x-placement^=right],.dropdown-menu[x-placement^=top] {\n    right:auto;\n    bottom:auto;\n    }\n    \n    .dropdown-divider {\n    height:0;\n    overflow:hidden;\n    border-top:1px solid #e9ecef;\n    margin:.5rem 0;\n    }\n    \n    .dropdown-item {\n    display:block;\n    width:100%;\n    clear:both;\n    font-weight:400;\n    color:#212529;\n    text-align:inherit;\n    white-space:nowrap;\n    background-color:transparent;\n    border:0;\n    padding:.25rem 1.5rem;\n    }\n    \n    .dropdown-item:focus,.dropdown-item:hover {\n    color:#16181b;\n    text-decoration:none;\n    background-color:#f8f9fa;\n    }\n    \n    .dropdown-item.active,.dropdown-item:active {\n    color:#fff;\n    text-decoration:none;\n    background-color:#007bff;\n    }\n    \n    .dropdown-item.disabled,.dropdown-item:disabled {\n    color:#6c757d;\n    pointer-events:none;\n    background-color:transparent;\n    }\n    \n    .dropdown-header {\n    display:block;\n    margin-bottom:0;\n    font-size:.875rem;\n    color:#6c757d;\n    white-space:nowrap;\n    padding:.5rem 1.5rem;\n    }\n    \n    .dropdown-item-text {\n    display:block;\n    color:#212529;\n    padding:.25rem 1.5rem;\n    }\n    \n    .btn-group,.btn-group-vertical {\n    position:relative;\n    display:inline-flex;\n    vertical-align:middle;\n    }\n    \n    .btn-group-vertical>.btn,.btn-group>.btn {\n    position:relative;\n    flex:1 1 auto;\n    }\n    \n    .btn-toolbar {\n    display:flex;\n    flex-wrap:wrap;\n    justify-content:flex-start;\n    }\n    \n    .btn-toolbar .input-group {\n    width:auto;\n    }\n    \n    .dropdown-toggle-split {\n    padding-right:.5625rem;\n    padding-left:.5625rem;\n    }\n    \n    .dropleft .dropdown-toggle-split::before {\n    margin-right:0;\n    }\n    \n    .btn-group-sm>.btn+.dropdown-toggle-split,.btn-sm+.dropdown-toggle-split {\n    padding-right:.375rem;\n    padding-left:.375rem;\n    }\n    \n    .btn-group-lg>.btn+.dropdown-toggle-split,.btn-lg+.dropdown-toggle-split {\n    padding-right:.75rem;\n    padding-left:.75rem;\n    }\n    \n    .btn-group-vertical {\n    flex-direction:column;\n    align-items:flex-start;\n    justify-content:center;\n    }\n    \n    .btn-group-vertical>.btn-group:not(:first-child),.btn-group-vertical>.btn:not(:first-child) {\n    margin-top:-1px;\n    }\n    \n    .btn-group-vertical>.btn-group:not(:last-child)>.btn,.btn-group-vertical>.btn:not(:last-child):not(.dropdown-toggle) {\n    border-bottom-right-radius:0;\n    border-bottom-left-radius:0;\n    }\n    \n    .btn-group-vertical>.btn-group:not(:first-child)>.btn,.btn-group-vertical>.btn:not(:first-child) {\n    border-top-left-radius:0;\n    border-top-right-radius:0;\n    }\n    \n    .btn-group-toggle>.btn input[type=checkbox],.btn-group-toggle>.btn input[type=radio],.btn-group-toggle>.btn-group>.btn input[type=checkbox],.btn-group-toggle>.btn-group>.btn input[type=radio] {\n    position:absolute;\n    clip:rect(0,0,0,0);\n    pointer-events:none;\n    }\n    \n    .input-group {\n    position:relative;\n    display:flex;\n    flex-wrap:wrap;\n    align-items:stretch;\n    width:100%;\n    }\n    \n    .input-group>.custom-file,.input-group>.custom-select,.input-group>.form-control,.input-group>.form-control-plaintext {\n    position:relative;\n    flex:1 1 auto;\n    width:1%;\n    margin-bottom:0;\n    }\n    \n    .input-group>.custom-file .custom-file-input:focus {\n    z-index:4;\n    }\n    \n    .input-group>.custom-file {\n    display:flex;\n    align-items:center;\n    }\n    \n    .input-group-append,.input-group-prepend {\n    display:flex;\n    }\n    \n    .input-group-append .btn,.input-group-prepend .btn {\n    position:relative;\n    z-index:2;\n    }\n    \n    .input-group-prepend {\n    margin-right:-1px;\n    }\n    \n    .input-group-text {\n    display:flex;\n    align-items:center;\n    margin-bottom:0;\n    font-size:1rem;\n    font-weight:400;\n    line-height:1.5;\n    color:#495057;\n    text-align:center;\n    white-space:nowrap;\n    background-color:#e9ecef;\n    border:1px solid #ced4da;\n    border-radius:.25rem;\n    padding:.375rem .75rem;\n    }\n    \n    .input-group-text input[type=checkbox],.input-group-text input[type=radio] {\n    margin-top:0;\n    }\n    \n    .input-group-lg>.custom-select,.input-group-lg>.form-control:not(textarea) {\n    height:calc(1.5em+1rem+2px);\n    }\n    \n    .input-group-sm>.custom-select,.input-group-sm>.form-control:not(textarea) {\n    height:calc(1.5em+.5rem+2px);\n    }\n    \n    .input-group-lg>.custom-select,.input-group-sm>.custom-select {\n    padding-right:1.75rem;\n    }\n    \n    .custom-control {\n    position:relative;\n    display:block;\n    min-height:1.5rem;\n    padding-left:1.5rem;\n    }\n    \n    .custom-control-inline {\n    display:inline-flex;\n    margin-right:1rem;\n    }\n    \n    .custom-control-input {\n    position:absolute;\n    z-index:-1;\n    opacity:0;\n    }\n    \n    .custom-control-input:checked~.custom-control-label::before {\n    color:#fff;\n    background-color:#007bff;\n    border-color:#007bff;\n    }\n    \n    .custom-control-input:focus~.custom-control-label::before {\n    box-shadow:0 0 0 .2rem rgba(0,123,255,.25);\n    }\n    \n    .custom-control-input:focus:not(:checked)~.custom-control-label::before {\n    border-color:#80bdff;\n    }\n    \n    .custom-control-input:not(:disabled):active~.custom-control-label::before {\n    color:#fff;\n    background-color:#b3d7ff;\n    border-color:#b3d7ff;\n    }\n    \n    .custom-control-label {\n    position:relative;\n    margin-bottom:0;\n    vertical-align:top;\n    }\n    \n    .custom-control-label::before {\n    position:absolute;\n    top:.25rem;\n    left:-1.5rem;\n    display:block;\n    width:1rem;\n    height:1rem;\n    pointer-events:none;\n    content:\"\";\n    background-color:#fff;\n    border:#adb5bd solid 1px;\n    }\n    \n    .custom-control-label::after {\n    position:absolute;\n    top:.25rem;\n    left:-1.5rem;\n    display:block;\n    width:1rem;\n    height:1rem;\n    content:\"\";\n    background:no-repeat 50% 50%;\n    }\n    \n    .custom-checkbox .custom-control-input:checked~.custom-control-label::after {\n    background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0088'%3e%3cpath fill='%23fff' d='M6.564.75l-3.593.612-1.538-1.55L04.262.9747.2582.193z'/%3e%3c/svg%3e\");\n    }\n    \n    .custom-checkbox .custom-control-input:indeterminate~.custom-control-label::before {\n    background-color:#007bff;\n    border-color:#007bff;\n    }\n    \n    .custom-checkbox .custom-control-input:indeterminate~.custom-control-label::after {\n    background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0044'%3e%3cpath stroke='%23fff' d='M02h4'/%3e%3c/svg%3e\");\n    }\n    \n    .custom-radio .custom-control-label::before {\n    border-radius:50%;\n    }\n    \n    .custom-radio .custom-control-input:checked~.custom-control-label::after {\n    background-image:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4-488'%3e%3ccircle r='3' fill='%23fff'/%3e%3c/svg%3e\");\n    }\n    \n    .custom-switch {\n    padding-left:2.25rem;\n    }\n    \n    .custom-switch .custom-control-label::before {\n    left:-2.25rem;\n    width:1.75rem;\n    pointer-events:all;\n    border-radius:.5rem;\n    }\n    \n    .custom-switch .custom-control-label::after {\n    top:calc(.25rem+2px);\n    left:calc(-2.25rem+2px);\n    width:calc(1rem-4px);\n    height:calc(1rem-4px);\n    background-color:#adb5bd;\n    border-radius:.5rem;\n    transition:transform .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    }\n    \n    .custom-switch .custom-control-input:checked~.custom-control-label::after {\n    background-color:#fff;\n    transform:translateX(.75rem);\n    }\n    \n    .custom-select {\n    display:inline-block;\n    width:100%;\n    height:calc(1.5em+.75rem+2px);\n    font-size:1rem;\n    font-weight:400;\n    line-height:1.5;\n    color:#495057;\n    vertical-align:middle;\n    background:url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0045'%3e%3cpath fill='%23343a40' d='M20L02h4zm05L03h4z'/%3e%3c/svg%3e\") no-repeat right .75rem center/8px 10px;\n    background-color:#fff;\n    border:1px solid #ced4da;\n    border-radius:.25rem;\n    -webkit-appearance:none;\n    -moz-appearance:none;\n    appearance:none;\n    padding:.375rem 1.75rem .375rem .75rem;\n    }\n    \n    .custom-select:focus {\n    outline:0;\n    box-shadow:0 0 0 .2rem rgba(0,123,255,.25);\n    border-color:#80bdff;\n    }\n    \n    .custom-select[multiple],.custom-select[size]:not([size=\"1\"]) {\n    height:auto;\n    padding-right:.75rem;\n    background-image:none;\n    }\n    \n    .custom-select:disabled {\n    color:#6c757d;\n    background-color:#e9ecef;\n    }\n    \n    .custom-select-sm {\n    height:calc(1.5em+.5rem+2px);\n    padding-top:.25rem;\n    padding-bottom:.25rem;\n    padding-left:.5rem;\n    font-size:.875rem;\n    }\n    \n    .custom-select-lg {\n    height:calc(1.5em+1rem+2px);\n    padding-top:.5rem;\n    padding-bottom:.5rem;\n    padding-left:1rem;\n    font-size:1.25rem;\n    }\n    \n    .custom-file {\n    position:relative;\n    display:inline-block;\n    width:100%;\n    height:calc(1.5em+.75rem+2px);\n    margin-bottom:0;\n    }\n    \n    .custom-file-input {\n    position:relative;\n    z-index:2;\n    width:100%;\n    height:calc(1.5em+.75rem+2px);\n    opacity:0;\n    margin:0;\n    }\n    \n    .custom-file-input:focus~.custom-file-label {\n    box-shadow:0 0 0 .2rem rgba(0,123,255,.25);\n    border-color:#80bdff;\n    }\n    \n    .custom-file-input:lang(en)~.custom-file-label::after {\n    content:\"Browse\";\n    }\n    \n    .custom-file-input~.custom-file-label[data-browse]::after {\n    content:attr(data-browse);\n    }\n    \n    .custom-file-label {\n    position:absolute;\n    top:0;\n    right:0;\n    left:0;\n    z-index:1;\n    height:calc(1.5em+.75rem+2px);\n    font-weight:400;\n    line-height:1.5;\n    color:#495057;\n    background-color:#fff;\n    border:1px solid #ced4da;\n    border-radius:.25rem;\n    padding:.375rem .75rem;\n    }\n    \n    .custom-file-label::after {\n    position:absolute;\n    top:0;\n    right:0;\n    bottom:0;\n    z-index:3;\n    display:block;\n    height:calc(1.5em+.75rem);\n    line-height:1.5;\n    color:#495057;\n    content:\"Browse\";\n    background-color:#e9ecef;\n    border-left:inherit;\n    border-radius:0 .25rem .25rem 0;\n    padding:.375rem .75rem;\n    }\n    \n    .custom-range {\n    width:100%;\n    height:calc(1rem+.4rem);\n    background-color:transparent;\n    -webkit-appearance:none;\n    -moz-appearance:none;\n    appearance:none;\n    padding:0;\n    }\n    \n    .custom-range::-webkit-slider-thumb {\n    width:1rem;\n    height:1rem;\n    margin-top:-.25rem;\n    background-color:#007bff;\n    border:0;\n    border-radius:1rem;\n    -webkit-transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    -webkit-appearance:none;\n    appearance:none;\n    }\n    \n    .custom-range::-moz-range-thumb {\n    width:1rem;\n    height:1rem;\n    background-color:#007bff;\n    border:0;\n    border-radius:1rem;\n    -moz-transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    -moz-appearance:none;\n    appearance:none;\n    }\n    \n    .custom-range::-ms-thumb {\n    width:1rem;\n    height:1rem;\n    margin-top:0;\n    margin-right:.2rem;\n    margin-left:.2rem;\n    background-color:#007bff;\n    border:0;\n    border-radius:1rem;\n    -ms-transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    appearance:none;\n    }\n    \n    .custom-range::-ms-track {\n    width:100%;\n    height:.5rem;\n    color:transparent;\n    cursor:pointer;\n    background-color:transparent;\n    border-color:transparent;\n    border-width:.5rem;\n    }\n    \n    .custom-range::-ms-fill-lower {\n    background-color:#dee2e6;\n    border-radius:1rem;\n    }\n    \n    .custom-range::-ms-fill-upper {\n    margin-right:15px;\n    background-color:#dee2e6;\n    border-radius:1rem;\n    }\n    \n    .custom-control-label::before,.custom-file-label,.custom-select {\n    transition:background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    }\n    \n    .nav {\n    display:flex;\n    flex-wrap:wrap;\n    padding-left:0;\n    margin-bottom:0;\n    list-style:none;\n    }\n    \n    .nav-link {\n    display:block;\n    padding:.5rem 1rem;\n    }\n    \n    .nav-link.disabled {\n    color:#6c757d;\n    pointer-events:none;\n    cursor:default;\n    }\n    \n    .nav-tabs {\n    border-bottom:1px solid #dee2e6;\n    }\n    \n    .nav-tabs .nav-item {\n    margin-bottom:-1px;\n    }\n    \n    .nav-tabs .nav-link {\n    border:1px solid transparent;\n    border-top-left-radius:.25rem;\n    border-top-right-radius:.25rem;\n    }\n    \n    .nav-tabs .nav-link:focus,.nav-tabs .nav-link:hover {\n    border-color:#e9ecef #e9ecef #dee2e6;\n    }\n    \n    .nav-tabs .nav-link.disabled {\n    color:#6c757d;\n    background-color:transparent;\n    border-color:transparent;\n    }\n    \n    .nav-tabs .nav-item.show .nav-link,.nav-tabs .nav-link.active {\n    color:#495057;\n    background-color:#fff;\n    border-color:#dee2e6 #dee2e6 #fff;\n    }\n    \n    .nav-tabs .dropdown-menu {\n    margin-top:-1px;\n    border-top-left-radius:0;\n    border-top-right-radius:0;\n    }\n    \n    .nav-pills .nav-link.active,.nav-pills .show>.nav-link {\n    color:#fff;\n    background-color:#007bff;\n    }\n    \n    .nav-fill .nav-item {\n    flex:1 1 auto;\n    text-align:center;\n    }\n    \n    .nav-justified .nav-item {\n    flex-basis:0;\n    flex-grow:1;\n    text-align:center;\n    }\n    \n    .navbar {\n    position:relative;\n    display:flex;\n    flex-wrap:wrap;\n    align-items:center;\n    justify-content:space-between;\n    padding:.5rem 1rem;\n    }\n    \n    .navbar>.container,.navbar>.container-fluid {\n    display:flex;\n    flex-wrap:wrap;\n    align-items:center;\n    justify-content:space-between;\n    }\n    \n    .navbar-brand {\n    display:inline-block;\n    padding-top:.3125rem;\n    padding-bottom:.3125rem;\n    margin-right:1rem;\n    font-size:1.25rem;\n    line-height:inherit;\n    white-space:nowrap;\n    }\n    \n    .navbar-nav {\n    display:flex;\n    flex-direction:column;\n    padding-left:0;\n    margin-bottom:0;\n    list-style:none;\n    }\n    \n    .navbar-nav .dropdown-menu {\n    position:static;\n    float:none;\n    }\n    \n    .navbar-text {\n    display:inline-block;\n    padding-top:.5rem;\n    padding-bottom:.5rem;\n    }\n    \n    .navbar-collapse {\n    flex-basis:100%;\n    flex-grow:1;\n    align-items:center;\n    }\n    \n    .navbar-toggler {\n    font-size:1.25rem;\n    line-height:1;\n    background-color:transparent;\n    border:1px solid transparent;\n    border-radius:.25rem;\n    padding:.25rem .75rem;\n    }\n    \n    .navbar-toggler-icon {\n    display:inline-block;\n    width:1.5em;\n    height:1.5em;\n    vertical-align:middle;\n    content:\"\";\n    background:no-repeat center center;\n    background-size:100% 100%;\n    }\n    \n    .navbar-expand {\n    flex-flow:row nowrap;\n    justify-content:flex-start;\n    }\n    \n    .navbar-expand>.container,.navbar-expand>.container-fluid {\n    padding-right:0;\n    padding-left:0;\n    flex-wrap:nowrap;\n    }\n    \n    .navbar-expand .navbar-nav {\n    flex-direction:row;\n    }\n    \n    .navbar-expand .navbar-nav .dropdown-menu {\n    position:absolute;\n    }\n    \n    .navbar-expand .navbar-nav .nav-link {\n    padding-right:.5rem;\n    padding-left:.5rem;\n    }\n    \n    .navbar-expand .navbar-collapse {\n    display:flex!important;\n    flex-basis:auto;\n    }\n    \n    .navbar-light .navbar-nav .nav-link {\n    color:rgba(0,0,0,.5);\n    }\n    \n    .navbar-light .navbar-nav .nav-link:focus,.navbar-light .navbar-nav .nav-link:hover {\n    color:rgba(0,0,0,.7);\n    }\n    \n    .navbar-light .navbar-nav .nav-link.disabled {\n    color:rgba(0,0,0,.3);\n    }\n    \n    .navbar-light .navbar-toggler {\n    color:rgba(0,0,0,.5);\n    border-color:rgba(0,0,0,.1);\n    }\n    \n    article,aside,figcaption,figure,footer,header,hgroup,main,nav,section,.form-control.is-valid~.valid-feedback,.form-control.is-valid~.valid-tooltip,.was-validated .form-control:valid~.valid-feedback,.was-validated .form-control:valid~.valid-tooltip,.custom-select.is-valid~.valid-feedback,.custom-select.is-valid~.valid-tooltip,.was-validated .custom-select:valid~.valid-feedback,.was-validated .custom-select:valid~.valid-tooltip,.form-control-file.is-valid~.valid-feedback,.form-control-file.is-valid~.valid-tooltip,.was-validated .form-control-file:valid~.valid-feedback,.was-validated .form-control-file:valid~.valid-tooltip,.form-check-input.is-valid~.valid-feedback,.form-check-input.is-valid~.valid-tooltip,.was-validated .form-check-input:valid~.valid-feedback,.was-validated .form-check-input:valid~.valid-tooltip,.custom-control-input.is-valid~.valid-feedback,.custom-control-input.is-valid~.valid-tooltip,.was-validated .custom-control-input:valid~.valid-feedback,.was-validated .custom-control-input:valid~.valid-tooltip,.custom-file-input.is-valid~.valid-feedback,.custom-file-input.is-valid~.valid-tooltip,.was-validated .custom-file-input:valid~.valid-feedback,.was-validated .custom-file-input:valid~.valid-tooltip,.form-control.is-invalid~.invalid-feedback,.form-control.is-invalid~.invalid-tooltip,.was-validated .form-control:invalid~.invalid-feedback,.was-validated .form-control:invalid~.invalid-tooltip,.custom-select.is-invalid~.invalid-feedback,.custom-select.is-invalid~.invalid-tooltip,.was-validated .custom-select:invalid~.invalid-feedback,.was-validated .custom-select:invalid~.invalid-tooltip,.form-control-file.is-invalid~.invalid-feedback,.form-control-file.is-invalid~.invalid-tooltip,.was-validated .form-control-file:invalid~.invalid-feedback,.was-validated .form-control-file:invalid~.invalid-tooltip,.form-check-input.is-invalid~.invalid-feedback,.form-check-input.is-invalid~.invalid-tooltip,.was-validated .form-check-input:invalid~.invalid-feedback,.was-validated .form-check-input:invalid~.invalid-tooltip,.custom-control-input.is-invalid~.invalid-feedback,.custom-control-input.is-invalid~.invalid-tooltip,.was-validated .custom-control-input:invalid~.invalid-feedback,.was-validated .custom-control-input:invalid~.invalid-tooltip,.custom-file-input.is-invalid~.invalid-feedback,.custom-file-input.is-invalid~.invalid-tooltip,.was-validated .custom-file-input:invalid~.invalid-feedback,.was-validated .custom-file-input:invalid~.invalid-tooltip,.dropdown-menu.show,.tab-content>.active {\n    display:block;\n    }\n    \n    p,dl,ol,ul {\n    margin-top:0;\n    margin-bottom:1rem;\n    }\n    \n    ol ol,ol ul,ul ol,ul ul,.form-check-label,.btn-group-toggle>.btn,.btn-group-toggle>.btn-group>.btn {\n    margin-bottom:0;\n    }\n    \n    blockquote,figure {\n    margin:0 0 1rem;\n    }\n    \n    a:hover,.btn-link:hover {\n    color:#0056b3;\n    text-decoration:underline;\n    }\n    \n    a:not([href]):not([tabindex]),a:not([href]):not([tabindex]):focus,a:not([href]):not([tabindex]):hover {\n    color:inherit;\n    text-decoration:none;\n    }\n    \n    a:not([href]):not([tabindex]):focus,.custom-range:focus {\n    outline:0;\n    }\n    \n    [type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button,select.form-control[multiple],select.form-control[size],textarea.form-control {\n    height:auto;\n    }\n    \n    output,.list-inline-item,.figure {\n    display:inline-block;\n    }\n    \n    template,.collapse:not(.show),.custom-select::-ms-expand,.tab-content>.tab-pane,.navbar-expand .navbar-toggler {\n    display:none;\n    }\n    \n    .list-unstyled,.list-inline {\n    padding-left:0;\n    list-style:none;\n    }\n    \n    .container,.container-fluid {\n    width:100%;\n    padding-right:15px;\n    padding-left:15px;\n    margin-right:auto;\n    margin-left:auto;\n    }\n    \n    .no-gutters>.col,.no-gutters>[class*=col-],.form-control-plaintext.form-control-lg,.form-control-plaintext.form-control-sm,.navbar-nav .nav-link {\n    padding-right:0;\n    padding-left:0;\n    }\n    \n    .table-bordered,.table-bordered td,.table-bordered th {\n    border:1px solid #dee2e6;\n    }\n    \n    .table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th,.table-dark.table-bordered,.table-responsive>.table-bordered,.custom-range::-moz-focus-outer {\n    border:0;\n    }\n    \n    .table-hover .table-primary:hover,.table-hover .table-primary:hover>td,.table-hover .table-primary:hover>th {\n    background-color:#9fcdff;\n    }\n    \n    .table-hover .table-secondary:hover,.table-hover .table-secondary:hover>td,.table-hover .table-secondary:hover>th {\n    background-color:#c8cbcf;\n    }\n    \n    .table-hover .table-success:hover,.table-hover .table-success:hover>td,.table-hover .table-success:hover>th {\n    background-color:#b1dfbb;\n    }\n    \n    .table-hover .table-info:hover,.table-hover .table-info:hover>td,.table-hover .table-info:hover>th {\n    background-color:#abdde5;\n    }\n    \n    .table-hover .table-warning:hover,.table-hover .table-warning:hover>td,.table-hover .table-warning:hover>th {\n    background-color:#ffe8a1;\n    }\n    \n    .table-hover .table-danger:hover,.table-hover .table-danger:hover>td,.table-hover .table-danger:hover>th {\n    background-color:#f1b0b7;\n    }\n    \n    .table-hover .table-light:hover,.table-hover .table-light:hover>td,.table-hover .table-light:hover>th {\n    background-color:#ececf6;\n    }\n    \n    .table-hover .table-dark:hover,.table-hover .table-dark:hover>td,.table-hover .table-dark:hover>th {\n    background-color:#b9bbbe;\n    }\n    \n    .table-active,.table-active>td,.table-active>th,.table-hover .table-active:hover,.table-hover .table-active:hover>td,.table-hover .table-active:hover>th {\n    background-color:rgba(0,0,0,.075);\n    }\n    \n    .form-control::-webkit-input-placeholder,.form-control::-moz-placeholder,.form-control:-ms-input-placeholder,.form-control::-ms-input-placeholder,.form-control::placeholder {\n    color:#6c757d;\n    opacity:1;\n    }\n    \n    select.form-control:focus::-ms-value,.custom-select:focus::-ms-value {\n    color:#495057;\n    background-color:#fff;\n    }\n    \n    .form-control-file,.form-control-range,.btn-block {\n    display:block;\n    width:100%;\n    }\n    \n    .form-check-input:disabled~.form-check-label,.custom-control-input:disabled~.custom-control-label {\n    color:#6c757d;\n    }\n    \n    .form-control.is-valid:focus,.was-validated .form-control:valid:focus,.custom-select.is-valid:focus,.was-validated .custom-select:valid:focus,.custom-file-input.is-valid:focus~.custom-file-label,.was-validated .custom-file-input:valid:focus~.custom-file-label {\n    box-shadow:0 0 0 .2rem rgba(40,167,69,.25);\n    border-color:#28a745;\n    }\n    \n    .was-validated textarea.form-control:valid,textarea.form-control.is-valid,.was-validated textarea.form-control:invalid,textarea.form-control.is-invalid {\n    padding-right:calc(1.5em+.75rem);\n    background-position:top calc(.375em+.1875rem) right calc(.375em+.1875rem);\n    }\n    \n    .form-check-input.is-valid~.form-check-label,.was-validated .form-check-input:valid~.form-check-label,.custom-control-input.is-valid~.custom-control-label,.was-validated .custom-control-input:valid~.custom-control-label {\n    color:#28a745;\n    }\n    \n    .custom-control-input.is-valid~.custom-control-label::before,.was-validated .custom-control-input:valid~.custom-control-label::before,.custom-control-input.is-valid:focus:not(:checked)~.custom-control-label::before,.was-validated .custom-control-input:valid:focus:not(:checked)~.custom-control-label::before,.custom-file-input.is-valid~.custom-file-label,.was-validated .custom-file-input:valid~.custom-file-label {\n    border-color:#28a745;\n    }\n    \n    .form-control.is-invalid:focus,.was-validated .form-control:invalid:focus,.custom-select.is-invalid:focus,.was-validated .custom-select:invalid:focus,.custom-file-input.is-invalid:focus~.custom-file-label,.was-validated .custom-file-input:invalid:focus~.custom-file-label {\n    box-shadow:0 0 0 .2rem rgba(220,53,69,.25);\n    border-color:#dc3545;\n    }\n    \n    .form-check-input.is-invalid~.form-check-label,.was-validated .form-check-input:invalid~.form-check-label,.custom-control-input.is-invalid~.custom-control-label,.was-validated .custom-control-input:invalid~.custom-control-label {\n    color:#dc3545;\n    }\n    \n    .custom-control-input.is-invalid~.custom-control-label::before,.was-validated .custom-control-input:invalid~.custom-control-label::before,.custom-control-input.is-invalid:focus:not(:checked)~.custom-control-label::before,.was-validated .custom-control-input:invalid:focus:not(:checked)~.custom-control-label::before,.custom-file-input.is-invalid~.custom-file-label,.was-validated .custom-file-input:invalid~.custom-file-label {\n    border-color:#dc3545;\n    }\n    \n    .form-inline .form-check,input[type=button].btn-block,input[type=reset].btn-block,input[type=submit].btn-block,.btn-group-vertical>.btn,.btn-group-vertical>.btn-group {\n    width:100%;\n    }\n    \n    .btn-primary,.btn-primary.disabled,.btn-primary:disabled,.btn-outline-primary:hover,.btn-outline-primary:not(:disabled):not(.disabled).active,.btn-outline-primary:not(:disabled):not(.disabled):active,.show>.btn-outline-primary.dropdown-toggle {\n    color:#fff;\n    background-color:#007bff;\n    border-color:#007bff;\n    }\n    \n    .btn-primary.focus,.btn-primary:focus,.btn-primary:not(:disabled):not(.disabled).active:focus,.btn-primary:not(:disabled):not(.disabled):active:focus,.show>.btn-primary.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(38,143,255,.5);\n    }\n    \n    .btn-secondary,.btn-secondary.disabled,.btn-secondary:disabled,.btn-outline-secondary:hover,.btn-outline-secondary:not(:disabled):not(.disabled).active,.btn-outline-secondary:not(:disabled):not(.disabled):active,.show>.btn-outline-secondary.dropdown-toggle {\n    color:#fff;\n    background-color:#6c757d;\n    border-color:#6c757d;\n    }\n    \n    .btn-secondary.focus,.btn-secondary:focus,.btn-secondary:not(:disabled):not(.disabled).active:focus,.btn-secondary:not(:disabled):not(.disabled):active:focus,.show>.btn-secondary.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(130,138,145,.5);\n    }\n    \n    .btn-success,.btn-success.disabled,.btn-success:disabled,.btn-outline-success:hover,.btn-outline-success:not(:disabled):not(.disabled).active,.btn-outline-success:not(:disabled):not(.disabled):active,.show>.btn-outline-success.dropdown-toggle {\n    color:#fff;\n    background-color:#28a745;\n    border-color:#28a745;\n    }\n    \n    .btn-success.focus,.btn-success:focus,.btn-success:not(:disabled):not(.disabled).active:focus,.btn-success:not(:disabled):not(.disabled):active:focus,.show>.btn-success.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(72,180,97,.5);\n    }\n    \n    .btn-info,.btn-info.disabled,.btn-info:disabled,.btn-outline-info:hover,.btn-outline-info:not(:disabled):not(.disabled).active,.btn-outline-info:not(:disabled):not(.disabled):active,.show>.btn-outline-info.dropdown-toggle {\n    color:#fff;\n    background-color:#17a2b8;\n    border-color:#17a2b8;\n    }\n    \n    .btn-info.focus,.btn-info:focus,.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(58,176,195,.5);\n    }\n    \n    .btn-warning,.btn-warning.disabled,.btn-warning:disabled,.btn-outline-warning:hover,.btn-outline-warning:not(:disabled):not(.disabled).active,.btn-outline-warning:not(:disabled):not(.disabled):active,.show>.btn-outline-warning.dropdown-toggle {\n    color:#212529;\n    background-color:#ffc107;\n    border-color:#ffc107;\n    }\n    \n    .btn-warning.focus,.btn-warning:focus,.btn-warning:not(:disabled):not(.disabled).active:focus,.btn-warning:not(:disabled):not(.disabled):active:focus,.show>.btn-warning.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(222,170,12,.5);\n    }\n    \n    .btn-danger,.btn-danger.disabled,.btn-danger:disabled,.btn-outline-danger:hover,.btn-outline-danger:not(:disabled):not(.disabled).active,.btn-outline-danger:not(:disabled):not(.disabled):active,.show>.btn-outline-danger.dropdown-toggle {\n    color:#fff;\n    background-color:#dc3545;\n    border-color:#dc3545;\n    }\n    \n    .btn-danger.focus,.btn-danger:focus,.btn-danger:not(:disabled):not(.disabled).active:focus,.btn-danger:not(:disabled):not(.disabled):active:focus,.show>.btn-danger.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(225,83,97,.5);\n    }\n    \n    .btn-light,.btn-light.disabled,.btn-light:disabled,.btn-outline-light:hover,.btn-outline-light:not(:disabled):not(.disabled).active,.btn-outline-light:not(:disabled):not(.disabled):active,.show>.btn-outline-light.dropdown-toggle {\n    color:#212529;\n    background-color:#f8f9fa;\n    border-color:#f8f9fa;\n    }\n    \n    .btn-light.focus,.btn-light:focus,.btn-light:not(:disabled):not(.disabled).active:focus,.btn-light:not(:disabled):not(.disabled):active:focus,.show>.btn-light.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(216,217,219,.5);\n    }\n    \n    .btn-dark,.btn-dark.disabled,.btn-dark:disabled,.btn-outline-dark:hover,.btn-outline-dark:not(:disabled):not(.disabled).active,.btn-outline-dark:not(:disabled):not(.disabled):active,.show>.btn-outline-dark.dropdown-toggle {\n    color:#fff;\n    background-color:#343a40;\n    border-color:#343a40;\n    }\n    \n    .btn-dark.focus,.btn-dark:focus,.btn-dark:not(:disabled):not(.disabled).active:focus,.btn-dark:not(:disabled):not(.disabled):active:focus,.show>.btn-dark.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(82,88,93,.5);\n    }\n    \n    .btn-outline-primary.focus,.btn-outline-primary:focus,.btn-outline-primary:not(:disabled):not(.disabled).active:focus,.btn-outline-primary:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-primary.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(0,123,255,.5);\n    }\n    \n    .btn-outline-secondary.focus,.btn-outline-secondary:focus,.btn-outline-secondary:not(:disabled):not(.disabled).active:focus,.btn-outline-secondary:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-secondary.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(108,117,125,.5);\n    }\n    \n    .btn-outline-success.focus,.btn-outline-success:focus,.btn-outline-success:not(:disabled):not(.disabled).active:focus,.btn-outline-success:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-success.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(40,167,69,.5);\n    }\n    \n    .btn-outline-info.focus,.btn-outline-info:focus,.btn-outline-info:not(:disabled):not(.disabled).active:focus,.btn-outline-info:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-info.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(23,162,184,.5);\n    }\n    \n    .btn-outline-warning.focus,.btn-outline-warning:focus,.btn-outline-warning:not(:disabled):not(.disabled).active:focus,.btn-outline-warning:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-warning.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(255,193,7,.5);\n    }\n    \n    .btn-outline-danger.focus,.btn-outline-danger:focus,.btn-outline-danger:not(:disabled):not(.disabled).active:focus,.btn-outline-danger:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-danger.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(220,53,69,.5);\n    }\n    \n    .btn-outline-light.focus,.btn-outline-light:focus,.btn-outline-light:not(:disabled):not(.disabled).active:focus,.btn-outline-light:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-light.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(248,249,250,.5);\n    }\n    \n    .btn-outline-dark.focus,.btn-outline-dark:focus,.btn-outline-dark:not(:disabled):not(.disabled).active:focus,.btn-outline-dark:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-dark.dropdown-toggle:focus {\n    box-shadow:0 0 0 .2rem rgba(52,58,64,.5);\n    }\n    \n    .btn-group-lg>.btn,.btn-lg,.input-group-lg>.custom-select,.input-group-lg>.form-control,.input-group-lg>.input-group-append>.btn,.input-group-lg>.input-group-append>.input-group-text,.input-group-lg>.input-group-prepend>.btn,.input-group-lg>.input-group-prepend>.input-group-text {\n    font-size:1.25rem;\n    line-height:1.5;\n    border-radius:.3rem;\n    padding:.5rem 1rem;\n    }\n    \n    .btn-group-sm>.btn,.btn-sm,.input-group-sm>.custom-select,.input-group-sm>.form-control,.input-group-sm>.input-group-append>.btn,.input-group-sm>.input-group-append>.input-group-text,.input-group-sm>.input-group-prepend>.btn,.input-group-sm>.input-group-prepend>.input-group-text {\n    font-size:.875rem;\n    line-height:1.5;\n    border-radius:.2rem;\n    padding:.25rem .5rem;\n    }\n    \n    .dropdown-toggle:empty::after,.dropup .dropdown-toggle:empty::after,.dropright .dropdown-toggle:empty::after,.dropleft .dropdown-toggle:empty::after,.dropdown-toggle-split::after,.dropright .dropdown-toggle-split::after,.dropup .dropdown-toggle-split::after {\n    margin-left:0;\n    }\n    \n    .btn-group-vertical>.btn:hover,.btn-group>.btn:hover,.btn-group-vertical>.btn.active,.btn-group-vertical>.btn:active,.btn-group-vertical>.btn:focus,.btn-group>.btn.active,.btn-group>.btn:active,.btn-group>.btn:focus {\n    z-index:1;\n    }\n    \n    .btn-group>.btn-group:not(:first-child),.btn-group>.btn:not(:first-child),.input-group>.custom-file+.custom-file,.input-group>.custom-file+.custom-select,.input-group>.custom-file+.form-control,.input-group>.custom-select+.custom-file,.input-group>.custom-select+.custom-select,.input-group>.custom-select+.form-control,.input-group>.form-control+.custom-file,.input-group>.form-control+.custom-select,.input-group>.form-control+.form-control,.input-group>.form-control-plaintext+.custom-file,.input-group>.form-control-plaintext+.custom-select,.input-group>.form-control-plaintext+.form-control,.input-group-append .btn+.btn,.input-group-append .btn+.input-group-text,.input-group-append .input-group-text+.btn,.input-group-append .input-group-text+.input-group-text,.input-group-prepend .btn+.btn,.input-group-prepend .btn+.input-group-text,.input-group-prepend .input-group-text+.btn,.input-group-prepend .input-group-text+.input-group-text,.input-group-append {\n    margin-left:-1px;\n    }\n    \n    .btn-group>.btn-group:not(:last-child)>.btn,.btn-group>.btn:not(:last-child):not(.dropdown-toggle),.input-group>.custom-select:not(:last-child),.input-group>.form-control:not(:last-child),.input-group>.custom-file:not(:last-child) .custom-file-label,.input-group>.custom-file:not(:last-child) .custom-file-label::after,.input-group>.input-group-append:last-child>.btn:not(:last-child):not(.dropdown-toggle),.input-group>.input-group-append:last-child>.input-group-text:not(:last-child),.input-group>.input-group-append:not(:last-child)>.btn,.input-group>.input-group-append:not(:last-child)>.input-group-text,.input-group>.input-group-prepend>.btn,.input-group>.input-group-prepend>.input-group-text {\n    border-top-right-radius:0;\n    border-bottom-right-radius:0;\n    }\n    \n    .btn-group>.btn-group:not(:first-child)>.btn,.btn-group>.btn:not(:first-child),.input-group>.custom-select:not(:first-child),.input-group>.form-control:not(:first-child),.input-group>.custom-file:not(:first-child) .custom-file-label,.input-group>.input-group-append>.btn,.input-group>.input-group-append>.input-group-text,.input-group>.input-group-prepend:first-child>.btn:not(:first-child),.input-group>.input-group-prepend:first-child>.input-group-text:not(:first-child),.input-group>.input-group-prepend:not(:first-child)>.btn,.input-group>.input-group-prepend:not(:first-child)>.input-group-text {\n    border-top-left-radius:0;\n    border-bottom-left-radius:0;\n    }\n    \n    .input-group>.custom-file .custom-file-input:focus~.custom-file-label,.input-group>.custom-select:focus,.input-group>.form-control:focus,.input-group-append .btn:focus,.input-group-prepend .btn:focus {\n    z-index:3;\n    }\n    \n    .custom-control-input:disabled~.custom-control-label::before,.custom-file-input:disabled~.custom-file-label {\n    background-color:#e9ecef;\n    }\n    \n    .custom-checkbox .custom-control-label::before,.nav-pills .nav-link {\n    border-radius:.25rem;\n    }\n    \n    .custom-checkbox .custom-control-input:disabled:checked~.custom-control-label::before,.custom-checkbox .custom-control-input:disabled:indeterminate~.custom-control-label::before,.custom-radio .custom-control-input:disabled:checked~.custom-control-label::before,.custom-switch .custom-control-input:disabled:checked~.custom-control-label::before {\n    background-color:rgba(0,123,255,.5);\n    }\n    \n    .custom-range:focus::-webkit-slider-thumb,.custom-range:focus::-moz-range-thumb,.custom-range:focus::-ms-thumb {\n    box-shadow:0 0 0 1px #fff,0 0 0 .2rem rgba(0,123,255,.25);\n    }\n    \n    .custom-range::-webkit-slider-thumb:active,.custom-range::-moz-range-thumb:active,.custom-range::-ms-thumb:active {\n    background-color:#b3d7ff;\n    }\n    \n    .custom-range::-webkit-slider-runnable-track,.custom-range::-moz-range-track {\n    width:100%;\n    height:.5rem;\n    color:transparent;\n    cursor:pointer;\n    background-color:#dee2e6;\n    border-radius:1rem;\n    border-color:transparent;\n    }\n    \n    .custom-range:disabled::-webkit-slider-thumb,.custom-range:disabled::-moz-range-thumb,.custom-range:disabled::-ms-thumb {\n    background-color:#adb5bd;\n    }\n    \n    .custom-range:disabled::-webkit-slider-runnable-track,.custom-range:disabled::-moz-range-track {\n    cursor:default;\n    }\n    \n    .nav-link:focus,.nav-link:hover,.navbar-brand:focus,.navbar-brand:hover,.navbar-toggler:focus,.navbar-toggler:hover {\n    text-decoration:none;\n    }\n    \n    .navbar-light .navbar-brand,.navbar-light .navbar-brand:focus,.navbar-light .navbar-brand:hover,.navbar-light .navbar-nav .active>.nav-link,.navbar-light .navbar-nav .nav-link.active,.navbar-light .navbar-nav .nav-link.show,.navbar-light .navbar-nav .show>.nav-link {\n    color:rgba(0,0,0,.9);\n    }\n    \n    @media min-width576px{\n    .container {\n    max-width:540px;\n    }\n    \n    .col-sm {\n    flex-basis:0;\n    flex-grow:1;\n    max-width:100%;\n    }\n    \n    .col-sm-auto {\n    flex:0 0 auto;\n    width:auto;\n    max-width:100%;\n    }\n    \n    .col-sm-1 {\n    flex:0 0 8.333333%;\n    max-width:8.333333%;\n    }\n    \n    .col-sm-2 {\n    flex:0 0 16.666667%;\n    max-width:16.666667%;\n    }\n    \n    .col-sm-3 {\n    flex:0 0 25%;\n    max-width:25%;\n    }\n    \n    .col-sm-4 {\n    flex:0 0 33.333333%;\n    max-width:33.333333%;\n    }\n    \n    .col-sm-5 {\n    flex:0 0 41.666667%;\n    max-width:41.666667%;\n    }\n    \n    .col-sm-6 {\n    flex:0 0 50%;\n    max-width:50%;\n    }\n    \n    .col-sm-7 {\n    flex:0 0 58.333333%;\n    max-width:58.333333%;\n    }\n    \n    .col-sm-8 {\n    flex:0 0 66.666667%;\n    max-width:66.666667%;\n    }\n    \n    .col-sm-9 {\n    flex:0 0 75%;\n    max-width:75%;\n    }\n    \n    .col-sm-10 {\n    flex:0 0 83.333333%;\n    max-width:83.333333%;\n    }\n    \n    .col-sm-11 {\n    flex:0 0 91.666667%;\n    max-width:91.666667%;\n    }\n    \n    .col-sm-12 {\n    flex:0 0 100%;\n    max-width:100%;\n    }\n    \n    .order-sm-first {\n    order:-1;\n    }\n    \n    .order-sm-last {\n    order:13;\n    }\n    \n    .order-sm-0 {\n    order:0;\n    }\n    \n    .order-sm-1 {\n    order:1;\n    }\n    \n    .order-sm-2 {\n    order:2;\n    }\n    \n    .order-sm-3 {\n    order:3;\n    }\n    \n    .order-sm-4 {\n    order:4;\n    }\n    \n    .order-sm-5 {\n    order:5;\n    }\n    \n    .order-sm-6 {\n    order:6;\n    }\n    \n    .order-sm-7 {\n    order:7;\n    }\n    \n    .order-sm-8 {\n    order:8;\n    }\n    \n    .order-sm-9 {\n    order:9;\n    }\n    \n    .order-sm-10 {\n    order:10;\n    }\n    \n    .order-sm-11 {\n    order:11;\n    }\n    \n    .order-sm-12 {\n    order:12;\n    }\n    \n    .offset-sm-0 {\n    margin-left:0;\n    }\n    \n    .offset-sm-1 {\n    margin-left:8.333333%;\n    }\n    \n    .offset-sm-2 {\n    margin-left:16.666667%;\n    }\n    \n    .offset-sm-3 {\n    margin-left:25%;\n    }\n    \n    .offset-sm-4 {\n    margin-left:33.333333%;\n    }\n    \n    .offset-sm-5 {\n    margin-left:41.666667%;\n    }\n    \n    .offset-sm-6 {\n    margin-left:50%;\n    }\n    \n    .offset-sm-7 {\n    margin-left:58.333333%;\n    }\n    \n    .offset-sm-8 {\n    margin-left:66.666667%;\n    }\n    \n    .offset-sm-9 {\n    margin-left:75%;\n    }\n    \n    .offset-sm-10 {\n    margin-left:83.333333%;\n    }\n    \n    .offset-sm-11 {\n    margin-left:91.666667%;\n    }\n    \n    .form-inline label {\n    display:flex;\n    align-items:center;\n    justify-content:center;\n    margin-bottom:0;\n    }\n    \n    .form-inline .form-group {\n    display:flex;\n    flex:0 0 auto;\n    flex-flow:row wrap;\n    align-items:center;\n    margin-bottom:0;\n    }\n    \n    .form-inline .form-control {\n    display:inline-block;\n    width:auto;\n    vertical-align:middle;\n    }\n    \n    .form-inline .form-control-plaintext {\n    display:inline-block;\n    }\n    \n    .form-inline .custom-select,.form-inline .input-group {\n    width:auto;\n    }\n    \n    .form-inline .form-check {\n    display:flex;\n    align-items:center;\n    justify-content:center;\n    width:auto;\n    padding-left:0;\n    }\n    \n    .form-inline .form-check-input {\n    position:relative;\n    flex-shrink:0;\n    margin-top:0;\n    margin-right:.25rem;\n    margin-left:0;\n    }\n    \n    .form-inline .custom-control {\n    align-items:center;\n    justify-content:center;\n    }\n    \n    .form-inline .custom-control-label {\n    margin-bottom:0;\n    }\n    \n    .dropdown-menu-sm-left {\n    right:auto;\n    left:0;\n    }\n    \n    .dropdown-menu-sm-right {\n    right:0;\n    left:auto;\n    }\n    \n    .navbar-expand-sm {\n    flex-flow:row nowrap;\n    justify-content:flex-start;\n    }\n    \n    .navbar-expand-sm .navbar-nav {\n    flex-direction:row;\n    }\n    \n    .navbar-expand-sm .navbar-nav .dropdown-menu {\n    position:absolute;\n    }\n    \n    .navbar-expand-sm .navbar-nav .nav-link {\n    padding-right:.5rem;\n    padding-left:.5rem;\n    }\n    \n    .navbar-expand-sm>.container,.navbar-expand-sm>.container-fluid {\n    flex-wrap:nowrap;\n    }\n    \n    .navbar-expand-sm .navbar-collapse {\n    display:flex!important;\n    flex-basis:auto;\n    }\n    \n    .navbar-expand-sm .navbar-toggler {\n    display:none;\n    }\n    }\n    \n    @media min-width768px{\n    .container {\n    max-width:720px;\n    }\n    \n    .col-md {\n    flex-basis:0;\n    flex-grow:1;\n    max-width:100%;\n    }\n    \n    .col-md-auto {\n    flex:0 0 auto;\n    width:auto;\n    max-width:100%;\n    }\n    \n    .col-md-1 {\n    flex:0 0 8.333333%;\n    max-width:8.333333%;\n    }\n    \n    .col-md-2 {\n    flex:0 0 16.666667%;\n    max-width:16.666667%;\n    }\n    \n    .col-md-3 {\n    flex:0 0 25%;\n    max-width:25%;\n    }\n    \n    .col-md-4 {\n    flex:0 0 33.333333%;\n    max-width:33.333333%;\n    }\n    \n    .col-md-5 {\n    flex:0 0 41.666667%;\n    max-width:41.666667%;\n    }\n    \n    .col-md-6 {\n    flex:0 0 50%;\n    max-width:50%;\n    }\n    \n    .col-md-7 {\n    flex:0 0 58.333333%;\n    max-width:58.333333%;\n    }\n    \n    .col-md-8 {\n    flex:0 0 66.666667%;\n    max-width:66.666667%;\n    }\n    \n    .col-md-9 {\n    flex:0 0 75%;\n    max-width:75%;\n    }\n    \n    .col-md-10 {\n    flex:0 0 83.333333%;\n    max-width:83.333333%;\n    }\n    \n    .col-md-11 {\n    flex:0 0 91.666667%;\n    max-width:91.666667%;\n    }\n    \n    .col-md-12 {\n    flex:0 0 100%;\n    max-width:100%;\n    }\n    \n    .order-md-first {\n    order:-1;\n    }\n    \n    .order-md-last {\n    order:13;\n    }\n    \n    .order-md-0 {\n    order:0;\n    }\n    \n    .order-md-1 {\n    order:1;\n    }\n    \n    .order-md-2 {\n    order:2;\n    }\n    \n    .order-md-3 {\n    order:3;\n    }\n    \n    .order-md-4 {\n    order:4;\n    }\n    \n    .order-md-5 {\n    order:5;\n    }\n    \n    .order-md-6 {\n    order:6;\n    }\n    \n    .order-md-7 {\n    order:7;\n    }\n    \n    .order-md-8 {\n    order:8;\n    }\n    \n    .order-md-9 {\n    order:9;\n    }\n    \n    .order-md-10 {\n    order:10;\n    }\n    \n    .order-md-11 {\n    order:11;\n    }\n    \n    .order-md-12 {\n    order:12;\n    }\n    \n    .offset-md-0 {\n    margin-left:0;\n    }\n    \n    .offset-md-1 {\n    margin-left:8.333333%;\n    }\n    \n    .offset-md-2 {\n    margin-left:16.666667%;\n    }\n    \n    .offset-md-3 {\n    margin-left:25%;\n    }\n    \n    .offset-md-4 {\n    margin-left:33.333333%;\n    }\n    \n    .offset-md-5 {\n    margin-left:41.666667%;\n    }\n    \n    .offset-md-6 {\n    margin-left:50%;\n    }\n    \n    .offset-md-7 {\n    margin-left:58.333333%;\n    }\n    \n    .offset-md-8 {\n    margin-left:66.666667%;\n    }\n    \n    .offset-md-9 {\n    margin-left:75%;\n    }\n    \n    .offset-md-10 {\n    margin-left:83.333333%;\n    }\n    \n    .offset-md-11 {\n    margin-left:91.666667%;\n    }\n    \n    .dropdown-menu-md-left {\n    right:auto;\n    left:0;\n    }\n    \n    .dropdown-menu-md-right {\n    right:0;\n    left:auto;\n    }\n    \n    .navbar-expand-md {\n    flex-flow:row nowrap;\n    justify-content:flex-start;\n    }\n    \n    .navbar-expand-md .navbar-nav {\n    flex-direction:row;\n    }\n    \n    .navbar-expand-md .navbar-nav .dropdown-menu {\n    position:absolute;\n    }\n    \n    .navbar-expand-md .navbar-nav .nav-link {\n    padding-right:.5rem;\n    padding-left:.5rem;\n    }\n    \n    .navbar-expand-md>.container,.navbar-expand-md>.container-fluid {\n    flex-wrap:nowrap;\n    }\n    \n    .navbar-expand-md .navbar-collapse {\n    display:flex!important;\n    flex-basis:auto;\n    }\n    \n    .navbar-expand-md .navbar-toggler {\n    display:none;\n    }\n    }\n    \n    @media min-width992px{\n    .container {\n    max-width:960px;\n    }\n    \n    .col-lg {\n    flex-basis:0;\n    flex-grow:1;\n    max-width:100%;\n    }\n    \n    .col-lg-auto {\n    flex:0 0 auto;\n    width:auto;\n    max-width:100%;\n    }\n    \n    .col-lg-1 {\n    flex:0 0 8.333333%;\n    max-width:8.333333%;\n    }\n    \n    .col-lg-2 {\n    flex:0 0 16.666667%;\n    max-width:16.666667%;\n    }\n    \n    .col-lg-3 {\n    flex:0 0 25%;\n    max-width:25%;\n    }\n    \n    .col-lg-4 {\n    flex:0 0 33.333333%;\n    max-width:33.333333%;\n    }\n    \n    .col-lg-5 {\n    flex:0 0 41.666667%;\n    max-width:41.666667%;\n    }\n    \n    .col-lg-6 {\n    flex:0 0 50%;\n    max-width:50%;\n    }\n    \n    .col-lg-7 {\n    flex:0 0 58.333333%;\n    max-width:58.333333%;\n    }\n    \n    .col-lg-8 {\n    flex:0 0 66.666667%;\n    max-width:66.666667%;\n    }\n    \n    .col-lg-9 {\n    flex:0 0 75%;\n    max-width:75%;\n    }\n    \n    .col-lg-10 {\n    flex:0 0 83.333333%;\n    max-width:83.333333%;\n    }\n    \n    .col-lg-11 {\n    flex:0 0 91.666667%;\n    max-width:91.666667%;\n    }\n    \n    .col-lg-12 {\n    flex:0 0 100%;\n    max-width:100%;\n    }\n    \n    .order-lg-first {\n    order:-1;\n    }\n    \n    .order-lg-last {\n    order:13;\n    }\n    \n    .order-lg-0 {\n    order:0;\n    }\n    \n    .order-lg-1 {\n    order:1;\n    }\n    \n    .order-lg-2 {\n    order:2;\n    }\n    \n    .order-lg-3 {\n    order:3;\n    }\n    \n    .order-lg-4 {\n    order:4;\n    }\n    \n    .order-lg-5 {\n    order:5;\n    }\n    \n    .order-lg-6 {\n    order:6;\n    }\n    \n    .order-lg-7 {\n    order:7;\n    }\n    \n    .order-lg-8 {\n    order:8;\n    }\n    \n    .order-lg-9 {\n    order:9;\n    }\n    \n    .order-lg-10 {\n    order:10;\n    }\n    \n    .order-lg-11 {\n    order:11;\n    }\n    \n    .order-lg-12 {\n    order:12;\n    }\n    \n    .offset-lg-0 {\n    margin-left:0;\n    }\n    \n    .offset-lg-1 {\n    margin-left:8.333333%;\n    }\n    \n    .offset-lg-2 {\n    margin-left:16.666667%;\n    }\n    \n    .offset-lg-3 {\n    margin-left:25%;\n    }\n    \n    .offset-lg-4 {\n    margin-left:33.333333%;\n    }\n    \n    .offset-lg-5 {\n    margin-left:41.666667%;\n    }\n    \n    .offset-lg-6 {\n    margin-left:50%;\n    }\n    \n    .offset-lg-7 {\n    margin-left:58.333333%;\n    }\n    \n    .offset-lg-8 {\n    margin-left:66.666667%;\n    }\n    \n    .offset-lg-9 {\n    margin-left:75%;\n    }\n    \n    .offset-lg-10 {\n    margin-left:83.333333%;\n    }\n    \n    .offset-lg-11 {\n    margin-left:91.666667%;\n    }\n    \n    .dropdown-menu-lg-left {\n    right:auto;\n    left:0;\n    }\n    \n    .dropdown-menu-lg-right {\n    right:0;\n    left:auto;\n    }\n    \n    .navbar-expand-lg {\n    flex-flow:row nowrap;\n    justify-content:flex-start;\n    }\n    \n    .navbar-expand-lg .navbar-nav {\n    flex-direction:row;\n    }\n    \n    .navbar-expand-lg .navbar-nav .dropdown-menu {\n    position:absolute;\n    }\n    \n    .navbar-expand-lg .navbar-nav .nav-link {\n    padding-right:.5rem;\n    padding-left:.5rem;\n    }\n    \n    .navbar-expand-lg>.container,.navbar-expand-lg>.container-fluid {\n    flex-wrap:nowrap;\n    }\n    \n    .navbar-expand-lg .navbar-collapse {\n    display:flex!important;\n    flex-basis:auto;\n    }\n    \n    .navbar-expand-lg .navbar-toggler {\n    display:none;\n    }\n    }\n    \n    @media min-width1200px{\n    .container {\n    max-width:1140px;\n    }\n    \n    .col-xl {\n    flex-basis:0;\n    flex-grow:1;\n    max-width:100%;\n    }\n    \n    .col-xl-auto {\n    flex:0 0 auto;\n    width:auto;\n    max-width:100%;\n    }\n    \n    .col-xl-1 {\n    flex:0 0 8.333333%;\n    max-width:8.333333%;\n    }\n    \n    .col-xl-2 {\n    flex:0 0 16.666667%;\n    max-width:16.666667%;\n    }\n    \n    .col-xl-3 {\n    flex:0 0 25%;\n    max-width:25%;\n    }\n    \n    .col-xl-4 {\n    flex:0 0 33.333333%;\n    max-width:33.333333%;\n    }\n    \n    .col-xl-5 {\n    flex:0 0 41.666667%;\n    max-width:41.666667%;\n    }\n    \n    .col-xl-6 {\n    flex:0 0 50%;\n    max-width:50%;\n    }\n    \n    .col-xl-7 {\n    flex:0 0 58.333333%;\n    max-width:58.333333%;\n    }\n    \n    .col-xl-8 {\n    flex:0 0 66.666667%;\n    max-width:66.666667%;\n    }\n    \n    .col-xl-9 {\n    flex:0 0 75%;\n    max-width:75%;\n    }\n    \n    .col-xl-10 {\n    flex:0 0 83.333333%;\n    max-width:83.333333%;\n    }\n    \n    .col-xl-11 {\n    flex:0 0 91.666667%;\n    max-width:91.666667%;\n    }\n    \n    .col-xl-12 {\n    flex:0 0 100%;\n    max-width:100%;\n    }\n    \n    .order-xl-first {\n    order:-1;\n    }\n    \n    .order-xl-last {\n    order:13;\n    }\n    \n    .order-xl-0 {\n    order:0;\n    }\n    \n    .order-xl-1 {\n    order:1;\n    }\n    \n    .order-xl-2 {\n    order:2;\n    }\n    \n    .order-xl-3 {\n    order:3;\n    }\n    \n    .order-xl-4 {\n    order:4;\n    }\n    \n    .order-xl-5 {\n    order:5;\n    }\n    \n    .order-xl-6 {\n    order:6;\n    }\n    \n    .order-xl-7 {\n    order:7;\n    }\n    \n    .order-xl-8 {\n    order:8;\n    }\n    \n    .order-xl-9 {\n    order:9;\n    }\n    \n    .order-xl-10 {\n    order:10;\n    }\n    \n    .order-xl-11 {\n    order:11;\n    }\n    \n    .order-xl-12 {\n    order:12;\n    }\n    \n    .offset-xl-0 {\n    margin-left:0;\n    }\n    \n    .offset-xl-1 {\n    margin-left:8.333333%;\n    }\n    \n    .offset-xl-2 {\n    margin-left:16.666667%;\n    }\n    \n    .offset-xl-3 {\n    margin-left:25%;\n    }\n    \n    .offset-xl-4 {\n    margin-left:33.333333%;\n    }\n    \n    .offset-xl-5 {\n    margin-left:41.666667%;\n    }\n    \n    .offset-xl-6 {\n    margin-left:50%;\n    }\n    \n    .offset-xl-7 {\n    margin-left:58.333333%;\n    }\n    \n    .offset-xl-8 {\n    margin-left:66.666667%;\n    }\n    \n    .offset-xl-9 {\n    margin-left:75%;\n    }\n    \n    .offset-xl-10 {\n    margin-left:83.333333%;\n    }\n    \n    .offset-xl-11 {\n    margin-left:91.666667%;\n    }\n    \n    .dropdown-menu-xl-left {\n    right:auto;\n    left:0;\n    }\n    \n    .dropdown-menu-xl-right {\n    right:0;\n    left:auto;\n    }\n    \n    .navbar-expand-xl {\n    flex-flow:row nowrap;\n    justify-content:flex-start;\n    }\n    \n    .navbar-expand-xl .navbar-nav {\n    flex-direction:row;\n    }\n    \n    .navbar-expand-xl .navbar-nav .dropdown-menu {\n    position:absolute;\n    }\n    \n    .navbar-expand-xl .navbar-nav .nav-link {\n    padding-right:.5rem;\n    padding-left:.5rem;\n    }\n    \n    .navbar-expand-xl>.container,.navbar-expand-xl>.container-fluid {\n    flex-wrap:nowrap;\n    }\n    \n    .navbar-expand-xl .navbar-collapse {\n    display:flex!important;\n    flex-basis:auto;\n    }\n    \n    .navbar-expand-xl .navbar-toggler {\n    display:none;\n    }\n    }\n    \n    @media max-width57598px{\n    .table-responsive-sm {\n    display:block;\n    width:100%;\n    overflow-x:auto;\n    -webkit-overflow-scrolling:touch;\n    }\n    \n    .table-responsive-sm>.table-bordered {\n    border:0;\n    }\n    \n    .navbar-expand-sm>.container,.navbar-expand-sm>.container-fluid {\n    padding-right:0;\n    padding-left:0;\n    }\n    }\n    \n    @media max-width76798px{\n    .table-responsive-md {\n    display:block;\n    width:100%;\n    overflow-x:auto;\n    -webkit-overflow-scrolling:touch;\n    }\n    \n    .table-responsive-md>.table-bordered {\n    border:0;\n    }\n    \n    .navbar-expand-md>.container,.navbar-expand-md>.container-fluid {\n    padding-right:0;\n    padding-left:0;\n    }\n    }\n    \n    @media max-width99198px{\n    .table-responsive-lg {\n    display:block;\n    width:100%;\n    overflow-x:auto;\n    -webkit-overflow-scrolling:touch;\n    }\n    \n    .table-responsive-lg>.table-bordered {\n    border:0;\n    }\n    \n    .navbar-expand-lg>.container,.navbar-expand-lg>.container-fluid {\n    padding-right:0;\n    padding-left:0;\n    }\n    }\n    \n    @media max-width119998px{\n    .table-responsive-xl {\n    display:block;\n    width:100%;\n    overflow-x:auto;\n    -webkit-overflow-scrolling:touch;\n    }\n    \n    .table-responsive-xl>.table-bordered {\n    border:0;\n    }\n    \n    .navbar-expand-xl>.container,.navbar-expand-xl>.container-fluid {\n    padding-right:0;\n    padding-left:0;\n    }\n    }\n    \n    @media prefers-reduced-motionreduce{\n    .form-control,.btn,.fade,.collapsing,.custom-switch .custom-control-label::after,.custom-range::-webkit-slider-thumb,.custom-range::-moz-range-thumb,.custom-range::-ms-thumb,.custom-control-label::before,.custom-file-label,.custom-select {\n    -webkit-transition:none;\n    -moz-transition:none;\n    -ms-transition:none;\n    transition:none;\n    }\n    }", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/index.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "body {\n  margin: 0;\n  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',\n    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',\n    sans-serif;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n\ncode {\n  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',\n    monospace;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./src/App.css":
/*!*********************!*\
  !*** ./src/App.css ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.css */ "./src/App.css");
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_App_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Page_Home__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Page/Home */ "./src/Page/Home.jsx");
/* harmony import */ var _Page_NewsPage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Page/NewsPage */ "./src/Page/NewsPage.jsx");
/* harmony import */ var _Page_NewsList__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Page/NewsList */ "./src/Page/NewsList.jsx");
/* harmony import */ var _Page_Page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Page/Page */ "./src/Page/Page.jsx");
var _jsxFileName = "/Users/auliasandie/Documents/web/landing-page-nle/src/App.js";
 // import logo from './logo.svg';








function App() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["BrowserRouter"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
    exact: true,
    path: "/",
    component: _Page_Home__WEBPACK_IMPORTED_MODULE_3__["default"],
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
    path: "/nle/news/:id",
    component: _Page_NewsPage__WEBPACK_IMPORTED_MODULE_4__["default"],
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
    path: "/news",
    component: _Page_NewsList__WEBPACK_IMPORTED_MODULE_5__["default"],
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
    path: "/nle/:code",
    component: _Page_Page__WEBPACK_IMPORTED_MODULE_6__["default"],
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 9
    }
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/Components/NewsCard.css":
/*!*************************************!*\
  !*** ./src/Components/NewsCard.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./NewsCard.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Components/NewsCard.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./NewsCard.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Components/NewsCard.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./NewsCard.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Components/NewsCard.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/Components/NewsCard.jsx":
/*!*************************************!*\
  !*** ./src/Components/NewsCard.jsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! html-to-react */ "./node_modules/html-to-react/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(html_to_react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _NewsCard_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./NewsCard.css */ "./src/Components/NewsCard.css");
/* harmony import */ var _NewsCard_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_NewsCard_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/auliasandie/Documents/web/landing-page-nle/src/Components/NewsCard.jsx";







class NewsCard extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor() {
    super();
    this.state = {
      news: []
    };
  }

  fetchingData() {
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`https://cors-anywhere.herokuapp.com/https://contohportal.herokuapp.com/getNews`).then(response => {
      console.log(response);
      this.setState({
        news: response.data,
        title: response.data.title,
        image: response.data.image,
        content: response.data.news
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  componentDidMount() {
    this.fetchingData();
  }

  render() {
    var HtmlToReactParser = html_to_react__WEBPACK_IMPORTED_MODULE_3___default.a.Parser;
    var HtmlToReactParser = new HtmlToReactParser(); // const news = this.props.location.state

    console.log(this.props);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "App",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 13
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-4 list-news",
      style: {
        marginLeft: '64%',
        marginTop: '-58%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39,
        columnNumber: 13
      }
    }, this.state.news.map((item, index) => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "card.servicepage mb-3",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43,
          columnNumber: 33
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "card-body",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44,
          columnNumber: 37
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45,
          columnNumber: 41
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-7",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46,
          columnNumber: 45
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
        to: {
          pathname: `/nle/news/${item.id}`,
          state: {
            idnews: item.id,
            imagenews: item.image,
            contentnews: item.news,
            titlenews: item.title,
            datenews: item.created_date
          }
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47,
          columnNumber: 49
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
        className: "card-title",
        style: {
          color: 'black',
          textDecoration: 'none'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52,
          columnNumber: 53
        }
      }, item.title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "number",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 54,
          columnNumber: 49
        }
      }, "10 min")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-5",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56,
          columnNumber: 45
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: "img-fluid img-list-music",
        src: item.image,
        alt: "",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 49
        }
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60,
          columnNumber: 41
        }
      }))));
    }))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (NewsCard);

/***/ }),

/***/ "./src/Page/Home.css":
/*!***************************!*\
  !*** ./src/Page/Home.css ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Home.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/Home.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Home.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/Home.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Home.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/Home.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/Page/Home.jsx":
/*!***************************!*\
  !*** ./src/Page/Home.jsx ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _Home_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Home.css */ "./src/Page/Home.css");
/* harmony import */ var _Home_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Home_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! html-to-react */ "./node_modules/html-to-react/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(html_to_react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_multi_carousel_lib_styles_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-multi-carousel/lib/styles.css */ "./node_modules/react-multi-carousel/lib/styles.css");
/* harmony import */ var react_multi_carousel_lib_styles_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_multi_carousel_lib_styles_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _assets_img_trucking_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../assets/img/trucking.png */ "./src/assets/img/trucking.png");
/* harmony import */ var _assets_img_trucking_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_assets_img_trucking_png__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _assets_img_logo_api_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../assets/img/logo-api.png */ "./src/assets/img/logo-api.png");
/* harmony import */ var _assets_img_logo_api_png__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logo_api_png__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _assets_img_email_png__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../assets/img/email.png */ "./src/assets/img/email.png");
/* harmony import */ var _assets_img_email_png__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_assets_img_email_png__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _assets_img_phone_png__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../assets/img/phone.png */ "./src/assets/img/phone.png");
/* harmony import */ var _assets_img_phone_png__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_assets_img_phone_png__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../assets/img/logonle.png */ "./src/assets/img/logonle.png");
/* harmony import */ var _assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../assets/img/logo-nle.png */ "./src/assets/img/logo-nle.png");
/* harmony import */ var _assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _assets_img_logoitruck_png__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../assets/img/logoitruck.png */ "./src/assets/img/logoitruck.png");
/* harmony import */ var _assets_img_logoitruck_png__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logoitruck_png__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _assets_img_digico_png__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../assets/img/digico.png */ "./src/assets/img/digico.png");
/* harmony import */ var _assets_img_digico_png__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_assets_img_digico_png__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _assets_img_mandiri_png__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../assets/img/mandiri.png */ "./src/assets/img/mandiri.png");
/* harmony import */ var _assets_img_mandiri_png__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_assets_img_mandiri_png__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _assets_img_radar_png__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../assets/img/radar.png */ "./src/assets/img/radar.png");
/* harmony import */ var _assets_img_radar_png__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_assets_img_radar_png__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _assets_img_prahu_png__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../assets/img/prahu.png */ "./src/assets/img/prahu.png");
/* harmony import */ var _assets_img_prahu_png__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_assets_img_prahu_png__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _assets_img_lontar_png__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../assets/img/lontar.png */ "./src/assets/img/lontar.png");
/* harmony import */ var _assets_img_lontar_png__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_assets_img_lontar_png__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _assets_img_logol_png__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../assets/img/logol.png */ "./src/assets/img/logol.png");
/* harmony import */ var _assets_img_logol_png__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logol_png__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _assets_img_logistic_png__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../assets/img/logistic.png */ "./src/assets/img/logistic.png");
/* harmony import */ var _assets_img_logistic_png__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logistic_png__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _assets_img_clickargo_png__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../assets/img/clickargo.png */ "./src/assets/img/clickargo.png");
/* harmony import */ var _assets_img_clickargo_png__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(_assets_img_clickargo_png__WEBPACK_IMPORTED_MODULE_21__);
var _jsxFileName = "/Users/auliasandie/Documents/web/landing-page-nle/src/Page/Home.jsx";




 // import { Link as Linkscroll } from "react-scroll";

 // import Carousel from 'react-multi-carousel';


















class HomePage extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.state = {
      about: [],
      service: [],
      news: [],
      api: [],
      contentapi: null,
      titleapi: null,
      summaryapi: null,
      contentabout: null,
      titleabout: null,
      summaryabout: null,
      contentservice: null,
      titleservice: null,
      summaryservice: null,
      titlenews: null,
      contentnews: null,
      datenews: null,
      imagenews: null,
      idnews: null,
      iconservice: null
    };
  }

  fetchingAbout() {
    axios__WEBPACK_IMPORTED_MODULE_3___default.a.get(`https://contohportal.herokuapp.com/getContent?code=about`).then(response => {
      console.log(response);
      this.setState({
        about: response.data.data,
        contentabout: response.data.data.content,
        titleabout: response.data.data.title,
        summaryabout: response.data.data.summary
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  fetchingService() {
    axios__WEBPACK_IMPORTED_MODULE_3___default.a.get(`https://contohportal.herokuapp.com/getContent?code=service`).then(response => {
      console.log(response);
      this.setState({
        service: response.data.category_code,
        titleservice: response.data.data.title,
        summaryservice: response.data.category_code.summary,
        iconservice: response.data.category_code.icon,
        contentservice: response.data.category_code.content
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  fetchingNews() {
    axios__WEBPACK_IMPORTED_MODULE_3___default.a.get(`https://cors-anywhere.herokuapp.com/https://contohportal.herokuapp.com/getNews`) // .then(response => response.json())
    .then(response => {
      console.log(response);
      this.setState({
        news: response.data
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  fetchingApi() {
    axios__WEBPACK_IMPORTED_MODULE_3___default.a.get(`https://contohportal.herokuapp.com/getContent?code=apicollaboration`).then(response => {
      console.log(response);
      this.setState({
        api: response.data.data,
        contentapi: response.data.data.content,
        titleapi: response.data.data.title
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  componentDidMount() {
    this.fetchingAbout();
    this.fetchingService();
    this.fetchingNews();
    this.fetchingApi();
  }

  render() {
    var HtmlToReactParser = html_to_react__WEBPACK_IMPORTED_MODULE_4___default.a.Parser;
    var HtmlToReactParser = new HtmlToReactParser();
    const about = this.state.about;
    const api = this.state.api;
    const item = this.state.service;
    console.log(this.state.service);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "App",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123,
        columnNumber: 17
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
      className: "navbar navbar-expand-lg navbar-dark fixed-top",
      id: "mainNav",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["Link"], {
      to: "/",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png",
      style: {
        width: "11%",
        marginLeft: '-75%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127,
        columnNumber: 33
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_12___default.a,
      style: {
        width: "50%",
        marginLeft: '10px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "navbar-toggler navbar-toggler-right",
      type: "button",
      "data-toggle": "collapse",
      "data-target": "#navbarResponsive",
      "aria-controls": "navbarResponsive",
      "aria-expanded": "false",
      "aria-label": "Toggle navigation",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 131,
        columnNumber: 29
      }
    }, "Menu", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-bars",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133,
        columnNumber: 9
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "collapse navbar-collapse",
      id: "navbarResponsive",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: "navbar-nav  ml-auto",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 136,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#about",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 138,
        columnNumber: 41
      }
    }, "About")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#services",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 141,
        columnNumber: 41
      }
    }, "Services")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 143,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#news",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144,
        columnNumber: 41
      }
    }, "News")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 146,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#api",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147,
        columnNumber: 41
      }
    }, "API's")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 149,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#collaboration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150,
        columnNumber: 41
      }
    }, "Entity Collaboration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#registration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 153,
        columnNumber: 41
      }
    }, "Registration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"], {
      title: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: "text-light my-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 155,
          columnNumber: 57
        }
      }, "Support"),
      id: "nav-dropdown white",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["Link"], {
      to: "/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157,
        columnNumber: 45
      }
    }, "FAQ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 159,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["Link"], {
      to: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160,
        columnNumber: 45
      }
    }, "Register Training")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["Link"], {
      to: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 163,
        columnNumber: 45
      }
    }, "Tutorial Documentation")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 165,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["Link"], {
      to: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 166,
        columnNumber: 45
      }
    }, "Contact Us"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 169,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      className: "btn btn-outline-light",
      style: {
        width: '84px',
        height: '37px',
        marginLeft: '-6px',
        marginRight: '-46px',
        marginTop: '9px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 170,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        fontSize: '14px',
        lineHeight: '0.95',
        marginTop: '-1px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 171,
        columnNumber: 45
      }
    }, "Go To Platform"))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("header", {
      className: "masthead",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 179,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"], {
      style: {
        marginTop: '4%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 180,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 181,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/14ReLiY2Xh-fcjphLZbBksARC8WG3Qsy4=w1280-h640-iv1",
      alt: "First slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 182,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 189,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/1OMOj02q6eJ2EmDymK-wo24vvLE4GOYy6=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 190,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 197,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/1j5pYYsiUx17ryW2vOUc07pcT0dlNstbZ=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 198,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/19aLQNNTtsGq1JjXnlqRI4RoKsgKkymuV=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 206,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 213,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/10dDFN61Db2fIVLi6PVoke8jzEas0rOQO=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 214,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 221,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/1aJMnPgsclPEZbQ-jg2Hbhr2qdJ320sH3=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 222,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 229,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/1kl3s47ZBLV64UyJog0SMO6SMhE8S51I5=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 230,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 237,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/1A0er0s46IAbHUjjYI49F0uAlM32T-uJJ=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 238,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 245,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "d-block w-100",
      src: "https://lh3.google.com/u/0/d/1FIGb2hLl9G3V4Q26EIs-iFOCUsHMHkz_=w1280-h640-iv1",
      alt: "Third slide",
      style: {
        height: '430px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 246,
        columnNumber: 33
      }
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: "page-section",
      id: "about",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 256,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 257,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 258,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12 text-center",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 259,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: "section-heading text-uppercase",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 261,
        columnNumber: 37
      }
    }, this.state.titleabout))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row text-center",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 265,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-muted",
      style: {
        width: '46%',
        textAlign: 'left',
        marginTop: '6%',
        color: 'black'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 274,
        columnNumber: 33
      }
    }, HtmlToReactParser.parse(about.content)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_trucking_png__WEBPACK_IMPORTED_MODULE_7___default.a,
      alt: "industry",
      style: {
        marginTop: '27%',
        width: '22%',
        marginLeft: '-47%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 275,
        columnNumber: 33
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "btn btn-light",
      href: "/nle/about",
      style: {
        width: '13%',
        height: '10%',
        marginTop: '29%',
        marginLeft: '7%',
        color: 'black',
        border: '2px solid'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 276,
        columnNumber: 33
      }
    }, "Tell Me More"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 277,
        columnNumber: 33
      }
    }, HtmlToReactParser.parse(about.icon))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: "page-section",
      id: "services",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 300,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 301,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12 text-center",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 302,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: "section-heading text-uppercase",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 304,
        columnNumber: 33
      }
    }, this.state.titleservice)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 307,
        columnNumber: 29
      }
    }, this.state.service.map((item, index) => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-4",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 311,
          columnNumber: 37
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "services-grid",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 312,
          columnNumber: 37
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "service service1",
        style: {
          marginTop: '0%',
          height: '349px'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 313,
          columnNumber: 33
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "ti-bar-chart",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 314,
          columnNumber: 37
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "fa-stack fa-4x",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 315,
          columnNumber: 37
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: item.icon,
        style: {
          width: '30px',
          height: '30px',
          marginTop: '-63%'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 317,
          columnNumber: 53
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "title-service",
        style: {
          marginTop: '-36%'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 319,
          columnNumber: 37
        }
      }, item.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "content-service",
        style: {
          marginTop: '-18%'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 320,
          columnNumber: 33
        }
      }, HtmlToReactParser.parse(item.content))))));
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: "page-section",
      id: "news",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 336,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      style: {
        marginTop: '0%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 337,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 338,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12 text-center",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 339,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: "section-heading text-uppercase",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 340,
        columnNumber: 37
      }
    }, "NEWS"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      style: {
        marginTop: '16%',
        marginLeft: '2%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 344,
        columnNumber: 29
      }
    }, this.state.news.map((item, index) => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-3",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 348,
          columnNumber: 45
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("body", {
        className: "body news",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 349,
          columnNumber: 49
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["Link"], {
        to: {
          pathname: `/nle/news/${item.id}`,
          state: {
            idnews: item.id,
            titlenews: item.title,
            contentnews: item.news,
            imagenews: item.image,
            datenews: item.created_date
          }
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 350,
          columnNumber: 53
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("article", {
        class: "card news",
        style: {
          height: '277px'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 359,
          columnNumber: 57
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("header", {
        class: "cardThumb",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 360,
          columnNumber: 61
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: `/nle/news/${item.id}`,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 361,
          columnNumber: 65
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: item.image,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 364,
          columnNumber: 69
        }
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        class: "cardBody",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 367,
          columnNumber: 61
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        className: "cardTitle",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 368,
          columnNumber: 65
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: true,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 368,
          columnNumber: 91
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        className: "block-with-text",
        style: {
          fontSize: '15px',
          textAlign: 'left'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 370,
          columnNumber: 69
        }
      }, item.title)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("footer", {
        className: "cardFooter",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 375,
          columnNumber: 61
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 376,
          columnNumber: 65
        }
      }), item.created_date))))));
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: "page-section",
      id: "api",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 391,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 392,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 393,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12 text-center",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 394,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: "section-heading text-uppercase",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 395,
        columnNumber: 37
      }
    }, this.state.titleapi))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      style: {
        marginTop: '-3%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 399,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logo_api_png__WEBPACK_IMPORTED_MODULE_8___default.a,
      style: {
        width: "200px",
        marginLeft: '4%',
        marginTop: '7%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 400,
        columnNumber: 29
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        color: '#1d2d4b',
        width: '50%',
        marginLeft: '4%',
        marginTop: '11%',
        textAlign: 'justify'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 401,
        columnNumber: 29
      }
    }, HtmlToReactParser.parse(api.content))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
      variant: "light",
      className: "menu-button right",
      style: {
        width: '90px',
        height: '30px',
        marginLeft: '86%',
        marginTop: '-16%',
        borderRadius: '10px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 405,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        fontSize: 12,
        textAlign: 'center'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 409,
        columnNumber: 29
      }
    }, "Subscribe")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: "page-section",
      id: "collaboration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 415,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 416,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 417,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12 text-center",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 418,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: "section-heading text-uppercase",
      style: {
        marginBottom: '11%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 419,
        columnNumber: 37
      }
    }, "ENTITY COLLABORATION"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row row-1",
      style: {
        marginTop: '-5%',
        marginLeft: '1%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 422,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-3 collab-menu collab-icon",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 423,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "collab",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 424,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "hidden-xs hidden-sm",
      style: {
        marginLeft: '15%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 425,
        columnNumber: 37
      }
    }, "Government", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-icon-standard pt-icon-chevron-down bt-caret",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 427,
        columnNumber: 37
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row row-2",
      style: {
        marginLeft: '10px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 429,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Logo_of_the_Ministry_of_Finance_of_the_Republic_of_Indonesia.png",
      style: {
        width: '40px',
        height: '40px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 430,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Logo_of_the_Ministry_of_Transportation_of_the_Republic_of_Indonesia.svg/1200px-Logo_of_the_Ministry_of_Transportation_of_the_Republic_of_Indonesia.svg.png",
      style: {
        width: '40px',
        height: '40px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 431,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/id/thumb/4/48/LOGO_KEMENTERIAN_KELAUTAN_DAN_PERIKANAN.png/1200px-LOGO_KEMENTERIAN_KELAUTAN_DAN_PERIKANAN.png",
      style: {
        width: '40px',
        height: '40px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 432,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/0/03/Logo_of_the_Ministry_of_Trade_of_the_Republic_of_Indonesia.jpg",
      style: {
        width: '40px',
        height: '40px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 433,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://www.pertanian.go.id/img/logo.png",
      style: {
        width: '40px',
        height: '40px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 434,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://lh3.googleusercontent.com/proxy/tfE4LjMJIaAbwRZfwq6LlLbrt1bIgXwz577fBwYDFmD2ygLtZxmt44kZANuU8Kzb-9PjVJ0WQkb53O67bscOvW5dT_RWbjbuCMf0bJZf7miq0bub4WIUkMXNFN6yCD9Vq9MmZhGeB-ywZ0uTeAA",
      style: {
        width: '40px',
        height: '40px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 435,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://www.pinterpandai.com/wp-content/uploads/2017/09/Logo-BPOM.jpg",
      style: {
        width: '40px',
        height: '40px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 436,
        columnNumber: 41
      }
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-3 collab-menu",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 440,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "hidden-xs hidden-sm",
      style: {
        marginLeft: '15%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 441,
        columnNumber: 33
      }
    }, "Private Sector", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-icon-standard pt-icon-chevron-down bt-caret",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 443,
        columnNumber: 37
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row row-2",
      style: {
        marginLeft: '10px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 445,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logoitruck_png__WEBPACK_IMPORTED_MODULE_13___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 446,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_digico_png__WEBPACK_IMPORTED_MODULE_14___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 447,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_mandiri_png__WEBPACK_IMPORTED_MODULE_15___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 448,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_radar_png__WEBPACK_IMPORTED_MODULE_16___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 449,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logol_png__WEBPACK_IMPORTED_MODULE_19___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 450,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_clickargo_png__WEBPACK_IMPORTED_MODULE_21___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 451,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_lontar_png__WEBPACK_IMPORTED_MODULE_18___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 452,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_prahu_png__WEBPACK_IMPORTED_MODULE_17___default.a,
      style: {
        width: '99px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 453,
        columnNumber: 37
      }
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-3 collab-menu",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 457,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "hidden-xs hidden-sm",
      style: {
        marginLeft: '15%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 458,
        columnNumber: 33
      }
    }, "Assosiation", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-icon-standard pt-icon-chevron-down bt-caret",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 460,
        columnNumber: 37
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row row-2",
      style: {
        marginLeft: '10px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 462,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://aptrindo.id/wp-content/uploads/2018/11/aptrindo_logo.png",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 463,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://yt3.ggpht.com/a/AGF-l78wTSyViwyoeJFfLgrab8eUyJJMvvI9oZfP1g=s900-c-k-c0xffffffff-no-rj-mo",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 464,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTcXang5YbCIrL5-RDVOd6CeJkof3OpSh2FxjMtmijewGcAweRp&usqp=CAU",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 465,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://lh3.googleusercontent.com/proxy/lcUQy1oBx7eYRC-JM1befUZE161hSaCmquQCC6G6vcyC4aq5Z_YxCHZg92H_yo-sCu6QUv-YIbPbqh3I07tIgR2JMKQ6FJb9YSS4oPklp80mBKfgmA14r4Ip2LR9uunKbVkKLNERysSh",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 466,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://lh3.googleusercontent.com/proxy/PI9lTjymhGU8IdsByOW6DCXnSTjbDIULWkKh8sutnWtIm8vod6tZ4Zu4NxdxttUNU-rGwcCOolqcbErQDOe_Qe7DZv4-MXenJrQbFizAAaY",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 467,
        columnNumber: 37
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logistic_png__WEBPACK_IMPORTED_MODULE_20___default.a,
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 468,
        columnNumber: 37
      }
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-3 collab-menu",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 472,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "footer-call-center",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 473,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "hidden-xs hidden-sm",
      style: {
        marginLeft: '15%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 474,
        columnNumber: 37
      }
    }, "Other Entity"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row row-2",
      style: {
        marginLeft: '10px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 476,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Logo_of_the_Ministry_of_Finance_of_the_Republic_of_Indonesia.png",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 477,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Logo_of_the_Ministry_of_Finance_of_the_Republic_of_Indonesia.png",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 478,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Logo_of_the_Ministry_of_Finance_of_the_Republic_of_Indonesia.png",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 479,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Logo_of_the_Ministry_of_Finance_of_the_Republic_of_Indonesia.png",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 480,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Logo_of_the_Ministry_of_Finance_of_the_Republic_of_Indonesia.png",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 481,
        columnNumber: 41
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Logo_of_the_Ministry_of_Finance_of_the_Republic_of_Indonesia.png",
      style: {
        width: '30px',
        height: '30px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 482,
        columnNumber: 41
      }
    }))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 499,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Jumbotron"], {
      style: {
        marginLeft: '-13%',
        width: '137%',
        marginLeft: '-18%',
        height: '350px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 500,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 501,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_11___default.a,
      style: {
        width: "22%",
        marginTop: '-2%',
        marginLeft: '-91%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 502,
        columnNumber: 26
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "address",
      style: {
        textAlign: 'left',
        marginLeft: '-6%',
        marginTop: '2%',
        fontSize: '14px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 504,
        columnNumber: 9
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 505,
        columnNumber: 13
      }
    }, "Jl. Ahmad Yani By Pass"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 506,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 507,
        columnNumber: 13
      }
    }, "Kantor Pusat Bea dan Cukai"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 508,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 509,
        columnNumber: 13
      }
    }, "Jakarta Timur")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 511,
        columnNumber: 9
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_email_png__WEBPACK_IMPORTED_MODULE_9___default.a,
      style: {
        width: '29px',
        height: '25px',
        marginLeft: '37%',
        marginTop: '-8%',
        color: 'blue'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 512,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        marginTop: '-8%',
        marginLeft: '2%',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 513,
        columnNumber: 13
      }
    }, "nle@kemenkeu.go.id")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 515,
        columnNumber: 9
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_phone_png__WEBPACK_IMPORTED_MODULE_10___default.a,
      style: {
        width: '29px',
        height: '25px',
        marginLeft: '37%',
        marginTop: '-3%',
        color: 'blue',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 516,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        marginTop: '-3%',
        marginLeft: '2%',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 518,
        columnNumber: 13
      }
    }, "021-1234567")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png",
      style: {
        width: "8%",
        float: 'right',
        marginTop: '-9%',
        marginRight: '7%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 521,
        columnNumber: 9
      }
    }))))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (HomePage);
{
  /* <section className="page-section" id="services">
                         <div className="container">
                             <div className="row">
                                 <div className="col-lg-12 text-center">
                                     <h2 className="section-heading text-uppercase">{this.state.titleservice}</h2>
                                     <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                                 </div>
                             </div>
                             <div className="row text-center">
                                 {this.state.service.map((item, index) => {
                                     return (
                                         <>
                                             <div className="col-md-4">
                                                 <span className="fa-stack fa-4x">
                                                     <i className="fa fa-circle fa-stack-2x text-primary"></i>
                                                     <i className="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                                                 </span>
                                                 <h4 className="service-heading">{item.title}</h4>
                                                 <p className="text-muted">{item.summary}</p>
                                             </div>
                                          </>
                                     )
                                 })}
                              </div>
                         </div>
                     </section> */
}

/***/ }),

/***/ "./src/Page/NewsList.css":
/*!*******************************!*\
  !*** ./src/Page/NewsList.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./NewsList.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/NewsList.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./NewsList.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/NewsList.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./NewsList.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Page/NewsList.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/Page/NewsList.jsx":
/*!*******************************!*\
  !*** ./src/Page/NewsList.jsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! html-to-react */ "./node_modules/html-to-react/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(html_to_react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _NewsList_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./NewsList.css */ "./src/Page/NewsList.css");
/* harmony import */ var _NewsList_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_NewsList_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _Components_NewsCard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Components/NewsCard */ "./src/Components/NewsCard.jsx");
var _jsxFileName = "/Users/auliasandie/Documents/web/landing-page-nle/src/Page/NewsList.jsx";








class NewsList extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      title: null,
      image: null,
      content: null,
      idnews: null,
      imagenews: null,
      datenews: null,
      titlenews: null,
      contentnews: null
    };
  }

  fetchingData() {
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`https://cors-anywhere.herokuapp.com/https://contohportal.herokuapp.com/getNews`).then(response => {
      console.log(response);
      this.setState({
        news: response.data,
        title: response.data.title,
        image: response.data.image,
        content: response.data.news
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  componentDidMount() {
    this.fetchingData();
  }

  render() {
    var HtmlToReactParser = html_to_react__WEBPACK_IMPORTED_MODULE_3___default.a.Parser;
    var HtmlToReactParser = new HtmlToReactParser(); // const news = this.props.location.state

    console.log(this.props);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "App",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 13
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
      className: "navbar navbar-expand-lg navbar-dark fixed-top",
      id: "mainNav",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "navbar-brand js-scroll-trigger",
      href: "#page-top",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 29
      }
    }, "Start Bootstrap"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "navbar-toggler navbar-toggler-right",
      type: "button",
      "data-toggle": "collapse",
      "data-target": "#navbarResponsive",
      "aria-controls": "navbarResponsive",
      "aria-expanded": "false",
      "aria-label": "Toggle navigation",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51,
        columnNumber: 29
      }
    }, "Menu", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-bars",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 9
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "collapse navbar-collapse",
      id: "navbarResponsive",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: "navbar-nav text-uppercase ml-auto",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#about",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 41
      }
    }, "About")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#services",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 41
      }
    }, "Services")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "/news",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 41
      }
    }, "News")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#api",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67,
        columnNumber: 41
      }
    }, "API's")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "collaboration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70,
        columnNumber: 41
      }
    }, "Entity Collaboration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "registration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73,
        columnNumber: 41
      }
    }, "Registration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"], {
      title: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: "text-light my-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75,
          columnNumber: 57
        }
      }, "Support"),
      id: "nav-dropdown white",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77,
        columnNumber: 45
      }
    }, "FAQ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80,
        columnNumber: 45
      }
    }, "Register Training")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 45
      }
    }, "Tutorial Documentation")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 45
      }
    }, "Contact Us"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      className: "btn btn-outline-light",
      style: {
        width: '84px',
        height: '37px',
        marginLeft: '-6px',
        marginRight: '-46px',
        marginTop: '9px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        fontSize: '14px',
        lineHeight: '0.95',
        marginTop: '-1px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91,
        columnNumber: 45
      }
    }, "Go To Platform"))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      style: {
        marginTop: '-2%',
        marginLeft: '5%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100,
        columnNumber: 21
      }
    }, "Berita NLE"), this.state.news.map((item, index) => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "newslist",
        style: {
          marginLeft: '-41%'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104,
          columnNumber: 33
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        className: "ui-list",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105,
          columnNumber: 37
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: `/nle/news/${item.id}`,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106,
          columnNumber: 41
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
        to: {
          pathname: `/nle/news/${item.id}`,
          state: {
            idnews: item.id,
            titlenews: item.title,
            contentnews: item.news,
            imagenews: item.image,
            datenews: item.created_date
          },
          query: {
            idnews: '2'
          }
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107,
          columnNumber: 41
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "covernews",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 116,
          columnNumber: 45
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: item.image,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117,
          columnNumber: 49
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "news-wrap",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119,
          columnNumber: 45
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "news-title",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120,
          columnNumber: 49
        }
      }, item.title)))))));
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Components_NewsCard__WEBPACK_IMPORTED_MODULE_6__["default"], {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 130,
        columnNumber: 21
      }
    }))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (NewsList);

/***/ }),

/***/ "./src/Page/NewsPage.jsx":
/*!*******************************!*\
  !*** ./src/Page/NewsPage.jsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! html-to-react */ "./node_modules/html-to-react/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(html_to_react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Home_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Home.css */ "./src/Page/Home.css");
/* harmony import */ var _Home_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_Home_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/auliasandie/Documents/web/landing-page-nle/src/Page/NewsPage.jsx";







class NewsPage extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      title: null,
      image: null,
      content: null,
      idnews: this.props.location.state.idnews,
      titlenews: this.props.location.state.titlenews,
      contentnews: this.props.location.state.contentnews,
      imagenews: this.props.location.state.imagenews,
      datenews: this.props.location.state.datenews
    };
  }

  fetchingData() {
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(`https://cors-anywhere.herokuapp.com/https://contohportal.herokuapp.com/getNews`).then(response => {
      console.log(response);
      this.setState({
        news: response.data,
        title: response.data.title,
        image: response.data.image,
        content: response.data.news
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  componentDidMount() {
    this.fetchingData();
  }

  render() {
    var HtmlToReactParser = html_to_react__WEBPACK_IMPORTED_MODULE_3___default.a.Parser;
    var HtmlToReactParser = new HtmlToReactParser();
    const news = this.props.location.state;
    console.log(this.props);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "App",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 13
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
      className: "navbar navbar-expand-lg navbar-dark fixed-top",
      id: "mainNav",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "navbar-brand js-scroll-trigger",
      href: "#page-top",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49,
        columnNumber: 29
      }
    }, "Start Bootstrap"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "navbar-toggler navbar-toggler-right",
      type: "button",
      "data-toggle": "collapse",
      "data-target": "#navbarResponsive",
      "aria-controls": "navbarResponsive",
      "aria-expanded": "false",
      "aria-label": "Toggle navigation",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 29
      }
    }, "Menu", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-bars",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52,
        columnNumber: 9
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "collapse navbar-collapse",
      id: "navbarResponsive",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: "navbar-nav text-uppercase ml-auto",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#about",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 41
      }
    }, "About")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#services",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 41
      }
    }, "Services")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "/news",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 41
      }
    }, "News")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#api",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 41
      }
    }, "API's")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "collaboration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 41
      }
    }, "Entity Collaboration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "registration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 41
      }
    }, "Registration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"], {
      title: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: "text-light my-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74,
          columnNumber: 57
        }
      }, "Support"),
      id: "nav-dropdown white",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 45
      }
    }, "FAQ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 45
      }
    }, "Register Training")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 45
      }
    }, "Tutorial Documentation")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NavDropdown"].Item, {
      href: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      to: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 45
      }
    }, "Contact Us"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      className: "btn btn-outline-light",
      style: {
        width: '84px',
        height: '37px',
        marginLeft: '-6px',
        marginRight: '-46px',
        marginTop: '9px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        fontSize: '14px',
        lineHeight: '0.95',
        marginTop: '-1px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90,
        columnNumber: 45
      }
    }, "Go To Platform"))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      style: {
        marginTop: '10%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: news.imagenews,
      style: {
        width: "51%",
        marginLeft: '10%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99,
        columnNumber: 21
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      key: "1",
      style: {
        marginLeft: '10%',
        marginTop: '2%',
        width: '51%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 17
      }
    }, news.titlenews), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        marginLeft: '13%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103,
        columnNumber: 21
      }
    }, news.datenews), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        marginLeft: '10%',
        width: '50%',
        textAlign: 'justify',
        marginTop: "2%"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105,
        columnNumber: 17
      }
    }, HtmlToReactParser.parse(news.contentnews)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107,
        columnNumber: 17
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 108,
        columnNumber: 17
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-4 list-news",
      style: {
        marginLeft: '64%',
        marginTop: '-104%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      style: {
        marginTop: '-25%',
        textAlign: 'center',
        marginBottom: '4%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 113,
        columnNumber: 25
      }
    }, "Berita Terkini"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 114,
        columnNumber: 25
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 115,
        columnNumber: 25
      }
    }), this.state.news.map((item, index) => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "card.servicepage mb-3",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119,
          columnNumber: 41
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "card-body",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120,
          columnNumber: 45
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121,
          columnNumber: 49
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-7",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122,
          columnNumber: 53
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
        to: {
          pathname: `/nle/news/${item.id}`,
          state: {
            idnews: item.id,
            imagenews: item.image,
            contentnews: item.news,
            titlenews: item.title,
            datenews: item.created_date
          }
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124,
          columnNumber: 57
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
        className: "card-title",
        style: {
          color: 'black',
          textDecoration: 'none'
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127,
          columnNumber: 61
        }
      }, item.title))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "number",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130,
          columnNumber: 57
        }
      }, "10 min")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-5",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132,
          columnNumber: 53
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        className: "img-fluid img-list-music",
        src: item.image,
        alt: "Sunshine",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133,
          columnNumber: 57
        }
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137,
          columnNumber: 49
        }
      }))));
    }))))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (NewsPage);

/***/ }),

/***/ "./src/Page/Page.jsx":
/*!***************************!*\
  !*** ./src/Page/Page.jsx ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! html-to-react */ "./node_modules/html-to-react/index.js");
/* harmony import */ var html_to_react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(html_to_react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_img_email_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../assets/img/email.png */ "./src/assets/img/email.png");
/* harmony import */ var _assets_img_email_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_img_email_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_img_phone_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../assets/img/phone.png */ "./src/assets/img/phone.png");
/* harmony import */ var _assets_img_phone_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_assets_img_phone_png__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../assets/img/logonle.png */ "./src/assets/img/logonle.png");
/* harmony import */ var _assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../assets/img/logo-nle.png */ "./src/assets/img/logo-nle.png");
/* harmony import */ var _assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
var _jsxFileName = "/Users/auliasandie/Documents/web/landing-page-nle/src/Page/Page.jsx";










class Page extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      content: null,
      summary: null,
      title: null
    };
  }

  fetchingData() {
    axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(`https://contohportal.herokuapp.com/getContent?code=${this.props.match.params.code}`).then(response => {
      console.log(response);
      this.setState({
        data: response.data.data,
        content: response.data.data.content,
        title: response.data.data.title,
        summary: response.data.data.summary
      });
    }).catch(function (error) {
      console.log(error);
    });
  }

  componentDidMount() {
    this.fetchingData();
  }

  render() {
    var HtmlToReactParser = html_to_react__WEBPACK_IMPORTED_MODULE_3___default.a.Parser;
    var HtmlToReactParser = new HtmlToReactParser();
    const data = this.state.data;
    console.log(this.state.data);
    console.log(HtmlToReactParser.parse(data.content));
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: "s-service",
      id: "service",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 17
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "App",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49,
        columnNumber: 16
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
      className: "navbar navbar-expand-lg navbar-dark fixed-top",
      id: "mainNav",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 16
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__["Link"], {
      to: "/",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png",
      style: {
        width: "11%",
        marginLeft: '-75%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 33
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logo_nle_png__WEBPACK_IMPORTED_MODULE_7___default.a,
      style: {
        width: "50%",
        marginLeft: '10px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 33
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "navbar-toggler navbar-toggler-right",
      type: "button",
      "data-toggle": "collapse",
      "data-target": "#navbarResponsive",
      "aria-controls": "navbarResponsive",
      "aria-expanded": "false",
      "aria-label": "Toggle navigation",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 29
      }
    }, "Menu", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-bars",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 9
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "collapse navbar-collapse",
      id: "navbarResponsive",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: "navbar-nav  ml-auto",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 33
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#about",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__["Link"], {
      style: {
        color: 'white',
        textDecoration: 'none'
      },
      to: "/nle/about",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 41
      }
    }, "About"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#services",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70,
        columnNumber: 41
      }
    }, "Services")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#news",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73,
        columnNumber: 41
      }
    }, "News")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#api",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 41
      }
    }, "API's")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#collaboration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 41
      }
    }, "Entity Collaboration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "nav-item",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      className: "nav-link js-scroll-trigger",
      href: "#registration",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 41
      }
    }, "Registration")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"], {
      title: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: "text-light my-auto",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84,
          columnNumber: 57
        }
      }, "Support"),
      id: "nav-dropdown white",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/nle/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__["Link"], {
      to: "/FAQ",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 45
      }
    }, "FAQ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__["Link"], {
      to: "/register-training",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 45
      }
    }, "Register Training")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__["Link"], {
      to: "/tutorial-documentation",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92,
        columnNumber: 45
      }
    }, "Tutorial Documentation")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavDropdown"].Item, {
      href: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__["Link"], {
      to: "/contact-us",
      style: {
        color: 'black',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95,
        columnNumber: 45
      }
    }, "Contact Us"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98,
        columnNumber: 37
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      className: "btn btn-outline-light",
      style: {
        width: '84px',
        height: '37px',
        marginLeft: '-6px',
        marginRight: '-46px',
        marginTop: '9px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99,
        columnNumber: 41
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        fontSize: '14px',
        lineHeight: '0.95',
        marginTop: '-1px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100,
        columnNumber: 45
      }
    }, "Go To Platform"))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      style: {
        marginTop: '8%',
        paddingBottom: '3%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110,
        columnNumber: 21
      }
    }, this.state.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      style: {
        width: '90%',
        textAlign: 'justify',
        marginLeft: '4%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111,
        columnNumber: 21
      }
    }, HtmlToReactParser.parse(data.content)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116,
        columnNumber: 21
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Jumbotron"], {
      style: {
        marginLeft: '-13%',
        width: '137%',
        marginLeft: '-18%',
        height: '350px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117,
        columnNumber: 25
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_logonle_png__WEBPACK_IMPORTED_MODULE_6___default.a,
      style: {
        width: "22%",
        marginTop: '-2%',
        marginLeft: '-91%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119,
        columnNumber: 26
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "address",
      style: {
        textAlign: 'left',
        marginLeft: '-6%',
        marginTop: '2%',
        fontSize: '14px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121,
        columnNumber: 9
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122,
        columnNumber: 13
      }
    }, "Jl. Ahmad Yani By Pass"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124,
        columnNumber: 13
      }
    }, "Kantor Pusat Bea dan Cukai"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126,
        columnNumber: 13
      }
    }, "Jakarta Timur")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128,
        columnNumber: 9
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_email_png__WEBPACK_IMPORTED_MODULE_4___default.a,
      style: {
        width: '29px',
        height: '25px',
        marginLeft: '37%',
        marginTop: '-8%',
        color: 'blue'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        marginTop: '-8%',
        marginLeft: '2%',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 130,
        columnNumber: 13
      }
    }, "nle@kemenkeu.go.id")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "row",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 132,
        columnNumber: 9
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _assets_img_phone_png__WEBPACK_IMPORTED_MODULE_5___default.a,
      style: {
        width: '29px',
        height: '25px',
        marginLeft: '37%',
        marginTop: '-3%',
        color: 'blue',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133,
        columnNumber: 13
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        marginTop: '-3%',
        marginLeft: '2%',
        fontSize: '13px'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135,
        columnNumber: 13
      }
    }, "021-1234567")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png",
      style: {
        width: "8%",
        float: 'right',
        marginTop: '-9%',
        marginRight: '7%'
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 138,
        columnNumber: 9
      }
    }))))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Page);

/***/ }),

/***/ "./src/assets/css/agency.min.css":
/*!***************************************!*\
  !*** ./src/assets/css/agency.min.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../node_modules/postcss-loader/src??postcss!./agency.min.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/agency.min.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../node_modules/postcss-loader/src??postcss!./agency.min.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/agency.min.css", function() {
		var newContent = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../node_modules/postcss-loader/src??postcss!./agency.min.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/agency.min.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/assets/css/bootstrap.min.css":
/*!******************************************!*\
  !*** ./src/assets/css/bootstrap.min.css ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../node_modules/postcss-loader/src??postcss!./bootstrap.min.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/bootstrap.min.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../node_modules/postcss-loader/src??postcss!./bootstrap.min.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/bootstrap.min.css", function() {
		var newContent = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../node_modules/postcss-loader/src??postcss!./bootstrap.min.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/assets/css/bootstrap.min.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/assets/img/clickargo.png":
/*!**************************************!*\
  !*** ./src/assets/img/clickargo.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/clickargo.ce0c465e.png";

/***/ }),

/***/ "./src/assets/img/digico.png":
/*!***********************************!*\
  !*** ./src/assets/img/digico.png ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/digico.d05f4cdd.png";

/***/ }),

/***/ "./src/assets/img/email.png":
/*!**********************************!*\
  !*** ./src/assets/img/email.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABEVBMVEUAAAAkkrYzjacyjqgyjagyjqgrgKovjqoyjagzjKgyjagyjacxjacyjagzmZkyjagyjagyjagziKorlaoxjagzjagxjKUxiacyjakyjKoyi6YyjagyjKczi6czjacyjagzjKgyjagyjqgzmbMA//8yjacxjakAgIAyjakyjKgzjqgyjagui6I2lK4yjacyjqkzjqozjagzjKYyjqozjagyjagxjqoxj6oyjagyjagzkaoyjagyjakzjagyjagyjKhAgL8zjKcyjagzjKgyjagyjKgyjagxjKcyjagyjqgyjagziqgyjag3kKYyjagxjagyjqkzjqkuiaQ1jacyjagyjakxjKgzjqgyjagyjagAAAC+2pGmAAAAWXRSTlMAB27I9GwGG9DPzmvJrwXH87wPDLXUHxrmMy7ixkum/GT7rQoBgJECrqdG8RYTw00t3igk2e4/OerTHvlcVfe7BJf+b/2en1T2tvgjwhfS7VbjHB3yt21qzZNwakEAAAABYktHRACIBR1IAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH5AQOCB0MrJdTSAAAAbxJREFUWMPtlmlXgkAUhi+imJpGWVmmbWa7bbbavmeLS1Y6//+PxB0ZBJ1Blj51uB8QD/M8DDP35QAQVFC0pJAcJh4qLEcU5KMjXuhuxaLa/X3whMQVCPnhCUnAKP4kU+6XLpVEcgxUKhqfcMunJymogj6VqWl3fCamg0xAZmbd8Nk5Yhbk6GTyzvm8amBUIM3jcSHilF9cwvHLBUMAK0U6ndU1J3hvcE/ApOuF4by0Qaeb0E7NAtjcwjN5exhf2qE52IV+AezRhd0/sOfLh6YtswogE8fzo2M7/uQUx5xVgCfQm+v8QsxfXuGIa9a2/QK4ucV/d0URf/+A1x+N4AwI2JAnbrYG9RwBPL+IspWu4pVX8wPyBPoyDWarmx7rEnMF8GbeKKOyvE3mC6D03msVVvw2EwhYs5qyJWh0kaA/W8KoCQXWW5rS41zAHvrDmh4XAmPZy9xNcSCASg0v1et4rFXAvYC9uG1e+kMEevOb0+NS0M2WOJwOBNBoNhvgR2BfgSAQ/J0AvzU+vfEtDf0CWTtWW574b/zMgR/iq9ogxfzwHe1zO+rD0MngsyiRnOqFVnNtxev+BfXf6hdPLXV/r8do0gAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0wNC0xNFQwODoyOToxMiswMDowMBMsvAUAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMDQtMTRUMDg6Mjk6MTIrMDA6MDBicQS5AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg=="

/***/ }),

/***/ "./src/assets/img/logistic.png":
/*!*************************************!*\
  !*** ./src/assets/img/logistic.png ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logistic.69f96229.png";

/***/ }),

/***/ "./src/assets/img/logo-api.png":
/*!*************************************!*\
  !*** ./src/assets/img/logo-api.png ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo-api.7934dd1e.png";

/***/ }),

/***/ "./src/assets/img/logo-nle.png":
/*!*************************************!*\
  !*** ./src/assets/img/logo-nle.png ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo-nle.f14451ec.png";

/***/ }),

/***/ "./src/assets/img/logoitruck.png":
/*!***************************************!*\
  !*** ./src/assets/img/logoitruck.png ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logoitruck.df22d24f.png";

/***/ }),

/***/ "./src/assets/img/logol.png":
/*!**********************************!*\
  !*** ./src/assets/img/logol.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logol.7e8969fd.png";

/***/ }),

/***/ "./src/assets/img/logonle.png":
/*!************************************!*\
  !*** ./src/assets/img/logonle.png ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logonle.67ff3484.png";

/***/ }),

/***/ "./src/assets/img/lontar.png":
/*!***********************************!*\
  !*** ./src/assets/img/lontar.png ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/lontar.8aade26f.png";

/***/ }),

/***/ "./src/assets/img/mandiri.png":
/*!************************************!*\
  !*** ./src/assets/img/mandiri.png ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/mandiri.f572ed61.png";

/***/ }),

/***/ "./src/assets/img/map-image.png":
/*!**************************************!*\
  !*** ./src/assets/img/map-image.png ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/map-image.f40ed5b1.png";

/***/ }),

/***/ "./src/assets/img/phone.png":
/*!**********************************!*\
  !*** ./src/assets/img/phone.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAzFBMVEUAAAAxj6oyjagyjagyjKcyjagyi6Yyjagzjakyjqgzjagxjag1j6UyjakyjKgzj6MzjKYwj6cyjagyjagyjqkzkao3kKYyjagyjagxkqo1iqozjKkwjqgxjqoA//8zjqgzjag0jKYxjKcxiacyjqg0j6Qwjac1jqYzjakzlKgyjqkyjqkyjagyjKgyjagyjqgyjac0i6cyjagyjagziqhAgJ8zjqgyjaczjacui6Izjagwj6oyjKgyjagyjagyjagxjagzi6cyjagAAACHKoxhAAAAQnRSTlMAOcf5xjhC/EHasC8isdgZFCC+zL0eF7lwFRhQTz8BRlVFVBqZOzo/iDJNjkyK/aKjQOTlIwh+wH0W2TCy18X4xDfuyXhKAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAAd0SU1FB+QEDgggJh3E0iAAAAESSURBVFjD7dfBUsJADAbgCCIIrSIrUBCxYoUiUCiKKKjg//4PZTsMncIhu/TiJf+lbWbyzU562RAd5iyXB5P8eYHYXBShSbHEApcoVywmFRtXXP81UN2/39RUktt6VGg0lUMtoM0AFpC836UP3okK90COCLAMge6Dm+SxG1dKvScDwNOEB551/yBOnwEGgK8JMOAB0kQAAQQQQAABBBBAAAH+B1Ca8MDQ5LI9ZIDouu9qYr5wxHkZed5ofDSDE4DJ7sx+VqAdYOr7MwSNjECIefx4RZgReEPguK4THLScNAN7N4Oy+QyixbOV+ly8L5VafixSpU9+8aQV7DW3+q6/8M31myzfIQtQ4WfDtW+2v0cNfwARo5VsYXDQAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIwLTA0LTE0VDA4OjMyOjM4KzAwOjAwzBfAAQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMC0wNC0xNFQwODozMjozOCswMDowML1KeL0AAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/assets/img/prahu.png":
/*!**********************************!*\
  !*** ./src/assets/img/prahu.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/prahu.eb8b27cf.png";

/***/ }),

/***/ "./src/assets/img/radar.png":
/*!**********************************!*\
  !*** ./src/assets/img/radar.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/radar.eb8c403e.png";

/***/ }),

/***/ "./src/assets/img/trucking.png":
/*!*************************************!*\
  !*** ./src/assets/img/trucking.png ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/trucking.df635e22.png";

/***/ }),

/***/ "./src/index.css":
/*!***********************!*\
  !*** ./src/index.css ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.css */ "./src/index.css");
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_index_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./App */ "./src/App.js");
/* harmony import */ var _serviceWorker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./serviceWorker */ "./src/serviceWorker.js");
/* harmony import */ var _assets_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./assets/css/bootstrap.min.css */ "./src/assets/css/bootstrap.min.css");
/* harmony import */ var _assets_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_assets_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _assets_css_agency_min_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./assets/css/agency.min.css */ "./src/assets/css/agency.min.css");
/* harmony import */ var _assets_css_agency_min_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_assets_css_agency_min_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ "./node_modules/bootstrap/dist/css/bootstrap.min.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/auliasandie/Documents/web/landing-page-nle/src/index.js";








react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.StrictMode, {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 12,
    columnNumber: 3
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_App__WEBPACK_IMPORTED_MODULE_3__["default"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }
})), document.getElementById('root')); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

_serviceWorker__WEBPACK_IMPORTED_MODULE_4__["unregister"]();

/***/ }),

/***/ "./src/serviceWorker.js":
/*!******************************!*\
  !*** ./src/serviceWorker.js ***!
  \******************************/
/*! exports provided: register, unregister */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "register", function() { return register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unregister", function() { return unregister; });
// This optional code is used to register a service worker.
// register() is not called by default.
// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.
// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA
const isLocalhost = Boolean(window.location.hostname === 'localhost' || // [::1] is the IPv6 localhost address.
window.location.hostname === '[::1]' || // 127.0.0.0/8 are considered localhost for IPv4.
window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));
function register(config) {
  if (false) {}
}

function registerValidSW(swUrl, config) {
  navigator.serviceWorker.register(swUrl).then(registration => {
    registration.onupdatefound = () => {
      const installingWorker = registration.installing;

      if (installingWorker == null) {
        return;
      }

      installingWorker.onstatechange = () => {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            // At this point, the updated precached content has been fetched,
            // but the previous service worker will still serve the older
            // content until all client tabs are closed.
            console.log('New content is available and will be used when all ' + 'tabs for this page are closed. See https://bit.ly/CRA-PWA.'); // Execute callback

            if (config && config.onUpdate) {
              config.onUpdate(registration);
            }
          } else {
            // At this point, everything has been precached.
            // It's the perfect time to display a
            // "Content is cached for offline use." message.
            console.log('Content is cached for offline use.'); // Execute callback

            if (config && config.onSuccess) {
              config.onSuccess(registration);
            }
          }
        }
      };
    };
  }).catch(error => {
    console.error('Error during service worker registration:', error);
  });
}

function checkValidServiceWorker(swUrl, config) {
  // Check if the service worker can be found. If it can't reload the page.
  fetch(swUrl, {
    headers: {
      'Service-Worker': 'script'
    }
  }).then(response => {
    // Ensure service worker exists, and that we really are getting a JS file.
    const contentType = response.headers.get('content-type');

    if (response.status === 404 || contentType != null && contentType.indexOf('javascript') === -1) {
      // No service worker found. Probably a different app. Reload the page.
      navigator.serviceWorker.ready.then(registration => {
        registration.unregister().then(() => {
          window.location.reload();
        });
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl, config);
    }
  }).catch(() => {
    console.log('No internet connection found. App is running in offline mode.');
  });
}

function unregister() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    }).catch(error => {
      console.error(error.message);
    });
  }
}

/***/ }),

/***/ 1:
/*!**************************************************************************************************************!*\
  !*** multi (webpack)/hot/dev-server.js ./node_modules/react-dev-utils/webpackHotDevClient.js ./src/index.js ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/auliasandie/Documents/web/landing-page-nle/node_modules/webpack/hot/dev-server.js */"./node_modules/webpack/hot/dev-server.js");
__webpack_require__(/*! /Users/auliasandie/Documents/web/landing-page-nle/node_modules/react-dev-utils/webpackHotDevClient.js */"./node_modules/react-dev-utils/webpackHotDevClient.js");
module.exports = __webpack_require__(/*! /Users/auliasandie/Documents/web/landing-page-nle/src/index.js */"./src/index.js");


/***/ }),

/***/ 2:
/*!**********************************!*\
  !*** ./WritableStream (ignored) ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[1,"runtime-main",0]]]);
//# sourceMappingURL=main.chunk.js.map