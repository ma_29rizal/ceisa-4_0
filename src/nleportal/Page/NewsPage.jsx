import React, { Component } from 'react';
import axios from 'axios';
import { NavDropdown } from 'react-bootstrap';
import HtmlToReact from 'html-to-react';
import { Link } from "react-router-dom";
import { Link as Linkscroll } from "react-scroll";
import Moment from 'moment';
import './Home.css';
import './NewsPage.css';
import nle from '../assets/img/logo-nle.png'


class NewsPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            news: [],
            title: null,
            image: null,
            content: null,
            idnews: this.props.location.state.idnews,
            titlenews: this.props.location.state.titlenews,
            contentnews: this.props.location.state.contentnews,
            imagenews: this.props.location.state.imagenews,
            datenews: this.props.location.state.datenews,
            offset: 0,
            page: 5
        }
    }

    fetchingData() {
        axios.get(`https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.kontan.co.id%2Fnews%2Fexportexpert`)
            .then(response => {
                const news = response.data.items;
                console.log(news.length)
                const slice = news.slice(this.state.offset, this.state.offset + this.state.page);
                const dataNews = slice.map((item,index )=> {
                    return (
                        <>
                            <div className="card.servicepage mb-3">
                                <div className="card-body card-page">
                                    <>
                                    <Link style={{textDecoration: 'none'}} to={{ pathname:`/nle/news/${index}`, state : {
                                        idnews : index,
                                        titlenews: item.title,
                                        contentnews: item.link,
                                        imagenews: item.thumbnail,
                                        datenews: Moment(item.pubDate).format('LLLL')
                                    }
                                    }}>
                                        <div className="row">
                                            <div className="col-md-6" style={{ padding: "0%"}}>
                                                <p  className="card-title" style={{ margin: "0%" }}>{item.title}</p>
                                                <p className="number">{Moment(item.pubDate).format('LLL')}</p>
                                            </div>
                                            <div className="col-md-6">
                                                <img  style={{marginLeft: '16%'}} className="img-fluid img-list-music" src={item.thumbnail} alt={index} />
                                            </div>
                                        </div>
                                    </Link>
                                    </>
                                </div>
                                <hr style={{marginLeft:"16%", borderTop: "1px solid rgba(0, 0, 0, 0.18)"}}/>
                            </div>
                        </>
                    )
                })
            this.setState({
                dataNews,
                news: response.data.items, 
                title: response.data.items.title, 
                image: response.data.items.thumbnail,
                content: response.data.items.content
            })
        })
        .catch(function (error) {
            console.log(error)
        })
    }

    componentDidMount() {
        this.fetchingData()
        window.scrollTo(0, 0);
    }

    render(){
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        const news = this.props.location.state
        console.log(this.props)
        return (
            <>
                <div className="App">
                    <div className="row">
                        <div className="col-md-8" style={{ padding: "7% 6% 6% 6%" }}>
                                <h3 className="newspage-title" key="1" >{news.titlenews}</h3>
                            <img className="img-page" src={news.imagenews}/>
                            <div style={{ padding: "3% 5% 10% 5%" }}>
                                <p className="newspage-date">{Moment(news.datenews).format('LLLL')}</p>
                                <hr style={{margin: "0% 60% 2% 0%", borderTop: "1px solid rgba(0, 0, 0, 0.18)"}}/>
                                <p style={{textAlign: 'justify'}}>
                                {news.contentnews}
                                </p>
                            </div>
                        </div>
                        <div className="col-md-4" style={{ padding: "11% 9% 6% 1%"}}>
                            <>
                                <h5 className="latcard">LATEST NEWS</h5>
                                {this.state.dataNews}
                            </>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default NewsPage
