import React, { Component } from 'react';
import axios from 'axios';
import { NavDropdown, Jumbotron } from 'react-bootstrap';
import HtmlToReact from 'html-to-react';
import { Link } from "react-router-dom";
import { Link as Linkscroll } from "react-scroll";
import './NewsList.css'
import Moment from 'moment';
import ReactPaginate from 'react-paginate';
import NewsCard from '../Components/NewsCard';
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'

class NewsList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            news: [],
            title: null,
            image: null,
            content: null,
            idnews: null,
            imagenews: null,
            datenews: null,
            titlenews: null,
            contentnews: null,
            offset: 0,
            perPage: 5,
            currentPage: 0,
            pageCount: null
        }
        this.handlePageClick = this
            .handlePageClick
            .bind(this);
    }

    fetchingData() {
        axios.get(`https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.kontan.co.id%2Fnews%2Fexportexpert`)
            .then(response => {
                const news = response.data.items;
                console.log(news.length)
                const slice = news.slice(this.state.offset, this.state.offset + this.state.perPage)
                const dataNews = slice.map((item, index) => {
                    return (
                        <>
                            <div className="newslist">
                                <Link style={{ textDecoration: "none" }} to={{
                                    pathname: `/nle/news/${index}`, state: {
                                        idnews: index,
                                        titlenews: item.title,
                                        contentnews: item.link,
                                        imagenews: item.thumbnail,
                                        datenews: Moment(item.pubDate).format('LLLL')
                                    }, query: {
                                        idnews: '2'
                                    }
                                }} >
                                    <div className="row" style={{ padding: "3%" }}>
                                        <div className="col-md-6">
                                            <img className="news-img" src={item.thumbnail}></img>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="news-title">{item.title}</div>
                                            <div className="news-date">{Moment(item.pubDate).format('LLLL')}</div>
                                            <div className="news-date block-with-text">
                                                {/* CUSTOMS (Instansi Kepabeanan) di mana pun di dunia ini adalah suatu organisasi yang keberadaannya amat essensial bagi suatu negara, demikian pula dengan Direktorat Jenderal Bea dan Cukai
                                                CUSTOMS (Instansi Kepabeanan) di mana pun di dunia ini adalah suatu organisasi yang keberadaannya amat essensial bagi suatu negara, demikian pula dengan Direktorat Jenderal Bea dan Cukai... */}
                                                {item.link}
                                                </div>
                                            {/* <div className="news-date">{item.content}</div> */}
                                        </div>
                                    </div>
                                    <hr style={{ marginLeft: "0%", borderTop: "1px solid rgba(0, 0, 0, 0.18)" }} />
                                </Link>
                                {/* </a>
                                </li> */}
                            </div>

                        </>
                    )
                })
                this.setState({
                    pageCount: Math.ceil(news.length / this.state.perPage),
                    dataNews,
                    news: response.data.items,
                    title: response.data.items.title,
                    image: response.data.items.thumbnail,
                    content: response.data.items.content
                })
            })
            .catch(function (error) {
                console.log(error)
            })

    }
    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset: offset
        }, () => {
            this.fetchingData()
        });

    };
    componentDidMount() {
        this.fetchingData()
    }

    render() {
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        // const news = this.props.location.state
        console.log(this.props)
        return (
            <>
                <div className="App">
                    <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                        <Link to="/">
                            <img className="garudanavbarnews" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '6%', float: 'left', marginLeft: '-429px' }}></img>
                            <img className="nlenavbarnews" src={nle} style={{ width: '23%', marginLeft: '-383px', marginTop: '-2px', float: 'left' }}></img>
                        </Link>
                            <button className="navbar-toggler navbar-toggler-right navbar news" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav" style={{marginLeft: '25%', marginTop: '-59px'}}>                                
                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                        <Link className="c-main-about" to="/nle/about" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger about" href="/nle/about">About</a></Link>           
                                    </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/service"  style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/services">Services</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/news" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="/nle/news">News</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/apicollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/apicollaboration">API's</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/entitycollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/entitycollaboration">Entity Collaboration</a>
                                        </Link>            </li>
                                   
                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/faq" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/faq" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>
                                        
                                        <NavDropdown.Item a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>
                                            <a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</a>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>
                                    <Link to="/login" >
                                    <button type="button" className="btn btn-outline-light about" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                        <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                        </button>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <div className="row">
                        <div className="col-md-8" style={{ padding: "7% 6% 6% 6%" }}>
                            <h5 className="latestnews">LATEST NEWS</h5>
                            <hr className="linenews" style={{ margin: "0%", borderTop: "1px solid rgba(0, 0, 0, 0.18)" }} />
                            <div>
                                {this.state.dataNews}
                                <ReactPaginate
                                    previousLabel={"prev"}
                                    nextLabel={"next"}
                                    breakLabel={"..."}
                                    breakClassName={"break-me"}
                                    pageCount={this.state.pageCount}
                                    marginPagesDisplayed={2}
                                    pageRangeDisplayed={5}
                                    onPageChange={this.handlePageClick}
                                    containerClassName={"pagination"}
                                    subContainerClassName={"pages pagination"}
                                    activeClassName={"active"}
                                />
                            </div>
                        </div>
                        <div className="col-md-4" style={{ padding: "11% 9% 6% 1%" }}>
                            <h5 className="newscard">POPULER NEWS</h5>
                            <NewsCard />
                        </div>
                    </div>
                    <Jumbotron className="jumbo news" style={{ marginLeft: '-13%', width: '137%', marginLeft: '-18%', height: '232px' }}>
                        <div className="container footer" >
                            <img src={logo} className="logofooternews" style={{ width: "22%", marginTop: '0%', marginLeft: '-86%' }}>
                            </img>
                            <div className="address" style={{ textAlign: 'left', marginLeft: '27%', marginTop: '-6%', fontSize: '14px' }}>
                                <span>Kantor Pusat Bea dan Cukai</span>
                                <br></br>
                                <span>Jl. Ahmad Yani By Pass Rawamangun</span>
                                <br></br>
                                <span>Jakarta Timur - 13230</span>
                            </div>
                            <div className='row footer' style={{marginLeft: '31%', marginTop: '-2%'}}>
                                <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }} className="mail"></img>
                                <span style={{ marginTop: '-8%', marginLeft: '2%', fontSize: '13px' }} className="mail">nle@beacukai.go.id</span>
                                <img className="phone" src={phone} style={{ width: '29px', height: '25px', marginLeft: '-20%', marginTop: '-3%', color: 'blue', fontSize: '14px' }}>
                                </img>
                                <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }}className="phone">021-1500225</span>
                            </div>
                            <div className="footer-garuda">
                            <img className="garudanews" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-10%', marginLeft: '31%', marginRight: '12%' }}></img>
                            </div>
                        </div>
                    </Jumbotron>
                    </div>
            </>
        )
    }
}

export default NewsList;