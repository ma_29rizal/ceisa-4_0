import React, { Component } from 'react';
import { NavDropdown, Carousel, Button, Jumbotron } from 'react-bootstrap';
import axios from 'axios'
import HtmlToReact from 'html-to-react'
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'
import { Link } from "react-router-dom";
import './Page.css'


class Page extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            content: null,
            icon: null,
            title: null,
        }
    }

    fetchingData() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=${this.props.match.params.code}`)
            .then(response => {
                console.log(response);
                this.setState({ data: response.data, content: response.data.content, title: response.data.title, icon: response.data.icon })
            })
            .catch(function (error) {
                console.log(error)
            })
    }

    componentDidMount() {
        this.fetchingData()
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.code !== prevProps.match.params.code) {
            this.fetchingData(this.props.match.params.code)

        }
    }



    render() {
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        const item = this.state.data
        return (
            <>
                <section className="s-service" id="service"></section>
                <div className="App">
                    <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                            <Link to="/">
                                <img className="garudanavbarpage" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '6%', float: 'left', marginLeft: '-429px' }}></img>
                                <img className="nlenavbarpage" src={nle} style={{ width: '23%', marginLeft: '-383px', marginTop: '-2px', float: 'left' }}></img>
                            </Link>
                            <button className="navbar-toggler navbar-toggler-right navbar page" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                                <ul className="navbar-nav" style={{ marginLeft: '25%', marginTop: '-59px' }}>                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                    <Link className="c-main-about" to="/nle/about" style={{ color: 'white' }}>
                                        <a className="nav-link js-scroll-trigger about" href="/nle/about">About</a></Link>
                                </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/service" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/services">Services</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/news" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="/nle/news">News</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/apicollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/apicollaboration">API's</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/entitycollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/entitycollaboration">Entity Collaboration</a>
                                        </Link>            </li>

                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/faq" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/faq" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>

                                        <NavDropdown.Item a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>
                                            <a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</a>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>
                                        <Link to="/login" >
                                            <button type="button" className="btn btn-outline-light about" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                                <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                            </button>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>



                    <h3 style={{ marginTop: '8%', paddingBottom: '3%' }}>{this.state.title}</h3>
                    <p style={{ width: '90%', textAlign: 'justify', marginLeft: '4%' }}>{HtmlToReactParser.parse(item.content)}</p>



                    <Jumbotron style={{ marginLeft: '-13%', width: '137%', marginLeft: '-18%', height: '232px' }}>
                        <div className="container footer" >
                            <img src={logo} className="logofooterpage" style={{ width: "22%", marginTop: '0%', marginLeft: '-86%' }}>
                            </img>
                            <div className="address" style={{ textAlign: 'left', marginLeft: '27%', marginTop: '-6%', fontSize: '14px' }}>
                                <span>Kantor Pusat Bea dan Cukai</span>
                                <br></br>
                                <span>Jl. Ahmad Yani By Pass Rawamangun</span>
                                <br></br>
                                <span>Jakarta Timur - 13230</span>
                            </div>
                            <div className='row footer' style={{ marginLeft: '31%', marginTop: '-2%' }}>
                                <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }} className="mail"></img>
                                <span style={{ marginTop: '-8%', marginLeft: '2%', fontSize: '13px' }} className="mail">nle@beacukai.go.id</span>
                                <img className="phone" src={phone} style={{ width: '29px', height: '25px', marginLeft: '-20%', marginTop: '-3%', color: 'blue', fontSize: '14px' }}>
                                </img>
                                <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }} className="phone">021-1500225</span>
                            </div>
                            <div className="footer-garuda">
                                <img className="garudapage" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-10%', marginLeft: '31%', marginRight: '12%' }}></img>
                            </div>
                        </div>
                    </Jumbotron>
                </div>
            </>
        )

    }

}

export default Page;