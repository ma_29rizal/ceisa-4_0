import React, { Component } from 'react';
import { NavDropdown, Carousel, Button, Jumbotron, Spinner, Card } from 'react-bootstrap';
import './Home.css'
import axios from 'axios';
import HtmlToReact from 'html-to-react';
import { Link as Linkscroll } from "react-scroll";
import { Link } from "react-router-dom";
import 'react-multi-carousel/lib/styles.css';
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'
import { StyleSheet, css } from 'aphrodite';
import FadeIn from 'react-fade-in';
import carousel1 from '../assets/img/first-carousel.jpeg'
import carousel2 from '../assets/img/second-carousel.jpeg'
import carousel3 from '../assets/img/third-carousel.jpeg'
import carousel4 from '../assets/img/four-carousel.jpeg'
import carousel5 from '../assets/img/five-carousel.jpeg'
import carousel6 from '../assets/img/six-carousel.jpeg'
import carousel7 from '../assets/img/seven-carousel.jpeg'
import carousel8 from '../assets/img/eight-carousel.jpeg'
import carousel9 from '../assets/img/nine-carousel.jpeg'
import * as Parser from 'rss-parser';
import { string } from 'prop-types';
import Moment from 'moment';

let parser = new Parser();
class HomePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            about: [],
            service: [],
            news: [],
            api: [],
            feed: [],
            collab: [],
            collabchildren: [],
            contentapi: null,
            titleapi: null,
            summaryapi: null,
            contentabout: null,
            titleabout: null,
            summaryabout: null,
            contentservice: null,
            titleservice: null,
            summaryservice: null,
            titlenews: null,
            contentnews: null,
            datenews: null,
            imagenews: null,
            idnews: null,
            iconservice: null,
            isLoading: false,
            iconapi: null,
            offset: 0,
            perPage: 5,


        }

    }

    fetchingAbout() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=about`)
            .then(response => {
                console.log(response);
                this.setState({ about: response.data, contentabout: response.data.content, titleabout: response.data.title, summaryabout: response.data.icon })
            })
            .catch(function (error) {
                console.log(error)
            })

    }

    fetchingService() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=service`)
            .then(response => {
                console.log(response);
                this.setState({ service: response.data.children, titleservice: response.data.title, summaryservice: response.data.children.summary, iconservice: response.data.children.icon, contentservice: response.data.children.content, isLoading: true })
            })
            .catch(function (error) {
                console.log(error)
            })

    }

    fetchingApi() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=apicollaboration`)
            .then(response => {
                console.log(response);
                this.setState({ api: response.data, contentapi: response.data.content, titleapi: response.data.title, iconapi: response.data.icon })
            })
            .catch(function (error) {
                console.log(error)
            })
    }

    getNews() {
        axios.get(`https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.kontan.co.id%2Fnews%2Fexportexpert`)
            .then(response => {
                this.setState({ feed: response.data.items })

            })
            .catch(function (error) {
                console.log(error)
            })

    }

    fetchingCollab() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=entitycollaboration`)
            .then(response => {
                console.log(response);
                this.setState({ collab: response.data, collabchildren: response.data.children })
            })
            .catch(function (error) {
                console.log(error)
            })

    }



    componentDidMount() {
        this.fetchingAbout()
        this.fetchingService()
        // this.fetchingNews()
        this.fetchingApi()
        this.getNews()
        this.fetchingCollab()
    }

    render() {
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        const about = this.state.about
        const api = this.state.api
        const item = this.state.service
        console.log(this.state.about)
        console.log(this.props)

        return (
            <>
                <div className="App home">
                    <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                            <Link to="/">
                                <img className="garudanavbar" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '6%', float: 'left', marginLeft: '-429px' }}></img>
                                <img className="nlenavbar" src={nle} style={{ width: '23%', marginLeft: '-383px', marginTop: '-2px', float: 'left' }}></img>
                            </Link>
                            <button className="navbar-toggler navbar-toggler-right navbar home" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                                <ul className="navbar-nav" style={{ marginLeft: '25%', marginTop: '-59px' }}>
                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                        <Linkscroll className="c-main-about" to="about" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger about" href="#about">About</a></Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="services" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="#services">Services</a>
                                        </Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="news" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="#news">News</a>
                                        </Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="api" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="#api">API's</a>
                                        </Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="collaboration" smooth={true} duration={1000} offset={-50} ß style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="#collaboration">Entity Collaboration</a>
                                        </Linkscroll>
                                    </li>
                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/faq" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/faq" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>
                                            <a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</a>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>

                                        <Link to="/login" >
                                            <button type="button" className="btn btn-outline-light about" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                                <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                            </button>
                                        </Link>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <header className="masthead">


                        <Carousel style={{ marginTop: '4%' }} fade={true} indicators={false}>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel1}
                                    alt="First slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark className="caption" style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel2}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel3}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel4}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel5}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel6}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel7}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel8}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel9}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                    <div class="carousel-caption" style={{ top: '44%' }}>
                                        <h1><mark style={{ backgroundColor: 'grey', color: 'white' }}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{ marginTop: '25px' }}><mark style={{ backgroundColor: '#233552', color: 'white' }}>Turning Problem Into Opportunity</mark></p>

                                    </div>
                                </div>
                            </Carousel.Item>
                        </Carousel>
                    </header>

                    <FadeIn transitionDuration={2000}>
                        <div style={{ backgroundColor: '#233552', marginBottom: '-8%' }}>
                            <section className="page-section" id="about">
                                <div className="container" style={{ marginTop: '-7%' }} >

                                    <div className="row">
                                        <div className="col-lg-12 text-center">
                                            <h2 className="section-heading text-uppercase" style={{ color: 'white' }}>{this.state.titleabout}</h2>
                                        </div>
                                    </div>
                                    <div className="row text-center" style={{ marginTop: '-5%' }}>
                                        <p className="text-muted" style={{ width: '47%', textAlign: 'left', marginTop: '6%', color: 'white', }}>{HtmlToReactParser.parse(about.content)}</p>
                                        <div className="youtube" style={{ marginLeft: '-7%', height: '389px' }}>
                                            {HtmlToReactParser.parse(about.icon)}
                                        </div>

                                    </div>

                                </div>
                            </section>
                        </div>
                    </FadeIn>

                    <FadeIn transitionDuration={2000}>
                        <div style={{ backgroundColor: 'white', marginBottom: '-6%' }}>
                            <section className="page-section" id="services">
                                <div className="container" style={{ marginTop: '-7%' }}>
                                    <div className="col-lg-12 text-center">
                                        <h2 className="section-heading text-uppercase">{this.state.titleservice}</h2>
                                    </div>
                                    <div className="row">
                                        {this.state.service.map((item, index) => {
                                            return (
                                                <>
                                                    <div className="col-md-4">
                                                        <div className="services-grid">
                                                            <div className="service service1" style={{ marginTop: '0%', height: '349px' }}>
                                                                <i className="ti-bar-chart" />
                                                                <span className="fa-stack fa-4x">
                                                                    <img src={item.icon} style={{ width: '30px', height: '30px', marginTop: '-60%' }}></img>
                                                                </span>
                                                                <h4 className="title-service" style={{ marginTop: '-30%' }}>{item.title}</h4>
                                                                <p className="content-service" style={{ marginTop: '-24%' }}>{HtmlToReactParser.parse(item.content)}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                            )
                                        })}
                                    </div>
                                </div>
                            </section>
                        </div>
                    </FadeIn>

                    <FadeIn transitionDuration={2000}>
                        <div style={{ backgroundColor: '#233552', marginBottom: '-5%' }}>
                            <section className="page-section" id="news">
                                <div className="container" style={{ marginTop: '-7%' }}>
                                    <div className="row">
                                        <div className="col-lg-12 text-center">
                                            <h2 className="section-heading text-uppercase" style={{ color: 'white' }}>NEWS</h2>
                                        </div>
                                    </div>
                                    <div className="row news" style={{ marginTop: '16%', marginLeft: '2%' }}>
                                        {this.state.feed.map((item, index) => {
                                            return (
                                                <>
                                                    <div className="col-md-3">
                                                        <body className="body news">
                                                            <Link to={{
                                                                pathname: `/nle/news/${index}`, state: {
                                                                    idnews: index,
                                                                    titlenews: item.title,
                                                                    contentnews: item.link,
                                                                    imagenews: item.thumbnail,
                                                                    datenews: item.pubDate
                                                                }
                                                            }} >
                                                                <article class="card news" style={{ height: '277px' }}>
                                                                    <header class="cardThumb">
                                                                        <a href={`/nle/news/${index}`}>


                                                                            <img src={item.thumbnail} style={{ width: '225px' }}></img>
                                                                        </a>
                                                                    </header>
                                                                    <div class="cardBody">
                                                                        <h2 className="cardTitle"><a href>
                                                                            <h2 className="block-with-text" style={{ fontSize: '15px', textAlign: 'left' }}>{item.title}</h2></a></h2>
                                                                    </div>
                                                                    <footer className="cardFooter">
                                                                        <span style={{ textAlign: 'center' }}>{Moment(item.pubDate).format('LLL')}</span>
                                                                    </footer>
                                                                </article>
                                                            </Link>
                                                        </body>
                                                    </div>
                                                </>
                                            )
                                        })}
                                    </div>
                                    <div>
                                        <a className="btn btn-light news" href="/news" style={{ width: '13%', height: '10%', color: 'black', border: '2px solid', textDecoration: "none", marginTop: '18%' }}>
                                            <Link to="/news" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>

                                    </div>
                                </div>
                            </section>
                        </div>
                    </FadeIn>

                    <FadeIn transitionDuration={2000}>

                        <div style={{ backgroundColor: '#c5cbd4', marginBottom: '-8%' }}>
                            <section className="page-section" id="api">
                                <div className="container" style={{ marginTop: '-7%' }}>
                                    <div className="row" style={{ marginTop: '-8%' }}>
                                        <div className="col-lg-12 text-center" >
                                            <h2 className="section-heading text-uppercase">{this.state.titleapi}</h2>
                                        </div>
                                    </div>
                                    <div className="row" style={{ marginTop: '-8%' }}>
                                        <img src={this.state.iconapi} className="responsive" style={{ width: "220px", marginLeft: '4%', marginTop: '10%', height: '100px' }}></img>
                                        <p className="apitext" style={{ color: '#1d2d4b', width: '50%', marginLeft: '4%', marginTop: '11%', textAlign: 'justify' }}>
                                            {HtmlToReactParser.parse(api.content)}
                                        </p>
                                    </div>
                                    <Button variant="light" className="menu-button right" style={{
                                        width: '90px',
                                        height: '30px', marginLeft: '86%', marginTop: '-16%', borderRadius: '10px'
                                    }}>
                                        <p style={{ fontSize: 12, textAlign: 'center' }}>Subscribe</p>
                                    </Button>
                                </div>

                            </section>
                        </div>

                    </FadeIn>

                    <FadeIn transitionDuration={2000}>
                        <div style={{ backgroundColor: 'white' }}>
                            <section className="page-section" id="collaboration">
                                <div className="container" style={{ marginTop: '-7%' }}>
                                    <div className="row">
                                        <div className="col-lg-12 text-center">
                                            <h2 className="section-heading text-uppercase" style={{ marginBottom: '11%' }}>ENTITY COLLABORATION</h2>
                                        </div>
                                    </div>
                                    <div className="row row-1" style={{ marginTop: '-5%', marginLeft: '1%' }}>
                                        {this.state.collabchildren.map((item, index) => {
                                            return (
                                                <div className="col-md-3 collab-menu collab-icon">
                                                    <div className="collab">
                                                        <span className="hidden-xs hidden-sm" className="gov" style={{ marginLeft: '15%' }}>
                                                            {item.title}
                                                            <span className="pt-icon-standard pt-icon-chevron-down bt-caret"></span>
                                                        </span>
                                                        <div className="row row-2" style={{ marginLeft: '10px', marginTop: '22px' }}>
                                                            <div className="row-md-3">
                                                                {item.title === 'Private Sector' || item.title === 'Other Entity' ? (
                                                                    item.children.map((item2, index) => {
                                                                        return (
                                                                            <img src={item2.icon} style={{ width: '30%', marginLeft: '18px', marginBottom: '10px' }} title={item2.title} />
                                                                        )
                                                                    })
                                                                ) : (
                                                                        item.children.map((item2, index) => {
                                                                            return (
                                                                                <img src={item2.icon} style={{ width: '20%', marginLeft: '18px', marginBottom: '10px' }} title={item2.title} />
                                                                            )
                                                                        })
                                                                    )}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </section>
                        </div>
                    </FadeIn>

                    <Jumbotron style={{ marginLeft: '-13%', width: '137%', marginLeft: '-18%', height: '232px' }}>
                        <div className="container footer" >
                            <img src={logo} className="logofooter" style={{ width: "22%", marginTop: '0%', marginLeft: '-86%' }}>
                            </img>
                            <div className="address" style={{ textAlign: 'left', marginLeft: '27%', marginTop: '-6%', fontSize: '14px' }}>
                                <span>Kantor Pusat Bea dan Cukai</span>
                                <br></br>
                                <span>Jl. Ahmad Yani By Pass Rawamangun</span>
                                <br></br>
                                <span>Jakarta Timur - 13230</span>
                            </div>
                            <div className='row footer' style={{ marginLeft: '31%', marginTop: '-2%' }}>
                                <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }} className="mail"></img>
                                <span style={{ marginTop: '-8%', marginLeft: '2%', fontSize: '13px' }} className="mail">nle@beacukai.go.id</span>
                                <img className="phone" src={phone} style={{ width: '29px', height: '25px', marginLeft: '-20%', marginTop: '-3%', color: 'blue', fontSize: '14px' }}>
                                </img>
                                <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }} className="phone">021-1500225</span>
                            </div>
                            <div className="footer-garuda">
                                <img className="garuda" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-10%', marginLeft: '31%', marginRight: '12%' }}></img>
                            </div>
                        </div>
                    </Jumbotron>
                </div>
            </>
        )
    }
}

const styles = {
    block: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        background: 'white',
        borderBottom: '1px solid rgba(255,255,255,.2)'
    },
    titlemotion: {
        textAlign: 'center',
        fontSize: '40px',
        color: '#fff',
        fontFfamily: 'Poppins, sans-serif',
        fontWeight: 100
    },
};

export default HomePage;


