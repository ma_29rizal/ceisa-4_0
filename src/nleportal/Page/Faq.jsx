import React, { Component } from 'react';
import { NavDropdown, Button, Jumbotron, Form, FormGroup } from 'react-bootstrap';
import axios from 'axios'
import HtmlToReact from 'html-to-react'
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'
import { Link } from "react-router-dom";
import Faq from "react-faq-component";
import './Faq.css'



const data = {
    title: "FAQ",
    rows: [
        {
            title: "National Ecosystem Logistics",
            content: `NLE adalah Ekosistem logistik yang menyelaraskan arus lalu lintas barang dan dokumen international sejak kedatangan sarana pengangkut hingga barang tiba di gudang, berorientasi pada kerja sama antar instansi pemerintah dan swasta, melalui pertukaran data, simplifikasi proses, penghapusan repetisi dan duplikasi, serta didukung oleh sistem teknologi informasi yang mencakup seluruh proses logistic terkait dan menghubungkan sistem – sistem logoistik yang telah ada.`,
        },
        {
            title: "Apa itu My Cargo ?",
            content:
                "My Cargo merupakan solusi yang terhubung dengan modul-modul logistik untuk memudahkan pengguna jasa dalam melakukan proses logistik kepelabuhanan secara online dan mudah dalam 1 aplikasi",
        },
        {
            title: "Bagaimana cara menggunakan My Cargo ?",
            content: `Untuk mendapatkan account user My Cargo, Anda dapat melakukan registrasi di mycargo.ilcs.co.id. Panduan registrasi My Cargo dapat diakses melalui menu Supports, kemudian pilih sub menu "Tutorial Documentation". `,
        },
        {
            title: "Apa keuntungan menggunakan My Cargo ?",
            content: "Dengan menggunakan My Cargo, Anda dapat melakukan transaksi dan pembayaran secara online yang dapat diakses 24/7. Layanan My Cargo dapat membantu Anda meningkatkan efisiensi waktu dan resource. My Cargo juga terintegrasi dengan semua modul logistik untuk memudahkan proses logistik Anda.",
        },
    ],
};



const styles = {
    titleTextColor: "black",
    rowTitleColor: "black",
}


export default class faq extends Component {
    constructor() {
        super()
        this.state = {
            faq: [],
            titlefaq: [],
            title: null,
            content: null
        }
    }

    fetchingFaq() {
        axios.get(`https://contohportal.herokuapp.com/getContent?code=faq`)
            .then(response => {
                console.log(response);
                this.setState({ faq: response.data.category_code, title: response.data.category_code.title })
            })
            .catch(function (error) {
                console.log(error)
            })

    }


    componentDidMount() {
        window.scrollTo(0, 0)
        this.fetchingFaq()

    }
    render() {
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        const faq = this.state.faq
        console.log(this.state.title)
        return (
            <div className="App">
                <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                    <div className="container" style={{ marginLeft: '34%' }}>
                        <Link to="/">
                            <img className="garudanavbarfaq" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '6%', float: 'left', marginLeft: '-429px' }}></img>
                            <img className="nlenavbarfaq" src={nle} style={{ width: '23%', marginLeft: '-383px', marginTop: '-2px', float: 'left' }}></img>
                        </Link>
                        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <i className="fa fa-bars"></i>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav" style={{ marginLeft: '25%', marginTop: '-59px' }}>
                                <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                    <Link className="c-main-about" to="/nle/about" style={{ color: 'white' }}>
                                        <a className="nav-link js-scroll-trigger about" href="/nle/about">About</a></Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="c-main-about" to="/nle/service" style={{ color: 'white' }}>
                                        <a className="nav-link js-scroll-trigger" href="/nle/services">Services</a>
                                    </Link>            </li>
                                <li className="nav-item">
                                    <Link className="c-main-about" to="/news" style={{ color: 'white' }}>
                                        <a className="nav-link js-scroll-trigger-smooth" href="/nle/news">News</a>
                                    </Link>            </li>
                                <li className="nav-item">
                                    <Link className="c-main-about" to="/nle/apicollaboration" style={{ color: 'white' }}>
                                        <a className="nav-link js-scroll-trigger" href="/nle/apicollaboration">API's</a>
                                    </Link>            </li>
                                <li className="nav-item">
                                    <Link className="c-main-about" to="/nle/entitycollaboration" style={{ color: 'white' }}>
                                        <a className="nav-link js-scroll-trigger" href="/nle/entitycollaboration">Entity Collaboration</a>
                                    </Link>            </li>
                                <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                    <NavDropdown.Item href="/faq" style={{ color: 'black', fontSize: '13px' }}>
                                        <Link to="/faq" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                    </NavDropdown.Item>

                                    <NavDropdown.Item a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>
                                        <a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</a>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                        <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                    </NavDropdown.Item>
                                </NavDropdown>
                                <li>
                                    <Link to="/login" >
                                        <button type="button" className="btn btn-outline-light about" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                            <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                        </button>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="faq" style={{ marginTop: '-6%', marginLeft: '-15%', color: 'black' }}>
                    <Faq data={data} styles={styles} />
                </div>


                <Jumbotron className="faq" style={{ marginLeft: '-13%', width: '137%', marginLeft: '-18%', height: '232px' }}>
                    <div className="container footer" style={{marginTop: '-10%'}}>
                        <img src={logo} className="logofooterfaq" style={{ width: "22%", marginTop: '0%', marginLeft: '-86%' }}>
                        </img>
                        <div className="address" style={{ textAlign: 'left', marginLeft: '27%', marginTop: '-6%', fontSize: '14px' }}>
                            <span>Kantor Pusat Bea dan Cukai</span>
                            <br></br>
                            <span>Jl. Ahmad Yani By Pass Rawamangun</span>
                            <br></br>
                            <span>Jakarta Timur - 13230</span>
                        </div>
                        <div className='row footer' style={{ marginLeft: '31%', marginTop: '-2%' }}>
                            <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }} className="mail"></img>
                            <span style={{ marginTop: '-8%', marginLeft: '2%', fontSize: '13px' }} className="mail">nle@beacukai.go.id</span>
                            <img className="phone" src={phone} style={{ width: '29px', height: '25px', marginLeft: '-20%', marginTop: '-3%', color: 'blue', fontSize: '14px' }}></img>
                            <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }} className="phone">021-1500225</span>
                        </div>
                        <div className="footer-garuda">
                            <img className="garuda" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-10%', marginLeft: '31%', marginRight: '12%' }}></img>
                        </div>
                    </div>
                </Jumbotron>
            </div>

        );
    }
}