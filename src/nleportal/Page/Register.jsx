import React, { Component } from 'react';
import { NavDropdown, Button, Jumbotron, Form, FormGroup } from 'react-bootstrap';
import axios from 'axios'
import HtmlToReact from 'html-to-react'
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'
import { Link } from "react-router-dom";
import './Register.css'


class RegisterPage extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);

    }

    render() {
        return (
            <>
                <div className="App">
                    <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                            <Link to="/">
                                <img className="garudanavbarsignup" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '6%', float: 'left', marginLeft: '-429px' }}></img>
                                <img className="nlenavbarsignup" src={nle} style={{ width: '23%', marginLeft: '-383px', marginTop: '-2px', float: 'left' }}></img>
                            </Link>
                            <button className="navbar-toggler navbar-toggler-right navbar register" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                                <ul className="navbar-nav" style={{ marginLeft: '25%', marginTop: '-59px' }}>
                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                        <Link className="c-main-about" to="/nle/about" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger about" href="/nle/about">About</a></Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/service" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/services">Services</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/news" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="/nle/news">News</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/apicollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/apicollaboration">API's</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/entitycollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/entitycollaboration">Entity Collaboration</a>
                                        </Link>            </li>

                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/faq" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/faq" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>

                                        <NavDropdown.Item a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>
                                            <a href="https://nledocs.kemenkeu.go.id/" target="_blank" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</a>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>
                                        <Link to="/login" >
                                            <button type="button" className="btn btn-outline-light about" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                                <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                            </button>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>




                    <div className="section" />
                    <main className="register" style={{ marginTop: '10%', marginLeft: '-64%' }}>
                        <center>
                            <div className="section" />
                            <h5 className="indigo-text" style={{ marginTop: '-5%', marginLeft: '4%', marginBottom: '1%' }}>Please, sign up your account</h5>
                            <div className="section" />
                            <div className="container">
                                <div className="z-depth-1 black dark-4 row register" style={{ display: 'inline-block', padding: '32px 48px 0px 48px', border: '1px solid black', marginLeft: '60px', width: '32%', marginTop: '18%' }}>
                                    <form className="col s12" method="post">
                                        <div className="row">
                                            <div className="col s12">
                                            </div>
                                        </div>
                                        <br />
                                        <center>
                                            <div className="row">
                                                <div className="subhead">
                                                    <h4 style={{ marginTop: '-48px', marginLeft: '30px', fontSize: '24px' }}>Registration Form</h4>
                                                </div>
                                                <div className="input login" style={{ marginTop: '10%' }}>
                                                    <Form>
                                                        <Form.Group controlId="exampleForm.ControlSelect1">
                                                            <Form.Label style={{ marginLeft: '-62%' }}>Group User</Form.Label>
                                                            <Form.Control as="select">
                                                                <option>Cargo Owner</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                        <Form.Group controlId="formBasicEmail">
                                                            <Form.Label style={{ marginLeft: '-62%' }}>Username</Form.Label>    <Form.Control type="email" placeholder="Enter Username" />
                                                        </Form.Group>
                                                        <Form.Group controlId="formBasicPassword">
                                                            <Form.Label style={{ marginLeft: '-62%' }}> Password</Form.Label>    <Form.Control type="password" placeholder="Password" />
                                                        </Form.Group>
                                                        <div>
                                                            <a href="#!" style={{ marginLeft: '43%' }}>Forgot Password?</a>
                                                        </div>
                                                        <Button variant="light" type="submit" style={{ marginTop: '7%', marginBottom: '3%', border: '1px solid' }}>
                                                            Register
                                                        </Button>

                                                        <div style={{ marginBottom: '10px' }}>
                                                            <span style={{ marginLeft: '-3%' }}>Already have an account?</span> <a href="/login">Sign In</a>
                                                        </div>
                                                    </Form>
                                                </div>
                                            </div>
                                        </center>
                                    </form>
                                </div>
                            </div>

                        </center>
                        <div style={{ marginTop: '-19%' }}>
                            <h3 className="text-register" style={{ marginLeft: '57%', marginTop: '-28%' }}>
                                Turning Problem Into Opportunity
                            </h3>
                        </div>
                        <div className="section" />
                        <div className="section" />
                    </main>
                </div>
                <img className="img-register" src={require('../assets/img/login-removebg-preview.png')} style={{ marginLeft: '46%', marginTop: '5%', width: '46%' }}></img>
                <Jumbotron className="register" style={{ marginLeft: '-13%', width: '137%', marginLeft: '-18%', height: '232px', marginTop: '10%' }}>
                    <div className="container footer" >
                        <img src={logo} className="logofooterregister" style={{ width: "22%", marginTop: '0%', marginLeft: '-86%' }}>
                        </img>
                        <div className="address" style={{ textAlign: 'left', marginLeft: '27%', marginTop: '-6%', fontSize: '14px' }}>
                            <span>Kantor Pusat Bea dan Cukai</span>
                            <br></br>
                            <span>Jl. Ahmad Yani By Pass Rawamangun</span>
                            <br></br>
                            <span>Jakarta Timur - 13230</span>
                        </div>
                        <div className='row footer' style={{ marginLeft: '31%', marginTop: '-2%' }}>
                            <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }} className="mail"></img>
                            <span style={{ marginTop: '-8%', marginLeft: '2%', fontSize: '13px' }} className="mail">nle@beacukai.go.id</span>
                            <img className="phone" src={phone} style={{ width: '29px', height: '25px', marginLeft: '-20%', marginTop: '-3%', color: 'blue', fontSize: '14px' }}>
                            </img>
                            <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }} className="phone">021-1500225</span>
                        </div>
                        <div className="footer-garuda">
                            <img className="garudaregister" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-10%', marginLeft: '31%', marginRight: '12%' }}></img>
                        </div>
                    </div>
                </Jumbotron>
            </>
        )

    }

}

export default RegisterPage;