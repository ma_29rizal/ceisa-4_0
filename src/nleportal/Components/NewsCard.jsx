import React, { Component } from 'react';
import axios from 'axios';
import { NavDropdown } from 'react-bootstrap';
import HtmlToReact from 'html-to-react';
import { Link } from "react-router-dom";
import Moment from 'moment';
import './NewsCard.css'

class NewsCard extends Component {
    constructor(){
        super()
        this.state = {
            news: [],
            offset: 0,
            page: 5
        }
    }

    fetchingData() {
        axios.get(`https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.kontan.co.id%2Fnews%2Fexportexpert`)
        .then(response => {
            const news = response.data.items;
            console.log(news.length)
            const slice = news.slice(this.state.offset, this.state.offset + this.state.page);
            const dataNews = slice.map((item,index )=> {
                return (
                    <>
                        <div className="card.servicepage div-card">
                            <div className="card-body">
                                <Link style={{textDecoration: 'none'}} to={{
                                    pathname: `/nle/news/${index}`, state: {
                                        idnews : index,
                                        titlenews: item.title,
                                        contentnews: item.content,
                                        imagenews: item.thumbnail,
                                        datenews: Moment(item.pubDate).format('LLLL')
                                    }
                                }}>
                                    <img className="img-fluid img-list-music" src={item.thumbnail} alt="" />
                                    <h7 className="card-title" style={{ margin: "0%" }} >{item.title}</h7>
                                </Link>
                                <p className="number">{Moment(item.pubDate).format('LLL')}</p>
                            </div>
                        </div>
                        <hr style={{ margin: "0%", borderTop: "1px solid rgba(0, 0, 0, 0.18)" }}/>
                    </>
                )
            })
            this.setState({
                dataNews,
                news: response.data.items, 
                title: response.data.items.title, 
                image: response.data.items.thumbnail,
                content: response.data.items.content
            })
        })
        .catch(function (error) {
            console.log(error)
        })
    }

    componentDidMount() {
        this.fetchingData()
    }

    render(){
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        // const news = this.props.location.state
        console.log(this.props)
        return (
            <>
            <div className="App">
                <div className="list-news">
                    {this.state.dataNews}
                </div>
            </div>
            </>
        )
    }
}

export default NewsCard;