import React,{Component, Fragment} from 'react';
import {NavLink, HashRouter} from 'react-router-dom';
import {Tabs, Tab} from 'react-bootstrap';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import Customs from '../Customs/Customs'
import Warehouse from './Warehouse'
import Trucking from './Trucking'
import Shipping from './Shipping'
import Documents from './Documents'

class Analytics extends Component{
  constructor(props){
    super(props)
    this.state = {
      
    }
  }

  contentAnalyticsHeader = (breadcrump) =>{
    return(
      <Fragment>
        <div className="kt-portlet__head">

          <div className="kt-portlet__head-label">
              <h3 className="kt-portlet__head-title">
              {
                breadcrump.map((bcp, i) => {
                  return (
                    <span key={i}>
                    {
                      bcp.link === null 
                      ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                      : (<a href="/#/analytics/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                    } 
                    {
                      i !== (breadcrump.length-1)
                      ? " > "
                      : null
                    }
                    </span>
                  )
                })
              }
              </h3>
          </div>    
        </div>
      </Fragment>
    )
  }

  shipping() {
    return (<Shipping/>);
  }
  trucking() {
    return (<Trucking/>);
  }
  documents() {
    return (
      <Documents/>
    );
  }
  warehouse() {
    return (
      <Warehouse/>
    );
  }
  render(){
    const breadcrump = [
      {
        label: 'Analytics',
        link: null
      }
    ];
    return(
      <div className="kt-portlet kt-portlet--height-fluid">
        {this.contentAnalyticsHeader(breadcrump)}
        <Fragment>
          <div className="kt-portlet__body">
            <Tabs defaultActiveKey="shipping" id="shipping_tabs">
              <Tab eventKey="shipping" title="Shipping">
                {this.shipping()}
              </Tab>
              <Tab eventKey="trucking" title="Trucking">
                {this.trucking()}
              </Tab>
              <Tab eventKey="documents" title="Documents">
                {this.documents()}
              </Tab>
              <Tab eventKey="warehouse" title="Warehouse">
                {this.warehouse()}
              </Tab>
            </Tabs>
          </div>
        </Fragment>
      </div>
    )
  }

}

export default Analytics;