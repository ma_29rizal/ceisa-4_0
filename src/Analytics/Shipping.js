import React,{Component, Fragment} from 'react';
import {NavLink, HashRouter} from 'react-router-dom';
import {Map,GoogleApiWrapper,Marker} from 'google-maps-react';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";

class Shipping extends Component{
  constructor(props){
    super(props)
    this.state = {
      // Shipping
      slowestShippingLine: [
        {id: 1, line:'ABC Line', time:'4.5 Jam' },
        {id: 2, line:'Laut Indah', time:'4.3 Jam'},
        {id: 3, line:'Laut Lepas', time:'4.1 Jam'},
        {id: 4, line:'Samudera Raya', time:'4 Jam'},
        {id: 5, line:'Laut Hitam', time:'3.9 Jam'},
        {id: 6, line:'Laut Jawa Sejahtera', time:'3.5 Jam'},
        {id: 7, line:'Lautan Merah', time:'3.3 Jam'},
        {id: 8, line:'Bromo Line', time:'3.2 Jam'},
      ],
      do_distribution: [
        {id: 1, provider: 'Digico', totalDocument:42051 },
        {id: 2, provider: 'E-DO', totalDocument: 39705 },
        {id: 3, provider: 'DO Online', totalDocument: 34301 },
        {id: 4, provider: 'ABC DO', totalDocument: 25490 },
      ],
      color_do_distribution:[
        Helper.randomColor(),
        Helper.randomColor(),
        Helper.randomColor(),
        Helper.randomColor()
      ],
      do_release: [3, 3.5, 2.9, 2, 1, 0],
      sp2_release: [2, 1.7, 1.5, 1, 0.8, 0.2],
      release_date: ['2 Nov', '4 Nov', '11 Nov', '18 Nov', '25 Nov', '2 Dec']
    }
  }
  //Use in donut Chart
  getTitleTotalDocuments(){
    const {do_distribution} = this.state
    let total = 0
    let do_distribution_total_documents = do_distribution.map(item => item.totalDocument)
    for (let num of do_distribution_total_documents){
      total = total + num
    }
    return total
  }
  getNameAndY(param) {
    return(
      this.state.do_distribution.map(item => ({
        name: item.provider,
        y: item.totalDocument
      }))
    )
  }
  render(){
    const graphic = {
    chart: {
      type: 'spline',
      height: '290px'
    },
    title:{
      text: ''
    },
    xAxis: {
      categories: this.state.release_date
    },
    yAxis: {
      labels: {
        formatter: function(){
          return this.value + ' jam'
        },
      },
      title: {
        text: ''
      }
    },
    plotOptions: {
      spline: {
        marker: {
         enabled: false
        }
      }
    },
    series: [{
      name: 'Graphic Waktu Rata-rata Release DO',
      data: this.state.do_release,
      color: '#f7a35c'  

    }, {
      name: 'Graphic Waktu Rata-rata Release SP2',
      data: this.state.sp2_release,
      color: '#2786fb'
    }],
    credits: {
      enabled: false
    },
  };
  
  const donutChart = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      height: '270px'
    },
    title: {
      verticalAlign: 'middle',
      floating: true,
      text: (this.getTitleTotalDocuments()).toLocaleString().replace(/[,]/, ".") + ' DO'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        colors: this.state.color_do_distribution,
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        },
        innerSize: '50%',
        
      }
    },
    series: [{
      name: 'Brands',
      colorByPoint: true,
      data: this.getNameAndY(this.state.do_distribution)
    }],
    credits: {
      enabled: false
    },
  }
    return (
      <span>
       
      <div className="row justify-content-md-center">
        <div className="col-12">
        {/**Graphic*/}
          <div className='row mb-4'>
            <div className="col-lg-12" >
              <div className='kt-portlet kt-portlet--tabs kt-portlet--height-fluid'>
                <div className='row h-25'>
                  <div className='col-12'>
                    <div className='kt-portlet__body'>
                      <HighchartsReact highcharts={Highcharts} options={graphic}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           </div>
           {/**End of Graphic*/}

           {/**Donut Chart*/}
           <div className="row">
            <div className="col-lg-6">
              <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                <div className="kt-portlet__head">
                  <div className="kt-portlet__head-label">
                      <h5 className=" kt-font-bold">
                          DO Distribution
                      </h5>
                  </div>
                </div>
                <div className='kt-portlet__body'>
                  <HighchartsReact highcharts={Highcharts} options={donutChart} />
                {this.state.do_distribution.map((item, index) => 
                  <div className='row justify-content-center'>
                    {/*<div className='col-lg-3'></div>*/}
                    <div className='col-lg-3 col-md-3 col-sm-6' key={item.id}>
                      <div className='mr-2' style={{backgroundColor:this.state.color_do_distribution[index], width:"15px", height:"15px", display:"inline-flex", borderRadius: '50%', textAlign: 'center'}}>
                        <div className='' style={{backgroundColor: 'white', width:'8px', height: '8px', borderRadius:'50%', margin:'auto'}}></div>
                      </div>
                      {item.provider}
                    </div>
                    <div className='col-lg-3 col-md-3 col-sm-6 text-right' key={item.totalDocument}>{item.totalDocument.toLocaleString().replace(/[,]/, ".")}</div>
                    {/*<div className='col-lg-3'></div>*/}
                  </div>
                  )}
                </div>
              </div>
            </div>
            {/**End of Donut Chart*/}

            {/**Slowest Shipping Data*/}
            <div className="col-lg-6">
              <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid mb-2" style={{minHeight: '25rem'}}>
                <div className="kt-portlet__head">
                  <div className="kt-portlet__head-label">
                    <h5 className=" kt-font-bold">
                      Top 8 Slowest Shipping Line in DO Release
                    </h5>
                  </div>
                </div>
                <div className='kt-portlet__body'>

                  {this.state.slowestShippingLine.map((item, index) =>
                    <table className='detailTable'>
                      <tbody>
                        <tr style={{height: '3.5rem'}} className='border-bottom'>
                          <td style={{width: '1.5rem'}} className='align-middle'>{item.id}</td>
                          <td className="text-left align-middle">{item.line}</td>
                          <td className='text-right align-middle'>{item.time}</td>
                        </tr>
                      </tbody>
                    </table>
                  )}
                </div>
              </div>
            </div>
            {/**End of Slowest Shipping Data*/}
          </div>
        </div>
       </div>
      </span>
    );
  }

}

export default Shipping;