import React,{Component, Fragment} from 'react';
import {NavLink, HashRouter} from 'react-router-dom';
import {Map,GoogleApiWrapper,Marker} from 'google-maps-react';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";

class Warehouse extends Component{
  constructor(props){
    super(props)
    this.state = {
      markers: [
        {
          icon: 'assets/images/warehouse-icon-marker.png',
          lat: '-6.1023223',
          lng: '106.9019725'
        },
        {
          icon: 'assets/images/warehouse-icon-marker.png',
          lat: '-6.307923199999999',
          lng: '107.17208499999992'
        },
        {
          icon: 'assets/images/warehouse-icon-marker.png',
          lat: '-6.1404077',
          lng: '106.9370861'
        }
      ],
      dataWarehouseUtilisation : [
        {id: 1 , companyName:"PT Dieng Warehouse Corp", space: 3456, area:"Cikarang", utilisation : 90},
        {id: 2 , companyName:"PT Digdaya Gudang", space: 2100, area:"Jakarta", utilisation : 90},
        {id: 3 , companyName:"PT Lestari", space: 644, area:"Jakarta", utilisation : 90},
        {id: 4 , companyName:"PT Semesta", space: 789, area:"Bekasi", utilisation : 90},
        {id: 5 , companyName:"PT Amanah", space: 938, area:"Jakarta", utilisation : 90}
      ], 
      dataPieDiagram :[
        {id:1, name:"Available Space",storage:5000000},
        {id:2, name:"Occupied Space",storage:3000000}
      ],
      dataStorageWarehouse : [
        {id:1, types:"Indoor Warehouse", capacity:2500},
        {id:2, types:"Cold Strorage", capacity:500}
      ],
      dataMonthAreaSplineDiagram:['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des'],
      dataAreaSplineDiagram: [2,3,4,5,7,8,9,10,11,9,12,14]
    }
  }

  getDataPie(){
    const {dataPieDiagram} = this.state;
    return(
      dataPieDiagram.map(item =>({
        name:item.name,
        y:item.storage
      }))
    )
  }
  getDataAreaSpline(){
    const {dataAreaSplineDiagram} = this.state;
    return(
      dataAreaSplineDiagram.map(item =>({
        data:item.stock
      }))
    )
  }
  getTotalDataStorage(){
    const {dataPieDiagram} = this.state;
    let total = 0
    let totalStorage = dataPieDiagram.map(item => item.storage)
    for (let num of totalStorage){
      total = total + num
    }
    return total
  }
  getTotalDataPie(){
    const {dataStorageWarehouse} = this.state;
    let total = 0
    let totalStorage =  dataStorageWarehouse.map(item => item.capacity)
    for (let num of totalStorage){
      total = total + num
    }
    return total
  }
  contentWarehouseHeader = (breadcrump) =>{
    return(
      <div className="kt-portlet__head">

        <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">
            {
              breadcrump.map((bcp, i) => {
                return (
                  <span key={i}>
                  {
                    bcp.link === null 
                    ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                    : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  } 
                  {
                    i !== (breadcrump.length-1)
                    ? " > "
                    : null
                  }
                  </span>
                )
              })
            }
            </h3>
        </div>    
      </div>
    )
  } 
  contentMapsWareHouse = () =>{
    const {markers} = this.state;
    return(
      <div className="col-xl-6 col-lg-6 order-lg-3 order-xl-1">
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid" style={{borderBottom:"2px solid #99999982", borderRight:"2px solid #99999982"}}>
          <div className="kt-portlet__body">
            <div className="kt-widget15">
              <div className="kt-widget15__map">
                <div className="form-group row">
                  <div className="col-lg-3" style={{marginTop:"10px", paddingRight:"0px"}}>
                    <div className="kt-font-boldest">LIVE warehouse</div>
                  </div>
                  <div className="col-auto" style={{marginTop:"10px", paddingRight:"0px", textAlign:"right"}}>
                    <div className="kt-font-boldest">Area : </div>
                  </div>
                  <div className="col-lg-4" >
                    <div className="kt-input-icon kt-input-icon--right">
                      <input type="text" className="form-control"></input>
                      <span className="kt-input-icon__icon kt-input-icon__icon--right">
                        <span><i className="flaticon-placeholder-2"></i></span>
                      </span>
                    </div>
                  </div>
                  <div className="col-lg-3">
                    <button type="submit" className="btn btn-primary btn-wide" style={{paddingLeft:"3.5rem", paddingRight:"3.5rem"}}>Search</button>
                  </div>
                </div>
                <div style={{height: "450px", position: "relative", overflow: "hidden"}}>
                  <div style={{height: "100%", width: "100%", position: "absolute",
                      top: "0px", left: "0px"}}>
                    <div className="gm-err-container">
                      <Map
                        google={this.props.google} 
                        zoom={13} 
                        initialCenter={{lat: '-6.1066044', lng: '106.8942988'}}
                        bounds={new this.props.google.maps.LatLngBounds()}
                      >
                      {
                        markers.map((store, index) => {
                          return <Marker key={index} id={index}
                            icon={store.icon ? store.icon: null}
                            position={{lat: parseFloat(store.lat),lng: parseFloat(store.lng)}}
                            />
                        })
                      }
                      </Map>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  contentWareHousePieDiagram = () =>{
    const {dataStorageWarehouse} = this.state;
    const data ={
      chart: {
        type: 'pie',
        height: '300px',
    },
    title:{
      text:null
    },
    credits: {
      enabled:false
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
          colors: [
            '#228B22',
            '#FF0000' 
            ],
            dataLabels: {
              enabled: false,
            },
            allowPointSelect: true,
            cursor: 'pointer',
            showInLegend: true,
        }
    },
    legend: {
      enabled: true,
      layout: 'vertical',
      align: 'right',
      width: 130,
      verticalAlign: 'middle',
      useHTML: true,
      labelFormatter: function() {
          return '<div style="text-align: left; width:200px;float:left;">' 
          + this.name + 
          '</div><div style="width:40px; float:left;text-align:right;">' 
          + this.y.toLocaleString().replace(/[,]/, ".")
      }
    },
    series: [{
        type: 'pie',
        name: 'Storage',
        data: this.getDataPie()
    }]
    };
    return(
      <div className="col-xl-6 col-lg-6 order-lg-3 order-xl-1">
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid" style={{borderBottom:"2px solid #99999982", borderRight:"2px solid #99999982"}}>
          <div className="kt-portlet__body">
            <div className="row">
              <div className="col-7">
                <h3 style={{textAlign:"center"}}>Warehouse</h3>
              </div>
              <div className="col-lg-5">
                {/* <Select/> */}
                <div className="input-group date">
                  <input type="date" className="form-control"></input>
                  <div className="input-group-append">
                    <span className="input-group-text">
                      <i className="la la-calendar-check-o"></i>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <HighchartsReact highcharts={Highcharts} options={data}/>
            <div className="kt-widget__section" style={{textAlign:"right"}}>
              <span className="kt-font-boldest">Total Space </span>
                <b style={{color:"black"}}>
                  {this.getTotalDataStorage().toLocaleString().replace(/[,]/, ".")}<span>.m3</span>
                </b>
            </div>
            <div className="kt-portlet__body">
              <div className="kt-section">
                <div className="kt-section__content kt-section__content--solid" style={{border:"2px solid #908e8e2e"}}>
                  <div className="mb-3">
                    {dataStorageWarehouse.map((item,index) =>  
                      <div className="d-flex justify-content-between mb-1" key={index}>
                        <span className="kt-font-boldest" style={{fontSize:"15px"}}>{item.types}</span>
                      <span style={{fontSize:"15px"}}>{item.capacity.toLocaleString().replace(/[,]/, ".")}</span>
                      </div>
                    )}
                    <div className="d-flex justify-content-between mb-1">
                      <span className="kt-font-boldest" style={{fontSize:"15px"}}>Total Warehouse</span>
                      <span style={{fontSize:"15px"}}>{this.getTotalDataPie().toLocaleString().replace(/[,]/, ".")}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  contentTopWareHouse = () =>{
    const {dataWarehouseUtilisation} = this.state
    const columns = [
      {
        name: 'No',
        selector: 'id',
        sortable: false
      },
      {
        name: 'Company Name',
        selector: 'companyName',
        sortable: false,
        // hide: "sm"
      },
      {
        name: 'SPACE (m3)',
        selector: 'space',
        sortable: false
      },
      {
        name: 'Area',
        selector: 'area',
        sortable: false,
        hide: "sm",
      },
      {
        name: 'Utilisation',
        selector: 'utilisation',
        sortable: false
      }
    ];
    return(
      <div className="col-xl-6 col-lg-6 order-lg-3 order-xl-1">
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid" style={{borderBottom:"2px solid #99999982", borderRight:"2px solid #99999982"}}>
          <div className="kt-portlet__body">
            <h3 style={{textAlign:"center"}}>Top 10 Warehouse Utilisation</h3>
            <DataTable
              columns={columns}
              data={dataWarehouseUtilisation}
              pagination
            />
            <div className="form-group text-center">
              <button type="submit" className="btn-label-success btn btn-sm btn-bold btn-action" style={{marginTop:"5px"}}>Show More</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
  contentWarehouseUtilisation = () =>{
    const {dataAreaSplineDiagram, dataMonthAreaSplineDiagram} = this.state;
    const data ={
      chart: {
        type: 'areaspline'
    },
    title:{
      text:null
    },
    xAxis: {
        categories: dataMonthAreaSplineDiagram,
        enabled:false
    },
    tooltip: {
        shared: true,
        valueSuffix: ' Units'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        },
        allowPointSelect: true,
        cursor: 'pointer'
    },
    series: [{
        name: 'Usage',
        data: dataAreaSplineDiagram
    }]
    };
    return(
      <div className="col-xl-6 col-lg-6 order-lg-3 order-xl-1">
        <div className="kt-portlet kt-portlet--tabs kt-portlet--height-fluid" style={{borderBottom:"2px solid #99999982", borderRight:"2px solid #99999982"}}>
          <div className="kt-portlet__body">
            <div className="row" style={{marginBottom:"20px"}}>
              <div className="col-8">
                <h3 style={{textAlign:"center"}}>Warehouse Utilisation</h3>
              </div>
              <div className="col-lg-4">
                <Select/>
              </div>
            </div>
            <HighchartsReact highcharts={Highcharts} options={data}/>
          </div>
        </div>
      </div>
    )
  }

  render(){
    const breadcrump = [
      {
        label: 'Warehouse',
        link: null
      }
    ];
    return(
      <Fragment>
        <div className="kt-portlet kt-portlet--height-fluid">
          {this.contentWarehouseHeader(breadcrump)}
        </div>
        <div className="row">
          {this.contentMapsWareHouse()}
          {this.contentWareHousePieDiagram()}
          {this.contentTopWareHouse()}
          {this.contentWarehouseUtilisation()}
        </div>
      </Fragment>
    )
  }

}

export default GoogleApiWrapper({
  apiKey: (Constants.googleMapAPI)
})(Warehouse);