import React, {Component, Card, CardContent} from 'react';
// import Iframe from 'react-iframe'
import {Route, HashRouter, NavLink} from 'react-router-dom';
import logo_digico from '../assets/media/digico/DigicoAsset.png';
import * as Helper from '../Helper.js';
import * as Constants from '../Constants.js';
import axios from 'axios'
import Swal from 'sweetalert2'
import { useToasts } from 'react-toast-notifications'
import * as ApiClickargo from '../Clickargo/ApiList.js'

function withToast(Component) {
  return function WrappedComponent(props) {
    const toastFuncs = useToasts()
    return <Component {...props} {...toastFuncs} />;
  }
}

class ChoosePrinciple extends Component{
  constructor(props) {
    super(props);
    this.state  = {
      items:[],
      npwp: this.props.dataUserPortal.npwp,
      //CLICKARGO STATE
      clickargoInputForm: [{
        //
        actionType: '',
        actionTypeLabel: 'Please Select Action Type',
        freightMode: '',
        freightModeLabel: 'Please Select Freight Mode',
        moveType: '',
        moveTypeLabel: 'Please Select Move Type',
        blType: '',
        blTypeLabel: 'Please Select BL Type',
        blNumber: '',
        blNumberLabel: 'Please Select BL Number',
        //place of receipt
        placeOfReceiptLocation: '',
        placeOfReceiptLocationLabel: 'Enter Location',
        placeOfReceiptCountry: '',
        placeOfReceiptCountryLabel: 'Choose Country',
        //place of loading
        portOfLoadingLocation: '',
        portOfLoadingLocationLabel: 'Enter Location',
        portOfLoadingCountry: '',
        portOfLoadingCountryLabel: 'Choose Country',
        //place of destination
        portOfDestinationLocation: '',
        portOfDestinationLocationLabel: 'Enter Location',
        portOfDestinationCountry: '',
        portOfDestinationCountryLabel: 'Choose Country',
        //place of discharge
        placeOfDischargeLocation: '',
        placeOfDischargeLocationLabel: 'Enter Location',
        placeOfDischargeCountry: '',
        placeOfDischargeCountryLabel: 'Choose Country',
        placeOfDischargeState: '',
        placeOfDischargeStateLabel: 'Please Choose Country First',
        placeOfDischargeAddress: '',
        //requestor
        requestorNPWP: '',
        requestorNIB: '',
        requestorName: '',
        requestorAddress: '',
        //consignee
        consigneeName: '',
        consigneeNPWP: '',
        //notify
        notifyPartyName: '',
        notifyPartyNPWP: '',
        //payment information
        paymentInformationBankCode: '',
        paymentInformationBankName: '',
        paymentInformationAccountNumber: '',
        paymentInformationAmount: '',
      }],
      cursor: 'pointer'
      //CLICKARGO STATE
    }
  }
  async componentDidMount() {
    
  }

  //CLICKARGO DEV
  clickargoDO = () => {
    this.setState({
        cursor: 'not-allowed'
    })
    this.props.addToast('Clickargo service clicked, please wait!', {
        appearance: 'info',
        autoDismiss: true,
    })
    //clear session
    localStorage.removeItem("ckDOCountryList")
    localStorage.removeItem("ckDOInputForm")
    localStorage.removeItem("ckDOSuratKuasa")
    // this.props.handleChoosePrinciple("clickargo")
    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    setTimeout(() => {
        //country list
        axios
            .get(ApiClickargo.CLICKARGO_COUNTRY_LIST)
            .then(response => {
                localStorage.setItem('ckDOCountryList', JSON.stringify(response.data.data, null, 2))
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data.message)
                }
            })
        //auth check
        axios
            .post(ApiClickargo.CLICKARGO_CHECK_NPWP, {
                tax_number: this.props.dataUserPortal.npwp,
                email: this.props.dataUserPortal.email,
                // tax_number: '541267799887999',
                // email: 'kusnle5@yopmail.com'
            }, {
                headers: headers
            })
            .then(responseNPWP => {
                this.setState({
                    cursor: 'pointer'
                })
                if (responseNPWP.data.success === true) {
                    //save header token
                    localStorage.setItem('ckHeaderKey', responseNPWP.data.data.key)
                    localStorage.setItem('ckHeaderSecret', responseNPWP.data.data.secret)
                    localStorage.setItem('ckHeaderToken', responseNPWP.data.data.token)
                    localStorage.setItem('ckDOInputForm', JSON.stringify(this.state.clickargoInputForm, null, 2))
                    this.props.history.push('/clickargo_do/form')
                }
            })
            .catch(error => {
                this.setState({
                    cursor: 'pointer'
                })
                if (error.response) {
                    if(error.response.data.success === false) {
                        Swal.fire({
                            title: 'Sorry',
                            text: "You need to register first before using the platform. Just a simple step! Lets go :)",
                            icon: 'error',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes!'
                        }).then((result) => {
                            if (result.value) {
                                localStorage.setItem('ckServiceSelected', 'DO')
                                localStorage.setItem('ckDOInputForm', JSON.stringify(this.state.clickargoInputForm, null, 2))
                                this.props.history.push("/clickargo_register")
                            }
                        })
                    }
                }
            })
    }, 1200);
  }
  //CLICKARGO DEV

  render(){
    // const mystyle = {
    //     color: "white",
    //     backgroundColor: "DodgerBlue",
    //     padding: "10px",
    //     fontFamily: "Arial"
    // };
    const {npwp} = this.state;
    return(
          <HashRouter>
            <div className="row">
                <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-3" style={{"height": "200px"}}>
                    <div onClick={() => { this.props.handleChoosePrinciple("digico"); this.props.history.push('/digico_list_bol') }} className="card" style={{justifyContent: 'center', display:'flex'}}>
                            <div className="row" style={{justifyContent: 'center', display:'flex'}}>
                                <div>
                                    <img style={{width: '150px', height: '60px'}} src={logo_digico} />
                                </div>
                            </div>
                        </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-3" style={{"height": "200px"}}>
                    <div  onClick={() => { this.clickargoDO() }} className="card" style={{justifyContent: 'center', display:'flex', cursor: this.state.cursor}}>
                        <div className="row" style={{justifyContent: 'center', display:'flex'}}>
                            <div>
                                <img style={{ width: '150px', height: '60px' }} src={window.location.origin + '/assets/images/Clickargo5.png'} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-3" style={{"height": "200px"}}>
                    <div className="card" style={{justifyContent: 'center', display:'flex'}}>
                        <div className="row" style={{justifyContent: 'center', display:'flex'}}>
                            <div>
                                <label>Principle</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </HashRouter>
        );    
  }
}
export default withToast(ChoosePrinciple);
