import React, { Component } from 'react'
import { NavLink, HashRouter, Route, Switch } from 'react-router-dom';
import LayoutInbound from './views/Page/Inbound/LayoutInbound';
import LayoutOutbond from './views/Page/Outbond/LayoutOutbond';
import Home from './views/Page/Home';
import LayoutDomestic from './views/Page/Domestic/LayoutDomestic';

export default class Routes extends Component {
    constructor() {
        super()
        this.state = {
        }
    }
    render() {
        return (
            <HashRouter>
                <Switch>
                <Route exact path="/" name="Home"  component={Home} />
                {/* <Route exact path="/inbound" name="Inbound" component={Layout}/> */}
                <Route path="/dashboard/inbound" component={LayoutInbound} />
                    <Route path="/documents/inbound" component={LayoutInbound} />
                    <Route path="/pelimpahan/inbound" component={LayoutInbound} />
                    <Route path="/bookingtruck/inbound" component={LayoutInbound} />
                    <Route path="/vessel/outbond" component={LayoutOutbond}/>
                    <Route path="/dashboard/outbond" component={LayoutOutbond}/>
                    <Route path="/pelimpahan/outbond" component={LayoutOutbond}/>
                    {/* <Route path="/domestic/register" component={LayoutDomestic}/> */}
                    <Route path="/domestic/cari" component={LayoutDomestic}/>
                    <Route path="/domestic/hasil" component={LayoutDomestic}/>
                    <Route path="/domestic/dashboard" component={LayoutDomestic} />
                    <Route path="/domestic/pelimpahan" component={LayoutDomestic} />
              </Switch>     
            </HashRouter>
           
        );
    }
}
