export function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
  function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(window.location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
//portal
export const LOGOUT = "http://dev.beacukai.go.id:9014/portal/login/logout"
//For Devel: "AIzaSyBArECDZVbKOOEQ371Z2l5KoSfXm4LvEKU";
export const urlQuery = getUrlVars();
export const ENC_ID = getUrlParameter('enc');
export const googleMapAPI = "AIzaSyDukSjchPI9YmSKHoCVHC3PA_aiwvcjmR8";
export const API_DECRYPTOR = "https://esbbcext01.beacukai.go.id:8087/decodenpwp?npwp=";
export const API_USER_DIGICO = "https://esbbcext01.beacukai.go.id:8087/getuserdigico?npwp=";
export const API_DOCUMENTS = "https://esbbcext01.beacukai.go.id:8084/NLEMICROAPI-1.0/webresources/ceisa/activedocuments/";
export const API_DOCUMENTS_LOG_STATUS = "https://esbbcext01.beacukai.go.id:8084/NLEMICROAPI-1.0/webresources/ceisa/documentstatus/";
export const BC_API = "https://esbbcext01.beacukai.go.id";
export const E_TRUCK_API = "http://api.itruck.id:8666/";
export const E_TRUCK_BOOKING = "http://209.58.180.97:6666/booking";
export const E_TRUCK_LIVE_TRACKING = "http://toms-id.com/toms_server/api/get_container_tracking";
export const ITRUCK_PAYMENT = "http://toms-id.com/itruck-dev1/view/doku_checkout.php?enc="+encodeURIComponent(ENC_ID)+"&id=";
export const KAFKA_CONSUMER = "https://esbbcext01.beacukai.go.id:8087/kafka/getData";
export const KAFKA_PRODUCER = "https://esbbcext01.beacukai.go.id:8087/kafka/sendData";
export const SOCKET_SERVER = (document.location.protocol === "https" ? "wss": "ws")+"://esbbcext01.beacukai.go.id:8086";
export const SOCKET_TOKEN = "b8ssajd02lkdasoid92adspdoqq471sdnmek8lu";

//Trucking
export const TRUCKING_PLATFORM = ["PL000","PL001","PL004"];
export const API_TRUCKING_MULTIPLE_BOOKING = "https://esbbcext01.beacukai.go.id:8089/Booking/TruckingPlatform/";
export const API_TRUCKING_GET_SEARCH_RESULT = "https://esbbcext01.beacukai.go.id:8089/DetilBooking/idUser/?value=";
export const API_TRUCKING_GET_PORT_LIST = "https://esbbcext01.beacukai.go.id:8089/Tplaces_port/";
export const API_TRUCKING_GET_DEPO_LIST = "https://esbbcext01.beacukai.go.id:8089/Tplaces_empty_depo/";
export const API_TRUCKING_SAVE_BOOKING = "https://esbbcext01.beacukai.go.id:8089/Booking/";
export const API_TRUCKING_BOOKING_LIST = "https://esbbcext01.beacukai.go.id:8089/DetilBooking/LimitPage/";
export const API_TRUCKING_SET_BOOKED = "https://esbbcext01.beacukai.go.id:8089/DetilBooking/isBooked/?idRequestBooking=";
export const API_TRUCKING_LOG_STATUS = "https://esbbcext01.beacukai.go.id:8089/DataTransporterDetil/ListActiveTrucks/";
export const API_TRUCKING_HISTORY = "https://esbbcext01.beacukai.go.id:8089/DataTransporterDetil/ListHistoryTrucks/";
export const API_GET_TRUCK_BY_ID = "https://esbbcext01.beacukai.go.id:8089/DTransporter/idRequestBooking/?value=";
export const API_CONTAINER_TYPE = "https://esbbcext01.beacukai.go.id:8089/ContainerType/";
export const API_DANGEROUS_TYPE = "https://esbbcext01.beacukai.go.id:8089/DangerousType/";
export const API_BOOKING_CONTAINER = "https://esbbcext01.beacukai.go.id:8089/Booking/Container/?paidStatus=1&bl_no=";

//Pelimpahan
export const API_DATA_PELIMPAHAN = "https://esbbcext01.beacukai.go.id:8089/datapelimpahan/"
export const API_SURAT_KUASA = "https://esbbcext01.beacukai.go.id:8089/SuratKuasa/"
export const API_FILE_SURAT_KUASA = "https://esbbcext01.beacukai.go.id:8089/SuratKuasa/file"
export const API_DATA_PELIMPAHAN_CREATOR = "https://esbbcext01.beacukai.go.id:8089/datapelimpahan/?createdNpwp="
export const API_DATA_PELIMPAHAN_PPJK = "https://esbbcext01.beacukai.go.id:8089/datapelimpahan/?ppjk="
export const API_DATA_PELIMPAHAN_DOCTYPE = "https://esbbcext01.beacukai.go.id:8089/datapelimpahan/doctype/"

//General Function
export const GET_PROFILE = "https://esbbcext01.beacukai.go.id:9081/NLEMICROAPI-1.0/webresources/customer/profil/";
export const GET_LIST_BL2_0 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/noblbc20/";
export const GET_LIST_BL1_6 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/noblbc16/";
export const GET_LIST_BL2_3 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/noblbc23/";
export const GET_CONTAINER1_2_0 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/kontainerbc20/";
export const GET_CONTAINER1_1_6 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/kontainerbc16/";
export const GET_CONTAINER1_SP3B = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/kontainersp3b/";
export const GET_CONTAINER1_2_3 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/kontainerbc23/";
export const GET_CONTAINER1_2_7 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/kontainerbc27/";
export const GET_CONTAINER1_3_0 = BC_API + ":9081/NLEMICROAPI-1.0/webresources/ceisa/kontainerbc30/";
export const GLOBAL_NPWP = "313022626122000";//typeof urlQuery["npwp"] === "undefined"? null: urlQuery["npwp"];//"313022626122000";
export const USER_STATUS_OPTIONS = [
  {
    value: 'Cargo Owner',
    label: 'Cargo Owner'
  },
  {
    value: 'FF/PPJK',
    label: 'FF/PPJK'
  }
];
export const DOCUMENT_TYPE_OPTIONS = [
  {
    value: 'BC 2.0', 
    label: 'BC 2.0',
  },
  {
    value: 'BC 1.6', 
    label: 'BC 1.6',
  },
  {
    value: 'SP3B', 
    label: 'SP3B',
  },
  {
    value: 'BC 2.3', 
    label: 'BC 2.3',
  },
  {
    value: 'BC 2.7', 
    label: 'BC 2.7',
  },
  {
    value: 'BC 3.0', 
    label: 'BC 3.0',
  },
];


//Digico
export const DIGICO_BOL_REQUEST_NLE = BC_API + ":8089/DigicoRequest/";
export const IP = "http://165.22.53.128:1337/"; //IP Prod Digico
// export const IP = "http://165.22.53.128:1337/"; //IP Dev Digico
export const IP_PAYMENT = 'http://35.188.4.100/';

//user management Of Digico
export const GET_CAPCHA = IP + 'login/captcha';
export const POST_LOGIN = IP + 'login/action';

//Bill of Lading
export const GET_USER_DIGICO = 'http://34.72.203.134/0_1_0/dopayment/getuserdata';//'http://35.188.4.100/0_1_0/dopayment/getuserdata';
export const UNAME_API_DIGICO = 'nlebc1';
export const UPASS_API_DIGICO = '17L38c01';
export const GET_LIST_BOL = IP + 'dopayment/request/billoflading_list'; //get list bol
export const GET_LIST_BOL_SEARCH = IP + 'dopayment/request/billoflading_list_search'; //get list bol
export const VIEW_DETAIL_BOL = IP + 'dopayment/request/billoflading_view'; //view detail bol
export const POST_ADD_CONTAINER = IP + 'dopayment/request/billoflading_add_container'; //add container bol
export const POST_EDIT_CONTAINER = IP + 'dopayment/request/billoflading_edit_container'; //edit container bol
export const DELETE_CONTAINER = IP + 'dopayment/request/billoflading_del_container'; //delete container bol
export const GET_FORWARDER = IP + 'companies/forwarder'; //get forwarder
export const GET_SHIPPINGLINE = IP + 'companies/shipping'; //get shipping line
export const SUBMIT_BOL = IP + 'request/submit'; //submit Bol

//download
export const DOWNLOAD_FILE = IP + 'file/'; //download file

// Delivery Order
export const GET_LIST_DELIVERY = IP + 'dopayment/requests/listDeliveryOrder'; //get list delivery
export const GET_LIST_DELIVERY_SEARCH = IP + 'dopayment/requests/listDeliveryOrderbySearch'; //get list delivery search by
export const DETAIL_DELIVERY = IP + 'dopayment/requests/viewDeliveryOrder'; //detail delivery
export const APPROVE_DELIVERY = IP + 'dopayment/requests/approveDO'; //Approve delivery
export const REJECT_DELIVERY = IP + 'dopayment/requests/rejectDO'; //Reject delivery
export const POST_INQUIRY_PAYMENT = IP + 'dopayment/requests/inquiry'; //inquiry payment

//payment
export const USERNAME_DIGICO = 'guddodigico';
export const PASS_DIGICO = 'guddodigico';
export const APP_ID_DIGICO = 'guddodigico1';

//data role
export const ROLE_ADMIN = 0;
export const ROLE_CONSIGNEE = 1;
export const ROLE_SHIPPING_LINE = 2;
export const ROLE_FORWARDER = 8;
export const ROLE_CONSIGNEE_FORWARDER = 9;
export const ROLE_ADMIN_OPERATION = 100;

var currentUserName = localStorage.getItem('userName');
// if ( currentUserName === null || currentUserName === '' ) {
	var data_user = {
		id: 13,
		username: 'User_ff19',
		email: 'chalid.harvey@gmail.com',
		company_id: 1208,
		type: 'Admin',
    // role_id: 1
    role_id: null
	};
	  localStorage.setItem("id_user", data_user.id);
    localStorage.setItem("userName", data_user.username);
    localStorage.setItem("email", data_user.email);
    localStorage.setItem("companyID", data_user.company_id);
    localStorage.setItem("compType", data_user.type);
    localStorage.setItem("roleID", data_user.role_id);
// }