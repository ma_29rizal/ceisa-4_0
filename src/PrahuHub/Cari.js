import React,{Component, Fragment} from 'react';
import Select from 'react-select';
import { useHistory } from "react-router-dom";
import
{ Modal, ModalHeader, ModalBody, Button, ModalFooter, Form, FormGroup, Label, Input, FormText }
    from 'reactstrap'

class Cari extends Component{
    constructor(props){
        super(props);
        this.onRuteTypeHandler = this.onRuteTypeHandler.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleModal2 = this.toggleModal2.bind(this);
        this.state = {
            kodePartner: true,
            harbors:[],
            disabled: false,
            isModalShow: false,
            isModalShow2: false,
        }
        this.fetchHarbors();
    }
    toggleModal(e)   {
        this.setState({isModalShow: !this.state.isModalShow});
    }
    toggleModal2(e)   {
        this.setState({isModalShow2: !this.state.isModalShow2});
    }
    async fetchHarbors() {
        await fetch("https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/harbors")
            .then(response => response.json())
            .then(data => {
                let options = data.map(harb=>{
                    return {
                        label: harb.name+" ("+harb.shortName+" )",
                        value:harb.id}
                });
                this.setState({harbors: options});
            });
    }

    onRuteTypeHandler = (event)=>{
        if(event.target.id==="rute1"){
            this.setState({kodePartner:true})
        }else {
            this.setState({kodePartner:false})
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let data = new FormData(event.target);
        data.id = "";
        this.setState({disabled: true});
        fetch('https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/users', {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                this.setState({ disabled: false });
                this.props.history.push('/prahuhub/hasil');
            });
    }

    contentSp2Header = (breadcrump) =>{
        return(
            <div className="kt-portlet__head">

                <div className="kt-portlet__head-label">
                    <h3 className="kt-portlet__head-title">
                        {
                            breadcrump.map((bcp, i) => {
                                return (
                                    <span key={i}>
                  {
                      bcp.link === null
                          ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                          : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  }
                                        {
                                            i !== (breadcrump.length-1)
                                                ? " > "
                                                : null
                                        }
                  </span>
                                )
                            })
                        }
                    </h3>
                </div>
            </div>
        )
    }

    render(){
        const breadcrump = [
            {
                label: 'Mencari Rute Anda',
                link: null
            }
        ];

        const  currentStep= 0;

        return(
            <div className="kt-portlet kt-portlet--height-fluid">
                {this.contentSp2Header(breadcrump)}

                <form autoComplete={"off"}
                      onSubmit={this.handleSubmit}
                >
                    <div className="kt-portlet__body">
                        <div className="kt-widget15">
                            <div className="form-group  ">
                                <input
                                    name="ruteType"
                                    id="rute1"
                                    type="radio"
                                    className="ml-3"
                                    onChange={this.onRuteTypeHandler}
                                />
                                <label
                                    className="ml-3"
                                    htmlFor="rute1"
                                >Cari Rute Favorit Anda</label>
                            </div>
                            <div className="form-group  ">
                                <input
                                    name="ruteType"
                                    id="rute2"
                                    type="radio"
                                    className="ml-3"
                                    onChange={this.onRuteTypeHandler}
                                />
                                <label
                                    htmlFor="rute2"
                                    className="ml-3"
                                >Cari Partner Favorit Anda</label>
                                <input
                                    id="partnerCode"
                                    name="kodePartner"
                                    className={"form-control"}
                                    placeholder={"Masukan kode partner"}
                                    disabled={this.state.kodePartner}/>
                            </div>

                            <div className="form-group">
                                <label>Pelabuhan Asal (POL)</label>
                                <Select name="pol" options={this.state.harbors} />
                            </div>

                            <div className="form-group">
                                <label>Pelabuhan Tujuan (POD)</label>
                                <Select name="pod" options={this.state.harbors}/>
                            </div>
                            <div className="form-group">
                                <label>Tidak menemukan rute?</label>
                                <button type="button" className="btn btn-default border-0" onClick={this.toggleModal}> Buat Permintaan Rute</button>
                            </div>
                            <div className="form-group">
                                <button
                                    disabled={ this.state.disabled }
                                    type="submit"
                                    className={"btn btn-success pl-5 pr-5"}
                                >
                                    <i className="flaticon2-search-1"/>
                                    Mencari</button>
                                <button type="button" className={"btn btn-default ml-1"} onClick={this.toggleModal2}>Buka Opsi</button>
                            </div>
                        </div>
                    </div>
                </form>

                <Modal isOpen={this.state.isModalShow} backdrop={true} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Buat Permintaan Rute</ModalHeader>
                    <ModalBody>
                        <form>
                            <div className="form-group">
                                <label>Nama Rute</label>
                                <input className="form-control"/>
                            </div>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleModal}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.isModalShow2} backdrop={true} toggle={this.toggleModal2}>
                    <ModalHeader toggle={this.toggleModal2}>Opsi Rute</ModalHeader>
                    <ModalBody>
                        <form>
                            <div className="form-group">
                                <label>Opsi Rute</label>
                                <input className="form-control"/>
                            </div>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleModal2}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleModal2}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }

}

export default Cari;
