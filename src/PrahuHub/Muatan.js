import React,{Component, Fragment} from 'react';
import {Checkbox} from "semantic-ui-react";
import Select from "react-select";

class Muatan extends Component{
    constructor(props){
        super(props)
        this.state = {
            totalRow: 1,
            rows: [],
            packaging: [],
        }
        this.addRows=this.addRows.bind(this);
        this.rmRows=this.rmRows.bind(this);
        this.rowInput=this.rowInput.bind(this);
        this.fetchPackaging();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    addRows(event){
        this.setState({totalRow:this.state.totalRow+1})
    }

    rmRows(event){
        this.setState({totalRow:this.state.totalRow-1})
    }

    rowInput(event){

    }

    handleSubmit(event) {
        event.preventDefault();

        let data = new FormData(event.target);
        data.id = "";
        this.setState({disabled: true});
        fetch('https://my-json-server.typicode.com/mudiadamz/jsonplaceholder/users', {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                this.setState({disabled: false, alertShow: true});
                this.props.history.push('/prahuhub/payment');
            });
    }

    fetchPackaging = async ()=> {
        await fetch("https://my-json-server.typicode.com/mudiadamz/jsonplaceholder2/packaging")
            .then(response => response.json())
            .then(data => {
                let packagingSelect = data.map(row=>{return {value:row.id,label:row.name}});
                this.setState({packaging: packagingSelect})
            });
    }

    contentSp2Header = (breadcrump) =>{
        return(
            <div className="kt-portlet__head">

                <div className="kt-portlet__head-label">
                    <h3 className="kt-portlet__head-title">
                        {
                            breadcrump.map((bcp, i) => {
                                return (
                                    <span key={i}>
                  {
                      bcp.link === null
                          ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                          : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  }
                                        {
                                            i !== (breadcrump.length-1)
                                                ? " > "
                                                : null
                                        }
                  </span>
                                )
                            })
                        }
                    </h3>
                </div>
            </div>
        )
    }

    render(){
        const breadcrump = [
            {
                label: 'Input Daftar Muatan',
                link: null
            }
        ];
        let rows = [];
        for (let i=0; i<this.state.totalRow;i++){
            rows.push(
                <tr key={i}>
                    <td><input onChange={this.rowInput} className="form-control" value={"undefined"===typeof this.state.rows[i]?'':(this.state.rows[i].jns||'')}/></td>
                    <td><input onChange={this.rowInput} className="form-control" value={"undefined"===typeof this.state.rows[i]?'':(this.state.rows[i].des||'')}/></td>
                    <td><Select options={this.state.packaging} /></td>
                    <td><input onChange={this.rowInput} className="form-control" value={"undefined"===typeof this.state.rows[i]?'':(this.state.rows[i].kms||'')}/></td>
                    <td><input onChange={this.rowInput} className="form-control" value={"undefined"===typeof this.state.rows[i]?'':(this.state.rows[i].jml||'')}/></td>
                    <td>
                        {i===0?'':
                            <button className="btn btn-default" type="button" onClick={e=>console.log(i)}>
                                <i className="flaticon-cancel text-danger"/>
                            </button>
                        }
                    </td>
                </tr>);
        }
        return(
            <div className="kt-portlet kt-portlet--height-fluid">
                {this.contentSp2Header(breadcrump)}
                <form autoComplete={"off"} onSubmit={this.handleSubmit} method="post">
                    <div className="kt-portlet__body">
                        <div className="kt-widget15">
                            <div className="form-group row">
                                <div className="col-sm-6">
                                    <label>
                                        <input type="checkbox"/> Ya saya menyetujui ketentuan ini. <a href="#">Lihat ketentuan daftar muatan</a>
                                    </label>
                                    <p>Anda dapat mengisi daftar muatan per kontainer menggunakan templat excel</p>

                                    <a className="btn btn-default" type="button" target="_blank"
                                       href="https://github.com/mudiadamz/jsonplaceholder/raw/master/daftar_muatan.xlsx">
                                        <i className="flaticon-download"/>
                                        Unduh form excel daftar muatan
                                    </a>
                                </div>
                                <div className="col-sm-6">
                                    <input
                                        id="myInput"
                                        type="file" ref={(ref) => this.upload = ref}
                                        style={{ display: 'none' }} />
                                    <button
                                        onClick={(e) => this.upload.click() }
                                        className="btn btn-info float-right" type="button">
                                        <i className="flaticon-upload-1"/>
                                        Unggah dokumen packing list
                                    </button>
                                </div>
                            </div>

                            <div className="form-group">
                                <b>Kontainer 1</b>
                                <input
                                    id="myInput2"
                                    type="file" ref={(ref) => this.upload2 = ref}
                                    style={{ display: 'none' }} />
                                <button
                                    onClick={(e) => this.upload2.click() }
                                    className="btn btn-success ml-3"
                                    type="button">
                                    <i className="flaticon-upload-1"/>
                                    Ungah Excel</button>
                            </div>
                            <table className="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Jenis Muatan</th>
                                    <th>Deskripsi</th>
                                    <th style={{width:"20%"}}>Kemasan</th>
                                    <th>Jumlah</th>
                                    <th>Berat(KG)</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>{
                                    rows
                                }
                                </tbody>
                            </table>

                        </div>

                        <div className="form-group row">
                            <div className="col-sm-6">
                                <button className="btn btn-default" type="button" onClick={this.addRows}>
                                    <i className="flaticon2-plus-1"/>
                                    Tambah baris input muatan</button>
                            </div>
                            <div className="col-sm-6">Jumlah muatan <span>0</span></div>
                        </div>
                        <div className="form-group">
                            <label>
                                <input type="checkbox"/> Barang Perlu penanganan khusus
                            </label>
                        </div>
                        <div className="form-group">
                            <div className="float-right">
                                <button className="btn btn-default ml-2" type="button" onClick={e=>
                                    this.props.history.push('/prahuhub/detail')}>
                                    <i className="flaticon2-back"/> Kembali
                                </button>
                                <button className="btn btn-secondary ml-2" type="button" onClick={e=>
                                    this.props.history.push('/prahuhub/detail')}>
                                    <i className="flaticon-cancel"/> Batal
                                </button>
                                <button className="btn btn-success ml-2" type="submit">
                                    <i className="flaticon-paper-plane"/> Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

}

export default Muatan;