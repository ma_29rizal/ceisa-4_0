import axios from 'axios'

export const url = axios.create({
    baseURL: "http://api.itruck.id:6666/booking",
    headers:{
        "Accept" : "application/json",
        "Content-Type" : "application/json"
    }
})
class servicesAPI{
    addBook(book){
        return axios.post(""+url,book);
    }
}
// export const client = axios.create({
//     baseURL: "http://api.itruck.id:6666/booking",
//     headers: {
//     "Accept": "application/json",    
//     "Content-Type": "application/json"
//   }
// })

export default servicesAPI;