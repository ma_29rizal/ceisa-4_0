import React,{Component, Fragment} from 'react';
import * as Constants from '../Constants.js';
import * as Helper from '../Helper.js';

class Railway extends Component{
  constructor(props){
    super(props)
    this.state = {
    }
  }

  contentRailwayHeader = (breadcrump) =>{
    return(
      <div className="kt-portlet__head">

        <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">
            {
              breadcrump.map((bcp, i) => {
                return (
                  <span key={i}>
                  {
                    bcp.link === null 
                    ? (<label className="kt-font-boldest" key={i}> {bcp.label}</label>)
                    : (<a href="/#/warehouse/" className="kt-link kt-font-transform-u" onClick={()=>{this.setActiveView(bcp.link)}} key={i}>{bcp.label}</a>)
                  } 
                  {
                    i !== (breadcrump.length-1)
                    ? " > "
                    : null
                  }
                  </span>
                )
              })
            }
            </h3>
        </div>    
      </div>
    )
  } 

  render(){
    const breadcrump = [
      {
        label: 'Railway',
        link: null
      }
    ];
    return(
      <div className="kt-portlet kt-portlet--height-fluid">
        {this.contentRailwayHeader(breadcrump)}
        <Fragment>
          <div className="kt-portlet__body">
            <div className="kt-widget15">
              Under Development
            </div>
          </div>
        </Fragment>
      </div>
    )
  }

}

export default Railway;